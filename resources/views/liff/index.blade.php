@extends('__layout/store')
<!-- -------------------- -->

@section('head')

<script charset="utf-8" src="https://static.line-scdn.net/liff/edge/2/sdk.js"></script>

<style>
    #menu-bottom {
        display: none;
    }
</style>
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="container">
            loading.....
        </div>

        <!-- @{{ result }} -->
    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.mounted = function () {
        this.initializeApp();
    }

    vueItem.data = Object.assign(vueItem.data, {
        error: null,
        data: null,
        result: {},
    });

    vueItem.methods = Object.assign(vueItem.methods, {

        sendMessage() {
            liff.sendMessages([{
                type: 'text',
                text: "You've successfully sent a message! Hooray!"
            }]).then(function () {
                window.alert("Message sent");
            }).catch(function (error) {
                window.alert("Error sending message: " + error);
            });
        },

        closeLiff() {
            liff.closeWindow();
        },

        initializeApp() {
            const self = this;

            try {

                liff.init({ liffId: vueData.liffID }).then(() => {

                    switch (liff.getOS()) {
                        case 'ios':
                        case 'android':
                            const idToken = liff.getIDToken();

                            const data = {
                                accessToken: idToken,
                            };

                            // alert(idToken);
                            self.$http.post('setUserDo', data).then(function (r) {
                                const body = r.body;
                                const data = body.data;

                                this.data = r;

                                if (data.isSuccess) {

                                    // this.result = r;
                                    // return;
                                    try {

                                        switch (this.typeID) {
                                            case 'store':
                                            case 'information':
                                            case 'deal':
                                            case 'main':
                                                document.location = '/user/' + vueData.typeID;
                                                break;
                                            case 'register':
                                                document.location = '/user/register';
                                                break;
                                            default:
                                                document.location = '/user/index';
                                                break;
                                        }
                                    } catch (e) {
                                        document.location = '/user/index';
                                    }
                                } else {
                                    self.alert('驗證失敗, 請關閉重試');
                                }
                            }).catch(function (e) {
                                this.error = e;
                                document.location = '/user/index';

                            });
                            break;
                        case 'web':
                        default:
                            alert('請用手機開啟');
                            break;
                    }

                }).catch((err) => {
                    self.alert(err);
                });

            } catch (e) {
                self.alert('error: ' + e);
            }
        }
    });

    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
