@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">

        <div class="  py-5 text-black">

            <template v-if="indexStep == 1">
                <div class="container">
                    <div @click="typeID = 5; stepNext()" class="pointer"><img src="/img/icon-45.png" class="w-full mb-5"></div>
                    <div @click="typeID = 6; stepNext()" class="pointer"><img src="/img/icon-46.png" class="w-full mb-5"></div>
                </div>
            </template>

            <template v-if="indexStep == 2">

                <div class="container py-5 text-black">
                    <qrcode :value="item.md5" :options="{ width: 1000 }" class="qrcode mt-10 mb-10"></qrcode>
                </div>

                <div class="bg-red text-white p-3 text-center text-24 mb-5" v-if="typeID == 5">
                    主食兌換用，限用一次
                </div>

                <div class="bg-red text-white p-3 text-center text-24 mb-5" v-if="typeID == 6">
                    湯品兌換用，限用一次
                </div>

                <div class="text-center text-18 text-white  ">
                    交易剩餘時間 00:@{{ second | pad(2) }}
                </div>



            </template>

        </div>

        <!-- <div class="mx-auto w-4/5 pt-10">

            <a href="transaction"><img src="/img/icon-45.png" class="w-full mb-5"></a>
            <a href="coupon"><img src="/img/icon-46.png" class="w-full mb-5"></a>
        </div> -->

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.data = Object.assign(vueItem.data, {
        item: {
            id: null,
            md5: '',
            price: 0,
        },
        typeID: 5,
        indexStep: 1,
        // indexStep: 2,
        second: 60,
        verifyCode: '',
        isSuccess: false,

        countTicket: 1,
    });



    vueItem.methods = Object.assign(vueItem.methods, {
        stepNext() {

            // get transcation
            const data = {
                typeID: this.typeID,
                // countTicket: this.countTicket
            };

            this.$http.post('/transaction/createDo', data).then(function (r) {
                const body = r.body;
                // this.items = body.data;


                // if (body.statusID == 0) {
                if (body.data.isUsed == false) {

                    this.item = body.data.item;

                    this.indexStep++;
                    if (this.indexStep == 2) {
                        this.startCountdown();
                        this.checkTransaction();
                    }

                } else {
                    // alert('發生錯誤, 請再試一次');
                    if (this.typeID == 5) {
                        this.alert('主食已經兌換過');
                    }
                    if (this.typeID == 6) {
                        this.alert('湯品已經兌換過');
                    }

                    // location.reload();
                }

            });
        },


        startCountdown() {
            if (this.item.isStoreScan != 1) {

                let second = this.second;
                second--;

                if (second < 0) {
                    // alert('qrcode過期');
                    // document.location = 'index';
                } else {
                    this.second = second;
                    this.timeoutCountdown = setTimeout(this.startCountdown, 1000);
                }
            }
        },

        checkTransaction() {

            const self = this;
            setTimeout(function () {

                let data = {
                    md5: self.item.md5,
                }

                // alert('checkTransactionDo');
                self.$http.post('/transaction/getItem', data).then(function (r) {
                    self.updateTimes++;

                    const body = r.body;

                    const data = body.data;
                    self.item = data.item;

                    console.log(data);

                    if (data.item != null) {

                        if (self.item.isSuccess == 1) {
                            clearTimeout(this.timeoutCountdown);
                            this.alert('已掃描', function () {
                                document.location = 'index';
                            });
                        }

                    } else {
                        this.alert('錯誤, 找不到交易', function () {
                            document.location = 'index';

                        });
                    }

                    self.checkTransaction();
                });

            }, 2000);
        },



    });


    var vue = new Vue(vueItem);


</script>
<!-- -------------------- -->

@stop
