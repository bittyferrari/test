@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="container py-5 text-black">

            <template v-if="indexStep == 1">
                <div class="frame mb-5">

                    <div class="mb-6">
                        <div class="mb-2">姓名 *</div>
                        <input v-model="item.name" class="form-control" placeholder="" />
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">生日(西元/月/日)</div>
                        <input v-model="item.birthday" class="form-control" placeholder="" type="date">
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">身份證字號 *</div>
                        <input v-model="item.nationID" class="form-control" placeholder="" />
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">手機 *</div>
                        <input v-model="item.phone" class="form-control" placeholder="" type="tel">
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">地址</div>
                        <input v-model="item.addressText" class="form-control" placeholder="">
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">推薦碼(選填)</div>
                        <!-- <input v-model="item.registerRecommendCode" class="form-control" placeholder="" :disabled="self.isCompleteRegister == 1"> -->
                        <div v-if="isLockRecommendCode" class="form-control">已輸入推薦碼</div>
                        <input v-else v-model="item.registerRecommendCode" class="form-control" placeholder="" :disabled="isRecommendCodeReadonly">
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">居住的總太社區</div>
                        <select class="form-control" v-model.number="item.communityID">
                            <option :value="null">--</option>
                            <option v-for="(x, i) of option.community" :value="x.id">@{{ x.name }}</option>
                        </select>
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">戶別(選填)</div>
                        <input v-model="item.household" class="form-control" placeholder="">
                    </div>


                    <div class="mb-6">
                        <div class="mb-2">備註(選填)</div>
                        <input v-model="item.memo" class="form-control" placeholder="" type="text">
                    </div>
                </div>

                <button class="btn-white" @click="stepNext()">
                    下一步：發送簡訊驗證碼
                </button>
            </template>

            <template v-if="indexStep == 2">
                <div class="frame mb-5">
                    <div class="mb-6">
                        <div class="mb-2">請輸入手機簡訊驗證碼</div>
                        <input v-model="verifyCode" class="form-control" placeholder="" type="number" maxlength="5">
                    </div>
                    <button class="bg-orange text-white px-5 py-3 text-18" @click="resendDo()" :disabled="second > 0">
                        重新發送 00:@{{ second | pad(2) }}
                    </button>
                </div>
                <button class="btn-white w-full p-5 text-18" @click="stepNext()" :disabled="verifyCode.length < 5">
                    送出
                </button>
            </template>

            <template v-if="indexStep == 3">

                <div class="bg-white p-5 mb-5 text-center text-blue" v-if="isSuccess">
                    <div class="text-24 mb-5">填寫完成</div>
                    <div class="text-18">手機驗證成功！</div>
                </div>

                <div class="bg-white p-5 mb-5 text-center text-red" v-else>
                    <div class="text-24 mb-5">填寫失敗</div>
                    <div class="text-18 text-grey">手機驗證失敗！</div>
                </div>

            </template>

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    vueItem.data = Object.assign(vueItem.data, {
        indexStep: 1,
        second: 60,
        verifyCode: '',
        isSuccess: false,
        // item: {
        //     name: '',
        //     phone: '',
        //     nationID: '',
        //     email: '',
        //     memo: null,
        //     recommendCode: '',
        //     birthday: null,
        // }
    });

    vueItem.mounted = function () {

        if (this.isDev) {
            this.item.name = 'test';
            // this.item.phone = '0978555333';
            this.item.phone = '0970325555';
            this.item.nationID = 'H654987321';
            this.item.email = 'xxxxxx@gmail.com';
            this.item.memo = 'test1234';
        }

        if (this.self.isCompleteRegister == 1) {

            // this.alert('此手機/證號已註冊', function () {
            //     document.location = 'main';
            // });
        }
    }
    vueItem.methods = Object.assign(vueItem.methods, {
        stepNext() {

            switch (this.indexStep) {
                case 1:

                    // check field
                    var isValid = true;

                    messages = [];

                    if (this.isEmpty(this.item.name)) {
                        messages.push('請填姓名');
                        isValid = false;
                    }
                    if (this.isEmpty(this.item.nationID)) {
                        messages.push('請填身分證字號');
                        isValid = false;

                    }

                    if (this.isEmpty(this.item.phone)) {
                        messages.push('請填手機')
                        isValid = false;
                    }

                    if (isValid) {

                        this.$http.post('registerDo', this.item).then(function (r) {
                            const body = r.body;
                            // this.items = body.data;

                            this.indexStep++;

                            this.startCountdown();

                            // if (this.indexStep == 2) {
                            // this.startCountdown();
                            // }

                        });

                    } else {
                        var message = messages.join('<br>');
                        this.alert(message);

                    }

                    break;
                case 2:

                    this.$http.post('registerVerifyDo', { verifyCode: this.verifyCode }).then(function (r) {
                        const body = r.body;
                        // this.items = body.data;

                        if (body.statusID == 0) {
                            this.isSuccess = true;
                            // alert('註冊成功');
                        } else {
                            this.isSuccess = false;

                            // alert('驗證失敗, 請再試一次');
                        }
                        this.indexStep++;

                        // if (this.indexStep == 2) {
                        // this.startCountdown();
                        // }

                    });



                    break;

            }



        },
        resendDo() {
            this.second = 60;

            this.$http.post('registerDo', this.item).then(function (r) {
                const body = r.body;
                // this.items = body.data;
                this.startCountdown();
                // if (this.indexStep == 2) {
                // this.startCountdown();
                // }

            });
        },

        startCountdown() {

            let second = this.second;
            second--;

            if (second < 0) {
                // alert('qrcode過期');
                // document.location = 'index';
            } else {
                this.second = second;
                this.timeoutCountdown = setTimeout(this.startCountdown, 1000);
            }
        },


    });

    var vue = new Vue(vueItem);


</script>
<!-- -------------------- -->

@stop
