@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="mx-auto w-4/5 pt-10">

            <!-- <a href="register" class="btn-link">
                註冊
            </a>
            <div class="w-full text-white text-center" style="bottom:18px">(再次填寫可修改資料)</div>

            <div class="h-10"></div>
 -->

            <template v-if="self.isCompleteRegister">
                <a href="recommend" class="btn-link">
                    推薦給好友
                </a>

                <div class="h-10"></div>
            </template>

            <a href="register" class="btn-link">
                註冊/更新資料
            </a>

            <div class="h-10"></div>

            <a href="profile" class="btn-link">
                個人資料
            </a>

            <!-- <div class="h-6"></div>
            <div class="text-white">
                您好：<br>
                為因應「新冠肺炎」(COVID-19)疫情,<br>
                需請您如實填寫相關個人資料以備防疫使用, 謝謝您!
            </div> -->

            <!-- <a href="profile"><img src="/img/icon-39.png" class="w-full mb-5"></a> -->
            <!-- <a href="transfer"><img src="/img/icon-40.png" class="w-full mb-5"></a> -->
            <!-- <a href="https://reurl.cc/x02Keb"><img src="/img/icon-55.png" class="w-full mb-5"></a> -->

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    // vueListing.data.condition = Object.assign(vueListing.data.condition, {
    // 	id: null,
    // 	name: null,
    // 	lineUserID: null,
    // 	roleID: null,
    // });
    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
