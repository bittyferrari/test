@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">

        <div class="bg-black relative text-white rounded-b-20 pt-2 pb-8">
            <div class="text-14 mb-8 text-center">總太地產 心生活美學聚落</div>
            <div class="flex items-center justify-center">
                <img :src="self.photoLine" class="w-16 rounded-full mr-5">
                <div class="text-24">@{{ self.name }}</div>
            </div>
            <div class="h-10"></div>
            <div class="h-2"></div>
        </div>

        <div class="flex w-full flex-wrap text-center" style="transform: translateY(-50px)">
            <div class="px-2 w-1/3">
                <div class="bg-white overflow-hidden shadow-3 rounded-5 pt-2">
                    <div class="text-10 mb-2 text-aaa">目前剩餘</div>
                    <div class="text-26 mb-2">@{{ self.price | number }}</div>
                    <div class="bg-C7B489 w-full text-white py-2 text-12">總太點數</div>
                </div>
            </div>
            <div class="px-2 w-1/3">
                <div class="bg-white overflow-hidden shadow-3 rounded-5 pt-2">
                    <div class="text-10 mb-2 text-aaa">目前剩餘</div>
                    <div class="text-26 mb-2">@{{ self.pricePoint | number }}</div>
                    <div class="bg-C2762C w-full text-white py-2 text-12">活動點數</div>
                </div>
            </div>
            <div class="px-2 w-1/3">
                <div class="bg-white overflow-hidden shadow-3 rounded-5 pt-2" v-if="couponNearest">
                    <div class="text-10 mb-2 text-aaa">@{{ couponNearest.timeTo | date }}到期</div>
                    <div class="text-26 mb-2">@{{ couponNearest.price }}</div>
                    <div class="bg-A38442 w-full text-white py-2 text-12">活動點數到期日</div>
                </div>
                <div class="bg-white overflow-hidden shadow-3 rounded-5 pt-2" v-else>
                    <div class="text-10 mb-2 text-aaa">--</div>
                    <div class="text-26 mb-2">--</div>
                    <div class="bg-A38442 w-full text-white py-2 text-12">活動點數到期日</div>
                </div>
            </div>
        </div>
    </div>
    <!--
    <div class="relative overflow-hidden ">
        <div class="flex flex-wrap items-center">
            <div class="text-16 w-1/2 pt-6 text-center bg-white text-black" style="height:76px;">目前剩餘點數</div>
            <div class="text-24 w-1/2 py-5 text-center bg-orange text-white" style="height:76px">@{{ self.price }}點</div>
        </div>
        <div class="absolute" style="width:calc(100% + 8px);left:-4px;top:0;height:76px;box-shadow: inset 0px 4px 4px rgba(0, 0, 0, 0.25);">
        </div>
    </div> -->


    <div class="container" style="margin-top:-20px;">

        <div class="flex rounded-full w-full bg-white text-16" v-if="typeID == 'exchange'">

            <a href="log?typeID=exchange" class="w-1/2 bg-black text-white rounded-full text-center py-4">
                兌換紀錄
            </a>
            <a href="log?typeID=log" class="w-1/2 text-999 text-center py-4">
                交易紀錄
            </a>
        </div>


        <div class="flex rounded-full w-full bg-white text-16" v-if="typeID == 'log'">

            <a href="log?typeID=exchange" class="w-1/2 text-999 rounded-full text-center py-4">
                兌換紀錄
            </a>
            <a href="log?typeID=log" class="w-1/2 bg-black text-white text-center py-4 rounded-full">
                交易紀錄
            </a>
        </div>

        <!--
        <div class="flex flex-wrap mt-5 ">
            <div class="w-1/2 px-5">
                <a href="log?typeID=exchange" v-if="typeID == 'exchange'"
                    class="text-center block w-full border-red border-2 py-3 text-red rounded-full bg-white">兌換紀錄</a>
                <a href="log?typeID=exchange" v-else class="text-center block w-full py-3 rounded-full bg-white text-black">兌換紀錄</a>
            </div>
            <div class="w-1/2 px-5">
                <a href="log?typeID=log" v-if="typeID == 'log'"
                    class="text-center block w-full border-red border-2 py-3 text-red rounded-full bg-white">交易紀錄</a>
                <a href="log?typeID=log" v-else class="text-center block w-full py-3 rounded-full bg-white text-black">交易紀錄</a>
            </div>
        </div> -->


        <div class="flex items-center justify-center mt-5 mb-6 text-20">
            <div class="pointer p-3" @click="monthPrevious()">
                <img src="/img/icon-68.png" class="w-2">
            </div>
            <div class="mx-5">@{{ year }}年@{{ month }}月</div>
            <div class="pointer p-3" @click="monthNext()">
                <img src="/img/icon-69.png" class="w-2">
            </div>
        </div>

        <div class="text-black ">
            <div v-for="(x, i) of items" class="mb-3">

                <div class="bg-white px-4 py-2 rounded-6 shadow-2 mb-3" v-if="x.isSuccess == 1">
                    <div class="mb-2 text-14" v-if="x.storeID">@{{ typeText('store', x.storeID) }}</div>
                    <div class="flex justify-between items-end">

                        <img src="/img/icon-66.png" class="h-full">
                        <!-- ['id' => 1, 'name' => '儲值'],
                        ['id' => 2, 'name' => '消費'],
                        ['id' => 3, 'name' => '退點'],
                        ['id' => 4, 'name' => '獎品'],
                        ['id' => 5, 'name' => '餐點'],
                        ['id' => 6, 'name' => '湯品'],
                        ['id' => 7, 'name' => '門票'],
                        ['id' => 8, 'name' => '轉移'],
                        ['id' => 9, 'name' => '序號'],
                         -->

                        <template v-if="x.typeID == 8">

                            <div>
                                <div v-if="x.userID == x.transferFromUserID">點數轉移給 @{{ x.phoneTransferTo }}</div>
                                <div v-else>點數轉移來自 @{{ x.phoneTransferFrom }}</div>
                                <div>

                                    <div v-if="x.userID == x.transferFromUserID">轉移點數
                                        <span class="font-bold ml-1 text-red">
                                            @{{ x.price | numberPositive }}
                                        </span>
                                    </div>
                                    <div v-else>
                                        獲得點數
                                        <span class="font-bold ml-1 text-green">
                                            @{{ x.price | numberPositive }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </template>



                        <template v-else>

                            <div>
                                <span>@{{ typeText('transactionTypeChinese', x.typeID) }} </span>
                                <span class="font-bold ml-1" :class="{ 'text-green': x.price > 0, 'text-red': x.price < 0}">@{{ x.price | numberPositive
                                    }}</span>
                            </div>
                        </template>


                        <div class="text-12 text-gray2">@{{ x.createdAt }}</div>
                    </div>
                </div>

                <div class="bg-f3f3f3 px-4 py-2 rounded-6 shadow-2 " v-if="x.isSuccess != 1">
                    <div class="flex justify-between items-center text-14">
                        <div class="mb-2">@{{ typeText('store', x.storeID) }}</div>
                        <div class="text-red">儲值失敗</div>
                    </div>
                    <div class="flex justify-between items-center text-828282">


                        <div>
                            <span>@{{ typeText('transactionTypeChinese', x.typeID) }} </span>
                            <span class="font-bold ml-1" :class="{ 'text-green': x.price > 0, 'text-red': x.price < 0}">@{{ x.price }}</span>
                        </div>
                        <!--
                        <template v-if="x.typeID == 1">
                            <div class=" " v-if="x.price > 0">兌換點數 <span class="text-green font-bold ml-1">+@{{ x.price }}</span></div>
                            <div class=" " v-else>兌換點數 <span class="text-red font-bold ml-1">@{{ x.price }}</span></div>
                        </template>
                        <template v-if="x.typeID == 2">
                            <div class=" " v-if="x.price > 0">儲值點數 <span class="text-green font-bold ml-1">+@{{ x.price }}</span></div>
                            <div class=" " v-else>交易點數 <span class="text-red font-bold ml-1">@{{ x.price }}</span></div>
                        </template>
                        <template v-if="x.typeID == 3">
                            <div class=" " v-if="x.price > 0">退貨點數 <span class="text-green font-bold ml-1">+@{{ x.price }}</span></div>
                            <div class=" " v-else>退貨點數 <span class="text-red font-bold ml-1">@{{ x.price }}</span></div>
                        </template> -->
                        <div class="text-12 text-gray2">@{{ x.createdAt }}</div>
                    </div>
                </div>


            </div>
        </div>


    </div>

</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    // vueListing.data.condition = Object.assign(vueListing.data.condition, {
    // 	id: null,
    // 	name: null,
    // 	lineUserID: null,
    // 	roleID: null,
    // });

    vueItem.methods = Object.assign(vueItem.methods, {
        monthPrevious() {
            var month = this.month - 1;
            var year = this.year;
            if (month <= 0) {
                month = 12;
                year -= 1;
            }
            document.location = 'log?typeID=' + this.typeID + '&year=' + year + '&month=' + month;
        },

        monthNext() {
            var month = this.month + 1;
            var year = this.year;
            if (month >= 13) {
                month = 1;
                year += 1;
            }
            document.location = 'log?typeID=' + this.typeID + '&year=' + year + '&month=' + month;
        },

    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
