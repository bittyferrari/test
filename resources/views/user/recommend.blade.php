@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="container py-5 text-black">

            <div class="frame text-center">

                <div>
                    <div class="text-26 mb-3">@{{ self.name }}</div>
                </div>
                <!--
                <a :href="'/recommend/item?recommendCode=' + self.recommendCode"
                    class="bg-red text-white py-5 text-20 flex justify-center items-center rounded-5 shadow-3 pointer">
                    <img src="/img/icon-64.png" class="mr-3 h-6">
                    <div>註冊分享</div>
                </a> -->

                <div @click="shareToLine()" class="bg-red text-white py-5 text-20 flex justify-center items-center rounded-5 shadow-3 pointer">
                    <img src="/img/icon-64.png" class="mr-3 h-6">
                    <div>註冊分享</div>
                </div>


                <input v-model="url" class="form-control" placeholder="" />

                <div class="h-5"></div>

                <div>
                    <div class="text-26 mb-1">@{{ self.recommendCode }}</div>
                </div>
                <div class="text-14 text-aaa">(我的推薦碼)</div>

                <div class="h-5"></div>

                <div class="bg-582602 text-white py-5 text-20 flex justify-center items-center rounded-5 shadow-3 pointer" @click="copyDo()">
                    <img src="/img/icon-65.png" class="mr-3 h-6">
                    <div>複製推薦碼</div>
                </div>

            </div>

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.mounted = function () {
        this.initializeApp();
    }


    vueItem.methods = Object.assign(vueItem.methods, {

        initializeApp() {
            const self = this;
            try {

                liff.init({ liffId: vueData.liffID }).then(() => {
                }).catch((err) => {
                    self.alert(err);
                });

            } catch (e) {
                self.alert('error: ' + e);
            }
            // alert(vueData.liffID);
        },

        shareToLine() {
            try {

                liff.shareTargetPicker([
                    {
                        'type': 'text',
                        text: this.url,
                    }
                ])
                    .then(function (res) {
                        if (res) {
                            alert('分享完成')

                            // succeeded in sending a message through TargetPicker
                            console.log(`[${res.status}] Message sent!`)
                        } else {
                            // alert('分享失敗')

                        }
                    }).catch(function (error) {
                        alert('分享失敗')

                    })


                // if (liff.isApiAvailable('shareTargetPicker')) {
                // liff.shareTargetPicker([
                //     {
                //         type: "text",
                //         text: this.url,
                //     }
                // ]).then(
                //     alert('分享完成')
                // ).catch(function (res) {
                //     alert('分享失敗')
                // });
                // } else {
                //     alert('發生錯誤.');
                // }

            } catch (e) {
                alert('發生錯誤');
            }

        },

        copyUrlDo() {
            var text = this.url;
            if (window.clipboardData && window.clipboardData.setData) {
                // Internet Explorer-specific code path to prevent textarea being shown while dialog is visible.
                return window.clipboardData.setData("Text", text);
            }
            else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
                var textarea = document.createElement("textarea");
                textarea.textContent = text;
                textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in Microsoft Edge.
                document.body.appendChild(textarea);
                textarea.select();
                try {
                    return document.execCommand("copy");  // Security exception may be thrown by some browsers.
                }
                catch (ex) {
                    console.warn("Copy to clipboard failed.", ex);
                    return false;
                }
                finally {
                    document.body.removeChild(textarea);
                }
            }

        },
        copyDo() {
            var text = this.self.recommendCode;
            if (window.clipboardData && window.clipboardData.setData) {
                // Internet Explorer-specific code path to prevent textarea being shown while dialog is visible.
                alert('已複製');

                return window.clipboardData.setData("Text", text);

            }
            else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
                var textarea = document.createElement("textarea");
                textarea.textContent = text;
                textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in Microsoft Edge.
                document.body.appendChild(textarea);
                textarea.select();
                alert('已複製');


                try {
                    return document.execCommand("copy");  // Security exception may be thrown by some browsers.
                }
                catch (ex) {
                    console.warn("Copy to clipboard failed.", ex);
                    return false;
                }
                finally {
                    document.body.removeChild(textarea);
                }
            }

            // alert('已複製');


        }

    });

    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
