@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="container py-5 text-black">

            <template v-if="indexStep == 1">
                <div class="frame  mb-5">

                    <div class="mb-6">
                        <div class="mb-2">姓名</div>
                        <div class="text-orange">@{{ self.name }}</div>
                        <!-- <input v-model="item.name" class="form-control" placeholder="" /> -->
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">手機</div>
                        <div class="text-orange">@{{ self.phone }}</div>
                        <!-- <input v-model="item.phone" class="form-control" placeholder="" type="tel"> -->
                    </div>
                    <!-- <div class="mb-6">
                        <div class="mb-2">身分證字號</div>
                        <input v-model="item.nationID" class="form-control" placeholder="" type="text" />
                    </div> -->
                    <div class="mb-6">
                        <div class="mb-2">E-mail</div>
                        <div class="text-orange">@{{ self.email }}</div>
                        <!-- <input v-model="item.email" class="form-control" placeholder="" type="email"> -->
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">備註(選填)</div>
                        <div class="text-orange">@{{ self.memo }}</div>
                        <!-- <input v-model="item.memo" class="form-control" placeholder="" type="text"> -->
                    </div>
                </div>
                <!--
                <button class="btn-white" @click="stepNext()">
                    下一步：發送簡訊驗證碼
                </button> -->
            </template>


        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    vueItem.data = Object.assign(vueItem.data, {
        indexStep: 1,
        second: 60,
        verifyCode: '',
        isSuccess: false,
        // item: {
        //     name: '',
        //     phone: '',
        //     nationID: '',
        //     email: '',
        //     memo: null,
        // }
    });

    vueItem.mounted = function () {

        if (this.self.isCompleteRegister == 1) {
            // this.alert('此手機/證號已註冊', function () {
            //     document.location = 'main';
            // });
        }
    }
    vueItem.methods = Object.assign(vueItem.methods, {


    });

    var vue = new Vue(vueItem);


</script>
<!-- -------------------- -->

@stop
