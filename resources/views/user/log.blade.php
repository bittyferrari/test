@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">

        <div class="bg-black relative text-white rounded-b-20 pt-2 pb-8">
            <div class="text-14 mb-8 text-center">總太地產 心生活美學聚落</div>
            <div class="flex items-center justify-center">
                <img :src="self.photoLine" class="w-16 rounded-full mr-5">
                <div class="text-24">@{{ self.name }}</div>
            </div>
            <div class="h-10"></div>
            <div class="h-2"></div>
        </div>

        <div class="flex w-full flex-wrap text-center justify-center" style="transform: translateY(-50px)">
            <div class="px-2 w-1/3">
                <div class="bg-white overflow-hidden shadow-3 rounded-5 pt-2">
                    <div class="text-10 mb-2 text-aaa">目前剩餘</div>
                    <div class="text-26 mb-2">@{{ self.price | number }}</div>
                    <div class="bg-C7B489 w-full text-white py-2 text-12">總太點數</div>
                </div>
            </div>
            <div class="px-2 w-1/3">
                <div class="bg-white overflow-hidden shadow-3 rounded-5 pt-2">
                    <div class="text-10 mb-2 text-aaa">目前剩餘</div>
                    <div class="text-26 mb-2">@{{ self.pricePointCoupon | number }}</div>
                    <div class="bg-C2762C w-full text-white py-2 text-12">活動點數</div>
                </div>
            </div>
            <!-- <div class="px-2 w-1/3">
                <div class="bg-white overflow-hidden shadow-3 rounded-5 pt-2" v-if="couponNearest">
                    <div class="text-10 mb-2 text-aaa">@{{ couponNearest.timeTo | date }}到期</div>
                    <div class="text-26 mb-2">@{{ couponNearest.pricePoint - couponNearest.pricePointUsed | number }}</div>
                    <div class="bg-A38442 w-full text-white py-2 text-12">活動點數到期日</div>
                </div>
                <div class="bg-white overflow-hidden shadow-3 rounded-5 pt-2" v-else>
                    <div class="text-10 mb-2 text-aaa">--</div>
                    <div class="text-26 mb-2">--</div>
                    <div class="bg-A38442 w-full text-white py-2 text-12">活動點數到期日</div>
                </div>
            </div> -->
        </div>
    </div>
    <!--
    <div class="relative overflow-hidden ">
        <div class="flex flex-wrap items-center">
            <div class="text-16 w-1/2 pt-6 text-center bg-white text-black" style="height:76px;">目前剩餘點數</div>
            <div class="text-24 w-1/2 py-5 text-center bg-orange text-white" style="height:76px">@{{ self.price }}點</div>
        </div>
        <div class="absolute" style="width:calc(100% + 8px);left:-4px;top:0;height:76px;box-shadow: inset 0px 4px 4px rgba(0, 0, 0, 0.25);">
        </div>
    </div> -->


    <div class="container" style="margin-top:-20px;">

        <div class="flex rounded-full w-full bg-white text-16" v-if="typeID == 'exchange'">

            <a href="log?typeID=exchange" class="w-1/2 bg-black text-white rounded-full text-center py-4">
                兌換紀錄
            </a>
            <a href="log?typeID=log" class="w-1/2 text-999 text-center py-4">
                交易紀錄
            </a>
        </div>


        <div class="flex rounded-full w-full bg-white text-16" v-if="typeID == 'log'">

            <a href="log?typeID=exchange" class="w-1/2 text-999 rounded-full text-center py-4">
                兌換紀錄
            </a>
            <a href="log?typeID=log" class="w-1/2 bg-black text-white text-center py-4 rounded-full">
                交易紀錄
            </a>
        </div>

        <!--
        <div class="flex flex-wrap mt-5 ">
            <div class="w-1/2 px-5">
                <a href="log?typeID=exchange" v-if="typeID == 'exchange'"
                    class="text-center block w-full border-red border-2 py-3 text-red rounded-full bg-white">兌換紀錄</a>
                <a href="log?typeID=exchange" v-else class="text-center block w-full py-3 rounded-full bg-white text-black">兌換紀錄</a>
            </div>
            <div class="w-1/2 px-5">
                <a href="log?typeID=log" v-if="typeID == 'log'"
                    class="text-center block w-full border-red border-2 py-3 text-red rounded-full bg-white">交易紀錄</a>
                <a href="log?typeID=log" v-else class="text-center block w-full py-3 rounded-full bg-white text-black">交易紀錄</a>
            </div>
        </div> -->


        <div class="flex items-center justify-center mt-5 mb-6 text-20">
            <div class="pointer p-3" @click="monthPrevious()">
                <img src="/img/icon-68.png" class="w-2">
            </div>
            <div class="mx-5">@{{ year }}年@{{ month }}月</div>
            <div class="pointer p-3" @click="monthNext()">
                <img src="/img/icon-69.png" class="w-2">
            </div>
        </div>

        <div class="text-black ">

            <!--
            .########.##.....##.##.....##..######.....###....##....##..######...########
            .##........##...##..##.....##.##....##...##.##...###...##.##....##..##......
            .##.........##.##...##.....##.##........##...##..####..##.##........##......
            .######......###....#########.##.......##.....##.##.##.##.##...####.######..
            .##.........##.##...##.....##.##.......#########.##..####.##....##..##......
            .##........##...##..##.....##.##....##.##.....##.##...###.##....##..##......
            .########.##.....##.##.....##..######..##.....##.##....##..######...########
            -->
            <div v-for="(x, i) of items" class="mb-3" v-if="typeID == 'exchange'">

                <!-- 活動點數 -->
                <template v-if="x.priceTypeID == 2">

                    <div class="flex mb-3 h-16">

                        <img src="/img/icon-66.png" class="h-full">

                        <div class="bg-C2762C text-white h-full flex items-center px-5" :title="x.id">
                            活動<br>點數
                        </div>

                        <div class="bg-white px-3 flex-auto ">

                            <div class="flex justify-between text-14 pt-2 mb-1">
                                <div>序號兌換</div>
                                <div class="text-red">@{{ x.couponTimeTo | date }}點數到期</div>
                            </div>
                            <div class="flex justify-between text-16 items-end">
                                <div>獲得 <span class="text-green font-bold">@{{ x.pricePoint | number }}</span></div>
                                <div class="text-bray2 text-12">@{{ x.createdAt }}</div>
                            </div>


                        </div>
                        <img src="/img/icon-67.png" class="h-full">

                    </div>

                </template>
                <!-- 總太點數 -->
                <template v-else>

                    <div class="bg-white flex rounded-6 overflow-hidden mb-3 h-16">

                        <div class="bg-C7B489 text-white h-full flex items-center px-5" :title="x.id">
                            總太<br>點數
                        </div>

                        <div style="width:5px" class="bg-C7B489 h-full"></div>
                        <div class="bg-white px-3 flex-auto ">

                            <div class="flex justify-between text-14 pt-2 mb-1">
                                <div>@{{ typeText('store', x.storeID) }}</div>
                                <div v-if="x.isSuccess != 1" class="text-red">
                                    失敗
                                </div>
                            </div>
                            <div class="flex justify-between text-16 items-end">
                                <div>@{{ typeText('transactionTypeChinese', x.typeID) }}
                                    <span class="font-bold" :class="{ 'text-green': x.price > 0, 'text-red': x.price < 0}">@{{ x.price | numberPositive | number
                                        }}</span>
                                </div>
                                <div class="text-bray2 text-12">@{{ x.createdAt }}</div>
                            </div>
                        </div>
                    </div>
                </template>

            </div>


            <!--
            .##........#######...######..
            .##.......##.....##.##....##.
            .##.......##.....##.##.......
            .##.......##.....##.##...####
            .##.......##.....##.##....##.
            .##.......##.....##.##....##.
            .########..#######...######..
            -->
            <div v-for="(x, i) of items" class="mb-3" v-if="typeID == 'log'">

                <div class="bg-white rounded-5 px-3 py-3">

                    <div class="flex justify-between mb-3 w-full items-center">


                        <div class="text-14">
                            <div v-if="x.typeID == 12">
                                課程: @{{ x.courseName }}
                            </div>
                            <div v-else>
                                @{{ typeText('store', x.storeID) }}

                            </div>

                        </div>
                        <div class="text-14 text-gray2">@{{ x.createdAt }}</div>
                    </div>

                    <div class="flex w-full items-center">

                        <template v-if="x.usedCouponPricePoint > 0">
                            <div class="mr-1 py-1 px-2 rounded-5 text-12 text-white"
                                style="background:url(/img/icon-71.png) center no-repeat;background-size:100% 100%">
                                活動點數
                            </div>
                            <div class="text-red font-bold text-15 mr-3">@{{ x.usedCouponPricePoint | number}}</div>

                        </template>

                        <template v-if="x.usedPrice > 0">
                            <div class="mr-1 py-1 px-2 rounded-5 text-12 bg-C7B489 text-white">總太點數</div>
                            <div class="text-red font-bold text-15">@{{ x.usedPrice | number }}</div>
                        </template>

                        <div class="ml-auto mr-1 text-15">
                            <!-- 共折抵 -->
                            @{{ typeText('transactionTypeChinese', x.typeID) }}
                        </div>
                        <div class="text-red font-bold text-15">@{{ x.price | numberPositive | number }}</div>

                    </div>


                </div>

            </div>

        </div>


    </div>

</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    // vueListing.data.condition = Object.assign(vueListing.data.condition, {
    // 	id: null,
    // 	name: null,
    // 	lineUserID: null,
    // 	roleID: null,
    // });

    vueItem.methods = Object.assign(vueItem.methods, {
        monthPrevious() {
            var month = this.month - 1;
            var year = this.year;
            if (month <= 0) {
                month = 12;
                year -= 1;
            }
            document.location = 'log?typeID=' + this.typeID + '&year=' + year + '&month=' + month;
        },

        monthNext() {
            var month = this.month + 1;
            var year = this.year;
            if (month >= 13) {
                month = 1;
                year += 1;
            }
            document.location = 'log?typeID=' + this.typeID + '&year=' + year + '&month=' + month;
        },

    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
