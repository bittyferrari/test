@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->


@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">


        <template v-if="indexStep == 1">

            <div class="container py-5 text-black">

                <div class="bg-white p-5 mb-8">

                    <div class="mb-6">
                        <div class="mb-2">門票張數</div>
                        <input v-model.number="countTicket" class="form-control bg-pink" placeholder="" type="number" />
                    </div>

                </div>

                <button class="bg-red text-white w-full p-5 text-18" @click="stepNext()">
                    送出
                </button>
            </div>

        </template>

        <template v-if="indexStep == 2">

            <div class="container py-5 text-black">
                <qrcode :value="item.md5" :options="{ width: 1000 }" class="qrcode mt-10 mb-10"></qrcode>
            </div>

            <div class="bg-red text-white p-3 text-center text-24 mb-5">
                點數：@{{ item.price }}點
            </div>

            <div class="text-center text-18  ">
                交易剩餘時間 00:@{{ second | pad(2) }}
            </div>

        </template>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.data = Object.assign(vueItem.data, {
        item: {
            id: null,
            md5: '',
            price: 0,
        },
        indexStep: 1,
        // indexStep: 2,
        second: 60,
        verifyCode: '',
        isSuccess: false,

        countTicket: 1,
    });

    vueItem.methods = Object.assign(vueItem.methods, {
        stepNext() {

            // get transcation
            const data = {
                typeID: 7,
                countTicket: this.countTicket
            };

            this.$http.post('/transaction/createDo', data).then(function (r) {
                const body = r.body;
                // this.items = body.data;

                if (body.data.isSuccess) {

                    this.item = body.data.item;

                    this.indexStep++;
                    if (this.indexStep == 2) {
                        this.startCountdown();
                        this.checkTransaction();
                    }

                } else {
                    // alert('發生錯誤, 請再試一次');
                    this.alert('點數餘額不足');
                    // location.reload();

                }

            });
        },

        resendDo() {
            alert('resendDo');
        },

        startCountdown() {
            if (this.item.isStoreScan != 1) {

                let second = this.second;
                second--;

                if (second < 0) {
                    // alert('qrcode過期');
                    // document.location = 'index';
                } else {
                    this.second = second;
                    this.timeoutCountdown = setTimeout(this.startCountdown, 1000);
                }
            }
        },

        checkTransaction() {

            const self = this;
            setTimeout(function () {

                let data = {
                    md5: self.item.md5,
                }

                // alert('checkTransactionDo');
                self.$http.post('/transaction/getItem', data).then(function (r) {
                    self.updateTimes++;

                    const body = r.body;

                    const data = body.data;
                    self.item = data.item;

                    console.log(data);

                    if (data.item.isSuccess == 1) {
                        this.alert('點數' + (self.item.price * -1) + ' 費用 交易成功', function () {
                            document.location = 'index';
                        });
                        return;
                    }
                    self.checkTransaction();
                });

            }, 2000);
        },



    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
