@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="container py-5">
            <!-- <h1 class=" ">User Transaction</h1> -->

            <template v-if="!isStoreSet">
                <!-- <qrcode :value="APP_URL + '/user/transaction?md5=' + item.md5" :options="{ width: 1000 }" class="qrcode mt-10 mb-10"></qrcode> -->
                <qrcode :value="item.md5" :options="{ width: 1000 }" class="qrcode mt-10 mb-10"></qrcode>

                <div class="text-center text-18">
                    兌換有效時間 00:@{{ second | pad(2) }}
                </div>

            </template>

            <template v-else>

                <div class="bg-white text-center py-10 text-20">
                    <div class="mb-2">
                        <div v-if="item.typeID == 1">本次儲值點數為</div>
                        <div v-if="item.typeID == 2">本次兌換點數為</div>
                        <div v-if="item.typeID == 3">本次退貨點數為</div>
                    </div>

                    <div class="text-yellow">
                        【 @{{ item.price | numberPositive }}點 】
                    </div>
                    <div class="text-yellow" v-if="item.priceCash">
                        【 現金@{{ item.priceCash | numberPositive }} 】
                    </div>

                </div>

                <div class="flex flex-wrap px-4 mt-10">
                    <div class="w-1/2 pr-5">
                        <button class="btn bg-white w-full text-normal" @click="cancelDo()">取消</button>
                    </div>
                    <div class="w-1/2 pl-5" v-if="item.isEnd != 1">
                        <button class="btn bg-yellow w-full" @click="confirmDo()">確認</button>
                    </div>
                </div>

            </template>
            <!--

            <div v-if="item.typeID != null">

                <div>
                    類型:
                    <span v-if="item.typeID == 1">儲值</span>
                    <span v-if="item.typeID == 2">兌換</span>
                    <span v-if="item.typeID == 3">退款</span>
                </div>
                <div>
                    金額: @{{ item.price | numberPositive }}
                </div>
            </div> -->

            <div v-if="isStoreSet">


                <!--
                display store detail
                <br>
                <button class="btn btn-primary" @click="confirmDo()">確認</button> -->

            </div>

            <!-- <hr>
            <hr>
            <hr>
            <hr>


            <div v-if="isStoreScan">store scanned</div>
            <div v-else>store not scanned</div>
            <div>update times: @{{ updateTimes }}</div> -->

        </div>


    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.data = Object.assign(vueItem.data, {
        isStoreScan: false,
        isStoreSet: false,
        updateTimes: 0,
        second: 60,
        timeoutCountdown: null,
    });

    vueItem.mounted = function () {
        this.startCountdown();
        this.checkTransaction();

        liff.init({
            liffId: vueData.liffID
        }).then(() => {
        }).catch((err) => {
            // alert('錯誤: ' + err);
            console.log(err);
        });

    }

    vueItem.methods = Object.assign(vueItem.methods, {

        lineSendMessage(q) {
            try {
                liff.sendMessages([{
                    type: 'text',
                    // text: "You've successfully sent a message! Hooray!"
                    text: q,
                }]).then(function () {
                    // window.alert('Message sent');
                }).catch(function (error) {
                    // window.alert('Error sending message: ' + error);
                });

            } catch (e) {
                // yada
            }

        },

        startCountdown() {
            if (this.item.isStoreScan != 1) {

                let second = this.second;
                second--;

                if (second < 0) {
                    alert('超過交易時間，交易取消');
                    document.location = 'index';
                } else {
                    this.second = second;
                    this.timeoutCountdown = setTimeout(this.startCountdown, 1000);
                }
            }
        },

        cancelDo() {
            const data = {
                id: this.item.id
            };
            this.$http.post('/transaction/userCancelDo', data).then(function (r) {
                document.location = 'index';
            });
        },

        confirmDo() {

            let data = {
                id: this.item.id,
            }

            this.$http.post('/transaction/userConfirmDo', data).then(function (r) {
                const body = r.body;
                // const data = body.data;

                switch (body.statusID) {
                    case 0:
                        alert('確認完成');
                        // this.lineSendMessage(this.item.createdAt + ' 兌換點數' + this.item.price + '點 成功');
                        this.lineSendMessage('我完成兌換了');
                        document.location = 'index';
                        break;
                    case -1:
                        alert('未知錯誤');
                        document.location = 'index';
                        break;
                    case 2:
                        // alert('error: transaction not won by user');
                        alert('錯誤: 交易不存在');
                        document.location = 'index';
                        break;
                    case 3:
                        alert('錯誤, 交易已經在其他裝置兌換');
                        document.location = 'index';
                        break;
                }

                // this.items = body.data;


            });

        },

        checkTransaction() {

            const self = this;
            setTimeout(function () {

                let data = {
                    md5: self.item.md5,
                }

                // alert('checkTransactionDo');
                self.$http.post('/transaction/getItem', data).then(function (r) {
                    self.updateTimes++;

                    const body = r.body;

                    // this.items = body.data;

                    const data = body.data;
                    self.item = data.item;

                    console.log(data);

                    if (data.item != null) {

                        if (self.item.isStoreScan == 1) {
                            clearTimeout(this.timeoutCountdown);

                            if (!self.isStoreScan) {

                                self.isStoreScan = true;
                                console.log('store scaned, please wait store');
                                // alert('store scaned, please wait store');
                                this.alert('商家已掃描, 請等待商家動作');
                            }

                            // if (self.item.price != 0 && self.item.typeID != null) {
                            if (self.item.typeID != null) {

                                if (!self.isStoreSet) {
                                    self.isStoreSet = true;

                                    console.log('store set complete, please confirm or not');
                                    // alert('store set complete, please confirm or not');
                                    this.alert('商家設置完成, 請確認');
                                }
                            }

                        } else {

                            if (data.isExpired) {

                                console.log('qrcode expired');
                                // alert('qrcode expired');
                                this.alert('交易已過期');
                                document.location = 'index';

                            }

                        }

                    } else {

                        this.alert('錯誤, 找不到交易');
                        document.location = 'index';

                    }


                    self.checkTransaction();
                });

            }, 2000);
        },

    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
