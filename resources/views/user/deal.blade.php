@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->


@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="bg-black relative text-white rounded-b-20 pt-2 pb-8">
            <div class="text-14 mb-8 text-center">總太地產 心生活美學聚落</div>
            <div class="flex items-center justify-center">
                <img :src="self.photoLine" class="w-16 rounded-full mr-5">

                <div class="text-24">@{{ self.name }}</div>
            </div>
            <div class="h-10"></div>
            <div class="h-2"></div>
        </div>

        <div class="flex w-full flex-wrap text-center justify-center" style="transform: translateY(-50px)">
            <div class="px-2 w-1/3">
                <div class="bg-white overflow-hidden shadow-3 rounded-5 pt-2">
                    <div class="text-10 mb-2 text-aaa">目前剩餘</div>
                    <div class="text-26 mb-2">@{{ self.price | number }}</div>
                    <div class="bg-C7B489 w-full text-white py-2 text-12">總太點數</div>
                </div>
            </div>
            <div class="px-2 w-1/3">
                <div class="bg-white overflow-hidden shadow-3 rounded-5 pt-2">
                    <div class="text-10 mb-2 text-aaa">目前剩餘</div>
                    <div class="text-26 mb-2">@{{ self.pricePointCoupon | number }}</div>
                    <div class="bg-C2762C w-full text-white py-2 text-12">活動點數</div>
                </div>
            </div>
            <!-- <div class="px-2 w-1/3">
                <div class="bg-white overflow-hidden shadow-3 rounded-5 pt-2" v-if="couponNearest">
                    <div class="text-10 mb-2 text-aaa">@{{ couponNearest.timeTo | date }}到期</div>
                    <div class="text-26 mb-2">@{{ couponNearest.pricePoint - couponNearest.pricePointUsed | number }}</div>
                    <div class="bg-A38442 w-full text-white py-2 text-12">活動點數到期日</div>
                </div>
                <div class="bg-white overflow-hidden shadow-3 rounded-5 pt-2" v-else>
                    <div class="text-10 mb-2 text-aaa">--</div>
                    <div class="text-26 mb-2">--</div>
                    <div class="bg-A38442 w-full text-white py-2 text-12">活動點數到期日</div>
                </div>
            </div> -->
        </div>
        <!-- <div class="h-10"></div> -->

        <div class="mx-auto w-4/5 py-3 bg-white rounded-5 shadow-3 px-6 text-18">

            <a class="flex w-full items-center py-3" href="transaction" v-if="self.isCompleteRegister">
                <img src="/img/icon-57.png" class="h-6 mr-3">
                <div>交易QR code</div>
                <img src="/img/icon-56.png" class="ml-auto h-4  ">
            </a>

            <a class="flex w-full items-center py-3 pointer" @click="remind()" v-else>
                <img src="/img/icon-57.png" class="h-6 mr-3">
                <div>交易QR code</div>
                <img src="/img/icon-56.png" class="ml-auto h-4  ">
            </a>

            <hr>
            <a class="flex w-full items-center py-3" href="log">
                <img src="/img/icon-58.png" class="h-6 mr-3">
                <div>點數查詢</div>
                <img src="/img/icon-56.png" class="ml-auto h-4  ">
            </a>
            <hr>
            <a class="flex w-full items-center py-3" href="coupon">
                <img src="/img/icon-59.png" class="h-6 mr-3">
                <div>序號兌換</div>
                <img src="/img/icon-56.png" class="ml-auto h-4  ">
            </a>

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.methods = Object.assign(vueItem.methods, {
        remind() {
            this.alert('已註冊的人才能使用');
        },

    });

    var vue = new Vue(vueItem);

</script>
<!-- -------------------- -->

@stop
