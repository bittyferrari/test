@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->


@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="container py-5">

            <template v-if="!isStoreSet">

                <qrcode :value="item.md5" :options="{ width: 1000 }" class="qrcode mt-10 mb-10 rounded-10 shadow-1"></qrcode>

                <div class="text-center text-18 text-white">
                    交易有效時間 00:@{{ second | pad(2) }}
                </div>

            </template>

            <template v-else>

                <div v-if="!isSuccess">
                    <div class="bg-white text-center py-10 text-20 rounded-10 shadow-1">
                        <div class="mb-2">
                            <div v-if="item.typeID == 1">本次儲值點數為</div>
                            <div v-if="item.typeID == 2">本次交易點數為</div>
                            <div v-if="item.typeID == 3">本次退貨點數為</div>
                        </div>

                        <div class="text-red">
                            【 @{{ item.price | numberPositive }}點 】
                        </div>


                        <div class="text-red mt-5" v-if="item.isUserPriceEnough === 0">
                            點數不足 交易失敗
                        </div>


                    </div>

                    <div class="flex flex-wrap mt-10 text-black">
                        <div class="w-1/2 pr-5">
                            <button class="btn-white w-full text-normal shadow-1" @click="cancelDo()">取消</button>
                        </div>
                        <div class="w-1/2 pl-5" v-if="item.isEnd != 1">
                            <button class="btn-white w-full shadow-1" @click="confirmDo()">確認</button>
                        </div>
                    </div>

                </div>

                <div v-else class="frame text-black text-24 p-20 text-center">
                    交易成功！
                </div>

            </template>

        </div>


    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.data = Object.assign(vueItem.data, {
        isStoreScan: false,
        isStoreSet: false,
        updateTimes: 0,
        second: 60,
        isSuccess: false,
        timeoutCountdown: null,
        // isUserPriceEnough:false,
    });

    vueItem.mounted = function () {
        this.startCountdown();
        this.checkTransaction();

        liff.init({
            liffId: vueData.liffID
        }).then(() => {
        }).catch((err) => {
            console.log(err);
        });

    }

    vueItem.methods = Object.assign(vueItem.methods, {

        lineSendMessage(q) {
            try {
                liff.sendMessages([{
                    type: 'text',
                    // text: "You've successfully sent a message! Hooray!"
                    text: q,
                }]).then(function () {
                    // window.alert('Message sent');
                }).catch(function (error) {
                    // window.alert('Error sending message: ' + error);
                });

            } catch (e) {
                // yada
            }

        },

        startCountdown() {
            if (this.item.isStoreScan != 1) {

                let second = this.second;
                second--;

                if (second < 0) {
                    alert('超過交易時間，交易取消');
                    document.location = 'index';
                } else {
                    this.second = second;
                    this.timeoutCountdown = setTimeout(this.startCountdown, 1000);
                }
            }
        },

        cancelDo() {
            const data = {
                id: this.item.id
            };
            this.$http.post('/transaction/userCancelDo', data).then(function (r) {
                document.location = 'index';
            });
        },

        confirmDo() {

            let data = {
                id: this.item.id,
            }

            this.$http.post('/transaction/userConfirmDo', data).then(function (r) {
                const body = r.body;
                // const data = body.data;

                switch (body.statusID) {
                    case 0:
                        // this.alert('確認完成');

                        // this.lineSendMessage(this.item.createdAt + ' 兌換點數' + this.item.price + '點 成功');
                        this.lineSendMessage('我完成交易了');
                        this.isSuccess = true;
                        // document.location = 'index';
                        break;
                    case -1:
                        this.alert('未知錯誤');
                        document.location = 'index';
                        break;
                    case 2:
                        // alert('error: transaction not won by user');
                        this.alert('錯誤: 交易不存在');
                        document.location = 'index';
                        break;
                    case 3:
                        this.alert('錯誤, 交易已經在其他裝置交易');
                        document.location = 'index';
                        break;
                }

            });

        },

        checkTransaction() {

            const self = this;
            setTimeout(function () {

                let data = {
                    md5: self.item.md5,
                }

                self.$http.post('/transaction/getItem', data).then(function (r) {
                    self.updateTimes++;

                    const body = r.body;
                    const data = body.data;
                    self.item = data.item;

                    if (self.item.isEnd == 1) {

                        clearTimeout(this.timeoutCountdown);

                        if (self.item.isStoreCancel == 1) {

                            this.alert('已取消', function () {
                                document.location = 'index';
                            });

                        } else {

                            this.alert('交易結束', function () {
                                document.location = 'index';
                            });

                        }

                    } else {


                        if (data.item != null) {

                            if (self.item.isStoreScan == 1) {
                                clearTimeout(this.timeoutCountdown);

                                if (!self.isStoreScan) {

                                    self.isStoreScan = true;
                                    console.log('store scaned, please wait store');
                                    // this.alert('商家已掃描, 請等待商家動作');
                                }

                                // if (self.item.price != 0 && self.item.typeID != null) {
                                if (self.item.typeID != null) {

                                    if (!self.isStoreSet) {
                                        self.isStoreSet = true;

                                        console.log('store set complete, please confirm or not');
                                        // this.alert('商家設置完成, 請確認');
                                    }
                                }

                            } else {

                                if (data.isExpired) {

                                    this.alert('交易已過期', function () {
                                        document.location = 'index';
                                    });


                                }

                            }

                        } else {

                            this.alert('找不到交易或已過期', function () {
                                document.location = 'index';
                            });

                        }
                        self.checkTransaction();

                    }


                });

            }, 2000);
        },

    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
