@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div class="max-w-md mx-auto " id="vue" v-cloak>

    <div class="flex items-center min-h-screen">
        <div class="container py-5 text-black ">

            <template v-if="indexStep == 1">

                <div class="frame mb-5">

                    <div class="">
                        <div class="mb-2">序號</div>
                        <input v-model="item.code" class="form-control" placeholder="" />
                    </div>
                    <div class="h-4"></div>
                    <!--
                    <div class="mb-6">
                        <div class="mb-2">驗證碼</div>
                        <input v-model="item.captcha" class="form-control bg-pink" placeholder="" type="text" />
                        <div>
                            <img :src="urlCaptcha" @click="refreshCaptcha()" class="pointer" />
                        </div>

                    </div> -->

                </div>

                <button class="btn-white" @click="stepNext()">
                    送出
                </button>

            </template>

            <!--
            <div class="h-10">

            </div> -->

            <template v-if="indexStep == 2">
                <div class="frame mb-5 text-center text-blue" v-if="isSuccess">
                    <div class="text-24" v-if="coupon.priceTypeID == 1">兌換點數【@{{ coupon.price }}點】成功！</div>
                    <div class="text-24" v-else>兌換點數【@{{ coupon.pricePoint }}點】成功！</div>
                </div>

                <div class="frame mb-5 text-center text-red" v-else>
                    <div class="text-24 mb-5" v-if="coupon.priceTypeID == 1">兌換點數【@{{ coupon.price }}點】失敗！</div>
                    <div class="text-24 mb-5" v-else>兌換點數【@{{ coupon.pricePoint }}點】失敗！</div>
                    <div class="text-18" v-if="isCodeError">序號錯誤</div>
                </div>
            </template>

            <div class="h-10"></div>
            <div class="h-10"></div>
            <div class="h-10"></div>
        </div>
    </div>

</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.data = Object.assign(vueItem.data, {
        indexStep: 1,
        second: 60,
        verifyCode: '',
        isSuccess: false,
        isCodeError: false,
        item: {
            cope: '',
            captcha: '',
        },
        coupon: {
            price: 0,
        },
        urlCaptcha: '',
    });

    vueItem.mounted = function () {

        if (this.isDev) {
            this.item.phone = '12345';
            this.item.price = 12;
        }

        // if (this.self.isCompleteRegister != 1) {
        //     this.alert('請先完成註冊', function () {
        //         document.location = 'register';

        //     });
        // }

        // this.refreshCaptcha();
    }

    vueItem.methods = Object.assign(vueItem.methods, {
        refreshCaptcha() {
            const url = '/_helper/refereshCapchaDo';
            this.$http.post(url, { a: 1 }).then(function (r) {
                this.urlCaptcha = r.body.data;
            });
        },

        stepNext() {

            switch (this.indexStep) {

                case 1:
                    this.$http.post('couponDo', this.item).then(function (r) {
                        const body = r.body;
                        // this.items = body.data;

                        switch (body.statusID) {
                            case 0:
                                this.indexStep++;
                                this.coupon = body.data.item;
                                this.isSuccess = true;
                                this.alert('使用序號成功');
                                break;
                            case 1:
                                this.alert('圖形驗證錯誤');
                                this.refreshCaptcha();
                                break;
                            case 2:
                                this.alert('找不到序號');
                                this.refreshCaptcha();
                                break;
                            case 3:
                                this.alert('已使用過序號');
                                this.refreshCaptcha();
                                break;
                            case 4:
                                this.alert('使用序號資格不符');
                                this.refreshCaptcha();
                                break;
                            case 5:
                                this.alert('序後已被兌換完畢');
                                this.refreshCaptcha();
                                break;
                            case 6:
                                this.alert('使用期限已過期');
                                this.refreshCaptcha();
                                break;

                            default:
                                this.alert('發生錯誤');
                                this.refreshCaptcha();
                                break;
                            // 1 使用序號成功 2. 輸入序號有誤 3. 使用期限已過期 4. 序號已使用過 5.   使用序號資格不符 6 已使用過序號

                        }
                    });

                    break;
                case 2:

                    break;
            }

            // this.indexStep++;
            // if (this.indexStep == 2) {
            //     this.startCountdown();
            // }
        },

        resendDo() {
            alert('resendDo');
        },

        startCountdown() {
            if (this.item.isStoreScan != 1) {

                let second = this.second;
                second--;

                if (second < 0) {
                    // alert('qrcode過期');
                    // document.location = 'index';
                } else {
                    this.second = second;
                    this.timeoutCountdown = setTimeout(this.startCountdown, 1000);
                }
            }
        },


    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
