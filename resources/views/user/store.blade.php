@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->


@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">

        <div class="flex justify-between items-center bg-orange px-4 py-4 text-14 text-white" v-if="isSearch == false">
            <div>
                <button class="flex items-center border border-fff px-4 py-1 rounded-6 pointer" @click="isSearch = true;">
                    <img src="/img/icon-32.png" class="w-6 mr-1" />
                    <span>搜尋</span>
                </button>
            </div>
            <div>
                <div class="flex items-center  ">
                    <!-- <img src="/img/icon-31.png" class="w-10 mr-1" /> -->
                    <!-- <span class="text-18">贊助廠商</span> -->
                    <span class="text-18">社區頭家</span>
                </div>
            </div>
            <div>
                <button class="flex items-center border border-white px-4 py-1 rounded-6 pointer" @click="switchDisplayType()">
                    <img src="/img/icon-33.png" class="w-6 mr-2  " />
                    <span class="nowrap">顯示模式</span>
                </button>
            </div>
        </div>

        <div class="flex items-center bg-orange px-4 py-4 text-14" v-else>
            <div class="flex-grow">
                <div class="flex items-center border border-fff pl-4 rounded-6 pointer overflow-hidden" @click="isSearch = true;">
                    <!-- <img src="/img/icon-14.png" class="w-6 mr-1" /> -->
                    <img src="/img/icon-32.png" class="w-6 mr-1" />
                    <input v-model="condition.name2" class="border-none bg-FFFDF0 w-full py-1 px-2" style="margin-top:1px; margin-bottom:1px;" placeholder="搜尋"
                        @keyup.enter="searchDo()" />
                </div>
            </div>
            <div>
                <button class="ml-3 flex items-center border border-fff px-4 py-1 rounded-6 pointer text-fff" @click="switchDisplayType()">
                    <img src="/img/icon-33.png" class="w-6  " />
                    <!-- <span class="nowrap">顯示模式</span> -->
                </button>
            </div>
        </div>

        <div class="container py-5 bg-fef9f9 min-h-screen">

            <div class="w-full overflow-auto pb-3">
                <div style="width:800px;">
                    <div class="pointer mr-4 px-5 py-2 inline-block text-red2 bg-white rounded-full shadow-2" @click="changeStoreType(null)"
                        :class="{ 'text-on-white':  condition.typeID == null, 'bg-orange': condition.typeID == null }">
                        全部
                    </div>

                    <div v-for="(x, i) of option.storeType" class="pointer mr-4 px-5 py-2 inline-block text-red2 bg-white rounded-full shadow-2"
                        :class="{ 'text-on-white':  condition.typeID == x.id, 'bg-orange': condition.typeID == x.id }" @click="changeStoreType(x.id)">
                        @{{ x.name }}
                    </div>
                </div>
            </div>

            <div v-if="items.length <= 0" class="py-10 text-center">
                找不到商家
            </div>

            <div v-for="(x, i) of items" class="bg-white rounded-6 shadow-1 py-5 px-3 mt-2 mb-5">

                <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                <div class="flex flex-wrap  text-14" v-if="displayTypeID == 1">
                    <div class="w-1/2 pr-3">

                        <div class="flex items-start  ">
                            <img src="/img/icon-15.png" class="w-6 mr-1" />
                            <div class="text-20 text-black">@{{ x.name }}</div>
                        </div>

                        <div class="flex items-start mb-2">
                            <img src="/img/icon-09.png" class="w-6 mr-1" />
                            <div class="text-4F4F4F">@{{ x.addressText }}</div>
                        </div>

                        <div class="flex items-start mb-2">
                            <img src="/img/icon-11.png" class="w-6 mr-1" />
                            <div class="text-828282">@{{ x.phone }}</div>
                        </div>

                        <a :href="x.url">
                            <div class="flex items-start">
                                <img src="/img/icon-10.png" class="w-6 mr-1" />
                                <div class=" text-2F80ED">@{{ x.nameUrl }}</div>
                            </div>
                        </a>

                    </div>
                    <div class="w-1/2">
                        <img :src="x.photo | asset">
                    </div>

                </div>

                <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                <div class="flex flex-wrap  text-14" v-if="displayTypeID == 2">
                    <div class="w-full">
                        <div class="flex items-center mb-3">
                            <img src="/img/icon-15.png" class="w-6 mr-1" />
                            <div class="text-20 text-black">@{{ x.name }}</div>
                        </div>
                        <div class="flex items-start mb-2">
                            <img src="/img/icon-09.png" class="w-6 mr-1" />
                            <div class="text-4F4F4F">@{{ x.addressText }}</div>
                        </div>
                        <div class="flex items-start mb-2">
                            <img src="/img/icon-11.png" class="w-6 mr-1" />
                            <div class="text-828282">@{{ x.phone }}</div>
                        </div>

                        <a :href="x.url">
                            <div class="flex items-start mb-2">
                                <img src="/img/icon-10.png" class="w-6 mr-1" />
                                <div class=" text-2F80ED">@{{ x.nameUrl }}</div>
                            </div>
                        </a>


                        <img :src="x.photo | asset">

                    </div>
                </div>

                <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                <div class="flex flex-wrap  text-14" v-if="displayTypeID == 3">
                    <div class="w-full">

                        <div class="flex items-center mb-3">
                            <img src="/img/icon-15.png" class="w-6 mr-1" />
                            <div class="text-20 text-black">@{{ x.name }}</div>
                        </div>

                        <div class="flex items-start mb-2">
                            <img src="/img/icon-09.png" class="w-6 mr-1" />
                            <div class="text-4F4F4F">@{{ x.addressText }}</div>
                        </div>

                        <div class="flex items-start mb-2">
                            <img src="/img/icon-11.png" class="w-6 mr-1" />
                            <div class="text-828282">@{{ x.phone }}</div>
                        </div>

                        <a :href="x.url">

                            <div class="flex items-start">
                                <img src="/img/icon-10.png" class="w-6 mr-1" />
                                <div class=" text-2F80ED">@{{ x.nameUrl }}</div>
                            </div>
                        </a>


                    </div>

                </div>
            </div>
            <!-- <h1 class=" ">User Transaction</h1> -->

        </div>


    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.data = Object.assign(vueItem.data, {
        displayTypeID: 1,
        isSearch: false,
        condition: {
            id: null,
            typeID: null,
            name: '',
            name2: '',
        }
    });

    vueItem.mounted = function () {
        // this.checkTransaction();
    }

    vueItem.methods = Object.assign(vueItem.methods, {

        searchDo() {
            this.condition.name = this.condition.name2;
            this.getListing();
            this.isSearch = false;
            this.condition.name2 = '';
        },

        changeStoreType(q) {
            this.condition.name = '';
            this.condition.typeID = q;
            this.getListing();
        },

        getListing() {
            var url = '/store/getListing';
            this.items = [];
            this.$http.post(url, this._data.condition).then(function (r) {
                this.dataResponse = r.body.data;
                var data = r.body.data;

                // this.pages = [];

                // this.isFirstTime = false;
                // this.isSearching = false;

                this.items = r.body.data.items;
                // this.pageTotal = r.body.data.pageTotal;
                // this.totalItem = r.body.data.totalItem;

                // var showingFrom = 0;
                // var showingTo = 0;


                // showingFrom = this.condition._page - 4;
                // showingTo = this.condition._page + 4;

                // if (showingFrom <= 1) {
                //     showingFrom = 1;
                // }
                // if (showingTo > this.pageTotal) {
                //     showingTo = this.pageTotal;
                // }

                // this.pageTotal = data.pageTotal;
                // this.showingFrom = showingFrom;
                // this.showingTo = showingTo;

                // for (var i = showingFrom; i <= showingTo; i++) {
                //     this.pages.push(i);
                // }

            });
        },

        switchDisplayType() {

            this.displayTypeID++;
            if (this.displayTypeID > 3) {
                this.displayTypeID = 1;
            }

        }

    });



    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
