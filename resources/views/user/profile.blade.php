@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="container py-5 text-black">

            <template v-if="indexStep == 1">
                <div class="frame  mb-5">

                    <div class="mb-6">
                        <div class="mb-2">姓名</div>
                        <input v-model="item.name" class="form-control" placeholder="" />
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">生日(西元/月/日)</div>
                        <input v-model="item.birthday" class="form-control" placeholder="" type="date">
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">身份證字號</div>
                        <input v-model="item.nationID" class="form-control" placeholder="" />
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">手機</div>
                        <input v-model="item.phone" class="form-control" placeholder="" type="tel">
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">地址</div>
                        <input v-model="item.addressText" class="form-control" placeholder="">
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">推薦碼(選填)</div>
                        <input v-model="item.recommendCode" class="form-control" placeholder="" readonly disabled>
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">推薦人</div>
                        <div class="form-control" readonly>
                            <div v-if="recommendUser">@{{ recommendUser.name }}</div>
                            <div v-else>--</div>
                        </div>
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">居住的總太社區</div>
                        <select class="form-control" v-model.number="item.communityID">
                            <option :value="null">--</option>
                            <option v-for="(x, i) of option.community" :value="x.id">@{{ x.name }}</option>
                        </select>
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">戶別(選填)</div>
                        <input v-model="item.household" class="form-control" placeholder="">
                    </div>
                    <div class="mb-6">
                        <div class="mb-2">備註(選填)</div>
                        <input v-model="item.memo" class="form-control" placeholder="" type="text">
                    </div>
                </div>

                <!-- <button class="btn-white" @click="submitDo()">
                    更新
                </button> -->
            </template>


        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    vueItem.data = Object.assign(vueItem.data, {
        indexStep: 1,
        second: 60,
        verifyCode: '',
        isSuccess: false,
        // item: {
        //     name: '',
        //     phone: '',
        //     nationID: '',
        //     email: '',
        //     memo: null,
        // }
    });

    vueItem.mounted = function () {

        if (this.self.isCompleteRegister == 1) {
            // this.alert('此手機/證號已註冊', function () {
            //     document.location = 'main';
            // });
        }
    }
    vueItem.methods = Object.assign(vueItem.methods, {

        submitDo() {
            this.notValidMessage = '';
            this.notValidMessages = [];
            if (this.isValid()) {
                this.isProcessing = true;
                this.$http
                    .post('updateProfileDo', this._data)
                    .then(function (res) {
                        this.isProcessing = false;
                        this.responseSource = res;

                        this.alert('更新成功');

                        // if (res.body.statusID != 0) {
                        //     this.onSaveError();
                        // } else {
                        //     if (res.body.data) {
                        //         var data = res.body.data;
                        //         this.item.id = data.item.id;
                        //         if (data.item.createdAt) {
                        //             this.item.createdAt = data.item.createdAt;
                        //         }
                        //         this.onAfterSave();
                        //     }
                        // }
                    })
                    .catch(function () {
                        this.alert('Save failed');
                        this.isProcessing = false;
                        this.onSaveError();
                    });
            } else {
                // window.alert(this.notValidMessage);
                this.alert('Please fill in all required field.');
            }
        },


    });

    var vue = new Vue(vueItem);


</script>
<!-- -------------------- -->

@stop
