@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->


@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">

        <div class="bg-black relative text-white rounded-b-20 pt-2 pb-8">
            <div class="text-14 mb-8 text-center">總太地產 心生活美學聚落</div>
            <div class="flex items-center justify-center">
                <img :src="self.photoLine" class="w-16 rounded-full mr-5">
                <div class="text-24">@{{ self.name }}</div>
            </div>
        </div>

        <div class="h-10"></div>

        <div class="mx-auto w-4/5 py-3 bg-white rounded-5 shadow-3 px-6 text-18">

            <a class="flex w-full items-center py-3" href="news">
                <img src="/img/icon-52.png" class="h-6 mr-3">
                <div>市集資訊</div>
                <img src="/img/icon-56.png" class="ml-auto h-4  ">
            </a>
            <hr>
            <a class="flex w-full items-center py-3" href="map">
                <img src="/img/icon-53.png" class="h-6 mr-3">
                <div>市集地圖</div>
                <img src="/img/icon-56.png" class="ml-auto h-4  ">
            </a>
            <hr>
            <a class="flex w-full items-center py-3" href="schedule">
                <img src="/img/icon-54.png" class="h-6 mr-3">
                <div>市集時程表</div>
                <img src="/img/icon-56.png" class="ml-auto h-4  ">
            </a>
            <hr>
            <a class="flex w-full items-center py-3" href="store">
                <img src="/img/icon-55.png" class="h-6 mr-3">
                <div>市集頭家</div>
                <img src="/img/icon-56.png" class="ml-auto h-4  ">
            </a>

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
