@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class=" container py-5  ">

        <div v-if="indexStep == 2" class="mt-10 text-center">


            <div v-if="word">

                <!-- <div v-if="word.typeID">@{{ typeText('wordType', word.typeID) }}</div>
                <div v-else>@{{ word.name }}</div> -->

                <div class="mt-5 text-theme">@{{ word.content }}</div>

                <div class="mt-3" v-if="word.photo != null && word.photo != '_default.jpg'">
                    <img :src="uploadUrl + word.photo" class="mx-auto w-1/3" />
                </div>

            </div>
            <div v-else>
                找不到結果
            </div>
        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.data = Object.assign(vueItem.data, {
        // isStoreScan: false,
        // updateTimes: 0,
        isScaned: false,
        noRearCamera: false,
        noFrontCamera: false,
        indexStep: 1,
        word: null,
        qq: {},
    });

    vueItem.mounted = function () {

        const self = this;

        // this.checkTransaction();
        liff.init({
            liffId: vueData.liffID
        }).then(() => {


            try {
                if (liff.scanCode) {
                    liff.scanCode().then(result => {
                        // result = { value: '' }

                        self.qq = result;
                        // window.alert(result);
                        self.onDecode(result.value);

                    });
                }

                // liff.scanCode()
            } catch (e) {
                alert('使用Line開啟qrcode掃描失敗');
            }

        }).catch((err) => {
            alert('錯誤: ' + err);
            console.log(err);
        });
    }

    vueItem.methods = Object.assign(vueItem.methods, {
        switchCamera() {
            switch (this.camera) {
                case 'front':
                    this.camera = 'rear'
                    break
                case 'rear':
                    this.camera = 'front'
                    break
            }
        },


        async onInit(promise) {
            try {
                await promise
            } catch (error) {
                const triedFrontCamera = this.camera === 'front'
                const triedRearCamera = this.camera === 'rear'

                const cameraMissingError = error.name === 'OverconstrainedError'

                if (triedRearCamera && cameraMissingError) {
                    this.noRearCamera = true
                }

                if (triedFrontCamera && cameraMissingError) {
                    this.noFrontCamera = true
                }

                console.error(error)
            }
        },

        testing() {
            var md5 = this.testingMd5;
            this.onDecode(md5);
        },


        lineSendMessage(q) {
            console.log(q);
            // alert(q);

            try {
                liff.sendMessages([{
                    type: 'text',
                    // text: "You've successfully sent a message! Hooray!"
                    text: q,
                }]).then(function () {
                    // window.alert('Message sent');
                }).catch(function (error) {
                    // window.alert('Error sending message: ' + error);
                });

            } catch (e) {
                // alert('zzzzzzz');
                // yada
            }

        },

        onDecode(result) {

            if (this.isScaned) {
                return;
            }

            this.isScaned = true;

            console.log(result);
            this.result = result;
            this.qrcode = result;

            try {

                var md5 = result;

                console.log(md5);

                var data = {
                    md5: md5,
                };

                // alert(md5);

                this.md5 = md5;

                this.$http.post('scanDo', data).then(function (r) {
                    const body = r.body;
                    const data = body.data;
                    // this.items = body.data;

                    this.word = data.item;

                    if (this.word != null) {
                        this.indexStep = 2;

                        // say blalalalalalalal
                        if (this.word.typeID) {
                            // alert('#我集到字了: ' + this.typeText('wordType', this.word.typeID));
                            this.lineSendMessage('#我集到字了: ' + this.typeText('wordType', this.word.typeID));
                        } else {
                            // alert('#我集到字了: ' + this.word.name);
                            this.lineSendMessage('#我集到字了: ' + this.word.name);
                        }
                    }
                    this.data = r;

                    // switch (body.statusID) {
                    //     case 0:
                    //         this.indexStep = 2;

                    //         break;

                    // }

                });

            } catch (e) {
                // yada
                alert('error: ' + e);
            }
        },


    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
