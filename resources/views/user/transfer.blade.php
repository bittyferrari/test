@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->


@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="container py-5 text-black">

            <template v-if="indexStep == 1">
                <div class="bg-white p-5 mb-5">


                    <div class="text-red text-19 mb-5">
                        目前剩餘點數【@{{ self.price }}點】
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">點數接受對象手機號碼</div>
                        <input v-model="item.phone" class="form-control bg-pink" placeholder="" type="tel" />
                    </div>

                    <div class="mb-6">
                        <div class="mb-2">轉移點數</div>
                        <input v-model="item.price" class="form-control bg-pink" placeholder="" type="number">
                    </div>
                    <div class="mb-6">
                        <div class="mb-2">驗證碼</div>
                        <input v-model="item.captcha" class="form-control bg-pink" placeholder="" type="text" />
                        <div class="mt-2">
                            <img :src="urlCaptcha" @click="refreshCaptcha()" class="pointer" />
                        </div>
                    </div>
                </div>

                <button class="bg-red text-white w-full p-5 text-18" @click="stepNext()"
                    :disabled="isEmpty(item.phone) || isEmpty(item.captcha) || item.price <= 0 || item.price > self.price || item.phone == self.phone">
                    送出
                </button>
            </template>

            <template v-if="indexStep == 2">

                <div class="bg-white p-5 mb-5 text-center text-blue text-20" v-if="isSuccess">

                    <div>轉移點數給</div>
                    <div>｛@{{ item.phone }}｝</div>
                    <div>【@{{ item.price }}點】</div>
                    <div>成功！</div>

                </div>

                <div class="bg-white p-5 mb-5 text-center text-red" v-else>

                    <div>轉移點數給</div>
                    <div>｛@{{ item.phone }}｝</div>
                    <div>【@{{ item.price }}點】</div>
                    <div>失敗！</div>

                </div>

            </template>

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    vueItem.data = Object.assign(vueItem.data, {
        indexStep: 1,
        // second: 60,
        // verifyCode: '',
        isSuccess: false,
        item: {
            phone: '',
            price: 0,
            captcha: '',
        },
        urlCaptcha: '',
    });

    vueItem.mounted = function () {

        if (this.isDev) {
            // this.item.name = 'test';
            // this.item.phone = '0978555333';
            this.item.phone = '12345';
            this.item.price = 12;
            // this.item.verifyCode = '12345';
        }

        if (this.self.isCompleteRegister != 1) {
            this.alert('請先完成註冊', function () {
                document.location = 'register';
            });
        }

        this.refreshCaptcha();
    }

    vueItem.methods = Object.assign(vueItem.methods, {

        refreshCaptcha() {
            const url = '/_helper/refereshCapchaDo';
            this.$http.post(url, { a: 1 }).then(function (r) {
                this.urlCaptcha = r.body.data;
            });
        },

        stepNext() {

            switch (this.indexStep) {
                case 1:

                    this.$http.post('transferDo', this.item).then(function (r) {
                        const body = r.body;
                        // this.items = body.data;

                        switch (body.statusID) {
                            case 0:
                                this.isSuccess = true;
                                break;
                            default:
                                this.isSuccess = false;
                                break;
                        }
                        this.indexStep++;


                    });

                    break;
                case 2:


                    this.$http.post('registerVerifyDo', { verifyCode: this.verifyCode }).then(function (r) {
                        const body = r.body;
                        // this.items = body.data;

                        if (body.statusID == 0) {
                            this.isSuccess = true;
                            // alert('註冊成功');
                        } else {
                            this.isSuccess = false;

                            // alert('驗證失敗, 請再試一次');
                        }
                        this.indexStep++;

                        // if (this.indexStep == 2) {
                        // this.startCountdown();
                        // }

                    });



                    break;

            }



        },
        resendDo() {
            this.second = 60;

            this.$http.post('registerDo', this.item).then(function (r) {
                const body = r.body;
                // this.items = body.data;
                this.startCountdown();
                // if (this.indexStep == 2) {
                // this.startCountdown();
                // }

            });
        },

        startCountdown() {

            let second = this.second;
            second--;

            if (second < 0) {
                // alert('qrcode過期');
                // document.location = 'index';
            } else {
                this.second = second;
                this.timeoutCountdown = setTimeout(this.startCountdown, 1000);
            }
        },


    });

    var vue = new Vue(vueItem);


</script>
<!-- -------------------- -->

@stop
