<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<meta name="description" content="" />
		<meta name="author" content="" />
		<title>--</title>
		<link href="/css/styles.css" rel="stylesheet" />
		<!-- <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" /> -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>

		<!-- CSRF -->
		<meta name="csrf-token" content="{{ csrf_token() }}" />

		<link href="{{ mix('/app/admin.css') }}" rel="stylesheet" />

		@yield('head')
	</head>
	<body>
		@yield('content')

		<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
		<script src="/js/scripts.js"></script>

		<script src="{{ mix('/app/app.js') }}"></script>

		<!-- ------------ -->
		{{ @printJson('vueData', $vueData)}}
		<!-- ------------ -->
		<script>
			vueItem.data = Object.assign(vueItem.data, vueData);
			vueListing.data = Object.assign(vueListing.data, vueData);
		</script>

		@yield('js')
	</body>
</html>
