<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!-- <title>Site</title> -->
    <title>總太地產x心生活美學聚落</title>

    <link href="/css/styles.css" rel="stylesheet" />
    <!-- <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" /> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script> -->

    <!-- CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link href="{{ mix('/app/app.css') }}" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css2?family=M+PLUS+Rounded+1c:wght@400;500&display=swap%27" rel="stylesheet" crossorigin="anonymous" />

    @yield('head')

</head>

<body class="relative bg">

    <div class="mx-auto border-ccc lg:border-r lg:border-l max-w-md min-h-screen">

        <!-- <div style="height:56px" id="header-spacing"></div> -->

        @yield('content')

        <div style="height:120px" id="footer-spacing"></div>

    </div>
    <!-- <div style="height:84px"></div> -->

    <div class="fixed w-full shadow-2 bg-white" style="bottom:0; left:0" id="menu-bottom">
        <div class="mx-auto max-w-md">
            <div class="w-full flex flex-wrap">

                <a class="w-1/4 text-center pb-1 pt-3" href="scan">
                    <div class="mb-1">
                        <img src="/img/05.png" class="h-10 mx-auto">
                    </div>
                    <div>
                        掃描
                    </div>
                </a>

                <a class="w-1/4 text-center pb-1 pt-3" href="transaction?typeID=exchange">
                    <div class="mb-1">
                        <img src="/img/06.png" class="h-10 mx-auto">
                    </div>
                    <div>
                        交易紀錄
                    </div>
                </a>

                <a class="w-1/4 text-center pb-1 pt-3" href="transaction?typeID=save">
                    <div class="mb-1">
                        <img src="/img/10.png" class="h-10 mx-auto">
                    </div>
                    <div>
                        儲值紀錄
                    </div>
                </a>

                <a class="w-1/4 text-center pb-1 pt-3" href="profile">
                    <div class="mb-1">
                        <img src="/img/11.png" class="h-10 mx-auto">
                    </div>
                    <div>
                        帳戶
                    </div>
                </a>

            </div>

        </div>
    </div>

    <script src="{{ mix('/app/app.js') }}"></script>

    <!-- ------------ -->
    {{ @printJson('vueData', $vueData)}}

    <!-- ------------ -->
    <script>
        vueItem.data = Object.assign(vueItem.data, vueData);
        vueListing.data = Object.assign(vueListing.data, vueData);

			// var vueSideFooter = new Vue({
			// 	el: '#vueSideFooter',
			// 	data: { user: vueData.user },
			// });
    </script>

    @yield('js')
</body>

</html>
