<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!-- <title>Site</title> -->
    <title>總太地產x心生活美學聚落</title>

    <link href="/css/styles.css" rel="stylesheet" />
    <!-- <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" /> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script> -->

    <!-- CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link href="{{ mix('/app/app.css') }}" rel="stylesheet" />

    <!-- line sdk -->
    <script charset="utf-8" src="https://static.line-scdn.net/liff/edge/2/sdk.js"></script>

    <link href="https://fonts.googleapis.com/css2?family=M+PLUS+Rounded+1c:wght@400;500&display=swap%27" rel="stylesheet" crossorigin="anonymous" />

    @yield('head')
</head>

<body class="relative bg">

    <div class="mx-auto border-ccc lg:border-r lg:border-l max-w-md min-h-screen">
        <!-- <div style="height:56px" id="header-spacing"></div> -->

        @yield('content')

        <!-- <div style="height:100px"></div> -->
        <div style="height:130px" id="footer-spacing"></div>
    </div>
    <!-- <div style="height:84px"></div> -->

    <div class="fixed w-full shadow-2" style="bottom:0; left:0" id="menu-bottom">
        <div class="mx-auto max-w-md bg-white">
            <div class="w-full flex flex-wrap pt-4 pb-4">

                <a class="w-1/5 text-center" href="/user/information">
                    <div class=" ">
                        <img src="/img/icon-63.png" class="mx-auto w-full">
                    </div>
                    <!-- <div>
                        集字尋寶
                    </div> -->
                </a>

                <a class="w-1/5 text-center" href="/user/shop">
                    <div class=" ">
                        <img src="/img/13.png" class="mx-auto w-full">
                    </div>
                    <!-- <div>
                        逢甲園遊卷
                    </div> -->
                </a>

                <a class="w-1/5 text-center" href="/user/deal">
                    <div class=" ">
                        <img src="/img/03.png" class="mx-auto w-full">
                    </div>
                    <!-- <div>
                        帳戶
                    </div> -->
                </a>

                <a class="w-1/5 text-center" href="/userSchool/main">
                    <div class=" ">
                        <img src="/img/icon-70.png" class="mx-auto w-full">
                    </div>
                    <!-- <div>
                        逢甲園遊卷
                    </div> -->
                </a>

                <!-- <a class="w-1/4 text-center pb-1 pt-3" href="transaction">
                    <div class="mb-1">
                        <img src="/img/icon-03.png" class="h-10 mx-auto">
                    </div>
                    <div>
                        交易
                    </div>
                </a> -->

                <a class="w-1/5 text-center" href="/user/main">
                    <div class=" ">
                        <img src="/img/04.png" class="mx-auto w-full">
                    </div>
                    <!-- <div>
                        推薦商家
                    </div> -->
                </a>

            </div>

        </div>
    </div>

    <script src="{{ mix('/app/app.js') }}"></script>

    <!-- ------------ -->
    {{ @printJson('vueData', $vueData)}}

    <!-- ------------ -->
    <script>
        vueItem.data = Object.assign(vueItem.data, vueData);
        vueListing.data = Object.assign(vueListing.data, vueData);

			// var vueSideFooter = new Vue({
			// 	el: '#vueSideFooter',
			// 	data: { user: vueData.user },
			// });
    </script>

    @yield('js')
</body>

</html>
