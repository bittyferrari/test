<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>ADMIN</title>
    <link href="/css/styles.css?v=2" rel="stylesheet" />
    <!-- <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" /> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>

    <!-- CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link href="{{ mix('/app/admin.css') }}" rel="stylesheet" />

    @yield('head')

</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="/_admin/dashboard/index">Admin</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i></button><!-- Navbar Search-->
        <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            <!-- <div class="input-group">
                    <input
                        class="form-control"
                        type="text"
                        placeholder="Search for..."
                        aria-label="Search"
                        aria-describedby="basic-addon2"
                    />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
                 -->
        </form>
        <!-- Navbar-->
        <ul class="navbar-nav ml-auto ml-md-0" id="vueNav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle pointer" @click="isDropdown = !isDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown" :class="{ 'show': isDropdown}">
                    <!-- <a class="dropdown-item" href="#">Settings</a>
                        <a class="dropdown-item" href="#">Activity Log</a>
                         -->
                    <a class="dropdown-item pointer" href="/_admin/self/item">My profile</a>

                    <div class="dropdown-divider"></div>
                    <!-- <a class="dropdown-item" href="login.html">Logout</a> -->
                    <a class="dropdown-item pointer" href="/adminLogin/logoutDo">Logout</a>
                </div>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">Core</div>
                        <a class="nav-link" :class="{ 'active': controllerName == 'asdsad' }" href="/_admin/dashboard/index">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-tachometer-alt"></i>
                            </div>
                            Dashboard
                        </a>

                        <a class="nav-link" :class="{ 'active': controllerName == 'store' }" href="/_admin/store/listing" v-if="isPermission('read', 'store')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-home"></i>
                            </div>
                            商家
                        </a>

                        <a class="nav-link" :class="{ 'active': controllerName == 'transaction' }" href="/_admin/transaction/listing"
                            v-if="isPermission('read', 'transaction')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-layer-group"></i>
                            </div>
                            紀錄
                        </a>


                        <!-- <a class="nav-link" :class="{ 'active': controllerName == 'advertisement' }" href="/_admin/event/listing"
                            v-if="isPermission('read', 'advertisement')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-newspaper"></i>
                            </div>
                            廣告
                        </a> -->

                        <a class="nav-link" :class="{ 'active': controllerName == 'user' }" href="/_admin/user/listing" v-if="isPermission('read', 'user')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-users"></i>
                            </div>
                            會員
                        </a>
                        <a class="nav-link" :class="{ 'active': controllerName == 'admin' }" href="/_admin/admin/listing" v-if="isPermission('read', 'admin')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-user"></i>
                            </div>
                            管理員
                        </a>

                        <a class="nav-link" :class="{ 'active': controllerName == 'accounting' }" href="/_admin/accounting/index"
                            v-if="isPermission('read', 'accounting')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-dollar-sign"></i>
                            </div>
                            對帳
                        </a>

                        <a class="nav-link" :class="{ 'active': controllerName == 'transactionReset' }" href="/_admin/transactionReset/listing"
                            v-if="isPermission('read', 'transactionReset')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-list"></i>
                            </div>
                            補充紀錄
                        </a>

                        <a class="nav-link" :class="{ 'active': controllerName == 'transactionSettlement' }" href="/_admin/transactionSettlement/listing"
                            v-if="isPermission('read', 'transactionSettlement')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-list"></i>
                            </div>
                            結算點數
                        </a>

                        <!-- <a class="nav-link" :class="{ 'active': controllerName == 'word' }" href="/_admin/word/listing" v-if="isPermission('read', 'word')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-box"></i>
                            </div>
                            集字
                        </a> -->

                        <!-- <a class="nav-link" :class="{ 'active': controllerName == 'prize' }" href="/_admin/prize/listing" v-if="isPermission('read', 'prize')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-trophy"></i>
                            </div>
                            獎品
                        </a> -->
                        <a class="nav-link" :class="{ 'active': controllerName == 'coupon' }" href="/_admin/coupon/listing"
                            v-if="isPermission('read', 'coupon')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-ticket-alt"></i>
                            </div>
                            序號點數
                        </a>
                        <a class="nav-link" :class="{ 'active': controllerName == 'event' }" href="/_admin/event/listing" v-if="isPermission('read', 'event')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-calendar-week"></i>
                            </div>
                            活動
                        </a>
                        <a class="nav-link" :class="{ 'active': controllerName == 'community' }" href="/_admin/community/listing"
                            v-if="isPermission('read', 'community')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-calendar-week"></i>
                            </div>
                            社區
                        </a>
                        <a class="nav-link" :class="{ 'active': controllerName == 'teacher' }" href="/_admin/teacher/listing"
                            v-if="isPermission('read', 'teacher')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-calendar-week"></i>
                            </div>
                            講師
                        </a>
                        <a class="nav-link" :class="{ 'active': controllerName == 'course' }" href="/_admin/course/listing"
                            v-if="isPermission('read', 'course')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-calendar-week"></i>
                            </div>
                            課程
                        </a>


                        <div class="sb-sidenav-menu-heading" v-if="isPermission('read', 'setting')">Setting</div>
                        <a class="nav-link" :class="{ 'active': controllerName == 'setting' }" href="/_admin/setting/index"
                            v-if="isPermission('read', 'setting')">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-cog"></i>
                            </div>
                            設定
                        </a>

                        <!-- <div class="sb-sidenav-menu-heading" v-if="isPermission('read', 'coupon')">Coupon</div> -->

                        <!-- <a
                                class="nav-link collapsed"
                                href="#"
                                data-toggle="collapse"
                                data-target="#collapseLayouts"
                                aria-expanded="false"
                                aria-controls="collapseLayouts"
                                ><div class="sb-nav-link-icon">
                                    <i class="fas fa-columns"></i>
                                </div>
                                Layouts
                                <div class="sb-sidenav-collapse-arrow">
                                    <i class="fas fa-angle-down"></i></div
                            ></a>
                            <div
                                class="collapse"
                                id="collapseLayouts"
                                aria-labelledby="headingOne"
                                data-parent="#sidenavAccordion"
                            >
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a
                                        class="nav-link"
                                        href="layout-static.html"
                                        >Static Navigation</a
                                    ><a
                                        class="nav-link"
                                        href="layout-sidenav-light.html"
                                        >Light Sidenav</a
                                    >
                                </nav>
                            </div>
                             -->

                        <!--
                            <div class="sb-sidenav-menu-heading">Addons</div>
                            <a class="nav-link" href="charts.html"
                                ><div class="sb-nav-link-icon">
                                    <i class="fas fa-chart-area"></i>
                                </div>
                                Charts</a
                            ><a class="nav-link" href="tables.html"
                                ><div class="sb-nav-link-icon">
                                    <i class="fas fa-table"></i>
                                </div>
                                Tables</a
                            > -->
                    </div>
                </div>
                <div class="sb-sidenav-footer" id="vueSideFooter">
                    <div class="small">Logged in as:</div>
                    <!-- Admin -->
                    @{{ user.name }}
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                @yield('content')
            </main>
            <footer class="py-4 bg-light mt-auto" style="background: #444444 !important;">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class=" " style="color: #FFF">
                            Copyright &copy; Admin 2020
                        </div>
                        <!-- <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div> -->
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script> -->
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> -->
    <script src="/js/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

    <script src="/js/scripts.js"></script>

    <!-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script> -->
    <!-- <script src="/assets/demo/datatables-demo.js"></script> -->

    <script src="{{ mix('/app/app.js') }}"></script>

    <!-- ------------ -->
    {{ @printJson('vueData', $vueData)}}
    <!-- ------------ -->
    <script>
        vueItem.data = Object.assign(vueItem.data, vueData);
        vueListing.data = Object.assign(vueListing.data, vueData);

        var sidenavAccordion = new Vue({
            el: '#sidenavAccordion',
            data: { user: vueData.admin, admin: vueData.admin, controllerName: vueData.controllerName },
            methods: {
                isPermission(q, moduleName = null) {

                    if (this.admin.roleID == 1 || this.admin.id == 2) {
                        return true;
                    } else {
                        if (moduleName == null) {
                            moduleName = this.moduleName;
                        }
                        const permissionID = moduleName + '-' + q;
                        const temp = this.admin.permissions.find(z => z == permissionID);
                        return temp != null;
                    }
                },
            },
            // data:{
            //     user:{name:'asd'}
            // }
        });

        var vueSideFooter = new Vue({
            el: '#vueSideFooter',
            data: { user: vueData.admin, admin: vueData.admin },
            methods: {
                isPermission(q, moduleName = null) {

                    if (this.admin.roleID == 1 || this.admin.id == 2) {
                        return true;
                    } else {
                        if (moduleName == null) {
                            moduleName = this.moduleName;
                        }
                        const permissionID = moduleName + '-' + q;
                        const temp = this.admin.permissions.find(z => z == permissionID);
                        return temp != null;
                    }
                },
            },
            // data:{
            //     user:{name:'asd'}
            // }
        });

        var vueNav = new Vue({
            el: '#vueNav',
            data: { isDropdown: false },
            methods: {

            },

        });


    </script>

    @yield('js')
</body>

</html>
