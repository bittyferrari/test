@extends('__layout/manager')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto pt-10 ">
        <div class="">

            <!--
            .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
            ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
            ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
            ....###.......###.......###.......###.......###.......###.......###.......###.......###.......###...
            ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
            ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
            .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
            -->
            <div v-if="indexStep == 1">

                <div class="container text-white">
                    <qrcode-stream :camera="camera" @decode="onDecode" @init="onInit">

                        <button class="py-3 px-0" @click="switchCamera">
                            <img src="/img/icon-47.png" style="width: 40px">
                        </button>
                    </qrcode-stream>

                    <p class="error text-center" v-if="noFrontCamera">
                        <!-- You don't seem to have a front camera on your device -->
                        此裝置沒有前視鏡頭
                    </p>

                    <p class="error text-center" v-if="noRearCamera">
                        <!-- You don't seem to have a rear camera on your device -->
                        此裝置沒有後視鏡頭
                    </p>

                </div>

            </div>

            <!--
            .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
            ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
            ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
            ....###.......###.......###.......###.......###.......###.......###.......###.......###.......###...
            ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
            ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
            .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
            -->
            <div v-if="indexStep == 2">

                <div class="container">

                    <div class="frame text-center mt-10 mb-10    ">

                        <div class="flex items-center mt-5">
                            <div class="mr-3 w-30">商家名稱</div>
                            <div class="flex-grow text-left">
                                @{{ store.name }}
                            </div>
                        </div>

                        <template v-if="item.typeID == 10">

                            <div class="flex items-center mt-5">
                                <div class="mr-3 w-30">結算金額</div>
                                <div class="flex-grow text-left">
                                    <div class="form-control" placeholder="" type="number">
                                        @{{ item.priceStoreSettlement }}
                                    </div>
                                </div>
                            </div>

                        </template>

                        <!-- reset -->
                        <template v-if="item.typeID == 11">
                            <div class="flex items-center mt-5">
                                <div class="mr-3 w-30">目前餘額</div>
                                <div class="flex-grow text-left">
                                    @{{ (store.priceStoreMax - store.priceStore) | int }}
                                </div>
                            </div>
                            <div class="flex items-center mt-5">
                                <div class="mr-3 w-30">金額</div>
                                <div class="flex-grow text-left">
                                    <!-- <input v-model.number="priceReset" class="form-control" placeholder="" type="number" /> -->
                                    <div class="form-control" placeholder="" type="number">
                                        @{{ item.priceReset }}
                                    </div>
                                </div>
                            </div>

                        </template>

                    </div>

                    <div class="flex flex-wrap mt-10" v-if="!isUserConfirm && !isStoreSet">
                        <div class="w-1/2 pr-5">
                            <button class="btn-white w-full text-normal" @click="cancelDo()">取消</button>
                        </div>
                        <div class="w-1/2 pl-5">
                            <button class="btn-white w-full" @click="confirmDo()">確認</button>
                        </div>
                    </div>

                    <div class="flex flex-wrap mt-10" v-if="isUserConfirm">
                        <div class="w-1/2 pr-5">
                        </div>
                        <div class="w-1/2 pl-5">
                            <button class="btn-white w-full" @click="completeDo()">完成</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.data = Object.assign(vueItem.data, {
        typeID: null,
        userID: '',
        camera: 'rear',
        qrcode: '',
        md5: '',
        result: '',
        noRearCamera: false,
        noFrontCamera: false,
        indexStep: 1,
        indexStep2TypeID: 'save',
        indexStep2: 1,
        isConfirmPrice: false,
        price: 0,
        item: {
            isStoreEnough: 0,
            isSuccess: false,
            price: 0,
            priceCash: 0,
            isUserConfirm: false,
        },
        isEnd: false,
        isStoreUpdate: false,
        store: {
            name: '',
            priceStoreMax: 0,
            priceStore: 0,
        },
        user: {
            name: '',
            photoLine: '',
        },
        isUserConfirm: false,
        priceReset: 0,
        isStoreSet: false,

    });

    vueItem.mounted = function () {
        // this.testing();
    }

    vueItem.methods = Object.assign(vueItem.methods, {

        completeDo() {
            location.reload();
        },

        confirmDo() {

            // update transaction
            const data = {
                md5: this.md5,
                priceReset: this.priceReset,
            };
            this.$http.post('/transaction/managerConfirmDo', data).then(function (r) {
                console.log('storeResetDo');
                const body = r.body;
                const data = body.data;

                // this.alert('確認完成', function () {
                //     location.reload();
                // });

                location.reload();

            });

        },

        cancelDo() {

            const data = {
                md5: this.md5,
                priceReset: this.priceReset,
            };

            this.$http.post('/transaction/managerCancelDo', data).then(function (r) {

                location.reload();

            });



        },

        switchCamera() {
            switch (this.camera) {
                case 'front':
                    this.camera = 'rear'
                    break
                case 'rear':
                    this.camera = 'front'
                    break
            }
        },

        setType(q) {
            this.typeID = q;
            this.indexStep++;
        },

        updateTransaction() {

            console.log('updateTransaction');
            const self = this;

            const data = {
                md5: this.md5
            };

            this.$http.post('/transaction/getItem', data).then(function (r) {
                const body = r.body;
                const data = body.data;

                if (data.item == null) {

                    this.alert('交易取消或不存在', function () {
                        location.reload();
                    });
                    return;

                } else {

                    this.item = data.item;
                    // this.user = data.user;
                    this.store = data.store;

                    if (this.isUserConfirm != true) {
                        if (this.item.isUserConfirm == 1) {
                            this.isUserConfirm = true;
                            this.alert('商家已確認');
                        }
                    }

                    setTimeout(this.updateTransaction, 2000);

                }
            });

        },

        testing() {

            var md5 = this.testingMd5;
            this.onDecode(md5);
        },

        onDecode(result) {
            console.log(result);
            this.result = result;
            this.qrcode = result;

            try {

                var md5 = result;


                var data = {
                    md5: md5,
                };

                this.md5 = md5;

                this.$http.post('/transaction/managerScanDo', data).then(function (r) {
                    const body = r.body;
                    const data = body.data;
                    // this.items = body.data;

                    switch (body.statusID) {
                        case 0:
                            this.item = data.item;
                            this.store = data.store;
                            this.indexStep = 2;
                            this.updateTransaction();
                            break;
                    }

                });

            } catch (e) {
                // yada
            }
        },

        async onInit(promise) {
            try {
                await promise
            } catch (error) {
                const triedFrontCamera = this.camera === 'front'
                const triedRearCamera = this.camera === 'rear'

                const cameraMissingError = error.name === 'OverconstrainedError'

                if (triedRearCamera && cameraMissingError) {
                    this.noRearCamera = true
                }

                if (triedFrontCamera && cameraMissingError) {
                    this.noFrontCamera = true
                }

                console.error(error)
            }
        },

    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
