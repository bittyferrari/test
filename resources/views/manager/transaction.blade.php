@extends('__layout/manager')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto pt-10 text-white">
        <div class="container">

            <div v-if="typeID == 'exchange'">

                <div class="flex justify-center items-center mb-6 mt-10">
                    <!-- <img src="/img/icon-12.png" class="mr-2 w-10"> -->
                    <div>
                        歷史紀錄
                    </div>
                </div>
            </div>

            <div v-else>

                <div class="flex justify-center items-center mb-6 mt-10">
                    <!-- <img src="/img/icon-12.png" class="mr-2 w-10"> -->
                    <div>
                        歷史紀錄
                    </div>
                </div>

            </div>

            <div class="flex items-center justify-center mt-5 mb-6">
                <div class="pointer p-3" @click="monthPrevious()">
                    <img src="/img/icon-24.svg" class="w-2">
                </div>
                <div class="mx-5">@{{ year }}年@{{ month }}月</div>
                <div class="pointer p-3" @click="monthNext()">
                    <img src="/img/icon-23.svg" class="w-2">
                </div>
            </div>

            <div v-for="(x, i) of items" class="mb-3 text-black">

                <template v-if="x.isSuccess == 1">


                    <!-- ['id' => 10, 'name' => '商家結算'], -->
                    <!-- ['id' => 11, 'name' => '補充儲值額度'], -->

                    <template v-if="x.typeID == 10">

                        <div class="bg-white px-4 py-2 rounded-6 shadow-2 mb-3" v-if="x.isSuccess == 1">
                            <div class="mb-2 text-14">@{{ typeText('store', x.storeID) }}</div>
                            <div class="flex justify-between items-center">
                                <div class=" ">
                                    <span>結算點數</span>
                                    <span class="text-red font-bold ml-1">@{{ x.priceStoreSettlement }}</span>
                                    <span class="ml-4">當時累積點數</span>

                                    <span class="text-red font-bold ml-1">@{{ x.priceBeforeSettlement }}</span>
                                </div>
                            </div>
                            <div class="text-right text-12 text-gray2">@{{ x.createdAt }}</div>

                        </div>
                    </template>

                    <template v-if="x.typeID == 11">
                        <div class="bg-white px-4 py-2 rounded-6 shadow-2 mb-3" v-if="x.isSuccess == 1">
                            <div class="mb-2 text-14">@{{ typeText('store', x.storeID) }}</div>
                            <div class="flex justify-between items-center">
                                <div class=" ">

                                    <span>補充額度</span>

                                    <span class="text-red font-bold ml-1">@{{ x.priceReset }}</span>
                                    <span class="ml-4">當時餘額</span>

                                    <span class="text-red font-bold ml-1">@{{ x.priceBeforeReset }}</span>
                                </div>
                            </div>
                            <div class="text-right text-12 text-gray2">@{{ x.createdAt }}</div>

                        </div>
                    </template>

                </template>
                <template>
                    <div class="bg-white px-4 py-2 rounded-6 shadow-2 mb-3">

                        <div class="flex justify-between items-center text-14">
                            <div class="mb-2">@{{ typeText('store', x.storeID) }}</div>
                            <div class="text-red">交易失敗</div>
                        </div>
                        <div class="flex justify-between items-center text-828282">
                            <!-- <div class=" " v-if="x.price > 0">當時餘額 <span class="text-green font-bold ml-1">+@{{ x.priceReset }}</span></div> -->
                            <!-- <div class=" " v-else>當時餘額 <span class="text-red font-bold ml-1">+@{{ x.priceReset }}</span></div> -->
                            <div class="text-12 text-gray2">@{{ x.createdAt }}</div>
                        </div>
                    </div>
                </template>


                <!--
                     <div class="bg-white px-4 py-2 rounded-6 shadow-2 mb-3" v-if="x.isSuccess == 1">
                    <div class="mb-2 text-14">@{{ typeText('store', x.storeID) }}</div>
                    <div class="flex justify-between items-center">
                        <div class=" ">回收金額 <span class="text-red font-bold ml-1">@{{ x.priceReset }}</span>
                            當時餘額 <span class="text-red font-bold ml-1">@{{ x.priceBeforeReset }}</span></div>
                        <div class="text-12 text-gray2">@{{ x.createdAt }}</div>
                    </div>
                </div>

                <div class="bg-f3f3f3 px-4 py-2 rounded-6 shadow-2 " v-if="x.isSuccess != 1">
                    <div class="flex justify-between items-center text-14">
                        <div class="mb-2">@{{ typeText('store', x.storeID) }}</div>
                        <div class="text-red">儲值失敗</div>
                    </div>
                    <div class="flex justify-between items-center text-828282">

                        <div class=" " v-if="x.price > 0">當時餘額 <span class="text-green font-bold ml-1">+@{{ x.priceReset }}</span></div>
                        <div class=" " v-else>當時餘額 <span class="text-red font-bold ml-1">+@{{ x.priceReset }}</span></div>
                        <div class="text-12 text-gray2">@{{ x.createdAt }}</div>
                    </div>
                </div>
                 -->

            </div>

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    // vueListing.data.condition = Object.assign(vueListing.data.condition, {
    // 	id: null,
    // 	name: null,
    // 	lineUserID: null,
    // 	roleID: null,
    // });

    vueListing.data = Object.assign(vueListing.data, {
        isStoreScanned: false,
    });

    vueItem.mounted = function () {
        // this.checkTransaction();
    }

    vueItem.methods = Object.assign(vueItem.methods, {

        monthPrevious() {
            var month = this.month - 1;
            var year = this.year;
            if (month <= 0) {
                month = 12;
                year -= 1;
            }
            document.location = 'transaction?typeID=' + this.typeID + '&year=' + year + '&month=' + month;
        },


        monthNext() {
            var month = this.month + 1;
            var year = this.year;
            if (month >= 13) {
                month = 1;
                year += 1;
            }
            document.location = 'transaction?typeID=' + this.typeID + '&year=' + year + '&month=' + month;
        },

        checkTransaction() {

            const self = this;
            setTimeout(function () {


                let data = {
                    id: this.transaction.id,
                }

                self.$http.post('checkTransactionDo', data).then(function (r) {
                    const body = r.body;
                    // this.items = body.data;

                    const data = body.data;
                    if (data.isFound) {

                        if (data.isExpired) {

                            this.alert('qrcode expired');

                        } else {

                            if (data.item.isStoreScanned) {
                                this.isStoreScanned = true;
                            }

                            this.alert('checkTransaction');
                            self.checkTransaction();
                        }



                    } else {

                        this.alert('qrcode not found');
                    }


                });

            }, 3000);



        },
    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
