@extends('__layout/manager')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="mx-auto w-4/5 pt-10">

            <!-- <a href="register" class="btn-link">
                註冊
            </a>
            <div class="w-full text-white text-center" style="bottom:18px">(再次填寫可修改資料)</div>

            <div class="h-10"></div>
             -->

            <a href="profile" class="btn-link">
                管理員資料
            </a>

            <div class="h-10"></div>

            <a href="password" class="btn-link">
                變更密碼
            </a>

            <div class="h-10"></div>

            <a href="logoutDo" class="btn-link">
                登出
            </a>

            <!-- <a href="profile"><img src="/img/icon-39.png" class="w-full mb-5"></a> -->
            <!-- <a href="transfer"><img src="/img/icon-40.png" class="w-full mb-5"></a> -->
            <!-- <a href="https://reurl.cc/x02Keb"><img src="/img/icon-55.png" class="w-full mb-5"></a> -->

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    // vueListing.data.condition = Object.assign(vueListing.data.condition, {
    // 	id: null,
    // 	name: null,
    // 	lineUserID: null,
    // 	roleID: null,
    // });
    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
