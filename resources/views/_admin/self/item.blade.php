@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
	<div class="container-fluid">
		<h1 class="mt-4">My profile</h1>


		<div class="card mb-3">
			<div class="card-header">
				<div class="row">
					<div class="col-12"><i class="fa fa-book"></i> Information</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-6">
						<div class="form-group">
							<label class="control-label">ID</label>
							<input v-model="item.id" class="form-control" placeholder="" disabled />
						</div>

						<div class="form-group">
							<label class="control-label">Role</label>
							<select class="form-control" v-model.number="item.roleID" disabled>
								<!-- <option :value="null">--</option> -->
								<option v-for="(x, i) of option.userRole" :value="x.id">@{{ x.name }}</option>
							</select>
						</div>

						<div class="form-group">
							<label class="control-label">Name</label>
							<input v-model="item.name" class="form-control" placeholder="" />
						</div>

						<div class="form-group">
							<label class="control-label">Reset Password</label>
							<input v-model="item.password" class="form-control" placeholder="" />
							<div>( fill in if reset )</div>
						</div>

						<div class="form-group">
							<label class="control-label">Email</label>
							<input v-model="item.email" class="form-control" placeholder="" disabled />
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							<label class="control-label">照片</label>
							<div>
								<img :src="getPhoto(item.photo)" @click="changePhoto(item, 'photo')" class="itemImg" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card-footer ">
				<div class="row">
					<div class="col-6 text-right"></div>
					<div class="col-6 text-left">
						<button type="button" class="btn btn-primary" @click="submitDo()" :disabled="isProcessing" v-if="isPermission('save')">
							<i class="fa fa-pen"></i>
							<span v-if="isProcessing">@{{ text.saving }}</span>
							<span v-if="!isProcessing">@{{ text.save }}</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	@stop
	<!-- -------------------- -->
	@section('js')

	<script>
		var vue = new Vue(vueItem);
	</script>
	<!-- -------------------- -->

	@stop
</div>
