@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">Setting</h1>


        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Ratio</label>
                            <input v-model="items.find(z => z.key == 'ratio').value" class="form-control" placeholder="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
                <div class="row">
                    <div class="col-6 text-right">
                        <!-- <a class="btn btn-secondary" href="listing"> <i class="fa fa-arrow-left"></i> Back to listing</a> -->
                    </div>
                    <div class="col-6 text-left">
                        <button type="button" class="btn btn-primary" @click="submitDo()" :disabled="isProcessing" v-if="isPermission('save')">
                            <i class="fa fa-pen"></i>
                            <span v-if="isProcessing">@{{ text.saving }}</span>
                            <span v-if="!isProcessing">@{{ text.save }}</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
    <!-- -------------------- -->
    @section('js')

    <script>
        vueItem.methods = Object.assign(vueItem.methods, {
            submitDo() {
                this.notValidMessage = '';
                if (this.isValid()) {
                    this.isProcessing = true;
                    this.$http
                        .post(this.updateUrl, this._data)
                        .then(function (res) {
                            this.isProcessing = false;
                            // this.isChanged = false;
                            this.responseSource = res;
                            this.alert('Save successed');

                            // if (res.body.statusID != 0) {
                            // 	this.onSaveError();
                            // } else {
                            // 	if (res.body.data) {
                            // 		var data = res.body.data;
                            // 		//get return id only
                            // 		this.item.id = data.item.id;
                            // 		if (data.item.createdAt) {
                            // 			// this.item.createdAt = data.item.createdAt;
                            // 			this.item.createdAt = data.item.createdAt;
                            // 		}
                            // 		alert('Save successed');
                            // 		this.onAfterSave();
                            // 	}
                            // }
                        })
                        .catch(function () {
                            this.alert('Save failed');
                            this.isProcessing = false;
                            this.onSaveError();
                        });
                } else {
                    this.alert(this.notValidMessage);
                }
            },
        });

        var vue = new Vue(vueItem);
    </script>
    <!-- -------------------- -->

    @stop
</div>
