@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">集字</h1>

        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">ID</label>
                            <input v-model="item.id" class="form-control" placeholder="" disabled />
                        </div>
                        <div class="form-group">
                            <label class="control-label star">Name</label>
                            <input v-model="item.name" class="form-control" placeholder="" />
                        </div>
                        <div class="form-group">
                            <label class="control-label star">啟用</label>
                            <select class="form-control" v-model.number="item.isActive">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label  ">內容</label>
                            <textarea v-model="item.content" class="form-control" placeholder=""></textarea>
                        </div>

                        <div class="form-group">
                            <label class="control-label  ">類別 (如果沒有對應到不要選)</label>
                            <select class="form-control" v-model.number="item.typeID">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.wordType" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">廣告</label>
                            <select class="form-control" v-model.number="item.advertisementID">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.advertisement" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <!-- <div class="form-group">
                            <label class="control-label  ">順序</label>
                            <input v-model="item.sequence" class="form-control" placeholder="" />
                        </div> -->

                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Created time</label>
                            <input v-model="item.createdAt" class="form-control" placeholder="" disabled />
                        </div>


                        <div class="form-group" v-if="item.md5">
                            <label class="control-label">Qrcode</label>
                            <div>
                                <qrcode :value="item.md5" :options="{ width: 200 }"></qrcode>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Photo</label>
                            <div>
                                <img :src="getPhoto(item.photo)" @click="changePhoto(item, 'photo')" class="itemImg" />
                            </div>
                            <div class="text-left mt-1">
                                <button class="btn btn-outline-danger btn-sm" @click="item.photo = '_default.jpg'">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="card-footer ">
                @include('/_admin/_include/itemFooter')
            </div>
        </div>
    </div>
    @stop
    <!-- -------------------- -->
    @section('js')

    <script>


        vueItem.methods = Object.assign(vueItem.methods, {
            onSaveError() {
                switch (this.responseSource.body.statusID) {
                    case 1:
                        this.alert('帳號被使用過了');
                        break;
                }
            },

            onAfterSave() {

                this.item.md5 = this.responseSource.body.data.item.md5;

            }

        });


        var vue = new Vue(vueItem);
    </script>
    <!-- -------------------- -->

    @stop
</div>
