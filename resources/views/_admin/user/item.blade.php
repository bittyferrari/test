@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">會員</h1>


        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">ID</label>
                            <input v-model="item.id" class="form-control" placeholder="" disabled />
                        </div>

                        <!-- <div class="form-group">
                            <label class="control-label star">Role</label>
                            <select class="form-control" v-model.number="item.roleID">
                                <option v-for="(x, i) of option.userRole" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                         -->

                        <div class="form-group">
                            <label class="control-label">Name</label>
                            <input v-model="item.name" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label  ">Line UserID</label>
                            <input v-model="item.lineUserID" class="form-control" placeholder="" disabled readonly />
                        </div>
                        <!--
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input v-model="item.password" class="form-control" placeholder="" />
                            <div>( fill in if reset )</div>
                        </div> -->
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input v-model="item.email" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">手機</label>
                            <input v-model="item.phone" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">身分證字號</label>
                            <input v-model="item.nationID" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">備註</label>
                            <textarea v-model="item.memo" class="form-control" placeholder=""></textarea>
                        </div>

                        <div class="form-group">
                            <label class="control-label">社區</label>
                            <select class="form-control" v-model.number="item.communityID">
                              <option :value="null">--</option>
                              <option v-for="(x, i) of option.community" :value="x.id">@{{ x.name }}</option>
                            </select>

                        </div>



                        <!-- <div class="form-group">
                            <label class="control-label">集字: 全</label>
                            <select class="form-control" v-model.number="item.isWord1">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">集字: 國</label>
                            <select class="form-control" v-model.number="item.isWord2">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">集字: 反</label>
                            <select class="form-control" v-model.number="item.isWord3">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">集字: 毒</label>
                            <select class="form-control" v-model.number="item.isWord4">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">集字: 愛在台中</label>
                            <select class="form-control" v-model.number="item.isWord5">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">是否領過獎</label>
                            <select class="form-control" v-model.number="item.isGetPrize">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div> -->

                        <div class="form-group">
                            <label class="control-label">是否完成註冊</label>
                            <select class="form-control" v-model.number="item.isCompleteRegister" readonly disabled>
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">推薦碼</label>
                            <input v-model="item.recommendCode" class="form-control" placeholder="" disabled />
                        </div>


                        <div class="form-group">
                            <label class="control-label">註冊時輸入的推薦碼</label>
                            <input v-model="item.registerRecommendCode" class="form-control" placeholder="" disabled />
                        </div>

                        <div class="form-group">
                            <label class="control-label">推薦人</label>
                            <div class="form-control">
                                <div v-if="recommendUser">@{{ recommendUser.name }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">源推薦人</label>
                            <div class="form-control">
                                <div v-if="parentRecommendUser">@{{ parentRecommendUser.name }}</div>
                            </div>
                        </div>


                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Created time</label>
                            <input v-model="item.createdAt" class="form-control" placeholder="" disabled />
                        </div>

                        <div class="form-group">
                            <label class="control-label">是否已兌換餐點</label>
                            <select class="form-control" v-model.number="item.isExchangeMeal">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">是否已兌換湯品</label>
                            <select class="form-control" v-model.number="item.isExchangeSoup">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">總太點數</label>
                            <input v-model="item.price" class="form-control" placeholder="" disabled />

                        </div>

                        <div class="form-group">
                            <label class="control-label">活動點數</label>
                            <input v-model="item.pricePointCoupon" class="form-control" placeholder="" disabled />
                        </div>

                        <div class="form-group">
                            <label class="control-label">Line photo</label>
                            <div>
                                <img :src="item.photoLine" class="itemImg" />
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label class="control-label">Photo ( 寬高限制 1:1 比例, png/jpg格式, size 不超過1M )</label>

                            <div>
                                <img :src="getPhoto(item.photo)" @click="changePhoto(item, 'photo')" class="itemImg" />
                            </div>
                            <div class="text-left mt-1">
                                <button class="btn btn-outline-danger btn-sm" @click="item.photo = '_default.jpg'">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>

                        </div> -->
                    </div>
                </div>
                <hr />
                <div class="row hidden">
                    <div class="col-12">
                        <table class="table table-listing border table-hover table-striped">
                            <colgroup>
                                <col width="200px" style="width:200px" />
                                <col width="auto" />
                            </colgroup>
                            <thead class="thead-dark">
                                <tr>
                                    <th>Module</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(x, i) of option.userPermission">
                                    <td class="text-right">@{{ x.name }}</td>
                                    <td>
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2" v-for="(xx, ii) of x.action">
                                                <label class="btn btn-sm d-flex align-items-center"
                                                    :class="{ 'btn-primary': isInclude(item.permissions, x.module + '-' + xx), 'btn-outline-primary': !isInclude(item.permissions, x.module + '-' + xx) }">
                                                    <input type="checkbox" class="mr-2" v-model="item.permissions" :value="x.module + '-' + xx" />
                                                    <span>@{{ xx }}</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
                @include('/_admin/_include/itemFooter')
            </div>
        </div>
    </div>
</div>

@stop
<!-- -------------------- -->
@section('js')

<script>
    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
