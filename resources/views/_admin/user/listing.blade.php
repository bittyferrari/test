@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">會員列表</h1>
        <!-- <ol class="breadcrumb mb-4">
			<li class="breadcrumb-item active">Dashboard</li>
        </ol> -->

        <div class="row">
            <div class="col-12">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <a href="item" class="btn btn-primary" v-if="isPermission('save')"><i class="fa fa-plus"></i> @{{ text.create }}</a>
                                <button @click="exportDo()" class="btn btn-info"><i class="fa fa-plus"></i> 匯出</a>
                            </div>
                            <div class="col-6 text-right">@include('/_admin/_include/listingSelectPageSize')</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped tableListing" width="100%" cellspacing="0">
                                <colgroup>
                                    <col style="width:80px;" />
                                    <col style="width:70px;" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:100px;" />
                                    <col style="width:100px;" />
                                    <col style="width:110px;" />
                                </colgroup>

                                <thead @change="isChangeCondition = true" @keyup.enter="getListing()">
                                    <tr class="tr-dark">
                                        <td class="sort" @click="changeOrder('id');" :class="{ sortAsc:isAsc('id'), sortDesc:isDesc('id')}">ID</td>
                                        <td>Photo</td>
                                        <!-- <td class="sort" @click="changeOrder('name');" :class="{sortAsc:isAsc('name'), sortDesc:isDesc('name')}">名稱</td> -->
                                        <td class="sort" @click="changeOrder('nameLine');" :class="{sortAsc:isAsc('nameLine'), sortDesc:isDesc('nameLine')}">
                                            Line名稱</td>
                                        <td class="sort" @click="changeOrder('birthday');" :class="{sortAsc:isAsc('birthday'), sortDesc:isDesc('birthday')}">生日
                                        </td>
                                        <td class="sort" @click="changeOrder('phone');" :class="{sortAsc:isAsc('phone'), sortDesc:isDesc('phone')}">手機</td>
                                        <td class="sort" @click="changeOrder('price');" :class="{sortAsc:isAsc('price'), sortDesc:isDesc('price')}">總太點數</td>
                                        <td class="sort" @click="changeOrder('pricePointCoupon');"
                                            :class="{sortAsc:isAsc('pricePointCoupon'), sortDesc:isDesc('pricePointCoupon')}">活動點數</td>
                                        <td class="sort" @click="changeOrder('recommendCode');"
                                            :class="{sortAsc:isAsc('recommendCode'), sortDesc:isDesc('recommendCode')}">推薦碼</td>
                                        <td class="sort" @click="changeOrder('isCompleteRegister');"
                                            :class="{sortAsc:isAsc('isCompleteRegister'), sortDesc:isDesc('isCompleteRegister')}">是否已註冊</td>

                                        <td class="sort" @click="changeOrder('registerRecommendUserID');"
                                            :class="{sortAsc:isAsc('registerRecommendUserID'), sortDesc:isDesc('registerRecommendUserID')}">推薦人</td>

                                        <td class="sort" @click="changeOrder('registerParentRecommendUserID');"
                                            :class="{sortAsc:isAsc('registerParentRecommendUserID'), sortDesc:isDesc('registerParentRecommendUserID')}">源推薦人
                                        </td>


                                        <!-- <td class="sort" @click="changeOrder('xxxxxxx');" :class="{sortAsc:isAsc('xxxxxxxx'), sortDesc:isDesc('xxxxx')}">推薦人-姓名</td>
										<td class="sort" @click="changeOrder('xxxxxxx');" :class="{sortAsc:isAsc('xxxxxxxx'), sortDesc:isDesc('xxxxx')}">推薦人-推薦碼</td>
										<td class="sort" @click="changeOrder('xxxxxxx');" :class="{sortAsc:isAsc('xxxxxxxx'), sortDesc:isDesc('xxxxx')}">原推薦人-姓名</td>
										<td class="sort" @click="changeOrder('xxxxxxx');" :class="{sortAsc:isAsc('xxxxxxxx'), sortDesc:isDesc('xxxxx')}">原推薦人-推薦碼</td> -->
                                        <td></td>

                                    </tr>
                                    <tr>
                                        <td><input v-model="condition.id" class="form-control" placeholder="" /></td>
                                        <td></td>
                                        <!-- <td><input v-model="condition.name" class="form-control" placeholder="" /></td> -->
                                        <td><input v-model="condition.nameLine" class="form-control" placeholder="" /></td>
                                        <td><input v-model="condition.birthday" class="form-control" placeholder="" type="text" /></td>
                                        <td><input v-model="condition.phone" class="form-control" placeholder="" /></td>
                                        <td><input v-model="condition.pricePoint" class="form-control" placeholder="" /></td>
                                        <td><input v-model="condition.price" class="form-control" placeholder="" /></td>
                                        <td><input v-model="condition.recommendCode" class="form-control" placeholder="" /></td>

                                        <td>
                                            <select class="form-control" v-model.number="condition.isCompleteRegister">
                                                <option :value="null">--</option>
                                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                                            </select>

                                        </td>
                                        <td>
                                            <input v-model="condition.recommendUserName" class="form-control" placeholder="名稱" />
                                            <input v-model="condition.recommendUserRecommendCode" class="form-control" placeholder="推薦碼" />
                                        </td>
                                        <td>
                                            <input v-model="condition.parentRecommendUserName" class="form-control" placeholder="名稱" />
                                            <input v-model="condition.parentRecommendUserRecommendCode" class="form-control" placeholder="推薦碼" />
                                        </td>

                                        <td>
                                            <button class="btn btn-secondary w-100" @click="getListing()">
                                                <i class="fa fa-search"></i> @{{ text.search }}
                                            </button>
                                        </td>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(x,i) of items">
                                        <td>@{{ x.id }}</td>
                                        <!-- <td><img :src="uploadUrl + x.photo" /></td> -->
                                        <td><img :src="x.photoLine" /></td>
                                        <!-- <td>@{{ x.name }}</td> -->
                                        <td>@{{ x.nameLine }}</td>
                                        <td>@{{ x.birthday }}</td>
                                        <td>@{{ x.phone }}</td>
                                        <td>@{{ x.price }}</td>
                                        <td>@{{ x.pricePointCoupon }}</td>
                                        <td>@{{ x.recommendCode }}</td>
                                        <td>@{{ typeText('is', x.isCompleteRegister) }}</td>
                                        <td>
                                            <div class="mb-1">@{{ typeText('user', x.registerRecommendUserID) }}</div>
                                            <div>@{{ typeText('user', x.registerRecommendUserID, '--', 'recommendCode') }}</div>
                                        </td>

                                        <td>
                                            <div class="mb-1">@{{ typeText('user', x.registerParentRecommendUserID) }}</div>
                                            <div>@{{ typeText('user', x.registerParentRecommendUserID, '--', 'recommendCode') }}</div>
                                        </td>

                                        <td>
                                            <a :href="'../transaction/listing?userID=' + x.id" class="btn btn-sm btn-info mb-1 w-100" target="_blank">
                                                紀錄
                                            </a>

                                            <a :href="'item?id=' + x.id" class="btn btn-sm btn-success mb-1 w-100" target="_blank">
                                                <i class="fa fa-pen"></i> @{{ text.update }}
                                            </a>
                                            <button v-if="isPermission('delete')" @click="deleteItem(i)" type="button" class="btn btn-sm btn-danger w-100">
                                                <i class="fa fa-times"></i> @{{ text.delete }}
                                            </button>
                                        </td>
                                    </tr>
                                    <tr v-if="items.length <= 0 && !isFirstTime">
                                        <td colspan="99" class="text-center">@{{ text.notFound }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @include('/_admin/_include/listingFooter')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    vueListing.data.condition = Object.assign(vueListing.data.condition, {
        id: null,
        name: null,
        nameLine: null,
        lineUserID: null,
        roleID: null,
        nameLine: null,
        birthday: null,
        phone: null,
        price: null,
        pricePointCoupon: null,
        isCompleteRegister: null,
        recommendCode: null,
        recommendUserName: null,
        parentRecommendUserName: null,

        recommendUserRecommendCode: null,
        parentRecommendUserRecommendCode: null,


    });

    vueListing.data.option = Object.assign(vueListing.data.option, {
        user: [],
    });

    vueListing.methods = Object.assign(vueListing.methods, {
        afterGetListing() {
            this.option.user = this.dataResponse.user;
        },

        exportDo(q) {
            this.condition.checkedStudy = [];
            this.condition.checkedSeries = [];
            this.condition.isExportAll = false;

            if (q) {
                this.condition.checkedStudy = Object.assign([], this.checkedStudy);
                this.condition.checkedSeries = Object.assign([], this.checkedSeries);
            } else {
                this.condition.isExportAll = true;
            }

            const url = 'exportDo';

            var xhr = new XMLHttpRequest();
            xhr.open('POST', url, true);
            const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);
            xhr.responseType = 'arraybuffer';

            const token = localStorage.getItem('token');

            xhr.setRequestHeader('Authorization', 'Bearer ' + token);

            xhr.onload = function () {
                if (this.status === 200) {
                    var filename = '';
                    var disposition = xhr.getResponseHeader('Content-Disposition');
                    if (disposition && disposition.indexOf('attachment') !== -1) {
                        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                        var matches = filenameRegex.exec(disposition);
                        if (matches != null && matches[1]) {
                            filename = matches[1].replace(/['"]/g, '');
                        }
                    }

                    filename = 'export.xlsx';

                    var type = xhr.getResponseHeader('Content-Type');

                    var blob;
                    if (typeof File === 'function') {
                        try {
                            blob = new File([this.response], filename, {
                                type: type
                            });
                        } catch (e) {
                            /* Edge */
                        }
                    }
                    if (typeof blob === 'undefined') {
                        blob = new Blob([this.response], { type: type });
                    }

                    if (typeof window.navigator.msSaveBlob !== 'undefined') {
                        // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                        window.navigator.msSaveBlob(blob, filename);
                    } else {
                        var URL = window.URL || window.webkitURL;
                        var downloadUrl = URL.createObjectURL(blob);

                        if (filename) {
                            // filename += '.csv';
                            // alert(filename);
                            // use HTML5 a[download] attribute to specify filename
                            var a = document.createElement('a');
                            // safari doesn't support this yet
                            if (typeof a.download === 'undefined') {
                                window.location = downloadUrl;
                            } else {
                                a.href = downloadUrl;
                                a.download = filename;
                                document.body.appendChild(a);
                                a.click();
                            }
                        } else {
                            window.location = downloadUrl;
                        }

                        setTimeout(function () {
                            URL.revokeObjectURL(downloadUrl);
                        }, 100); // cleanup
                    }
                }
            };
            xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
            xhr.send(JSON.stringify(this.condition));
        },

    });

    var vue = new Vue(vueListing);
</script>
<!-- -------------------- -->

@stop
