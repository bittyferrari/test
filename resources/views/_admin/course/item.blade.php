@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">課程資料</h1>

        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">ID</label>
                            <input v-model="item.id" class="form-control" placeholder="" disabled />
                        </div>
                        <div class="form-group">
                            <label class="control-label">名稱</label>
                            <input v-model="item.name" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">課程說明</label>

                            <summernote :model.sync="item.content"></summernote>

                            <!-- <textarea v-model="item.content" class="form-control" placeholder="" style="height: 160px"></textarea> -->
                        </div>
                        <div class="form-group">
                            <label class="control-label">注意事項</label>
                            <summernote :model.sync="item.notice"></summernote>
                            <!-- <textarea v-model="item.notice" class="form-control" placeholder="" style="height: 160px"></textarea> -->
                        </div>

                        <div class="form-group">
                            <label class="control-label">手機</label>
                            <input v-model="item.phone" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">價格</label>
                            <input v-model="item.price" class="form-control" placeholder="" />
                        </div>
                        <!--
                        <div class="form-group">
                            <label class="control-label">日期</label>
                            <input v-model="item.date" class="form-control" placeholder="" type="date" />
                        </div> -->

                        <div class="form-group">
                            <label class="control-label">可報名人數</label>
                            <input v-model="item.countAvailable" class="form-control" placeholder="" type="number" />
                        </div>

                        <div class="form-group">
                            <label class="control-label star">主要講師</label>

                            <select class="form-control" v-model.number="item.teacherID">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.teacher" :value="x.id">@{{ x.name }}</option>
                            </select>

                        </div>


                        <div class="form-group">
                            <label class="control-label star">講師</label>

                            <div>
                                <label class="btn btn-sm  mr-2 mb-2" v-for="(x, i) of option.teacher"
                                    :class="{ 'btn-info': isInclude(item.teacherIDs, x.id), 'btn-outline-info': !isInclude(item.teacherIDs, x.id)}">
                                    <div class="d-flex align-items-center">
                                        <input v-model="item.teacherIDs" :value="x.id" placeholder="" type="checkbox" />
                                        <div class="ml-2">@{{ x.name }}</div>
                                    </div>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label star">社區 (不選=不限)</label>
                            <div>
                                <label class="btn btn-sm  mr-2 mb-2" v-for="(x, i) of option.community"
                                    :class="{ 'btn-info': isInclude(item.communityIDs, x.id), 'btn-outline-info': !isInclude(item.communityIDs, x.id)}">
                                    <div class="d-flex align-items-center">
                                        <input v-model="item.communityIDs" :value="x.id" placeholder="" type="checkbox" />
                                        <div class="ml-2">@{{ x.name }}</div>
                                    </div>
                                </label>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label class="control-label star">社區</label>
                            <select class="form-control" v-model.number="item.communityID">
                                <option :value="null">--不限社區--</option>
                                <option v-for="(x, i) of option.community" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div> -->

                        <div class="form-group">
                            <label class="control-label  ">啟用</label>
                            <select class="form-control" v-model.number="item.isActive">
                                <!-- <option :value="null">--</option> -->
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>


                        <div class="form-group">
                            <label class="control-label  ">顯示在前端</label>
                            <select class="form-control" v-model.number="item.isVisible">
                                <!-- <option :value="null">--</option> -->
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>


                        <div class="form-group">
                            <label class="control-label  ">可候補</label>
                            <select class="form-control" v-model.number="item.isAlternatable">
                                <!-- <option :value="null">--</option> -->
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label  ">是否有線上課程</label>
                            <select class="form-control" v-model.number="item.hasOnline">
                                <!-- <option :value="null">--</option> -->
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label  ">線上課程網址</label>
                            <input v-model="item.url" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label  ">是否限制同社區選課</label>
                            <select class="form-control" v-model.number="item.isRestrictSameCommunity">
                                <!-- <option :value="null">--</option> -->
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Created time</label>
                            <input v-model="item.createdAt" class="form-control" placeholder="" disabled />
                        </div>
                        <div class="form-group">
                            <label class="control-label  ">地點</label>
                            <input v-model="item.location" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label star">報名起</label>
                            <timepicker :time.sync="item.signupTimestampFrom" :value="item.signupTimestampFrom"></timepicker>
                        </div>

                        <div class="form-group">
                            <label class="control-label star">報名迄</label>
                            <timepicker :time.sync="item.signupTimestampTo" :value="item.signupTimestampTo"></timepicker>
                        </div>

                        <div class="form-group">
                            <label class="control-label star">課程開始時間</label>
                            <timepicker :time.sync="item.timestampFrom" :value="item.timestampFrom"></timepicker>
                        </div>

                        <div class="form-group">
                            <label class="control-label star">課程結束時間</label>
                            <timepicker :time.sync="item.timestampTo" :value="item.timestampTo"></timepicker>
                        </div>

                        <div class="form-group">
                            <label class="control-label">照片</label>
                            <div>
                                <img :src="getPhoto(item.photo)" @click="changePhoto(item, 'photo')" class="itemImg" />
                            </div>
                            <div class="text-left mt-1">
                                <button class="btn btn-outline-danger btn-sm" @click="item.photo = '_default.jpg'">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="flex items-center">
                                <label class="control-label">多圖</label>
                                <button class="btn btn-primary" @click="addPhoto()">新增</button>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-6 mb-3" v-for="(x, i) of item.photos">
                                    <a :href="x.file | asset" target="_blank">
                                        <!-- <span v-if="x.ext != 'jpg' && x.ext != 'png'">@{{ x.file }}</span> -->
                                        <img :src="x.file | asset" class="itemImg mb-2 w-100"   />
                                    </a>
                                    <!-- <input v-model="x.name" class="form-control" placeholder="" /> -->
                                    <div class="text-right mb-2">
                                        <button class="btn btn-sm btn-outline-danger" @click="removeItem(item.photos, i)">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <!-- <div class="form-group">
                            <label class="control-label">Photo ( 寬高限制 1:1 比例, png/jpg格式, size 不超過1M )</label>

                            <div>
                                <img :src="getPhoto(item.photo)" @click="changePhoto(item, 'photo')" class="itemImg" />
                            </div>
                            <div class="text-left mt-1">
                                <button class="btn btn-outline-danger btn-sm" @click="item.photo = '_default.jpg'">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>

                        </div> -->
                    </div>
                </div>
                <hr />
            </div>
            <div class="card-footer ">
                @include('/_admin/_include/itemFooter')
            </div>
        </div>
    </div>
    @stop
    <!-- -------------------- -->
    @section('js')

    <script>

        vueItem.methods = Object.assign(vueItem.methods, {

            isValid() {
                let result = true;

                if (this.item.timestampFrom > this.item.timestampTo) {
                    result = false;
                    this.notValidMessages.push('起不能大於訖');
                }
                if (this.item.signupTimestampFrom > this.item.signupTimestampTo) {
                    result = false;
                    this.notValidMessages.push('報名起不能大於報名訖');
                }

                if (this.item.signupTimestampFrom > this.item.timestampFrom || this.item.signupTimestampTo > this.item.timestampFrom) {
                    result = false;
                    this.notValidMessages.push('報名日期要在上課日期之前');
                }

                return result;
            },

            addPhoto() {
                const self = this;
                var callback = function (q) {
                    const a = {
                        file: q.fileName,
                        ext: q.fileName.split('.').pop()
                    };
                    self.item.photos.push(a);
                };
                this.uploadFileCallback(callback);
            },

        });



        var vue = new Vue(vueItem);
    </script>
    <!-- -------------------- -->

    @stop
</div>
