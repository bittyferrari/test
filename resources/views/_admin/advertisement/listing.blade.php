@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">廣告列表</h1>
        <!-- <ol class="breadcrumb mb-4">
			<li class="breadcrumb-item active">Dashboard</li>
        </ol> -->

        <div class="row">
            <div class="col-12">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <a href="item" class="btn btn-primary" v-if="isPermission('save')"><i class="fa fa-plus"></i> @{{ text.create }}</a>
                                <!-- <a href="scan" class="btn btn-info" v-if="isPermission('qrcodeScan')"><i class="fa fa-camera"></i> 掃描QRcode</a> -->
                            </div>
                            <div class="col-6 text-right">@include('/_admin/_include/listingSelectPageSize')</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped tableListing" width="100%" cellspacing="0">
                                <colgroup>
                                    <col style="width:80px;" />
                                    <!-- <col style="width:70px;" /> -->
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:130px;" />
                                </colgroup>

                                <thead @change="isChangeCondition = true" @keyup.enter="getListing()">
                                    <tr class="tr-dark">
                                        <td class="sort" @click="changeOrder('id');" :class="{ sortAsc:isAsc('id'), sortDesc:isDesc('id')}">ID</td>
                                        <td class="sort" @click="changeOrder('storeID');" :class="{sortAsc:isAsc('storeID'), sortDesc:isDesc('storeID')}">商家
                                        </td>
                                        <td class="sort" @click="changeOrder('name');" :class="{sortAsc:isAsc('name'), sortDesc:isDesc('name')}">名稱</td>
                                        <td class="sort" @click="changeOrder('isActive');" :class="{sortAsc:isAsc('isActive'), sortDesc:isDesc('isActive')}">啟用
                                        </td>
                                        <td class="sort" @click="changeOrder('countSend');" :class="{sortAsc:isAsc('countSend'), sortDesc:isDesc('countSend')}">
                                            目前投放次數
                                        </td>

                                        <td class="sort" @click="changeOrder('countSendLimit');"
                                            :class="{sortAsc:isAsc('countSendLimit'), sortDesc:isDesc('countSendLimit')}">
                                            目標投放次數
                                        </td>
                                        <td class="sort" @click="changeOrder('sequence');" :class="{ sortAsc:isAsc('sequence'), sortDesc:isDesc('sequence')}">順序</td>

                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input v-model="condition.id" class="form-control" placeholder="" /></td>
                                        <!-- <td></td> -->
                                        <td>
                                            <select class="form-control" v-model.number="condition.storeID">
                                                <option :value="null">--</option>
                                                <option v-for="(x, i) of option.store" :value="x.id">@{{ x.name }}</option>
                                            </select>
                                        </td>

                                        <td><input v-model="condition.name" class="form-control" placeholder="" /></td>
                                        <!-- <td><input v-model="condition.username" class="form-control" placeholder="" /></td> -->
                                        <td>
                                            <select class="form-control" v-model.number="condition.isActive">
                                                <option :value="null">--</option>
                                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                                            </select>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button class="btn btn-secondary w-100" @click="getListing()">
                                                <i class="fa fa-search"></i> @{{ text.search }}
                                            </button>
                                        </td>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(x,i) of items">
                                        <td>@{{ x.id }}</td>
                                        <!-- <td><img :src="uploadUrl + x.photo" /></td> -->
                                        <td>@{{ typeText('store', x.storeID) }}</td>
                                        <td>@{{ x.name }}</td>
                                        <td>@{{ typeText('is', x.isActive) }}</td>
                                        <td>@{{ x.countSend }}</td>
                                        <td>@{{ x.countSendLimit }}</td>
                                        <td>@{{ x.sequence }}</td>

                                        <!-- <td>
                                            <a :href="'../map/item?id=' + x.id" target="_blank" class="btn btn-sm btn-primary w-100 mb-1">Map</a>
                                            <a :href="'qrcode?id=' + x.id" target="_blank" class="btn btn-sm btn-info w-100">Qrcode</a>
                                        </td>
                                         -->
                                        <td>
                                            <a :href="'item?id=' + x.id" class="btn btn-sm btn-success mb-1 w-100" target="_blank">
                                                <i class="fa fa-pen"></i> @{{ text.update }}
                                            </a>
                                            <button v-if="isPermission('delete')" @click="deleteItem(i)" type="button" class="btn btn-sm btn-danger w-100">
                                                <i class="fa fa-times"></i> @{{ text.delete }}
                                            </button>
                                        </td>
                                    </tr>
                                    <tr v-if="items.length <= 0 && !isFirstTime">
                                        <td colspan="99" class="text-center">@{{ text.notFound }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @include('/_admin/_include/listingFooter')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    vueListing.data.condition = Object.assign(vueListing.data.condition, {
        id: null,
        name: null,
        storeID: null,
        isActive: null,
        _orderField: 'sequence',
        _orderType: 'asc',
    });
    var vue = new Vue(vueListing);
</script>
<!-- -------------------- -->

@stop
