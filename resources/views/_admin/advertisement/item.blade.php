@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">廣告</h1>


        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">ID</label>
                            <input v-model="item.id" class="form-control" placeholder="" disabled />
                        </div>

                        <div class="form-group">
                            <label class="control-label star">商家</label>
                            <multiselect :options="option.store" v-model="store" deselect-label="" select-label=" " track-by="id" :multiple="false" label="name"
                                :close-on-select="true" return=" " :preserve-search="true" placeholder="">
                            </multiselect>

                            <!-- <select class="form-control" v-model.number="item.storeID">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.store" :value="x.id">@{{ x.name }}</option>
                            </select>
                             -->
                        </div>

                        <div class="form-group">
                            <label class="control-label star">名稱</label>
                            <input v-model="item.name" class="form-control" placeholder="" />
                        </div>
                        <div class="form-group">
                            <label class="control-label star">有效時間起</label>
                            <!-- <timepicker :time.sync="item.timeFrom"></timepicker> -->
                            <input v-model="item.timeFrom" class="form-control" placeholder="" type="date" />

                        </div>

                        <div class="form-group">
                            <label class="control-label star">有效時間迄</label>
                            <input v-model="item.timeTo" class="form-control" placeholder="" type="date" />
                            <!-- <timepicker :time.sync="item.timeTo"></timepicker> -->
                        </div>


                        <div class="form-group">
                            <label class="control-label star">啟用</label>
                            <select class="form-control" v-model.number="item.isActive">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label  ">網址 (完整網址包含http://或https://)</label>
                            <input v-model="item.url" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label  ">順序</label>
                            <input v-model="item.sequence" class="form-control" placeholder="" />
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Created time</label>
                            <input v-model="item.createdAt" class="form-control" placeholder="" disabled />
                        </div>
                        <div class="form-group">
                            <label class="control-label  ">目標投放次數</label>
                            <input v-model.number="item.countSendLimit" class="form-control" placeholder="" type="number" />
                        </div>

                        <div class="form-group">
                            <label class="control-label  ">目前投放次數</label>
                            <input v-model="item.countSend" class="form-control" placeholder="" readonly disabled />
                        </div>

                        <div class="form-group">
                            <label class="control-label star">訊息種類</label>
                            <select class="form-control" v-model.number="item.messageTypeID">
                                <!-- <option :value="null">--</option> -->
                                <option v-for="(x, i) of option.messageType" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">照片</label>
                            <div>
                                <img :src="getPhoto(item.photo)" @click="changePhoto(item, 'photo')" class="itemImg" />
                            </div>
                            <div class="text-left mt-1">
                                <button class="btn btn-outline-danger btn-sm" @click="item.photo = '_default.jpg'">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card-footer ">
                @include('/_admin/_include/itemFooter')
            </div>
        </div>
    </div>
</div>
@stop
<!-- -------------------- -->
@section('js')

<script>

    // init multiselect
    vueItem.data.store = vueItem.data.option.store.find(z => z.id == vueItem.data.item.storeID);

    vueItem.watch = {
        store(q) {
            this.item.storeID = null;
            if (q != null) {
                this.item.storeID = q.id;
            }
        }
    }

    vueItem.methods = Object.assign(vueItem.methods, {
        onSaveError() {
            switch (this.responseSource.body.statusID) {
                case 1:
                    this.alert('帳號被使用過了');
                    break;
            }
        }
    });



    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
