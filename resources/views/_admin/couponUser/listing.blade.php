@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">序號點數 User列表</h1>
        <!-- <ol class="breadcrumb mb-4">
			<li class="breadcrumb-item active">Dashboard</li>
        </ol> -->

        <div class="row">
            <div class="col-12">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <a :href="'item?couponID=' + condition.couponID" class="btn btn-primary" v-if="isPermission('save')"><i class="fa fa-plus"></i>
                                    @{{ text.create }}</a>

                                <button class="btn btn-info mr-2" @click="importDo()">匯入</button>

                                <a href="/download/sample.xlsx">sample下載</a>

                            </div>
                            <div class="col-6 text-right">@include('/_admin/_include/listingSelectPageSize')</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped tableListing" width="100%" cellspacing="0">
                                <colgroup>
                                    <col style="width:80px;" />
                                    <!-- <col style="width:70px;" /> -->
                                    <!-- <col style="width:auto" /> -->
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:130px;" />
                                </colgroup>

                                <thead @change="isChangeCondition = true" @keyup.enter="getListing()">
                                    <tr class="tr-dark">
                                        <td class="sort" @click="changeOrder('id');" :class="{ sortAsc:isAsc('id'), sortDesc:isDesc('id')}">ID</td>
                                        <!-- <td>Photo</td> -->
                                        <td class="sort" @click="changeOrder('phone');" :class="{sortAsc:isAsc('phone'), sortDesc:isDesc('phone')}">手機
                                        </td>
                                        <td class="sort" @click="changeOrder('nationID');" :class="{sortAsc:isAsc('nationID'), sortDesc:isDesc('nationID')}">
                                            身分證字號
                                        </td>

                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input v-model="condition.id" class="form-control" placeholder="" /></td>
                                        <td><input v-model="condition.phone" class="form-control" placeholder="" /></td>
                                        <td><input v-model="condition.nationID" class="form-control" placeholder="" /></td>

                                        <!--
                                        <td>
                                            <select class="form-control" v-model.number="condition.isActive">
                                                <option :value="null">--</option>
                                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                                            </select>
                                        </td> -->

                                        <td>
                                            <button class="btn btn-secondary w-100" @click="getListing()">
                                                <i class="fa fa-search"></i> @{{ text.search }}
                                            </button>
                                        </td>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(x,i) of items">
                                        <td>@{{ x.id }}</td>
                                        <td>@{{ x.phone }}</td>
                                        <td>@{{ x.nationID }}</td>
                                        <!-- <td>@{{ typeText('is', x.isActive) }}</td> -->


                                        <td>
                                            <a :href="'item?id=' + x.id" class="btn btn-sm btn-success mb-1 w-100" target="_blank">
                                                <i class="fa fa-pen"></i> @{{ text.update }}
                                            </a>
                                            <button v-if="isPermission('delete')" @click="deleteItem(i)" type="button" class="btn btn-sm btn-danger w-100">
                                                <i class="fa fa-times"></i> @{{ text.delete }}
                                            </button>
                                        </td>
                                    </tr>
                                    <tr v-if="items.length <= 0 && !isFirstTime">
                                        <td colspan="99" class="text-center">@{{ text.notFound }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @include('/_admin/_include/listingFooter')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    vueListing.data.condition = Object.assign(vueListing.data.condition, {
        id: null,
        nationID: null,
        couponID: null,
        phone: null,
    });


    vueListing.methods = Object.assign(vueListing.methods, {
        importDo() {
            let self = this;
            let fileInput = this.$el.querySelector('.file');
            fileInput = document.createElement('input');
            fileInput.setAttribute('type', 'file');
            // fileInput.setAttribute('accept', accept);
            // fileInput.setAttribute('accept', 'image/png, image/gif, image/jpeg, image/bmp, image/x-icon, image/svg+xml');
            fileInput.classList.add('d-none');
            fileInput.addEventListener('change', () => {

                if (fileInput.files != null && fileInput.files[0] != null) {
                    // check file size first
                    console.log(fileInput.files[0]);

                    // var fileSize = fileInput.files[0].size;
                    // if (fileSize / 1024 > maxSize * 1024) {
                    //     self.alert("File size can't above " + maxSize + "MB");
                    //     return;
                    // }

                    const file = fileInput.files[0];

                    self.uploadFile2(file);
                } else {
                }
            });
            this.$el.appendChild(fileInput);
            fileInput.click();
        },

        uploadFile2(file) {
            const self = this;
            //do upload file
            let formData = new FormData();
            // console.log(file.name);
            formData.append('file', file, file.name);
            formData.append('couponID', self.condition.couponID);

            let xhr = new XMLHttpRequest();
            const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            // xhr.open('POST', '/_admin/_helper/uploadFile', true);
            // xhr.open('POST', 'importDo?couponID=' + self.condition.couponID, true);
            xhr.open('POST', 'importDo', true);
            xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);
            xhr.onload = function () {
                if (xhr.status === 200) {
                    // let json = JSON.parse(xhr.response);
                    // variable[key] = json.fileName;
                    self.alert('上傳完成');
                    self.getListing();
                } else {
                    self.alert('上傳失敗');
                }
            };
            xhr.onerror = function () {
                self.alert('Upload failed.');
            };
            xhr.send(formData);
        },

    });


    var vue = new Vue(vueListing);
</script>
<!-- -------------------- -->

@stop
