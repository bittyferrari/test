@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
<style></style>

@stop

<!-- -------------------- -->
@section('content')

<div class="container-fluid" id="vue" v-cloak>
	<h1 class="mt-4">Dashboard</h1>
<!--
	<div class="row">
		<div class="col-xl-6">
			<div class="card mb-4">
				<div class="card-header"><i class="fas fa-chart-area mr-1"></i> QRcode latest 30 days</div>
				<div class="card-body">
					<barchart :chart-data="chartDataQrcode" :chart-labels="chartLabelsQrcode"></barchart>
				</div>
			</div>
		</div>
		<div class="col-xl-6">
			<div class="card mb-4">
				<div class="card-header"><i class="fas fa-chart-area mr-1"></i> Destination latest 30 days</div>
				<div class="card-body">
					<barchart :chart-data="chartDataDestination" :chart-labels="chartLabelsDestination"></barchart>
				</div>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col-xl-6">
			<div class="card mb-4">
				<div class="card-header"><i class="fas fa-chart-area mr-1"></i> Top 10 QRcode latest 30 days</div>
				<div class="card-body">
					<table class="table table-bordered table-striped tableListing mb-0" width="100%" cellspacing="0">
						<thead>
							<tr>
								<td></td>
								<td>Poi</td>
								<td>Times</td>
							</tr>
						</thead>
						<tbody>
							<tr v-for="(x, i) of poiQrcodeTop10">
								<td>@{{ i + 1 }}</td>
								<td>@{{ x.name }}</td>
								<td>@{{ x.countQrcode }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-xl-6">
			<div class="card mb-4">
				<div class="card-header"><i class="fas fa-chart-area mr-1"></i> Top 10 Destination latest 30 days</div>

				<div class="card-body">
					<table class="table table-bordered table-striped tableListing mb-0" width="100%" cellspacing="0">
						<thead>
							<tr>
								<td></td>
								<td>Poi</td>
								<td>Times</td>
							</tr>
						</thead>
						<tbody>
							<tr v-for="(x, i) of poiDestincationTop10">
								<td>@{{ i + 1 }}</td>
								<td>@{{ x.name }}</td>
								<td>@{{ x.countDestination }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-xl-12 text-center d-flex align-items-center justify-content-center">
			<input v-model="condition.dateFrom" class="form-control" placeholder="" type="date" style="width:200px" />
			<div class="mx-1">~</div>
			<input v-model="condition.dateTo" class="form-control" placeholder="" type="date" style="width:200px" />
			<button
				class="btn btn-primary ml-2"
				:disabled="condition.dateFrom == null || condition.dateTo == null || condition.dateFrom == '' || condition.dateTo == '' "
				@click="exportDo()"
			>
				Export
			</button>
		</div>
    </div>
     -->
</div>
@stop
<!-- -------------------- -->

@section('js')

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script> -->
<!-- <script src="/assets/demo/chart-area-demo.js"></script> -->
<!-- <script src="/assets/demo/chart-bar-demo.js"></script> -->

<script>
	vueItem.data = Object.assign(vueItem.data, {
		condition: {
			dateFrom: null,
			dateTo: null,
		},
	});

	vueItem.methods = Object.assign(vueItem.methods, {
		exportDo() {
			var url = 'exportDo?dateFrom=' +this.condition.dateFrom + '&dateTo=' + this.condition.dateTo;
			window.open(url, '_blank');
		},
	});

	var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
