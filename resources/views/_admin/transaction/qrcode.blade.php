@extends('__layout/blank')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
	<div v-for="(x, i) of items">
		<div>@{{ x.name }}</div>
		<qrcode :value="APP_URL + '/poi/index?md5=' + x.md5" :options="{ width: 1000 }"></qrcode>
		<hr />
	</div>
</div>
@stop
<!-- -------------------- -->
@section('js')

<script>
	var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
