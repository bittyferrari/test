@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">紀錄列表</h1>
        <!-- <ol class="breadcrumb mb-4">
			<li class="breadcrumb-item active">Dashboard</li>
        </ol> -->

        <div class="row">
            <div class="col-12">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <!-- <a href="item" class="btn btn-primary" v-if="isPermission('save')"><i class="fa fa-plus"></i> @{{ text.create }}</a> -->
                            </div>
                            <div class="col-6 text-right">@include('/_admin/_include/listingSelectPageSize')</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped tableListing" width="100%" cellspacing="0">
                                <colgroup>
                                    <col style="width:80px;" />
                                    <!-- <col style="width:70px;" /> -->
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <!-- <col style="width:auto" /> -->
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:130px;" />
                                </colgroup>

                                <thead @change="isChangeCondition = true" @keyup.enter="getListing()">
                                    <tr class="tr-dark">
                                        <td class="sort" @click="changeOrder('id');" :class="{ sortAsc:isAsc('id'), sortDesc:isDesc('id')}">ID</td>
                                        <td class="sort" @click="changeOrder('createdAt');"
                                            :class="{ sortAsc:isAsc('createdAt'), sortDesc:isDesc('createdAt')}">時間</td>
                                        <td class="sort" @click="changeOrder('typeID');" :class="{sortAsc:isAsc('typeID'), sortDesc:isDesc('typeID')}">
                                            交易類別
                                        </td>
                                        <!-- <td>Photo</td> -->
                                        <td class="sort" @click="changeOrder('storeID');" :class="{sortAsc:isAsc('storeID'), sortDesc:isDesc('storeID')}">
                                            商家
                                        </td>
                                        <td class="sort" @click="changeOrder('userID');" :class="{sortAsc:isAsc('userID'), sortDesc:isDesc('userID')}">
                                            使用者
                                        </td>


                                        <td class="sort" @click="changeOrder('priceOrigin');"
                                            :class="{sortAsc:isAsc('priceOrigin'), sortDesc:isDesc('priceOrigin')}">儲值面額</td>

                                        <td class="sort" @click="changeOrder('price');" :class="{sortAsc:isAsc('price'), sortDesc:isDesc('price')}">點數</td>
                                        <td class="sort" @click="changeOrder('priceCash');" :class="{sortAsc:isAsc('priceCash'), sortDesc:isDesc('priceCash')}">
                                            補現金</td>


                                        <!-- <td class="sort" @click="changeOrder('isStoreReceive');" :class="{sortAsc:isAsc('isStoreReceive'), sortDesc:isDesc('price')}">isStoreReceive</td> -->
                                        <!-- <td class="sort" @click="changeOrder('isStoreSettle');" :class="{sortAsc:isAsc('isStoreSettle'), sortDesc:isDesc('price')}">isStoreSettle</td> -->
                                        <!-- <td class="sort" @click="changeOrder('couponTypeID');" :class="{sortAsc:isAsc('couponTypeID'), sortDesc:isDesc('couponTypeID')}">couponTypeID</td> -->
                                        <td class="sort" @click="changeOrder('isSuccess');" :class="{sortAsc:isAsc('isSuccess'), sortDesc:isDesc('isSuccess')}">
                                            成功</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input v-model="condition.id" class="form-control" placeholder="" /></td>
                                        <td></td>
                                        <td>
                                            <select class="form-control" v-model.number="condition.typeID">
                                                <option :value="null">--</option>
                                                <option v-for="(x, i) of option.transactionTypeChinese" :value="x.id">@{{ x.name }}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control" v-model.number="condition.storeID">
                                                <option :value="null">--</option>
                                                <option v-for="(x, i) of option.store" :value="x.id">@{{ x.name }}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control" v-model.number="condition.userID">
                                                <option :value="null">--</option>
                                                <option v-for="(x, i) of option.user" :value="x.id">@{{ x.name }}</option>
                                            </select>
                                        </td>


                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>


                                        <td>
                                            <button class="btn btn-secondary w-100" @click="getListing()">
                                                <i class="fa fa-search"></i> @{{ text.search }}
                                            </button>
                                        </td>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(x,i) of items">
                                        <td>@{{ x.id }}</td>
                                        <td>@{{ x.createdAt }}</td>
                                        <td>@{{ typeText('transactionTypeChinese', x.typeID) }}</td>

                                        <!-- <td><img :src="uploadUrl + x.photo" /></td> -->
                                        <td>@{{ typeText('store', x.storeID) }}</td>
                                        <td>@{{ typeText('user', x.userID) }}</td>

                                        <td>
                                            <div v-if="x.typeID == 1">
                                                @{{ x.priceOrigin }}
                                            </div>
                                        </td>
                                        <td>
                                            <!-- ['id' => 1, 'name' => '儲值'],
                                            ['id' => 2, 'name' => '消費'],
                                            ['id' => 3, 'name' => '退點'],
                                            ['id' => 4, 'name' => '獎品'],
                                            ['id' => 5, 'name' => '餐點'],
                                            ['id' => 6, 'name' => '湯品'],
                                            ['id' => 7, 'name' => '門票'],
                                            ['id' => 8, 'name' => '轉移'],
                                            ['id' => 9, 'name' => '序號'], -->
                                            <!--
                                            <div v-if="x.typeID == 1" class="">
                                            </div>
                                            <div v-if="x.typeID == 2" class="">
                                                @{{ x.price * -1 }}
                                            </div>
                                            <div v-if="x.typeID == 3" class="text-danger">
                                                @{{ x.price * -1 }}
                                            </div>
                                            <div v-if="x.typeID == 4 || x.typeID == 7" class="text-danger">
                                                @{{ x.price * -1 }}
                                            </div> -->

                                            <div class="text-danger">
                                                @{{ x.price }}
                                            </div>
                                        </td>
                                        <td>@{{ x.priceCash }}</td>
                                        <td>@{{ typeText('is', x.isSuccess) }}</td>
                                        <!-- <td>
                                            <a :href="'../map/item?id=' + x.id" target="_blank" class="btn btn-sm btn-primary w-100 mb-1">Map</a>
                                            <a :href="'qrcode?id=' + x.id" target="_blank" class="btn btn-sm btn-info w-100">Qrcode</a>
                                        </td>
                                         -->
                                        <td>
                                            <a :href="'item?id=' + x.id" class="btn btn-sm btn-success mb-1 w-100" target="_blank">
                                                <i class="fa fa-pen"></i> @{{ text.update }}
                                            </a>
                                            <!-- <button v-if="isPermission('delete')" @click="deleteItem(i)" type="button" class="btn btn-sm btn-danger w-100">
                                                <i class="fa fa-times"></i> @{{ text.delete }}
                                            </button> -->
                                        </td>
                                    </tr>
                                    <tr v-if="items.length <= 0 && !isFirstTime">
                                        <td colspan="99" class="text-center">@{{ text.notFound }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @include('/_admin/_include/listingFooter')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    vueListing.data.condition = Object.assign(vueListing.data.condition, {
        id: null,
        userID: null,
        storeID: null,
        typeID: null,
    });
    var vue = new Vue(vueListing);
</script>
<!-- -------------------- -->

@stop
