@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">補充紀錄</h1>

        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">ID</label>
                            <input v-model="item.id" class="form-control" placeholder="" disabled />
                        </div>

                        <!--
                        <div class="form-group">
                            <label class="control-label star">Name</label>
                            <input v-model="item.name" class="form-control" placeholder="" />
                        </div> -->
                        <div class="form-group">
                            <label class="control-label star">商家</label>

                            <select class="form-control" v-model.number="item.storeID">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.store" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label star">使用者</label>
                            <select class="form-control" v-model.number="item.userID">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.user" :value="x.id">@{{ x.name }} @{{ x.lineUserID }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label star">金額</label>
                            <input v-model="item.price" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label star">原價</label>
                            <input v-model="item.priceOrigin" class="form-control" placeholder="" />
                        </div>
                        <div class="form-group">
                            <label class="control-label star">現金</label>
                            <input v-model="item.priceCash" class="form-control" placeholder="" />
                        </div>
<!--
                        <div class="form-group">
                            <label class="control-label star">振興券面額</label>
                            <select class="form-control" v-model.number="item.couponTypeID">
                                <option v-for="(x, i) of option.couponType" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div> -->

                        <div class="form-group">
                            <label class="control-label star">商家是否掃描</label>
                            <select class="form-control" v-model.number="item.isStoreScan">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label star">使用者是否確認</label>
                            <select class="form-control" v-model.number="item.isUserConfirm">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>


                        <div class="form-group">
                            <label class="control-label star">是否完成流程</label>
                            <select class="form-control" v-model.number="item.isEnd">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label star">是否取消</label>
                            <select class="form-control" v-model.number="item.isCancel">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>


                        <div class="form-group">
                            <label class="control-label star">商家儲值餘額是否足夠</label>
                            <select class="form-control" v-model.number="item.isStoreEnough">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>


                        <div class="form-group">
                            <label class="control-label star">比例</label>
                            <input v-model="item.ratio" class="form-control" placeholder="" />
                        </div>



                        <div class="form-group">
                            <label class="control-label star">是否成功</label>
                            <select class="form-control" v-model.number="item.isSuccess">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>


                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Created time</label>
                            <input v-model="item.createdAt" class="form-control" placeholder="" disabled />
                        </div>

                        <div class="form-group">
                            <label class="control-label star">商家是否收到</label>
                            <select class="form-control" v-model.number="item.isStoreReceive">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label star">商家是否結算</label>

                            <select class="form-control" v-model.number="item.isStoreSettle">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>

                        </div>

                        <div class="form-group" v-if="item.md5">
                            <label class="control-label">Qrcode</label>
                            <div>
                                <a :href="'printQrcode?id=' + item.id" target="_blank">
                                    <qrcode :value="item.md5" :options="{ width: 200 }"></qrcode>
                                </a>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="card-footer ">
            </div>
        </div>
    </div>
    @stop
    <!-- -------------------- -->
    @section('js')

    <script>

        vueItem.methods.onlyNumber = function ($event) {
            //console.log($event.keyCode); //keyCodes value
            let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
            // if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
            if ((keyCode < 48 || keyCode > 57)) { // 46 is dot
                $event.preventDefault();
            }

        }

        var vue = new Vue(vueItem);
    </script>
    <!-- -------------------- -->

    @stop
</div>
