<div class="row">
	<div class="col-6 text-right">
		<!-- <a class="btn btn-secondary" href="listing"> <i class="fa fa-arrow-left"></i> Back to listing</a> -->
	</div>
	<div class="col-6 text-left">
		<button type="button" class="btn btn-primary" @click="submitDo()" :disabled="isProcessing" v-if="isPermission('save')">
			<i class="fa fa-pen"></i>
			<span v-if="isProcessing">@{{ text.saving }}</span>
			<span v-if="!isProcessing">@{{ text.save }}</span>
		</button>
	</div>
</div>
