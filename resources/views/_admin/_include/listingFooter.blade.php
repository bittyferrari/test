<div class="panel-footer">
    <div class="row">
        <div class="col-sm-12  ">
            <div class="d-flex justify-content-between align-items-center">
                <div>
                    <div class="dataTables_paginate paging_simple_numbers">
                        <ul class="pagination mb-0" style="justify-content: start;">
                            <li class="paginate_button page-item previous  " @click="prevPage()" :class="{disabled: condition._page == 1}">
                                <button class="page-link "><i class="fa fa-caret-left"></i> prev</button>
                            </li>
                            <li v-for="n of pages" @click="changePage(n)" class="paginate_button page-item " :class="{active: (n == condition._page)}">
                                <button class="page-link">@{{ n }}</button>
                            </li>
                            <li class="paginate_button page-item next" @click="nextPage()" :class="{disabled: condition._page == pageTotal}">
                                <button class="page-link ">next <i class="fa fa-caret-right"></i></button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div>Total: @{{ totalItem }}</div>
            </div>
        </div>
    </div>
</div>
