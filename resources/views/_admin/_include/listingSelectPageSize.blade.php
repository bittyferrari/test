<span>Items per page</span>
<select class="form-control selectListingPerPage d-inline-block" v-model.number="condition._pageSize" @change="changePageSize();" style="width:80px">
	<option :value="10">10</option>
	<option :value="50">50</option>
	<option :value="100">100</option>
	<option :value="200">200</option>
	<option :value="500">500</option>
</select>
