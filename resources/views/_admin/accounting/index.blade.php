@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">對帳</h1>


        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label class="control-label">日期起</label>
                            <input v-model="condition.dateFrom" class="form-control" placeholder="" type="date" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">日期訖</label>
                            <input v-model="condition.dateTo" class="form-control" placeholder="" type="date" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">店家</label>

                            <multiselect :options="option.store" v-model="store" deselect-label="" select-label=" " track-by="id" :multiple="false" label="name"
                                :close-on-select="true" return=" " :preserve-search="true" placeholder="--">
                            </multiselect>

                        </div>

                        <button type="button" class="btn btn-info" @click="submitDo()">
                            顯示
                        </button>

                        <button type="button" class="btn btn-primary" @click="exportDo()">
                            匯出
                        </button>

                    </div>

                    <div class="col-12 col-md-9  ">

                        <table class="table table-bordered table-striped tableListing" width="100%" cellspacing="0">
                            <colgroup>
                                <col style="width:80px;" />
                                <col style="width:auto" />
                                <col style="width:auto" />
                                <col style="width:auto" />
                                <col style="width:auto" />
                                <col style="width:auto" />
                                <col style="width:auto" />
                            </colgroup>

                            <thead @change="isChangeCondition = true" @keyup.enter="getListing()">
                                <tr class="tr-dark">
                                    <td>ID</td>
                                    <td>時間</td>
                                    <td>交易類別</td>
                                    <td>商家</td>
                                    <td>交易對象</td>
                                    <td>儲值面額</td>
                                    <td>點數</td>
                                    <td>補現金</td>
                                    <td>成功</td>
                                </tr>
                            </thead>

                            <tbody>
                                <tr v-for="(x,i) of items">
                                    <td>@{{ x.id }}</td>
                                    <td>@{{ x.createdAt }}</td>
                                    <td>@{{ typeText('transactionTypeChinese', x.typeID) }}</td>
                                    <td>@{{ typeText('store', x.storeID) }}</td>
                                    <td>
                                        <!-- @{{ x.typeID }} -->
                                        <div v-if="x.typeID == 10 || x.typeID == 11">
                                            @{{ typeText('admin', x.adminID) }}

                                        </div>
                                        <div v-else>
                                            @{{ typeText('user', x.userID) }}
                                        </div>

                                    </td>
                                    <td>
                                        <div v-if="x.typeID == 1">
                                            @{{ x.priceOrigin * -1 | number}}
                                        </div>
                                        <div v-if="x.typeID == 11">
                                            @{{ x.priceReset | number}}
                                        </div>

                                    </td>
                                    <td>

                                        <div class="text-danger">
                                            <div v-if="x.typeID == 10"> @{{ x.priceStoreSettlement * -1 | number }}</div>
                                            <div v-else-if="x.typeID == 1"></div>
                                            <div v-else>@{{ x.price * -1 | number }}</div>
                                        </div>

                                    </td>
                                    <td>@{{ x.priceCash }}</td>
                                    <td>@{{ typeText('is', x.isSuccess) }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>@{{ getPriceOriginSum() | number }}</td>
                                    <td>@{{ getPriceSum() | number }}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr v-if="items.length <= 0 && !isFirstTime">
                                    <td colspan="99" class="text-center">@{{ text.notFound }}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>


                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-6 text-right">
                    </div>
                    <div class="col-6 text-left">

                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
    <!-- -------------------- -->
    @section('js')

    <script>
        // init multiselect
        vueListing.data.store = null;

        vueListing.watch = {
            store(q) {
                this.condition.storeID = null;
                if (q != null) {
                    this.condition.storeID = q.id;
                }
            }
        }


        vueListing.data = Object.assign(vueListing.data, {
            items: [],
            condition: {
                // storeID: null,
                storeID: 4,
                // dateFrom: '2020-01-01',
                dateFrom: new Date().toISOString().slice(0, 10),
                dateTo: new Date().toISOString().slice(0, 10),
                // dateTo: '2020-12-31',

            },
            isGetListingFirstTime: false,
        });

        vueListing.methods = Object.assign(vueListing.methods, {
            exportDo() {
                const url = 'exportDo?dateFrom=' + this.condition.dateFrom + '&dateTo=' + this.condition.dateTo + '&storeID=' + this.condition.storeID;
                var win = window.open(url, '_blank');
            },

            getPriceOriginSum() {
                var result = 0;
                for (const i in this.items) {
                    const x = this.items[i];
                    if (x.isSuccess) {

                        if (x.typeID == 1) {
                            result += x.priceOrigin * -1;
                        }
                        if (x.typeID == 11) {
                            result += x.priceReset;
                        }

                    }
                }

                return result;
            },

            getPriceSum() {
                var result = 0;
                for (const i in this.items) {
                    const x = this.items[i];

                    if (x.isSuccess) {

                        switch (x.typeID) {
                            case 10:
                                result += (x.priceStoreSettlement * -1);
                                break;
                            case 1:
                                break;
                            default:
                                result += (x.price * -1);
                                break;
                        }

                    }

                }
                return result;
            },

            submitDo() {

                this.$http.post('getData', this.condition)
                    .then(function (res) {
                        const body = res.body;
                        const data = body.data;
                        this.items = data.items;
                    })
                    .catch(function (e) {
                        this.alert('error');
                    });
            },
        });

        var vue = new Vue(vueListing);
    </script>
    <!-- -------------------- -->

    @stop
</div>
