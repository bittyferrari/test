@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">商家列表</h1>
        <!-- <ol class="breadcrumb mb-4">
			<li class="breadcrumb-item active">Dashboard</li>
        </ol> -->

        <div class="row">
            <div class="col-12">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <a href="item" class="btn btn-primary" v-if="isPermission('save')"><i class="fa fa-plus"></i> @{{ text.create }}</a>

                                <button class="btn btn-info" @click="uploadExcel()" :disabled="isUploading">匯入</button>
                                <button class="btn btn-success" @click="exportDo()">匯出</button>
                                <button class="btn btn-outline-danger" @click="deleteMutiDo()">批次刪除</button>

                                <a class="ml-2" href="/download/sample-store.xlsx">excel sample下載</a>


                                <!-- <a href="scan" class="btn btn-info" v-if="isPermission('qrcodeScan')"><i class="fa fa-camera"></i> 掃描QRcode</a> -->
                            </div>
                            <div class="col-6 text-right">@include('/_admin/_include/listingSelectPageSize')</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped tableListing" width="100%" cellspacing="0">
                                <colgroup>
                                    <col style="width:100px;" />
                                    <col style="width:80px;" />
                                    <col style="width:70px;" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:130px;" />
                                </colgroup>

                                <thead @change="isChangeCondition = true" @keyup.enter="getListing()">
                                    <tr class="tr-dark">
                                        <td></td>
                                        <td class="sort" @click="changeOrder('id');" :class="{ sortAsc:isAsc('id'), sortDesc:isDesc('id')}">ID</td>
                                        <td>Photo</td>
                                        <td class="sort" @click="changeOrder('name');" :class="{ sortAsc:isAsc('name'), sortDesc:isDesc('name')}">名稱</td>
                                        <td class="sort" @click="changeOrder('username');" :class="{ sortAsc:isAsc('username'), sortDesc:isDesc('username')}">帳號
                                        </td>
                                        <td>目前累積點數</td>
                                        <!-- <td>目前儲值</td> -->
                                        <!-- <td>最大儲值</td> -->
                                        <td>目前可儲值餘額</td>
                                        <td>可否儲值</td>
                                        <td class="sort" @click="changeOrder('isActive');" :class="{ sortAsc:isAsc('isActive'), sortDesc:isDesc('isActive')}">啟用
                                        </td>
                                        <td class="sort" @click="changeOrder('sequence');" :class="{ sortAsc:isAsc('sequence'), sortDesc:isDesc('sequence')}">順序
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="btn w-100  d-flex align-items-center"
                                                :class="{ 'btn-outline-primary': !isPickAll, 'btn-primary': isPickAll }">
                                                <input v-model="isPickAll" type="checkbox" placeholder="" class="mr-1" @change="onChangePickAll()" />
                                                全選
                                            </label>

                                            <!--
                                            <button class="btn btn-outline-primary w-100" @click="pickAll()">

                                                isPickAll

                                                全選</button> -->
                                        </td>
                                        <td><input v-model="condition.id" class="form-control" placeholder="" /></td>
                                        <td></td>
                                        <td><input v-model="condition.name" class="form-control" placeholder="" /></td>
                                        <td><input v-model="condition.username" class="form-control" placeholder="" /></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button class="btn btn-secondary w-100" @click="getListing()">
                                                <i class="fa fa-search"></i> @{{ text.search }}
                                            </button>
                                        </td>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(x,i) of items">
                                        <td>
                                            <label class="btn w-full d-block"
                                                :class="{ 'btn-outline-primary': !isInclude(pickIDs, x.id), 'btn-primary': isInclude(pickIDs, x.id) }">
                                                <input v-model="pickIDs" type="checkbox" placeholder="" :value="x.id" />

                                            </label>

                                        </td>

                                        <td>
                                            @{{ x.id }}
                                        </td>
                                        <td><img :src="uploadUrl + x.photo" /></td>
                                        <td>@{{ x.name }}</td>
                                        <td>@{{ x.username }}</td>
                                        <td>@{{ x.storeTotal }}</td>
                                        <!-- <td>@{{ x.priceStore }}</td> -->
                                        <!-- <td>@{{ x.priceStoreMax }}</td> -->
                                        <td>@{{ x.priceStoreMax - x.priceStore }}</td>

                                        <td>
                                            <select class="form-control" v-model.number="x.isStoreValue" @change="updateDo(x)">
                                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control" v-model.number="x.isActive" @change="updateDo(x)">
                                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                                            </select>
                                        </td>


                                        <td>@{{ x.sequence }}</td>

                                        <!-- <td>
                                            <a :href="'../map/item?id=' + x.id" target="_blank" class="btn btn-sm btn-primary w-100 mb-1">Map</a>
                                            <a :href="'qrcode?id=' + x.id" target="_blank" class="btn btn-sm btn-info w-100">Qrcode</a>
                                        </td>
                                         -->
                                        <td>
                                            <a :href="'item?id=' + x.id" class="btn btn-sm btn-success mb-1 w-100" target="_blank">
                                                <i class="fa fa-pen"></i> @{{ text.update }}
                                            </a>
                                            <button v-if="isPermission('delete')" @click="deleteItem(i)" type="button" class="btn btn-sm btn-danger w-100">
                                                <i class="fa fa-times"></i> @{{ text.delete }}
                                            </button>
                                        </td>
                                    </tr>
                                    <tr v-if="items.length <= 0 && !isFirstTime">
                                        <td colspan="99" class="text-center">@{{ text.notFound }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @include('/_admin/_include/listingFooter')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    vueListing.data.condition = Object.assign(vueListing.data.condition, {
        id: null,
        name: null,
        _orderField: 'sequence',
        _orderType: 'asc',
    });

    vueListing.data = Object.assign(vueListing.data, {
        isUploading: false,
        pickIDs: [],
        isPickAll: false,
    });


    vueListing.methods = Object.assign(vueListing.methods, {

        onChangePickAll() {

            if (this.isPickAll) {
                this.pickAll();
            } else {
                this.pickIDs = [];
            }

        },
        pickAll() {

            for (const i in this.items) {
                const x = this.items[i];

                if (!this.pickIDs.includes(x.id)) {

                    this.pickIDs.push(x.id);

                }
            }

        },
        async deleteMutiDo() {

            if (confirm('確認批次刪除?')) {

                for (const i in this.pickIDs) {
                    const x = this.pickIDs[i];
                    var url = 'deleteDo';
                    await this.$http.post(url, { id: x }).then(function (r) {
                    });

                }

                this.getListing();
                this.pickIDs = [];
            }
        },

        afterGetListing() {
            this.pickIDs = [];
            this.isPickAll = false;
        },

        exportDo(q) {
            this.condition.checkedStudy = [];
            this.condition.checkedSeries = [];
            this.condition.isExportAll = false;

            if (q) {
                this.condition.checkedStudy = Object.assign([], this.checkedStudy);
                this.condition.checkedSeries = Object.assign([], this.checkedSeries);
            } else {
                this.condition.isExportAll = true;
            }

            const url = 'exportDo';

            var xhr = new XMLHttpRequest();
            xhr.open('POST', url, true);
            const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);
            xhr.responseType = 'arraybuffer';

            const token = localStorage.getItem('token');

            xhr.setRequestHeader('Authorization', 'Bearer ' + token);

            xhr.onload = function () {
                if (this.status === 200) {
                    var filename = '';
                    var disposition = xhr.getResponseHeader('Content-Disposition');
                    if (disposition && disposition.indexOf('attachment') !== -1) {
                        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                        var matches = filenameRegex.exec(disposition);
                        if (matches != null && matches[1]) {
                            filename = matches[1].replace(/['"]/g, '');
                        }
                    }

                    filename = 'export.xlsx';

                    var type = xhr.getResponseHeader('Content-Type');

                    var blob;
                    if (typeof File === 'function') {
                        try {
                            blob = new File([this.response], filename, {
                                type: type
                            });
                        } catch (e) {
                            /* Edge */
                        }
                    }
                    if (typeof blob === 'undefined') {
                        blob = new Blob([this.response], { type: type });
                    }

                    if (typeof window.navigator.msSaveBlob !== 'undefined') {
                        // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                        window.navigator.msSaveBlob(blob, filename);
                    } else {
                        var URL = window.URL || window.webkitURL;
                        var downloadUrl = URL.createObjectURL(blob);

                        if (filename) {
                            // filename += '.csv';
                            // alert(filename);
                            // use HTML5 a[download] attribute to specify filename
                            var a = document.createElement('a');
                            // safari doesn't support this yet
                            if (typeof a.download === 'undefined') {
                                window.location = downloadUrl;
                            } else {
                                a.href = downloadUrl;
                                a.download = filename;
                                document.body.appendChild(a);
                                a.click();
                            }
                        } else {
                            window.location = downloadUrl;
                        }

                        setTimeout(function () {
                            URL.revokeObjectURL(downloadUrl);
                        }, 100); // cleanup
                    }
                }
            };
            xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
            xhr.send(JSON.stringify(this.condition));
        },
        uploadExcel() {

            /////////////////////////////////////////////////
            let self = this;
            let fileInput = this.$el.querySelector('.file');
            fileInput = document.createElement('input');
            fileInput.setAttribute('type', 'file');
            // fileInput.setAttribute('accept', accept);
            // fileInput.setAttribute('accept', 'image/png, image/gif, image/jpeg, image/bmp, image/x-icon, image/svg+xml');
            fileInput.classList.add('d-none');
            fileInput.addEventListener('change', () => {
                if (fileInput.files != null && fileInput.files[0] != null) {
                    // check file size first
                    // var fileSize = fileInput.files[0].size;
                    // if (fileSize / 1024 > maxSize * 1024) {
                    //     self.alert("File size can't above " + maxSize + 'MB');
                    //     return;
                    // }

                    const file = fileInput.files[0];

                    var callback = function (q) {

                        console.log(q);
                        alert('uploaded');

                        self.isUploading = false;
                        self.getListing();

                    }
                    self.isUploading = true;

                    self.uploadFileDo(file, callback);

                    // if (accept == 'image/svg+xml') {
                    //     // ignore width height check
                    //     self.uploadFileDo(variable, key, file);
                    // } else {
                    //     const _URL = window.URL || window.webkitURL;
                    //     const img = new Image();
                    //     img.onload = function () {
                    //         self.uploadFile(variable, key, file);
                    //     };
                    //     img.src = _URL.createObjectURL(file);
                    // }

                } else {
                }
            });
            this.$el.appendChild(fileInput);
            fileInput.click();
        },

        uploadFileDo(file, callback = null) {
            const self = this;
            //do upload file
            let formData = new FormData();
            formData.append('file', file, file.name);
            let xhr = new XMLHttpRequest();
            const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            xhr.open('POST', 'uploadExcelDo', true);
            xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);

            xhr.onload = function () {
                if (xhr.status === 200) {
                    let json = JSON.parse(xhr.response);

                    if (callback != null) {
                        callback(json);
                    }
                    //variable[key] = json.fileName;
                    // variable[key] = json[0];
                    // self.alert('Upload complete.');
                } else {
                    self.alert('Upload failed.');
                }
            };
            xhr.onerror = function () {
                self.alert('Upload failed.');
            };
            xhr.send(formData);
        },


        updateDo(q) {
            this.$http.post('updateDo', { item: q }).then(function (r) {
                // const body = r.body;
                // this.items = body.data;
            });
        }
    });



    var vue = new Vue(vueListing);
</script>
<!-- -------------------- -->

@stop
