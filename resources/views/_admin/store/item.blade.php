@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">商家</h1>

        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">ID</label>
                            <input v-model="item.id" class="form-control" placeholder="" disabled />
                        </div>
                        <div class="form-group">
                            <label class="control-label star">Name</label>
                            <input v-model="item.name" class="form-control" placeholder="" />
                        </div>
                        <div class="form-group">
                            <label class="control-label star">啟用</label>
                            <select class="form-control" v-model.number="item.isActive">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label  ">手機</label>
                            <input v-model="item.phone" class="form-control" placeholder="" />
                        </div>
                        <!-- <div class="form-group">
                            <label class="control-label  ">電話</label>
                            <input v-model="item.tel" class="form-control" placeholder="" />
                        </div> -->

                        <div class="form-group">
                            <label class="control-label  ">地址</label>
                            <input v-model="item.addressText" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label  ">網址名稱</label>
                            <input v-model="item.nameUrl" class="form-control" placeholder="" />
                        </div>
                        <div class="form-group">
                            <label class="control-label  ">網址</label>
                            <input v-model="item.url" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label star">類別</label>
                            <select class="form-control" v-model.number="item.typeID">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.storeType" :value="x.id">@{{ x.name }}</option>
                            </select>

                        </div>

                        <div class="form-group">
                            <label class="control-label star">可以儲值</label>
                            <select class="form-control" v-model.number="item.isStoreValue">
                                <!-- <option :value="null">--</option> -->
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label star">可以兌換</label>
                            <select class="form-control" v-model.number="item.isExchangable">
                                <!-- <option :value="null">--</option> -->
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label star">可以退款</label>
                            <select class="form-control" v-model.number="item.isRefundable">
                                <!-- <option :value="null">--</option> -->
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label  ">負責人</label>
                            <input v-model="item.personInCharge" class="form-control" placeholder="" />
                        </div>
                        <!-- <div class="form-group">
                            <label class="control-label  ">活動</label>
                            <div class="d-flex">
                                <label v-for="(x, i) of option.event" class="btn btn-info mr-2">
                                    <input v-model="item.eventIDs" :value="x.id" type="checkbox" />
                                    @{{ x.name }}
                                </label>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="control-label  ">順序</label>
                            <input v-model="item.sequence" class="form-control" placeholder="" />
                        </div>


                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Created time</label>
                            <input v-model="item.createdAt" class="form-control" placeholder="" disabled />
                        </div>
                        <div class="form-group">
                            <label class="control-label">帳號</label>
                            <input v-model="item.username" class="form-control" placeholder="" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">密碼(重設密碼才填)</label>
                            <input v-model="item.password" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">目前儲值</label>
                            <input v-model="item.priceStore" class="form-control" placeholder="" disabled />
                        </div>

                        <div class="form-group">
                            <label class="control-label">最多儲值</label>
                            <input v-model="item.priceStoreMax" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">照片</label>
                            <div>
                                <img :src="getPhoto(item.photo)" @click="changePhoto(item, 'photo')" class="itemImg" />
                            </div>
                            <div class="text-left mt-1">
                                <button class="btn btn-outline-danger btn-sm" @click="item.photo = '_default.jpg'">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">MD5</label>
                            <div>
                                <input v-model="item.md5" class="form-control" placeholder="" disabled readonly />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Qrcode</label>
                            <div>
                                <qrcode :value="item.md5" :options="{ width: 200 }"></qrcode>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card-footer ">
                @include('/_admin/_include/itemFooter')
            </div>
        </div>
    </div>
    @stop
    <!-- -------------------- -->
    @section('js')

    <script>



        vueItem.methods = Object.assign(vueItem.methods, {
            onSaveError() {
                switch (this.responseSource.body.statusID) {
                    case 1:
                        this.alert('帳號被使用過了');
                        break;
                }
            }
        });


        var vue = new Vue(vueItem);
    </script>
    <!-- -------------------- -->

    @stop
</div>
