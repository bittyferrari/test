@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">Coupon批次產生</h1>

        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">

                        <div class="form-group">
                            <label for="code">名稱</label>
                            <input class="form-control" v-model="item.name" />
                        </div>

                        <div class="form-group">
                            <label for="code">產生多少個</label>
                            <input class="form-control" v-model="item.count" type="number" />
                        </div>

                        <div class="form-group">
                            <label for="code">prefix</label>
                            <input class="form-control" v-model="item.prefix" />
                        </div>

                        <div class="form-group">
                            <label for="code">亂數使用字</label>
                            <input class="form-control" v-model="item.randomTexts" />
                        </div>

                        <div class="form-group">
                            <label for="code">亂數長度 (產生規則: prefix + 序號 + 亂數使用字配亂數長度)</label>
                            <input class="form-control" v-model="item.randomTextLength" />
                        </div>


                        <div class="form-group">
                            <label class="control-label">點數類型</label>
                            <select class="form-control" v-model.number="item.priceTypeID">
                                <!-- <option :value="null">--</option> -->
                                <option v-for="(x, i) of option.priceType" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group" v-if="item.priceTypeID == 1">
                            <label class="control-label">總太金額</label>
                            <input v-model.number="item.price" class="form-control" placeholder="" type="number" />
                        </div>
                        <div class="form-group" v-if="item.priceTypeID == 2">
                            <label class="control-label">活動金額</label>
                            <input v-model.number="item.pricePoint" class="form-control" placeholder="" type="number" />
                        </div>

                        <!--
                        <div class="form-group">
                            <label class="control-label">金額</label>
                            <input v-model.number="item.price" class="form-control" placeholder="" type="number" />
                        </div> -->


                        <div class="form-group">
                            <label class="control-label">啟用</label>
                            <select class="form-control" v-model.number="item.isActive">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">活動</label>
                            <select class="form-control" v-model.number="item.eventID">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.event" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>



                    </div>
                    <div class="col-6">


                        <div class="form-group">
                            <label class="control-label">起</label>
                            <input v-model="item.timeFrom" class="form-control" placeholder="" type="date" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">訖</label>
                            <input v-model="item.timeTo" class="form-control" placeholder="" type="date" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">啟用限制名單 (yes 才會去讀名單，no 就不特別限制使用對象)</label>
                            <select class="form-control" v-model.number="item.isUseListing">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>


                        <div class="form-group">
                            <label class="control-label">限量(0=不限)</label>
                            <input v-model.number="item.countLimit" class="form-control" placeholder="" type="number" />
                        </div>


                    </div>
                </div>
                <hr />
            </div>
            <div class="card-footer ">

                <div class="text-center">
                    <button class="btn btn-primary" @click="couponDo()" :disabled="isProcessing">批次產生</button>
                </div>

            </div>
        </div>
    </div>
    @stop
    <!-- -------------------- -->
    @section('js')

    <script>

        vueItem.data.item = Object.assign(vueItem.data.item, {
            name: '我是批量產生名稱',
            count: 10,
            prefix: 'xx-',
            randomTexts: '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            randomTextLength: 4,
            price: 0,
            pricePoint: 0,
            priceTypeID: 1,
            isActive: 1,
            eventID: null,
            // timeFrom: '',
            // timeTo: '',
            isUseListing: 0,
            countLimit: 1,
            countLimit: 1,

        })


        vueItem.methods = Object.assign(vueItem.methods, {

            couponDo() {
                this.isProcessing = true;
                // alert('asd');
                const url = 'batchCreateDo';
                this.$http.post(url, this.item).then(function (r) {
                    const body = r.body;
                    this.isProcessing = false;

                    window.alert('產生成功, 成功數量:' + body.countSuccess);
                    // this.items = body.data;

                    // location.reload();
                });
            },

        });


        var vue = new Vue(vueItem);
    </script>
    <!-- -------------------- -->

    @stop
</div>
