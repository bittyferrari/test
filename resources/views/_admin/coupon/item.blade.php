@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">序號點數資料</h1>

        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">ID</label>
                            <input v-model="item.id" class="form-control" placeholder="" disabled />
                        </div>

                        <!-- <div class="form-group">
                            <label class="control-label star">Role</label>
                            <select class="form-control" v-model.number="item.roleID">
                                <option v-for="(x, i) of option.userRole" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                         -->

                        <div class="form-group">
                            <label class="control-label">名稱</label>
                            <input v-model="item.name" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">代碼</label>
                            <input v-model="item.code" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">點數類型</label>
                            <select class="form-control" v-model.number="item.priceTypeID">
                                <!-- <option :value="null">--</option> -->
                                <option v-for="(x, i) of option.priceType" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>


                        <div class="form-group" v-if="item.priceTypeID == 1">
                            <label class="control-label">總太金額</label>
                            <input v-model.number="item.price" class="form-control" placeholder="" type="number" />
                        </div>
                        <div class="form-group" v-if="item.priceTypeID == 2">
                            <label class="control-label">活動金額</label>
                            <input v-model.number="item.pricePoint" class="form-control" placeholder="" type="number" />
                        </div>

                        <!--
                        <div class="form-group">
                            <label class="control-label">領取人</label>

                            <select class="form-control" v-model.number="item.userID">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.user" :value="x.id">@{{ x.name }}</option>
                            </select>

                        </div> -->

                        <div class="form-group">
                            <label class="control-label">啟用</label>
                            <select class="form-control" v-model.number="item.isActive">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>



                        <div class="form-group">
                            <label class="control-label">活動</label>
                            <select class="form-control" v-model.number="item.eventID">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.event" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Created time</label>
                            <input v-model="item.createdAt" class="form-control" placeholder="" disabled />
                        </div>

                        <div class="form-group">
                            <label class="control-label">起</label>
                            <input v-model="item.timeFrom" class="form-control" placeholder="" type="date" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">訖</label>
                            <input v-model="item.timeTo" class="form-control" placeholder="" type="date" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">啟用限制名單 (yes 才會去讀名單，no 就不特別限制使用對象)</label>
                            <select class="form-control" v-model.number="item.isUseListing">
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">限量(0=不限)</label>
                            <input v-model.number="item.countLimit" class="form-control" placeholder="" type="number" />
                        </div>


                        <!-- <div class="form-group">
                            <label class="control-label">領取時間</label>
                            <input v-model="item.timeTaken" class="form-control" placeholder="" disabled />
                        </div> -->

                        <!-- <div class="form-group">
                            <label class="control-label">Photo ( 寬高限制 1:1 比例, png/jpg格式, size 不超過1M )</label>

                            <div>
                                <img :src="getPhoto(item.photo)" @click="changePhoto(item, 'photo')" class="itemImg" />
                            </div>
                            <div class="text-left mt-1">
                                <button class="btn btn-outline-danger btn-sm" @click="item.photo = '_default.jpg'">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>

                        </div> -->
                    </div>
                </div>
                <hr />
            </div>
            <div class="card-footer ">
                @include('/_admin/_include/itemFooter')
            </div>
        </div>
    </div>
    @stop
    <!-- -------------------- -->
    @section('js')

    <script>
        var vue = new Vue(vueItem);
    </script>
    <!-- -------------------- -->

    @stop
</div>
