@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">序號點數列表</h1>
        <!-- <ol class="breadcrumb mb-4">
			<li class="breadcrumb-item active">Dashboard</li>
        </ol> -->

        <div class="row">
            <div class="col-12">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <a href="item" class="btn btn-primary" v-if="isPermission('save')"><i class="fa fa-plus"></i> @{{ text.create }}</a>
                                <a href="batchCreate" class="btn btn-info" v-if="isPermission('save')"><i class="fa fa-plus"></i> 批次產生</a>
                                <a href="exportDo" target="_blank" class="btn btn-success"><i class="fa fa-plus"></i> 匯出</a>
                            </div>
                            <div class="col-6 text-right">@include('/_admin/_include/listingSelectPageSize')</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped tableListing" width="100%" cellspacing="0">
                                <colgroup>
                                    <col style="width:80px;" />
                                    <!-- <col style="width:70px;" /> -->
                                    <!-- <col style="width:auto" /> -->
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:auto" />
                                    <col style="width:100px;" />
                                    <col style="width:100px;" />
                                    <col style="width:130px;" />
                                </colgroup>

                                <thead @change="isChangeCondition = true" @keyup.enter="getListing()">
                                    <tr class="tr-dark">
                                        <td class="sort" @click="changeOrder('id');" :class="{ sortAsc:isAsc('id'), sortDesc:isDesc('id')}">ID</td>
                                        <!-- <td>Photo</td> -->
                                        <td class="sort" @click="changeOrder('name');" :class="{sortAsc:isAsc('name'), sortDesc:isDesc('name')}">名稱
                                        </td>
                                        <td class="sort" @click="changeOrder('code');" :class="{sortAsc:isAsc('code'), sortDesc:isDesc('code')}">代碼</td>
                                        <td class="sort" @click="changeOrder('priceTypeID');"
                                            :class="{sortAsc:isAsc('priceTypeID'), sortDesc:isDesc('priceTypeID')}">點數類型</td>
                                        <td class="sort" @click="changeOrder('price');" :class="{sortAsc:isAsc('price'), sortDesc:isDesc('price')}">總太點數</td>
                                        <td class="sort" @click="changeOrder('pricePoint');"
                                            :class="{sortAsc:isAsc('pricePoint'), sortDesc:isDesc('pricePoint')}">活動點數</td>
                                        <td class="sort" @click="changeOrder('isActive');" :class="{sortAsc:isAsc('isActive'), sortDesc:isDesc('isActive')}">
                                            啟用</td>
                                        <td class="sort" @click="changeOrder('countUsed');" :class="{sortAsc:isAsc('countUsed'), sortDesc:isDesc('countUsed')}">
                                            已兌換次數</td>
                                        <td class="sort" @click="changeOrder('countLimit');"
                                            :class="{sortAsc:isAsc('countLimit'), sortDesc:isDesc('countLimit')}">
                                            限量</td>

                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input v-model="condition.id" class="form-control" placeholder="" /></td>
                                        <td><input v-model="condition.name" class="form-control" placeholder="" /></td>
                                        <td><input v-model="condition.code" class="form-control" placeholder="" /></td>
                                        <td>
                                            <select class="form-control" v-model.number="condition.priceTypeID">
                                                <option :value="null">--</option>
                                                <option v-for="(x, i) of option.priceType" :value="x.id">@{{ x.name }}</option>
                                            </select>
                                        </td>

                                        <td><input v-model="condition.price" class="form-control" placeholder="" /></td>
                                        <td><input v-model="condition.pricePoint" class="form-control" placeholder="" /></td>

                                        <td>
                                            <select class="form-control" v-model.number="condition.isActive">
                                                <option :value="null">--</option>
                                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                                            </select>
                                        </td>

                                        <td><input v-model="condition.countLimit" class="form-control" placeholder="" /></td>

                                        <td></td>
                                        <td></td>
                                        <td></td>

                                        <td>
                                            <button class="btn btn-secondary w-100" @click="getListing()">
                                                <i class="fa fa-search"></i> @{{ text.search }}
                                            </button>
                                        </td>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(x,i) of items">
                                        <td>@{{ x.id }}</td>
                                        <td>@{{ x.name }}</td>
                                        <td>@{{ x.code }}</td>
                                        <td>@{{ typeText('priceType', x.priceTypeID) }}</td>

                                        <!-- <td>
                                            <div v-if="x.priceTypeID == 1">@{{ x.price }}</div>
                                            <div v-if="x.priceTypeID == 2">@{{ x.pricePoint }}</div>
                                        </td> -->
                                        <td>@{{ x.price }}</td>
                                        <td>@{{ x.pricePoint }}</td>

                                        <td>@{{ typeText('is', x.isActive) }}</td>
                                        <td>@{{ x.countUsed }}</td>
                                        <td>@{{ x.countLimit }}</td>
                                        <td>
                                            <a :href="'../couponUser/listing?couponID=' + x.id" class="btn btn-sm btn-info mb-1 w-100" target="_blank">
                                                名單
                                            </a>
                                        </td>
                                        <td>
                                            <a :href="'../couponLog/listing?couponID=' + x.id" class="btn btn-sm btn-info mb-1 w-100" target="_blank">
                                                使用紀錄
                                            </a>
                                        </td>

                                        <td>
                                            <a :href="'item?id=' + x.id" class="btn btn-sm btn-success mb-1 w-100" target="_blank">
                                                <i class="fa fa-pen"></i> @{{ text.update }}
                                            </a>
                                            <button v-if="isPermission('delete')" @click="deleteItem(i)" type="button" class="btn btn-sm btn-danger w-100">
                                                <i class="fa fa-times"></i> @{{ text.delete }}
                                            </button>
                                        </td>
                                    </tr>
                                    <tr v-if="items.length <= 0 && !isFirstTime">
                                        <td colspan="99" class="text-center">@{{ text.notFound }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @include('/_admin/_include/listingFooter')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    vueListing.data.condition = Object.assign(vueListing.data.condition, {
        id: null,
        name: null,
        isActive: null,
        userID: null,
        price: null,
        lineUserID: null,
        roleID: null,
        priceTypeID: null,
        pricePoint: null,
        countLimit: null,
    });
    var vue = new Vue(vueListing);
</script>
<!-- -------------------- -->

@stop
