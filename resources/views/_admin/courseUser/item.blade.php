@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">課程名單資料</h1>

        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">ID</label>
                            <input v-model="item.id" class="form-control" placeholder="" disabled />
                        </div>

                        <!-- <div class="form-group">
                            <label class="control-label star">Role</label>
                            <select class="form-control" v-model.number="item.roleID">
                                <option v-for="(x, i) of option.userRole" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                         -->

                        <div class="form-group">
                            <label class="control-label">名稱</label>
                            <input v-model="item.name" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">電話</label>
                            <input v-model="item.phone" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">價格</label>
                            <input v-model="item.price" class="form-control" placeholder="" readonly disabled />
                        </div>


                        <!--
                        <div class="form-group">
                            <label class="control-label">領取人</label>

                            <select class="form-control" v-model.number="item.userID">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.user" :value="x.id">@{{ x.name }}</option>
                            </select>

                        </div> -->


                        <div class="form-group">
                            <label class="control-label">社區</label>

                            <select class="form-control" v-model.number="item.communityID">
                                <option :value="null">--</option>
                                <option v-for="(x, i) of option.community" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">候補</label>
                            <select class="form-control" v-model.number="item.isAlternate" readonly disabled>
                                <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>


                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Created time</label>
                            <input v-model="item.createdAt" class="form-control" placeholder="" disabled />
                        </div>

                        <div class="form-group">
                            <label class="control-label">交易</label>
                            <a :href="'../transaction/item?id=' + item.transactionID" v-if="transactionID"></a>
                        </div>

                        <!-- <div class="form-group">
                            <label class="control-label">Photo ( 寬高限制 1:1 比例, png/jpg格式, size 不超過1M )</label>

                            <div>
                                <img :src="getPhoto(item.photo)" @click="changePhoto(item, 'photo')" class="itemImg" />
                            </div>
                            <div class="text-left mt-1">
                                <button class="btn btn-outline-danger btn-sm" @click="item.photo = '_default.jpg'">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>

                        </div> -->
                    </div>
                </div>
                <hr />
            </div>
            <div class="card-footer ">
                @include('/_admin/_include/itemFooter')
            </div>
        </div>
    </div>
    @stop
    <!-- -------------------- -->
    @section('js')

    <script>
        var vue = new Vue(vueItem);
    </script>
    <!-- -------------------- -->

    @stop
</div>
