@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
	<div class="container-fluid">
		<h1 class="mt-4">User Login Listing</h1>
		<!-- <ol class="breadcrumb mb-4">
			<li class="breadcrumb-item active">Dashboard</li>
        </ol> -->

		<div class="row">
			<div class="col-12">
				<div class="card mb-3">
					<div class="card-header">
						<div class="row">
							<div class="col-6">
								<a href="exportDo" class="btn btn-primary" ><i class="fa fa-plus"></i> Export</a>
							</div>
							<div class="col-6 text-right">@include('/_admin/_include/listingSelectPageSize')</div>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-bordered table-striped tableListing" width="100%" cellspacing="0">
								<colgroup>
									<col style="width:80px;" />
									<!-- <col style="width:70px;" /> -->
									<col style="width:auto" />
									<col style="width:auto" />
									<col style="width:auto" />
									<col style="width:auto" />
									<col style="width:130px;" />
								</colgroup>

								<thead @change="isChangeCondition = true" @keyup.enter="getListing()">
									<tr class="tr-dark">
										<td class="sort" @click="changeOrder('id');" :class="{ sortAsc:isAsc('id'), sortDesc:isDesc('id')}">ID</td>
										<!-- <td>Photo</td> -->
										<td class="sort" @click="changeOrder('createdAt');" :class="{sortAsc:isAsc('createdAt'), sortDesc:isDesc('createdAt')}">
											Time
										</td>
										<td class="sort" @click="changeOrder('typeID');" :class="{sortAsc:isAsc('typeID'), sortDesc:isDesc('typeID')}">
											Type
										</td>

										<td class="sort" @click="changeOrder('userID');" :class="{sortAsc:isAsc('userID'), sortDesc:isDesc('userID')}">
											User
										</td>

										<td class="sort" @click="changeOrder('ip');" :class="{sortAsc:isAsc('ip'), sortDesc:isDesc('ip')}">
											IP
										</td>

										<td></td>
									</tr>
									<tr>
										<td><input v-model="condition.id" class="form-control" placeholder="" /></td>
										<!-- <td></td> -->
										<td></td>
										<td>
											<select class="form-control" v-model.number="condition.typeID">
												<option :value="null">--</option>
												<option v-for="(x, i) of option.userLoginType" :value="x.id">@{{ x.name }}</option>
											</select>
										</td>
										<td>
											<select class="form-control" v-model.number="condition.userID">
												<option :value="null">--</option>
												<option v-for="(x, i) of option.user" :value="x.id">@{{ x.name }}</option>
											</select>
										</td>

										<td><input v-model="condition.ip" class="form-control" placeholder="" /></td>

										<td>
											<button class="btn btn-secondary w-100" @click="getListing()">
												<i class="fa fa-search"></i> @{{ text.search }}
											</button>
										</td>
									</tr>
								</thead>

								<tbody>
									<tr v-for="(x,i) of items">
										<td>@{{ x.id }}</td>
										<!-- <td><img :src="uploadUrl + x.photo" /></td> -->
										<td>@{{ x.createdAt }}</td>
										<td>@{{ typeText('userLoginType', x.typeID) }}</td>
										<td>@{{ typeText('user', x.userID) }}</td>
										<td>@{{ x.ip }}</td>

										<td>
											<!-- <a :href="'item?id=' + x.id" class="btn btn-sm btn-success mb-1 w-100">
											<i class="fa fa-pen"></i> @{{ text.update }}
                                        </a>
                                         -->
											<!-- <button v-if="isPermission('delete')" @click="deleteItem(i)" type="button" class="btn btn-sm btn-danger w-100">
											<i class="fa fa-times"></i> @{{ text.delete }}
                                        </button>
                                         -->
										</td>
									</tr>
									<tr v-if="items.length <= 0 && !isFirstTime">
										<td colspan="99" class="text-center">@{{ text.notFound }}</td>
									</tr>
								</tbody>
							</table>
						</div>
						@include('/_admin/_include/listingFooter')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
	vueListing.data.condition = Object.assign(vueListing.data.condition, {
		id: null,
		userID: null,
		ip: null,
		typeID: null,
	});
	var vue = new Vue(vueListing);
</script>
<!-- -------------------- -->

@stop
