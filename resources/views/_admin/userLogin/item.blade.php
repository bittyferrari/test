@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
	<div class="container-fluid">
		<h1 class="mt-4">POI Type Detail</h1>

		<div class="card mb-3">
			<div class="card-header">
				<div class="row">
					<div class="col-12"><i class="fa fa-book"></i> Information</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-6">
						<div class="form-group">
							<label class="control-label">ID</label>
							<input v-model="item.id" class="form-control" placeholder="" disabled />
						</div>

						<div class="form-group">
							<label class="control-label">Type</label>
							<select class="form-control" v-model.number="item.typeID">
								<!-- <option :value="null">--</option> -->
								<option v-for="(x, i) of option.poiTypeType" :value="x.id">@{{ x.name }}</option>
							</select>
						</div>

						<!-- <div class="form-group">
							<label class="control-label">Code</label>
							<input v-model="item.code" class="form-control" placeholder="" />
                        </div>
                         -->

                         <div class="form-group">
							<label class="control-label">Name</label>
							<input v-model="item.name" class="form-control" placeholder="" />
						</div>

                        <div class="form-group">
							<label class="control-label">Code</label>
							<input v-model="item.code" class="form-control" placeholder="" />
						</div>

					</div>
					<div class="col-6">
						<div class="form-group">
							<label class="control-label">Created time</label>
							<input v-model="item.createdAt" class="form-control" placeholder="" disabled />
						</div>

						<div class="form-group">
							<label class="control-label">Icon</label>
							<div>
								<img :src="getPhoto(item.photo)" @click="changePhoto(item, 'photo')" class="itemImg" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card-footer ">
				@include('/_admin/_include/itemFooter')
			</div>
		</div>
	</div>
	@stop
	<!-- -------------------- -->
	@section('js')

	<script>
		var vue = new Vue(vueItem);
	</script>
	<!-- -------------------- -->

	@stop
</div>
