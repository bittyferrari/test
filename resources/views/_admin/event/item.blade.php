@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">活動</h1>


        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">ID</label>
                            <input v-model="item.id" class="form-control" placeholder="" disabled />
                        </div>

                        <!-- <div class="form-group">
                            <label class="control-label star">Role</label>
                            <select class="form-control" v-model.number="item.roleID">
                                <option v-for="(x, i) of option.userRole" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>
                         -->

                        <div class="form-group">
                            <label class="control-label">Name</label>
                            <input v-model="item.name" class="form-control" placeholder="" />
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Created time</label>
                            <input v-model="item.createdAt" class="form-control" placeholder="" disabled />
                        </div>


                    </div>
                </div>
                <hr />
                <div class="row hidden">
                    <div class="col-12">
                        <table class="table table-listing border table-hover table-striped">
                            <colgroup>
                                <col width="200px" style="width:200px" />
                                <col width="auto" />
                            </colgroup>
                            <thead class="thead-dark">
                                <tr>
                                    <th>Module</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(x, i) of option.userPermission">
                                    <td class="text-right">@{{ x.name }}</td>
                                    <td>
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2" v-for="(xx, ii) of x.action">
                                                <label class="btn btn-sm d-flex align-items-center"
                                                    :class="{ 'btn-primary': isInclude(item.permissions, x.module + '-' + xx), 'btn-outline-primary': !isInclude(item.permissions, x.module + '-' + xx) }">
                                                    <input type="checkbox" class="mr-2" v-model="item.permissions" :value="x.module + '-' + xx" />
                                                    <span>@{{ xx }}</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
                @include('/_admin/_include/itemFooter')
            </div>
        </div>
    </div>
    @stop
    <!-- -------------------- -->
    @section('js')

    <script>
        var vue = new Vue(vueItem);
    </script>
    <!-- -------------------- -->

    @stop
</div>
