@extends('__layout/admin')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="container-fluid">
        <h1 class="mt-4">講師資料</h1>

        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-12"><i class="fa fa-book"></i> Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">ID</label>
                            <input v-model="item.id" class="form-control" placeholder="" disabled />
                        </div>
                        <div class="form-group">
                            <label class="control-label">名稱</label>
                            <input v-model="item.name" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">簡介</label>
                            <textarea v-model="item.content" class="form-control" placeholder=""></textarea>
                        </div>

                        <div class="form-group">
                            <label class="control-label">手機</label>
                            <input v-model="item.phone" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">頭銜</label>
                            <input v-model="item.title" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">介紹連結(http://或https://開頭)</label>
                            <input v-model="item.url" class="form-control" placeholder="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">是否顯示在課程</label>
                            <select class="form-control" v-model.number="item.isInCourse">
                              <!-- <option :value="null">--</option> -->
                              <option v-for="(x, i) of option.is" :value="x.id">@{{ x.name }}</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label">Created time</label>
                            <input v-model="item.createdAt" class="form-control" placeholder="" disabled />
                        </div>

                        <div class="form-group">
                            <label class="control-label">照片</label>
                            <div>
                                <img :src="getPhoto(item.photo)" @click="changePhoto(item, 'photo')" class="itemImg" />
                            </div>
                            <div class="text-left mt-1">
                                <button class="btn btn-outline-danger btn-sm" @click="item.photo = '_default.jpg'">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>


                        <!-- <div class="form-group">
                            <label class="control-label">Photo ( 寬高限制 1:1 比例, png/jpg格式, size 不超過1M )</label>

                            <div>
                                <img :src="getPhoto(item.photo)" @click="changePhoto(item, 'photo')" class="itemImg" />
                            </div>
                            <div class="text-left mt-1">
                                <button class="btn btn-outline-danger btn-sm" @click="item.photo = '_default.jpg'">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>

                        </div> -->
                    </div>
                </div>
                <hr />
            </div>
            <div class="card-footer ">
                @include('/_admin/_include/itemFooter')
            </div>
        </div>
    </div>
    @stop
    <!-- -------------------- -->
    @section('js')

    <script>
        var vue = new Vue(vueItem);
    </script>
    <!-- -------------------- -->

    @stop
</div>
