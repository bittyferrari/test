@extends('__layout/store')
<!-- -------------------- -->

@section('head')

<script charset="utf-8" src="https://static.line-scdn.net/liff/edge/2/sdk.js"></script>

<style>
    #menu-bottom {
        display: none;
    }
</style>
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="container py-5 text-black">

            <div class="frame text-center">

                <!-- <div>
                    <div class="text-26 mb-3">@{{ self.name }}</div>
                </div> -->

                <!-- <a href="/user/register" class="bg-red text-white py-5 text-20 flex justify-center items-center rounded-5 shadow-3 pointer">
                    <img src="/img/icon-64.png" class="mr-3 h-6">
                    <div>前往註冊</div>
                </a> -->

            </div>

        </div>

    </div>
</div>

@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.mounted = function () {
        this.initializeApp();
    }

    vueItem.data = Object.assign(vueItem.data, {
        error: null,
        data: null,
        result: {},
    });

    vueItem.methods = Object.assign(vueItem.methods, {

        initializeApp() {
            const self = this;

            try {

                liff.init({ liffId: vueData.liffID }).then(() => {

                    // liff.getOS()
                    // ios
                    // android
                    // web

                    switch (liff.getOS()) {
                        case 'ios':
                        case 'android':
                            // open liff
                            document.location = 'https://liff.line.me/1655571827-63Y4yYJ8?typeID=shop';
                            break;
                        case 'web':
                        default:
                            alert('請用手機開啟');
                            break;
                    }

                    // var isInClient = liff.isInClient()

                    // if (isInClient) {
                    //     // in liff already
                    // } else {
                    //     // open liff url
                    //     // document.location = 'asdsad';

                    // }

                }).catch((err) => {
                    // self.alert(err);
                    console.log(err);
                });

            } catch (e) {
                self.alert('error: ' + e);
            }
        }
    });

    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
