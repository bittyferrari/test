@extends('__layout/store')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">

        <div class="container py-5">

            <div class="bg-white p-5 mb-8 rounded-10 shadow-1">

                <div class="mb-6">
                    <div class="mb-2">商家名稱</div>
                    <div class="" >
                        <input v-model="item.name" class="form-control" placeholder="" type="text" readonly disabled />
                    </div>
                </div>

                <div class="mb-3">
                    <div class="mb-2">聯絡電話</div>
                    <div class="" >
                        <input v-model="item.phone" class="form-control" placeholder="" type="text" readonly disabled />
                    </div>
                </div>

            </div>


        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    // vueListing.data.condition = Object.assign(vueListing.data.condition, {
    // 	id: null,
    // 	name: null,
    // 	lineUserID: null,
    // 	roleID: null,
    // });
    var vue = new Vue(vueListing);
</script>
<!-- -------------------- -->

@stop
