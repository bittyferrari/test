@extends('__layout/store')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->


@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">

        <div class="container py-5">

            <div class="text-18 mt-10">

                <template v-if="!isSuccess">


                    <div class="bg-white p-5 mb-8 rounded-10 shadow-1">

                        <div class="mb-6">
                            <div class="mb-2">變更密碼</div>
                            <div class="">
                                <input v-model="item.password" class="form-control" placeholder=" " type="password" />
                            </div>
                        </div>

                        <div class="mb-3">
                            <div class="mb-2">確認新密碼</div>
                            <div class="">
                                <input v-model="item.password2" class="form-control" placeholder=" " type="password" />
                                <div class="text-red  text-12" v-if="item.password != '' && item.password != item.password2">
                                    *兩次密碼輸入不同
                                </div>
                            </div>
                        </div>

                    </div>

                    <!-- <div class="frame text-18 mt-10">
                        <div class="text-center mb-10">變更密碼</div>
                        <div class="flex items-center  ">
                            <div class="mr-3 w-32">輸入新密碼</div>
                            <div class="flex-grow">
                                <input v-model="item.password" class="form-control" placeholder=" " type="password" />
                            </div>
                        </div>

                        <div class="flex items-center mt-10">
                            <div class="mr-3 w-32">確認新密碼</div>
                            <div class="flex-grow">
                                <input v-model="item.password2" class="form-control" placeholder=" " type="password" />

                                <div class="text-red  text-12" v-if="item.password != '' && item.password != item.password2">
                                    *兩次密碼輸入不同
                                </div>
                            </div>
                        </div>
                        <div class="flex items-center mt-2" v-if="">
                            <div class="mr-3 w-32">&nbsp;</div>
                            <div class="flex-grow">
                            </div>
                        </div>

                    </div>
                     -->
                    <div class="text-center mt-5">
                        <button class="btn-white" @click="submitDo()">送出</button>
                    </div>


                </template>


                <template v-else>
                    <div class="frame text-18 mt-10">

                        <div class="text-center py-10">
                            變更密碼成功！
                        </div>
                    </div>
                </template>

            </div>

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    vueItem.data = Object.assign(vueItem.data, {
        item: {
            password: '',
            password2: '',
        },
        isSuccess: false,
    });

    vueItem.methods = Object.assign(vueItem.methods, {

        submitDo() {

            this.$http.post('updatePasswordDo', this.item).then(function (r) {
                // const body = r.body;
                // this.items = body.data;
                this.isSuccess = true;
                console.log('asd');
            });
        }
    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
