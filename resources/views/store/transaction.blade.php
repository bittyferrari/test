@extends('__layout/store')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto ">
        <div class="container py-5">

            <div v-if="typeID == 'exchange'">
                <div class="flex justify-center items-center mb-6 mt-5 text-white text-24">
                    <!-- <img src="/img/icon-52.png" class="mr-2 w-12"> -->
                    <div>
                        交易紀錄
                    </div>
                </div>
            </div>

            <div v-else>
                <div class="flex justify-center items-center mb-6 mt-5 text-white text-24">
                    <!-- <img src="/img/icon-53.png" class="mr-2 w-12"> -->
                    <div>
                        儲值紀錄
                    </div>
                </div>

            </div>

            <div class="flex items-center justify-center mt-5 mb-6 text-white text-20">

                <div class="pointer p-3" @click="monthPrevious()">
                    <img src="/img/icon-24.svg" class="w-2">
                </div>
                <div class="mx-5">@{{ year }}年@{{ month }}月</div>
                <div class="pointer p-3" @click="monthNext()">
                    <img src="/img/icon-23.svg" class="w-2">
                </div>
            </div>

            <div v-for="(x, i) of items" class="mb-3">
                <div class="bg-white px-4 py-2 rounded-6 shadow-2 mb-3" v-if="x.isSuccess == 1">

                    <!-- ['id' => 10, 'name' => '商家結算'], -->
                    <!-- ['id' => 11, 'name' => '補充儲值額度'], -->

                    <template v-if="x.typeID == 10 || x.typeID == 11">
                        <div class="flex justify-between items-center mb-2">
                            <span class="text-AD9152">@{{ typeText('transactionTypeChinese', x.typeID) }}</span>
                            <div>
                                <template v-if="x.typeID == 11">@{{ x.priceReset | numberPositive | number }}</template>
                                <template v-else-if="x.typeID == 10">@{{ x.priceStoreSettlement | numberPositive | number }}</template>
                            </div>
                        </div>
                        <div class="text-12 text-gray2">@{{ x.createdAt }}</div>

                    </template>

                    <template v-else>

                        <div class="mb-2 text-14">@{{ typeText('user', x.userID) }}</div>
                        <div class="flex justify-between items-center">
                            <span>@{{ typeText('transactionTypeChinese', x.typeID) }}</span>
                            <span class="font-bold ml-1" :class="{ 'text-red': x.price > 0, 'text-green': x.price < 0}">
                                <template v-if="x.typeID == 11">@{{ x.priceReset | numberPositive | number }}</template>
                                <template v-else-if="x.typeID == 10">@{{ x.priceStoreSettlement | numberPositive | number }}</template>
                                <template v-else>@{{ x.price | numberPositive }}</template>
                            </span>
                        </div>
                        <div class="text-12 text-gray2">@{{ x.createdAt }}</div>

                    </template>


                </div>
            </div>


        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    // vueListing.data.condition = Object.assign(vueListing.data.condition, {
    // 	id: null,
    // 	name: null,
    // 	lineUserID: null,
    // 	roleID: null,
    // });

    vueListing.data = Object.assign(vueListing.data, {
        isStoreScanned: false,
    });

    vueItem.mounted = function () {
        this.checkTransaction();
    }

    vueItem.methods = Object.assign(vueItem.methods, {

        monthPrevious() {
            var month = this.month - 1;
            var year = this.year;
            if (month <= 0) {
                month = 12;
                year -= 1;
            }
            document.location = 'transaction?typeID=' + this.typeID + '&year=' + year + '&month=' + month;
        },


        monthNext() {
            var month = this.month + 1;
            var year = this.year;
            if (month >= 13) {
                month = 1;
                year += 1;
            }
            document.location = 'transaction?typeID=' + this.typeID + '&year=' + year + '&month=' + month;
        },

        checkTransaction() {

            const self = this;
            setTimeout(function () {


                let data = {
                    id: this.transaction.id,
                }

                self.$http.post('checkTransactionDo', data).then(function (r) {
                    const body = r.body;
                    // this.items = body.data;

                    const data = body.data;
                    if (data.isFound) {

                        if (data.isExpired) {

                            alert('qrcode expired');

                        } else {


                            if (data.item.isStoreScanned) {
                                this.isStoreScanned = true;
                            }

                            alert('checkTransaction');


                            self.checkTransaction();

                        }



                    } else {

                        alert('qrcode not found');
                    }


                });

            }, 3000);



        },
    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
