@extends('__layout/store')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="container">
            <h1 class=" ">Store Exchange</h1>

            <hr>

            <button class="btn-blue" @click="getQrcode()">我要兌換</button>

            <div v-if="isShowQrcode">
                <qrcode :value="APP_URL + '/poi/index?md5'" :options="{ width: 1000 }" class="qrcode"></qrcode>
            </div>

            <div>
                2020-06-21 香榭18巧克力 使用點數40點 成功
            </div>

            <div>
                2020-06-21 兌換點數240點 成功
            </div>



        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    vueListing.data = Object.assign(vueListing.data, {
        isShowQrcode: false,
    });


    vueListing.methods = Object.assign(vueListing.methods, {
        getQrcode() {

            this.isShowQrcode = true;
        }

    });




    var vue = new Vue(vueListing);
</script>
<!-- -------------------- -->

@stop
