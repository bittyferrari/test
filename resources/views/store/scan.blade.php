@extends('__layout/store')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->


@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="">

            <!--
            .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
            ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
            ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
            ....###.......###.......###.......###.......###.......###.......###.......###.......###.......###...
            ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
            ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
            .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
            -->
            <div v-if="indexStep == 1">

                <div class="container text-white">
                    <qrcode-stream :camera="camera" @decode="onDecode" @init="onInit">

                        <button class="py-3 px-0" @click="switchCamera">
                            <img src="/img/icon-47.png" style="width: 40px">
                        </button>
                    </qrcode-stream>

                    <p class="error text-center" v-if="noFrontCamera">
                        此裝置沒有前視鏡頭
                    </p>

                    <p class="error text-center" v-if="noRearCamera">
                        此裝置沒有後視鏡頭
                    </p>

                </div>

            </div>

            <!--
            .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
            ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
            ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
            ....###.......###.......###.......###.......###.......###.......###.......###.......###.......###...
            ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
            ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
            .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
            -->
            <div v-if="indexStep == 2">

                <div class="flex flex-wrap items-center bg-white text-black  ">
                    <div class="text-14 w-1/2 py-5 text-center">目前可儲值餘額</div>
                    <div class="text-24 w-1/2 py-5 text-center bg-C7B489 text-white">@{{ priceSaveRemain | number }}元</div>
                </div>

                <div class="container">

                    <div class="frame text-center mt-10 mb-10 text-black" v-if="user != null">
                        <img :src="user.photoLine" class="w-20 mb-5 mx-auto rounded-full bg-c4c4c4">
                        <div>@{{ user.name }}</div>
                    </div>

                    <div>
                        <!-- <img class="w-full mb-3" src="/img/icon-48.png" @click="setType('save')" v-if="self.isStoreValue == 1">
                        <img class="w-full mb-3" src="/img/icon-49.png" @click="setType('exchange')" v-if="self.isExchangable == 1">
                        <img class="w-full mb-3" src="/img/icon-50.png" @click="setType('refund')" v-if="self.isRefundable == 1">
                         -->
                        <div class="btn-white w-full mb-3" @click="setType('save')" v-if="self.isStoreValue == 1">儲值</div>
                        <div class="btn-white w-full mb-3" @click="setType('exchange')" v-if="self.isExchangable == 1">點數兌換</div>
                        <div class="btn-white w-full mb-3" @click="setType('refund')" v-if="self.isRefundable == 1">退貨/退點</div>

                    </div>

                </div>
            </div>

            <!--
            .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
            ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
            ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
            ....###.......###.......###.......###.......###.......###.......###.......###.......###.......###...
            ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
            ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
            .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
            -->
            <div v-if="indexStep == 3">

                <!--
                ..######.....###....##.....##.########
                .##....##...##.##...##.....##.##......
                .##........##...##..##.....##.##......
                ..######..##.....##.##.....##.######..
                .......##.#########..##...##..##......
                .##....##.##.....##...##.##...##......
                ..######..##.....##....###....########
                -->
                <template v-if="typeID == 'save'">
                    <div class="container pt-10">

                        <div class="bg-white text-black shadow-2 py-10 text-center mt-10 mb-10 px-10 rounded-6">
                            <!-- <img src="/img/icon-10.png" class="w-20 mb-5 mx-auto rounded-full bg-c4c4c4"> -->
                            <img :src="user.photoLine" class="w-20 mb-5 mx-auto rounded-full bg-c4c4c4">
                            <div class="mb-5">@{{ user.name }}</div>

                            <div class=" mt-5">
                                <div class="mb-2 text-left">面額</div>
                                <input v-model.number="price" class="form-control" placeholder="" type="number" />
                            </div>

                            <template v-if="isConfirmPrice">

                                <div class=" mt-5">
                                    <div class="mb-2 text-left">
                                        面額【@{{ item.priceOrigin }}】儲值點數【@{{ item.price }}】
                                    </div>
                                    <div v-if="item.isUserConfirm != 1" class="text-left">儲值結果：等待對方確認中</div>

                                </div>

                                <div class="flex items-center mt-5 text-red" v-if="item.isStoreEnough == 0">
                                    <div class="flex-grow text-left">
                                        可儲值餘額不足
                                    </div>
                                </div>

                                <template v-if="item.isUserConfirm == 1">
                                    <div class="flex items-center mt-5" v-if="item.isSuccess == 1">
                                        <div class="flex-grow text-left">
                                            儲值結果：成功
                                        </div>
                                    </div>

                                    <div class="text-red flex items-center mt-5 text-red" v-if="item.isSuccess != 1">
                                        <div class="flex-grow text-left">
                                            儲值結果：失敗
                                        </div>
                                    </div>
                                </template>

                            </template>
                        </div>

                    </div>
                </template>

                <!--
                .########.##.....##..######..##.....##....###....##....##..######...########
                .##........##...##..##....##.##.....##...##.##...###...##.##....##..##......
                .##.........##.##...##.......##.....##..##...##..####..##.##........##......
                .######......###....##.......#########.##.....##.##.##.##.##...####.######..
                .##.........##.##...##.......##.....##.#########.##..####.##....##..##......
                .##........##...##..##....##.##.....##.##.....##.##...###.##....##..##......
                .########.##.....##..######..##.....##.##.....##.##....##..######...########
                -->
                <template v-if="typeID == 'exchange'">
                    <div class="container pt-10">

                        <div class="bg-white text-black shadow-2 py-10 text-center mt-10 mb-10 px-10">
                            <!-- <img src="/img/icon-10.png" class="w-20 mb-5 mx-auto rounded-full bg-c4c4c4"> -->
                            <img :src="user.photoLine" class="w-20 mb-5 mx-auto rounded-full bg-c4c4c4">
                            <div class="mb-5">@{{ user.name }}</div>


                            <div v-if="!isStoreUpdate">
                                <div class="text-left mt-5">
                                    <div class="mb-2 mr-3">抵扣點數</div>

                                    <input v-model.number="price" class="form-control" placeholder="" type="number" />

                                </div>
                            </div>

                            <div v-else class="text-left">

                                <div>
                                    抵扣點數【@{{ price }}】
                                </div>
                                <div v-if="item.isEnd == 0">
                                    兌換結果：等待對方確認中
                                </div>
                            </div>

                            <template v-if="isConfirmPrice">


                                <div class="flex items-center mt-5" v-if="item.priceCash > 0">
                                    <div class="flex-grow text-left text-yellow">
                                        使用者點數不足
                                    </div>
                                </div>

                                <div v-if="item.isEnd == 1">
                                    <div class="flex items-center mt-5" v-if="item.isSuccess == 1">
                                        <div class="flex-grow text-left">
                                            兌換結果：成功
                                        </div>
                                    </div>

                                    <div class="flex items-center mt-5 text-red" v-else>
                                        <div class="flex-grow text-left">
                                            兌換結果：失敗
                                        </div>
                                    </div>
                                </div>

                            </template>

                            <div class="text-left text-red mt-5" v-if="item.isUserPriceEnough === 0">
                                點數不足 交易失敗
                            </div>


                        </div>

                    </div>
                </template>

                <!--
                .########..########.########.##.....##.##....##.########.
                .##.....##.##.......##.......##.....##.###...##.##.....##
                .##.....##.##.......##.......##.....##.####..##.##.....##
                .########..######...######...##.....##.##.##.##.##.....##
                .##...##...##.......##.......##.....##.##..####.##.....##
                .##....##..##.......##.......##.....##.##...###.##.....##
                .##.....##.########.##........#######..##....##.########.
                 -->
                <template v-if="typeID == 'refund'">
                    <div class="container pt-1">
                        <div class="frame py-5 text-center mt-10 mb-2 px-3" v-if="!isConfirmPrice">
                            <img :src="user.photoLine" class="w-20 mb-5 mx-auto rounded-full bg-c4c4c4">
                            <div class="mb-5">@{{ user.name }}</div>

                            <div class="mt-3 text-left overflow-auto" style="min-height:200px; height: calc(100vh - 420px)" v-if="isShowTransaction">
                                <div v-for="(x, i) of transactionItems" class="mb-3">
                                    <div class="bg-white px-4 py-2 rounded-6 border shadow mb-3 pointer" @click="setRefund(x)"
                                        :class="{ 'bg-FEECDC': refundTransactionID == x.id}">
                                        <div class="mb-2 text-14">@{{ typeText('user', x.userID) }}</div>

                                        <!-- <div class="flex justify-between items-center">
                                            <span>@{{ typeText('transactionTypeChinese', x.typeID) }}</span>
                                            <span class="font-bold ml-1" :class="{ 'text-red': x.price > 0, 'text-green': x.price < 0}">@{{ x.price |
                                                numberPositive }}</span>
                                        </div> -->
                                        <div class="flex items-center w-full">

                                            <template v-if="x.usedCouponPricePoint > 0">
                                                <div class="mr-1">活動點數</div>
                                                <div class="mr-2">@{{ x.usedCouponPricePoint }}</div>
                                            </template>

                                            <template v-if="x.usedPrice > 0">
                                                <div class="mr-1">總太點數</div>
                                                <div class="mr-2">@{{ x.usedPrice }}</div>
                                            </template>

                                            <div class="ml-auto mr-1">共消費</div>
                                            <div class="text-16 font-bold text-green">@{{ x.price | numberPositive }}</div>

                                        </div>

                                        <div class="text-12 text-gray2">@{{ x.createdAt }}</div>
                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="frame py-10 text-center mt-10 mb-2 px-10" v-else>
                            <!-- success -->

                            <img :src="user.photoLine" class="w-20 mb-5 mx-auto rounded-full bg-c4c4c4">
                            <div class="mb-5">@{{ user.name }}</div>


                            <div class="flex items-center mt-5" :class="{'text-red': item.isSuccess != 1}">
                                <div class="mr-3">退還「總太點數」</div>
                                <div class="flex-grow text-left">
                                    【@{{ item.price | numberPositive }}】
                                </div>
                            </div>

                            <template v-if="isEnd">
                                <div class="flex items-center mt-5" v-if="item.isSuccess == 1">
                                    <div class="mr-3">交易結果</div>
                                    <div class="flex-grow text-left">
                                        成功
                                    </div>
                                </div>
                                <div class="flex items-center mt-5 text-red" v-else>
                                    <div class="mr-3">交易結果</div>
                                    <div class="flex-grow text-left">
                                        ：失敗
                                    </div>
                                </div>

                                <div class="text-red mt-5" v-if="item.isUserPriceEnough === 0">
                                    點數不足 交易失敗
                                </div>

                            </template>

                        </div>



                    </div>
                </template>


                <!--
                .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
                ..##...##...##...##...##...##...##...##...##...##...##...##...##...##.
                ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
                ....###.......###.......###.......###.......###.......###.......###...
                ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
                ..##...##...##...##...##...##...##...##...##...##...##...##...##...##.
                .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
                -->
                <div class="container">
                    <div class="flex flex-wrap mt-3">
                        <div class="w-1/2 pr-4">
                            <button class="btn-white w-full" @click="cancelDo()" v-if="!isEnd">取消</button>
                        </div>
                        <div class="w-1/2 pl-4">

                            <template v-if="!isStoreUpdate || item.isUserConfirm == 1">
                                <button v-if="typeID == 'save'" class="btn-white w-full" @click="confirmDo()">確認</button>
                                <button v-if="typeID == 'exchange'" class="btn-white w-full" @click="confirmDo()">確認</button>
                                <button v-if="typeID == 'refund'" class="btn-white w-full" @click="confirmDo()">確認</button>
                            </template>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.data = Object.assign(vueItem.data, {
        typeID: null,
        userID: '',
        camera: 'rear',
        qrcode: '',
        md5: '',
        result: '',
        noRearCamera: false,
        noFrontCamera: false,
        indexStep: 1,
        indexStep2TypeID: 'save',
        indexStep2: 1,
        isConfirmPrice: false,
        price: 0,
        item: {
            isPriceEnough: 1,
            isStoreEnough: 1,
            isSuccess: false,
            price: 0,
            priceCash: 0,
            isUserConfirm: 0,
        },
        isEnd: false,
        isStoreUpdate: false,
        user: {
            name: '',
            photoLine: '',
        },
        data: {},
        isScaned: false,
        timeoutCountdown: null,
        second: 60,
        transactionItems: [],
        refundTransactionID: null,
        isShowTransaction: true,
    });


    vueItem.mounted = function () {
    }

    vueItem.methods = Object.assign(vueItem.methods, {

        setRefund(q) {
            // console.log(q);
            if (q.usedPrice <= 0) {
                alert('此交易無使用總太點數，無法退點');
            } else {
                this.refundTransactionID = q.id;
            }
        },

        startCountdown() {

            let second = this.second;
            second--;

            if (second < 0) {
                // this.alert('交易取消', function () {
                //     document.location = 'index';
                // });
                document.location = 'index';

            } else {
                this.second = second;
                this.timeoutCountdown = setTimeout(this.startCountdown, 1000);
            }
        },

        confirmDo() {


            if (this.typeID == 'save') {
                if (this.price <= 0) {
                    this.alert('請輸入面額');
                    return;
                }
            }

            if (this.typeID == 'exchange') {
                if (this.price <= 0) {
                    this.alert('請輸入抵扣點數');
                    return;
                }
            }


            clearTimeout(this.timeoutCountdown);

            if (this.item.isEnd == 1) {
                // this.alert('確認完成', function () {
                //     location.reload();
                // });
                location.reload();
            }


            if (!this.isConfirmPrice) {

                var isProceed = true
                if (this.typeID == 'refund') {
                    if (this.refundTransactionID == null) {
                        this.alert('請選擇交易');
                        isProceed = false;
                    }

                }

                if (isProceed) {

                    this.isConfirmPrice = true;

                    // update transaction
                    const data = {
                        price: this.price,
                        typeID: this.typeID,
                        md5: this.md5,
                        refundTransactionID: this.refundTransactionID,
                    };

                    this.$http.post('/transaction/storeUpdateDo', data).then(function (r) {
                        console.log('storeUpdateDo');
                        const body = r.body;
                        const data = body.data;
                        this.item = data.item;

                        switch (this.item.typeID) {
                            case 3:
                                if (this.item.isEnd) {
                                    alert('商家點數不足，無法退點。');
                                    location.reload();
                                }
                            default:

                                break;

                        }
                        this.isStoreUpdate = true;
                        this.isShowTransaction = false;

                        // console.log(body);

                    });

                }

            } else {

            }

        },

        cancelDo() {

            this.$http.post('/transaction/storeCancelDo', { id: this.item.id }).then(function (r) {
                const body = r.body;
                const data = body.data;

                this.alert('交易已取消', function () {
                    location.reload();
                });

            });



        },

        switchCamera() {
            switch (this.camera) {
                case 'front':
                    this.camera = 'rear'
                    break
                case 'rear':
                    this.camera = 'front'
                    break
            }
        },

        setType(q) {
            this.typeID = q;
            this.indexStep++;

            const data = {
                md5: this.item.md5
            };

            if (q == 'refund') {
                this.$http.post('getListingTransaction', data).then(function (r) {
                    const body = r.body;
                    const data = body.data;
                    this.option.user = data.users;
                    this.transactionItems = data.items;
                });

            }

        },

        updateTransaction() {

            console.log('updateTransaction');

            const self = this;
            const data = {
                md5: this.md5
            };

            this.$http.post('/transaction/getItem', data).then(function (r) {
                const body = r.body;
                const data = body.data;

                if (data.item == null) {

                    this.alert('交易取消或不存在', function () {
                        location.reload();
                    });

                } else {

                    this.item = data.item;
                    this.user = data.user;

                    setTimeout(this.updateTransaction, 2000);

                }
            });

        },

        testing() {

            var md5 = this.testingMd5;
            this.onDecode(md5);
        },

        onDecode(result) {

            if (this.isScaned) {
                return;
            }

            this.isScaned = true;

            this.result = result;
            this.qrcode = result;

            try {

                var md5 = result;

                console.log(md5);

                var data = {
                    md5: md5,
                };

                this.md5 = md5;

                this.$http.post('/transaction/storeScanDo', data).then(function (r) {
                    const body = r.body;
                    const data = body.data;

                    this.data = r;

                    if (body.data.isSuccess) {
                        var item = body.data.item;

                        switch (item.typeID) {
                            case null:
                            case 1:
                            case 2:
                            case 3:
                                this.indexStep = 2;
                                // this.alert('掃描成功');
                                this.startCountdown();
                                this.updateTransaction();
                                break;
                            case 4:
                                // no
                                isOK = false;
                                break;
                            case 5:
                                // this.alert('主餐掃描成功', function () {
                                //     location.reload();
                                // });
                                location.reload();

                                break;
                            case 6:
                                // this.alert('湯品掃描成功', function () {
                                //     location.reload();
                                // });
                                location.reload();

                                break;
                            case 7:
                                // this.alert('門票掃描成功', function () {
                                //     location.reload();
                                // });
                                location.reload();

                                break;
                            case 8:
                                // no
                                isOK = false;
                                break;
                        }

                    } else {
                        this.alert('掃描失敗, 找不到qrcode或是過期');
                    }


                });

            } catch (e) {
                // yada
                this.alert('error: ' + e);
            }
        },

        async onInit(promise) {
            try {
                await promise
            } catch (error) {
                const triedFrontCamera = this.camera === 'front'
                const triedRearCamera = this.camera === 'rear'

                const cameraMissingError = error.name === 'OverconstrainedError'

                if (triedRearCamera && cameraMissingError) {
                    this.noRearCamera = true
                }

                if (triedFrontCamera && cameraMissingError) {
                    this.noFrontCamera = true
                }

                console.error(error)
            }
        },

    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
