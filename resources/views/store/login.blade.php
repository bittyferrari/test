@extends('__layout/store')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->
<style>
    #menu-bottom {
        display: none;
    }
</style>

@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">
        <div class="container pt-10">

            <div class="flex justify-center items-center text-white mb-8 text-24 ">
                <!-- <img src="/img/icon-31.png" class="mr-2 w-10"> -->
                <div>
                    歡迎商家登入
                </div>
            </div>

            <div class="frame text-18">

                <div class="mr-3 w-12 mb-2">帳號</div>
                <div class="flex items-center">
                    <div class="flex-grow">
                        <input v-model="item.username" class="form-control" placeholder="請輸入帳號" type="text" @keyup.enter="loginDo()" />
                    </div>
                </div>
                <div class="mt-10 mr-3 w-12 mb-2">密碼</div>
                <div class="flex items-center">
                    <div class="flex-grow">
                        <input v-model="item.password" class="form-control" placeholder="請輸入密碼" type="password" @keyup.enter="loginDo()" />
                    </div>
                </div>
            </div>

            <div class="text-center mt-5">
                <button class="btn-white w-full" @click="loginDo()">送出</button>
            </div>

            <!--
            <hr>

            <div class="form-group">
                <div class="field-name">username</div>
                <input type="text" v-model="item.username">
            </div>

            <div class="form-group">
                <div class="field-name">password</div>
                <input type="password" v-model="item.password">
            </div>

            <div class="text-right">
                <button class="btn-blue" @click="loginDo()">登入</button>
            </div> -->


        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    // vueListing.data.condition = Object.assign(vueListing.data.condition, {
    // 	id: null,
    // 	name: null,
    // 	lineUserID: null,
    // 	roleID: null,
    // });

    vueItem.data.item = {
        username: '',
        password: '',
    }
    vueItem.methods = Object.assign(vueItem.methods, {
        loginDo() {

            this.$http.post('loginDo', this.item).then(function (r) {
                const body = r.body;
                this.items = body.data;

                if (body.statusID == 0) {

                    this.alert('登入成功', function () {
                        document.location = 'index';
                    });
                } else {
                    this.alert('登入失敗, 帳號或密碼錯誤');
                }

            });


        }
    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
