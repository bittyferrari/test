@extends('__layout/store')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->


@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">

        <template v-if="indexStep == 1">
            <div class="container py-5 text-black">
                <div class="bg-white p-5 mb-8 rounded-10 shadow-1">

                    <div class="mb-6">
                        <div class="mb-2">目前累積點數</div>
                        <div class="form-control" placeholder="" type="number">
                            @{{ storeTotal }}
                        </div>

                    </div>

                    <div class="mb-3">
                        <div class="mb-2">輸入結算點數</div>
                        <input v-model.number="priceStoreSettlement" class="form-control" placeholder="" type="number" />
                    </div>
                </div>
                <button class="btn-white w-full " @click="stepNext()">
                    送出
                </button>
            </div>
        </template>

        <template v-if="indexStep == 2">
            <div class="container py-5 text-black">
                <qrcode :value="item.md5" :options="{ width: 1000 }" class="qrcode rounded-10 shadow-1 mt-10 mb-10"></qrcode>
            </div>
            <div class="bg-333 text-white p-3 text-center text-24 mb-5">
                結算點數：@{{ item.priceStoreSettlement }}點
            </div>
            <div class="text-white text-center text-18  ">
                交易剩餘時間 00:@{{ second | pad(2) }}
            </div>
        </template>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.data = Object.assign(vueItem.data, {
        item: {
            id: null,
            md5: '',
            price: 0,
        },
        indexStep: 1,
        second: 60,
        verifyCode: '',
        isSuccess: false,

        priceStoreSettlement: 0,
    });

    vueItem.methods = Object.assign(vueItem.methods, {
        stepNext() {

            if (this.priceStoreSettlement > 0) {

                // get transcation
                const data = {
                    typeID: 10,
                    priceStoreSettlement: this.priceStoreSettlement
                };

                this.$http.post('/transaction/storeCreateDo', data).then(function (r) {
                    const body = r.body;
                    // this.items = body.data;

                    if (body.data.isSuccess) {

                        this.item = body.data.item;

                        this.indexStep++;
                        if (this.indexStep == 2) {
                            this.startCountdown();
                            this.checkTransaction();
                        }

                    } else {
                        this.alert('點數餘額不足');
                        // location.reload();
                    }

                });

            } else {
                this.alert('結算點數必須大於0');

            }
        },

        startCountdown() {
            if (this.item.isStoreScan != 1) {

                let second = this.second;
                second--;

                if (second < 0) {
                    // alert('qrcode過期');
                    // document.location = 'index';
                } else {
                    this.second = second;
                    this.timeoutCountdown = setTimeout(this.startCountdown, 1000);
                }
            }
        },

        checkTransaction() {

            const self = this;
            setTimeout(function () {

                let data = {
                    md5: self.item.md5,
                }

                self.$http.post('/transaction/getItem', data).then(function (r) {
                    self.updateTimes++;

                    const body = r.body;

                    const data = body.data;
                    self.item = data.item;

                    console.log(data);

                    if (data.item.isCancel == 1) {
                        alert('交易失敗');
                        document.location = '/store/profile';
                        return;
                    }

                    if (data.item.isSuccess == 1) {
                        // this.alert('結算完成', function () {
                        //     document.location = '/store/transaction?typeID=exchange';
                        // });
                        document.location = '/store/transaction?typeID=exchange';

                        return;
                    }


                    self.checkTransaction();
                });

            }, 2000);
        },



    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
