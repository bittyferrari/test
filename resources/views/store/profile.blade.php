@extends('__layout/store')
<!-- -------------------- -->

@section('head')
<!-- -------------------- -->


<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">


        <div class="flex flex-wrap items-start text-15 ">
            <div class="w-1/2 text-center">

                <div class="bg-black text-white py-5" style="border-right:1px #fff solid;">
                    目前可儲值餘額
                </div>

                <div class="bg-C7B489 text-26 py-5 text-white" style="border-right:1px #fff solid;">
                    @{{ store.priceStoreMax - store.priceStore | number }}
                </div>

            </div>
            <div class="w-1/2 text-center bg-red text-white">

                <div class="bg-black text-white py-5">
                    目前累積點數
                </div>

                <div class="bg-C2762C text-26 py-5 text-white">
                    @{{ storeTotal | number }}

                </div>
            </div>
        </div>

        <!-- XXXXX

        <div class="flex flex-wrap items-center bg-white text-black">
            <div class="text-14 w-1/2 py-5 text-center">目前可儲值餘額</div>
            <div class="text-24 w-1/2 py-5 text-center bg-red text-white">@{{ store.priceStoreMax - store.priceStore }}點</div>
        </div>

        <div class="my-8 py-4 text-center bg-333 text-white text-18">
            目前累積點數：@{{ storeTotal }}點
        </div> -->

        <div class="container">

            <div class="flex flex-wrap mt-10">
                <div class="w-1/2 pr-5">
                    <a class="btn-white w-full block" href="setting">商家資料</a>
                </div>
                <div class="w-1/2 pl-5">
                    <a class="btn-white w-full block" href="password">變更密碼</a>
                </div>
            </div>

            <div class="flex flex-wrap mt-10">
                <div class="w-1/2 pr-5">
                    <a class="btn-white w-full block" href="reset">補充儲值額度</a>
                </div>
                <div class="w-1/2 pl-5">
                    <a class="btn-white w-full block" href="settlement">點數結算</a>
                </div>
            </div>

            <div class="flex flex-wrap mt-10">
                <div class="w-1/2 pr-5">
                    <a class="btn-white w-full block" href="logoutDo">登出</a>
                </div>
            </div>

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    // vueListing.data.condition = Object.assign(vueListing.data.condition, {
    // 	id: null,
    // 	name: null,
    // 	lineUserID: null,
    // 	roleID: null,
    // });
    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
