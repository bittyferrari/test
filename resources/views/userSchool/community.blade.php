@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

<style>
    #footer-spacing {
        height: 108px !important;
    }
</style>
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto">

        <!-- <div class="bg-black relative text-white rounded-b-20 pt-2 pb-8">
            <div class="text-14 mb-8 text-center">總太地產 心生活美學聚落</div>
            <div class="flex items-center justify-center">
                <img :src="self.photoLine" class="w-16 rounded-full mr-5">
                <div class="text-24">@{{ self.name }}</div>
            </div>
        </div>

        <div class="h-10"></div> -->

        <!-- <div class="mx-auto w-4/5 py-3 bg-white rounded-5 shadow-3 px-6 text-18"> -->
        <div class="bg-white shadow-3 px-6 text-18">

            <a class="flex w-full items-center py-3" :href="'course?communityID=' + x.id" v-for="(x, i) of items">
                <!-- <img src="/img/icon-73.png" class="w-6 mr-3"> -->
                <div>@{{ x.name }}</div>
                <img src="/img/icon-56.png" class="ml-auto h-4  ">
            </a>

        </div>

        <!-- <div class="mx-auto w-4/5">
            <div class="text-white mt-5 text-18">
                您好：<br>
                為因應「新冠肺炎」(COVID-19)疫情,
                需請您如實填寫相關個人資料以備防疫使用, 謝謝您!
            </div>
        </div> -->

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>
    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
