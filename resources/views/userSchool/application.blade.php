@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

<style>
    #footer-spacing {
        height: 108px !important;
    }
</style>
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto min-h-screen bg-f7f7f7  ">

        <div class="px-5 py-5 bg-white">

            <div class="text-16 font-bold mb-3">欲報名之課程</div>

            <div class="text-16 text-red font-bold">@{{ course.name }}</div>

        </div>


        <div class="h-5"></div>


        <div class="px-5 py-5 bg-white">

            <div class="text-16 font-bold mb-5">報名資料</div>

            <div class="text-16 mb-2 text-808080">姓名</div>
            <input v-model="item.name" class="form-control bg-E7E7E7" placeholder="請填寫真實姓名" readonly>
            <div class="h-5"></div>
            <div class="text-16 mb-2 text-808080">電話</div>
            <input v-model="item.phone" class="form-control bg-E7E7E7" placeholder="請填寫聯絡電話" readonly>
            <div class="h-3"></div>

            <div class="text-16 mb-2 text-808080">社區 </div>

            <select class="form-control bg-E7E7E7" v-model.number="item.communityID" readonly disabled>
                <option :value="null">請選擇所在社區</option>
                <option v-for="(x, i) of option.community" :value="x.id">@{{ x.name }}</option>
            </select>
            <div class="h-3"></div>


        </div>
        <div class="h-5"></div>

        <template v-if="course.price > 0">
            <div class="px-5 py-5 bg-white">

                <div class="text-16 font-bold mb-2">課程費用</div>

                <div class="    px-3 py-5 text-16">
                    <div class="flex items-center justify-between">
                        <div class="text-808080">課程收費：</div>
                        <div class="">$@{{ course.price }}</div>
                    </div>

                    <div style="border: 1px dashed #F5ECED;" class="my-3"></div>
                    <div class="flex items-center justify-between font-bold">
                        <div>總付款金額：</div>
                        <div class="text-18 text-red">$@{{ course.price }}</div>
                    </div>
                </div>
            </div>

            <div class="h-5"></div>

            <div class="px-5 py-5 bg-white">

                <div class="text-20 text-center my-6   font-bold">
                    本次交易抵扣點數
                </div>
                <div class="border-F5ECED border px-5 py-3 text-24 text-center text-red font-bold" :class="{'bg-FBEBEF': isNotEnough}">
                    @{{ course.price }}
                </div>

                <div class="h-5"></div>

                <div v-if="isNotEnough" class="text-center font-bold text-red text-15">
                    *目前點數不足，無法完成交易！
                </div>

                <div class="h-5"></div>

            </div>

            <div class="h-5"></div>
        </template>

        <div style="box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.25);" class="bg-white py-3">

            <div class="flex ">

                <div class="w-1/2 px-3">
                    <a class="w-full btn-outline-red block" href="/userSchool/main">返回</a>
                </div>
                <div class="w-1/2 px-3">
                    <button class="w-full btn-red" @click="submitDo()">報名完成
                    </button>
                </div>
            </div>

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.data = Object.assign(vueItem.data, {
        item: {
            communityID: null,
            courseID: vueData.course.id,
            name: '',
            phone: '',
        },
        isNotEnough: false,
    });

    vueItem.mounted = function () {

        this.item.name = this.self.name;
        this.item.phone = this.self.phone;
        this.item.communityID = this.self.communityID;

        if (this.self.isCompleteRegister != 1) {
            this.alert('需完成註冊才能報名', function () {
                document.location = '/user/register';
            });
        }

    }

    vueItem.methods = Object.assign(vueItem.methods, {

        submitDo() {

            var isValid = true;

            var messages = [];

            if (this.isEmpty(this.item.name)) {
                messages.push('請填寫姓名');
                isValid = false;
            }
            if (this.isEmpty(this.item.phone)) {
                messages.push('請填寫電話');
                isValid = false;
            }
            if (this.isEmpty(this.item.communityID)) {
                messages.push('請選擇社區');
                isValid = false;
            }

            if (isValid) {

                const data = this.item;

                this.$http.post('updateApplicationDo', data).then(function (r) {
                    const body = r.body;
                    // this.items = body.data;

                    switch (body.statusID) {
                        case 1:
                            this.alert('找不到課程或是已下架');
                            break;
                        case 2:
                            this.alert('點數不足');
                            this.isNotEnough = true;
                            break;
                        case 3:
                            this.alert('報名已額滿');
                            break;
                        case 4:
                            this.alert('已經報過名了');
                            break;
                        default:
                            if (body.data.isAlternate) {
                                this.alert('報名成功, 候補名單', function () {
                                    document.location = '/userSchool/my';
                                });
                            } else {
                                this.alert('已成功預約此課程，可至【我的課程】中查看', function () {
                                    document.location = '/userSchool/my?typeID2=2';
                                });
                            }
                            break;
                    }

                });

            } else {
                // var message = messages.join('\n');
                var message = messages.join(', ');
                this.alert(message);
            }

        }
    });



    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
