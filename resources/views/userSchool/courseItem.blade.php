@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

<style>
    #footer-spacing {
        height: 108px !important;
    }

    .tab-active {
        /* background-color: #FFF; */
        /* box-shadow: 0px 2.66667px 2.66667px rgba(0, 0, 0, 0.25); */
        border-bottom: 3px solid #D4261F;

    }
</style>
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto min-h-screen bg-white  ">

        <div class="w-full relative" style="background:#eee">
            <splide :options="splideOptions">
                <splide-slide v-for="(x, i) of item.photos" :key="i">

                    <div class="w-full" style="height:100%;">

                        <div style="width:100%; height:100%; background-size:contain; background-repeat: no-repeat; background-position: center;"
                            class="text-center d-flex align-items-center" :style="{backgroundImage:getBackgroundUrl(x.file)}">
                        </div>
                    </div>
                </splide-slide>
            </splide>

            <!-- additional blabla -->
            <div class="w-12 h-12 absolute pointer" style="right: 10px; top: 10px" @click="likeDo(item)">
                <img src="/img/icon-94.png" class="w-full" v-if="isLike">
                <img src="/img/icon-93.png" class="w-full" v-else>
            </div>

            <img src="/img/icon-104.png" class="absolute h-12" style="left: 0px; bottom: 0px" v-if="statusID == 1">
            <img src="/img/icon-105.png" class="absolute h-12" style="left: 0px; bottom: 0px" v-if="statusID == 2">
            <img src="/img/icon-106.png" class="absolute h-12" style="left: 0px; bottom: 0px" v-if="statusID == 3">

        </div>

        <!--
        <div class="w-full relative" style="">
            <div style="padding-bottom: calc(100% / 100 * 100)"></div>

            <img :src="item.photo | asset" class=" cover">


            <div class="w-12 h-12 absolute pointer" style="right: 10px; top: 10px" @click="likeDo(item)">
                <img src="/img/icon-94.png" class="w-full" v-if="isLike">
                <img src="/img/icon-93.png" class="w-full" v-else>
            </div>

            <img src="/img/icon-104.png" class="absolute h-12" style="left: 0px; bottom: 0px" v-if="statusID == 1">
            <img src="/img/icon-105.png" class="absolute h-12" style="left: 0px; bottom: 0px" v-if="statusID == 2">
            <img src="/img/icon-106.png" class="absolute h-12" style="left: 0px; bottom: 0px" v-if="statusID == 3">

        </div>
 -->

        <div class="h-6"></div>

        <div class="px-5">

            <div class="text-20 font-bold">@{{ item.name }}</div>
            <div class="h-5"></div>

            <div class="text-666">
                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-81.png" class="h-5">
                    </div>
                    <div>
                        <!-- 時間：@{{ item.timeFrom | datetimeWeek }} -->
                        @{{ item.timeFrom | datetimeWeek }}
                    </div>
                </div>
                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-84.png" class="h-5">
                    </div>
                    <div>
                        <!-- 社區：@{{ communityText }} -->
                        <!-- @{{ communityText }} @{{ item.location }} -->
                        @{{ item.location }}
                    </div>
                </div>
                <!-- <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-84.png" class="h-5">
                    </div>
                    <div>
                        地點：@{{ item.location }}
                    </div>
                </div> -->

                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-86.png" class="h-5">
                    </div>
                    <div>
                        @{{ item.countAvailable }}人
                    </div>
                    <div class="ml-2 border py-0 px-1 border-red text-red">剩餘名額：@{{ countRemain }}</div>

                </div>

                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-85.png" class="h-5">
                    </div>
                    <div>
                        @{{ item.price }}元/人
                    </div>
                </div>
                <!--
                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-87.png" class="h-5">
                    </div>
                    <div>
                        講師：@{{ teacherText }}
                    </div>
                </div> -->

                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-88.png" class="h-5">
                    </div>
                    <div>
                        @{{ item.signupTo | datetimeWeek }}報名截止
                    </div>
                </div>

                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-83.png" class="h-5">
                    </div>
                    <div>

                        @{{ communityText }}可選課

                        <!-- 是否限制同社區可選課：
                        <span v-if="item.communityID == null">無</span>
                        <span v-else>@{{ typeText('community', item.communityID) }}</span>
                         -->
                    </div>
                </div>

                <div class="flex mb-2 items-center" v-if="item.hasOnline">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-82.png" class="h-5">
                    </div>
                    <div>
                        有線上課程
                    </div>
                </div>

                <!--
                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-82.png" class="h-5">
                    </div>
                    <div>
                        線上課程：
                        <span v-if="item.url == null">無</span>
                        <span v-else><a :href="item.url" target="_blank">@{{ item.url }}</a></span>
                    </div>
                </div> -->

            </div>

            <div class="h-5"></div>
            <div v-if="teacher" class="mb-5">
                <div class="flex items-center">

                    <div class="w-12 mr-3">
                        <div class="rounded-full bg-f3f4f6 w-12" style="padding-bottom: 100%; background-size: cover; background-position: center;"
                            :style="{ backgroundImage: 'url(/storage/photo/' + teacher.photo + ')'}"></div>
                    </div>
                    <div class="text-16 mr-2 font-bold">@{{ teacher.name }}</div>
                    <div class="text-14 text-666">@{{ teacher.title }}</div>
                </div>
            </div>


            <!-- <div class="flex flex-wrap rounded-full bg-E7E7E7 items-center">

                <div class="rounded-full py-2 text-center w-1/3 pointer" :class="{ 'tab-active': tabID == 1 }" @click="tabID = 1">
                    課程說明
                </div>
                <div class="rounded-full py-2 text-center w-1/3 pointer" :class="{ 'tab-active': tabID == 2 }" @click="tabID = 2">
                    講師介紹
                </div>
                <div class="rounded-full py-2 text-center w-1/3 pointer" :class="{ 'tab-active': tabID == 3 }" @click="tabID = 3">
                    注意事項
                </div>

            </div> -->


        </div>

        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxx -->

        <div class="flex flex-wrap rounded-full bg-fffafa items-center">

            <div class="  py-2 text-center w-1/3 pointer" :class="{ 'tab-active': tabID == 1 }" @click="tabID = 1">
                課程說明
            </div>
            <div class="  py-2 text-center w-1/3 pointer" :class="{ 'tab-active': tabID == 2 }" @click="tabID = 2">
                講師介紹
            </div>
            <div class="  py-2 text-center w-1/3 pointer" :class="{ 'tab-active': tabID == 3 }" @click="tabID = 3">
                注意事項
            </div>

        </div>

        <div class="h-3"></div>

        <div class="px-5">
            <template v-if="tabID == 1">
                <!-- <div>@{{ item.content }}</div> -->
                <div v-html="item.content"></div>
            </template>

            <template v-if="tabID == 2">
                <!-- <div>@{{ teacher.content }}</div> -->
                <div class="h-10"></div>

                <div v-for="(x, i) of teachers" class="mb-10">
                    <a :href="'teacherItem?id=' + x.id">
                        <div class="w-1/2 mx-auto">
                            <div class="rounded-full bg-f3f4f6 w-full" style="padding-bottom: 100%; background-size: cover; background-position: center;"
                                :style="{ backgroundImage: 'url(/storage/photo/' + x.photo + ')'}"></div>
                        </div>
                    </a>

                    <div class="text-center mt-5">
                        <div class="text-20 font-bold mb-">@{{ x.name }}</div>
                        <div class="h-3"></div>
                        <div class="text-16">@{{ x.title }}</div>
                        <div class="h-3"></div>

                        <div class="text-14" v-if="x.url">
                            <a :href="x.url" class="font-bold text-red">講師介紹連結</a>
                        </div>

                        <div class="h-3"></div>
                    </div>

                </div>

            </template>

            <template v-if="tabID == 3">

                <!-- <div>@{{ item.notice }}</div> -->

                <div v-html="item.notice"></div>

                <!--
                <ol style="list-style: disc" class="pl-5">
                    <li>了解質性研究專案的完整樣貌，有架構地規劃計畫</li>
                    <li>學習從應用目標制定問題，讓洞察成果更對準實務應用</li>
                    <li>掌握訪談操作技巧，有方向地打造一場順暢的訪談</li>
                    <li>具備分析基礎思維，避免辛苦收集資料但不知如何解讀</li>
                </ol> -->

            </template>

        </div>


        <div class="h-8"></div>
        <div style="box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.25);" class="py-3">

            <div class="flex ">

                <div class="w-full px-3">
                    <!-- <button class="w-full btn-red" @click="submitDo()">預約課程
                    </button> -->
                    <a :href="'application?courseID=' + item.id" class="w-full block btn-red">報名課程
                    </a>

                </div>
            </div>

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.data = Object.assign(vueItem.data, {
        tabID: 1,
        splideOptions: {
            pagination: true,
            type: 'loop',
            arrows: true,
            perPage: 1,
            // gap: '10px',
            fixedWidth: '100%',
            heightRatio: 1,
        }

    });

    vueItem.computed = Object.assign(vueItem.computed, {

        communityText() {
            var items = [];
            for (const i in this.item.communityIDs) {
                const x = this.item.communityIDs[i];
                const temp = this.option.community.find((z) => z.id == x);
                if (temp) {
                    items.push(temp.name);
                }
            }
            var text = items.join(', ');
            return text;
        },

        teacherText() {
            var items = [];
            for (const i in this.item.teacherIDs) {
                const x = this.item.teacherIDs[i];
                const temp = this.option.teacher.find((z) => z.id == x);
                if (temp) {
                    items.push(temp.name);
                }
            }
            var text = items.join(', ');
            return text;
        }

    });

    vueItem.methods = Object.assign(vueItem.methods, {
        getBackgroundUrl(x) {
            console.log(this.uploadUrl);
            return 'url(' + this.uploadUrl + x + ')';
        },
        likeDo(q) {
            const data = {
                courseID: q.id
            };
            this.$http.post('/_helper/courseLikeDo', data).then(function (r) {
                this.isLike = r.body.data.isLike;
            });

        }
    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
