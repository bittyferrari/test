@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

<style>
    #footer-spacing {
        height: 108px !important;
    }

    .tab-active {
        background-color: #FFF;
        box-shadow: 0px 2.66667px 2.66667px rgba(0, 0, 0, 0.25);
        border: 1px solid #808080;

    }
</style>
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto min-h-screen bg-white  ">

        <img :src="item.photo | asset" class=" w-full">

        <div class="h-6"></div>

        <div class="px-5">

            <div class="text-20 font-bold">@{{ item.name }}</div>
            <div class="h-3"></div>

            <div class="text-666">
                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-81.png" class="h-5">
                    </div>
                    <div>
                        時間：@{{ item.timeFrom | datetimeWeek }}
                    </div>
                </div>
                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-84.png" class="h-5">
                    </div>
                    <div>
                        社區：@{{ communityText }}
                    </div>
                </div>
                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-84.png" class="h-5">
                    </div>
                    <div>
                        地點：@{{ item.location }}
                    </div>
                </div>

                <!-- <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-87.png" class="h-5">
                    </div>
                    <div>
                        講師：@{{ teacher.name }}
                    </div>
                </div> -->

                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-87.png" class="h-5">
                    </div>
                    <div>
                        講師：@{{ teacherText }}
                    </div>
                </div>

                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-86.png" class="h-5">
                    </div>
                    <div>
                        名額：@{{ item.countPerson }}
                    </div>
                </div>
                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-85.png" class="h-5">
                    </div>
                    <div>
                        費用：@{{ item.price }}
                    </div>
                </div>
                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-88.png" class="h-5">
                    </div>
                    <div>
                        報名截止日期：@{{ item.signupTo | datetimeWeek }}
                    </div>
                </div>
                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-83.png" class="h-5">
                    </div>
                    <div>
                        是否限制同社區可選課：
                        <span v-if="item.communityID == null">無</span>
                        <span v-else>@{{ typeText('community', item.communityID) }}</span>
                    </div>
                </div>
                <div class="flex mb-2 items-center">
                    <div class="w-5 mr-2">
                        <img src="/img/icon-82.png" class="h-5">
                    </div>
                    <div>
                        線上課程：
                        <span v-if="item.url == null">無</span>
                        <span v-else><a :href="item.url" target="_blank">@{{ item.url }}</a></span>
                    </div>
                </div>

            </div>
            <div class="h-3"></div>


            <div class="flex flex-wrap rounded-full bg-E7E7E7 items-center">

                <div class="rounded-full py-2 text-center w-1/3 pointer" :class="{ 'tab-active': tabID == 1 }" @click="tabID = 1">
                    課程說明
                </div>
                <div class="rounded-full py-2 text-center w-1/3 pointer" :class="{ 'tab-active': tabID == 2 }" @click="tabID = 2">
                    講師介紹
                </div>
                <div class="rounded-full py-2 text-center w-1/3 pointer" :class="{ 'tab-active': tabID == 3 }" @click="tabID = 3">
                    注意事項
                </div>

            </div>

            <div class="h-3"></div>

            <template v-if="tabID == 1">
                <div>@{{ item.content }}</div>
            </template>

            <template v-if="tabID == 2">
                <div>@{{ teacher.content }}</div>
            </template>

            <template v-if="tabID == 3">

                <ol style="list-style: disc" class="pl-5">
                    <li>了解質性研究專案的完整樣貌，有架構地規劃計畫</li>
                    <li>學習從應用目標制定問題，讓洞察成果更對準實務應用</li>
                    <li>掌握訪談操作技巧，有方向地打造一場順暢的訪談</li>
                    <li>具備分析基礎思維，避免辛苦收集資料但不知如何解讀</li>
                </ol>

            </template>

        </div>

        <div class="h-8"></div>
        <div style="box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.25);" class="py-3">

            <div class="flex ">

                <div class="w-full px-3">
                    <!-- <button class="w-full btn-red" @click="submitDo()">預約課程
                    </button> -->
                    <a :href="'application?courseID=' + item.id" class="w-full block btn-red">預約課程
                    </a>

                </div>
            </div>

        </div>


    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>


    vueItem.data = Object.assign(vueItem.data, {
        tabID: 1,
    });

    vueItem.computed = Object.assign(vueItem.computed, {

        communityText() {
            var items = [];
            for (const i in this.item.communityIDs) {
                const x = this.item.communityIDs[i];
                const temp = this.option.community.find((z) => z.id == x);
                if (temp) {
                    items.push(temp.name);
                }
            }
            var text = items.join(', ');
            return text;
        },

        teacherText() {
            var items = [];
            for (const i in this.item.teacherIDs) {
                const x = this.item.teacherIDs[i];
                const temp = this.option.teacher.find((z) => z.id == x);
                if (temp) {
                    items.push(temp.name);
                }
            }
            var text = items.join(', ');
            return text;
        }

    });



    vueItem.methods = Object.assign(vueItem.methods, {
        likeDo(q) {

            const data = {
                courseID: q.id
            };
            this.$http.post('/_helper/courseLikeDo', data).then(function (r) {
                q.isLike = r.body.data.isLike;
            });

        }
    });


    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
