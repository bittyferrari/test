@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

<style>
    #footer-spacing {
        height: 88px !important;
    }
</style>
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto min-h-screen bg-white px-5 pb-5">

        <div class="h-6"></div>
        <input v-model="condition.name" class="form-control bg-white" placeholder="搜尋講師" @keyup.enter="getListing()">
        <div class="h-6"></div>

        <a class="flex w-full p-3 border mb-5 items-center py-3 rounded-14" :href="'teacherItem?id=' + x.id" v-for="(x, i) of items">
            <div class="w-1/2">
                <div class="rounded-full bg-f3f4f6 w-full" style="padding-bottom: 100%; background-size: cover; background-position: center;" :style="{ backgroundImage: 'url(/storage/photo/' + x.photo + ')'}"></div>
            </div>
            <div class="w-1/2 px-5 ">
                <div class="font-bold text-18 mb-2">@{{ x.name }}</div>
                <!-- <div class="text-12 text-666">@{{ x.content }}</div> -->
                <div class="text-12 text-666">@{{ x.title }}</div>
            </div>
        </a>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueListing.data = Object.assign(vueListing.data, {

        getListingUrl: 'getListingTeacher',

    });



    var vue = new Vue(vueListing);
</script>
<!-- -------------------- -->

@stop
