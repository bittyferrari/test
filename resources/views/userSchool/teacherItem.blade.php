@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

<style>
    #footer-spacing {
        height: 108px !important;
    }
</style>
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto min-h-screen bg-white  ">


        <div class="h-6"></div>

        <div class="px-5">

            <div class="w-1/2 mx-auto">
                <div class="rounded-full bg-f3f4f6 w-full" style="padding-bottom: 100%; background-size: cover; background-position: center;"
                    :style="{ backgroundImage: 'url(/storage/photo/' + item.photo + ')'}"></div>
            </div>

            <div class="text-center mt-5">
                <div class="text-20 font-bold mb-">@{{ item.name }}</div>
                <div class="h-3"></div>
                <div class="text-16">@{{ item.title }}</div>
                <div class="h-3"></div>

                <div class="text-14" v-if="item.url">
                    <a :href="item.url" class="font-bold text-red">講師介紹連結</a>
                </div>

                <div class="h-3"></div>
            </div>

        </div>

        <!-- <div class="mx-auto w-4/5 py-3 bg-white rounded-5 shadow-3 px-6 text-18"> -->
        <!-- <div class="    px-6 text-18"> -->

        <div class="h-5"></div>

        <div class="px-5 py-5">
            <div class="text-20 font-bold">開課清單</div>

            <div class="h-3"></div>

            <div v-if="items.length <= 0">
                無
            </div>

            @include('/userSchool/_include/courseItem')

        </div>

        <!-- </div> -->

        <!-- <div class="mx-auto w-4/5">
            <div class="text-white mt-5 text-18">
                您好：<br>
                為因應「新冠肺炎」(COVID-19)疫情,
                需請您如實填寫相關個人資料以備防疫使用, 謝謝您!
            </div>
        </div> -->

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueItem.methods = Object.assign(vueItem.methods, {
        likeDo(q) {

            const data = {
                courseID: q.id
            };
            this.$http.post('/_helper/courseLikeDo', data).then(function (r) {
                q.isLike = r.body.data.isLike;
            });

        }
    });



    var vue = new Vue(vueItem);
</script>
<!-- -------------------- -->

@stop
