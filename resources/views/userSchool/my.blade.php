@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

<style>
    #footer-spacing {
        height: 88px !important;

    }
</style>
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto bg-white min-h-screen pb-5">

        <div class="flex text-center bg-F8F8F8 text-16">
            <template v-if="condition.typeID2 == 1">
                <div class="pointer w-1/2 py-5 text-red font-bold border-b-2 border-b-red" @click="condition.typeID2 = 1; getListing()">已收藏課程</div>
                <div class="pointer w-1/2 py-5" @click="condition.typeID2 = 2; getListing();">已預約課程</div>
            </template>
            <template v-else>
                <div class="pointer w-1/2 py-5" @click="condition.typeID2 = 1; getListing();">已收藏課程</div>
                <div class="pointer w-1/2 py-5 text-red font-bold border-b-2 border-b-red" @click="condition.typeID2 = 2; getListing()">已預約課程</div>
            </template>
        </div>

        <div class="h-3"></div>

        <div class="px-5">

            <div class="text-16 font-bold">課程查詢</div>
            <div class="h-4"></div>
            <div class="flex  text-center">

                <div class="w-1/3">
                    <div class="rounded-full p-5 border border-EBF0FF inline-block pointer" @click="condition.typeID = 1; getListing()">
                        <img src="/img/icon-78.png" class="h-10">
                    </div>
                    <div>
                        尚未開課
                    </div>
                </div>

                <div class="w-1/3">
                    <div class="rounded-full p-5 border border-EBF0FF inline-block pointer" @click="condition.typeID = 2; getListing()">
                        <div class="w-10 mx-auto">
                            <img src="/img/icon-79.png" class="mx-auto h-10">
                        </div>
                    </div>
                    <div>
                        開課中
                    </div>

                </div>

                <div class="w-1/3">
                    <div class="rounded-full p-5 border border-EBF0FF inline-block pointer" @click="condition.typeID = 3; getListing()">
                        <img src="/img/icon-80.png" class="h-10">
                    </div>
                    <div>
                        已結束課程
                    </div>

                </div>


            </div>

            <div class="h-10"></div>

            <div class="text-16 font-bold">全部課程</div>
            <div class="h-2"></div>


            <div v-if="items.length <= 0">
                無
            </div>
            <!--
            <div class="w-full p-3 border mb-5 items-center py-3 rounded-14" :href="'teacherItem?id=' + x.id" v-for="(x, i) of items">

                <div class="w-full relative" style="">
                    <div style="padding-bottom: calc(100% / 100 * 100)"></div>
                    <img :src="x.photo | asset" class="cover rounded-10">

                    <div class="w-12 h-12 absolute pointer" style="right: 10px; top: 10px" @click="likeDo(x)">
                        <img src="/img/icon-94.png" class="w-full" v-if="x.isLike">
                        <img src="/img/icon-93.png" class="w-full" v-else>

                    </div>
                </div>

                <div class="h-3"></div>

                <div class="font-bold text-20">@{{ x.name }}</div>
                <div class="h-3"></div>

                <div class="">時間：@{{ x.timestampFrom | time }} ~ @{{ x.timestampTo | time }}</div>
                <div class="">地點：@{{ x.community.name }}</div>
                <div class="">講師：@{{ x.teacher.name }}</div>

                <div class="mt-10">
                    <a class="block mx-auto w-2/3 btn-red" :href="'application?courseID=' + x.id">報名課程</a>
                </div>

            </div> -->
            @include('/userSchool/_include/courseItem')

        </div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueListing.data = Object.assign(vueListing.data, {

        getListingUrl: 'getListingCourse',

    });


    vueListing.data.condition = Object.assign(vueListing.data.condition, {
        typeID: 1,
        typeID2: 1
    });


    vueListing.methods = Object.assign(vueListing.methods, {
        likeDo(q) {
            const data = {
                courseID: q.id
            };
            this.$http.post('/_helper/courseLikeDo', data).then(function (r) {
                q.isLike = r.body.data.isLike;
            });

        }
    });


    var vue = new Vue(vueListing);
</script>
<!-- -------------------- -->

@stop
