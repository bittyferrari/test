<div class="w-full overflow-hidden border mb-5 items-center rounded-6 shadow-4" :href="'courseItem?id=' + x.id" v-for="(x, i) of items">

    <div class="w-full relative" style="">
        <div style="padding-bottom: calc(100% / 100 * 100)"></div>

        <a class="" :href="'courseItem?id=' + x.id">
            <img :src="x.photo | asset" class="cover">
        </a>

        <div class="w-12 h-12 absolute pointer" style="right: 10px; top: 10px" @click="likeDo(x)">
            <img src="/img/icon-94.png" class="w-full" v-if="x.isLike">
            <img src="/img/icon-93.png" class="w-full" v-else>
        </div>

        <img src="/img/icon-104.png" class="absolute h-12" style="left: 0px; bottom: 0px" v-if="x.statusID == 1">
        <img src="/img/icon-105.png" class="absolute h-12" style="left: 0px; bottom: 0px" v-if="x.statusID == 2">
        <img src="/img/icon-106.png" class="absolute h-12" style="left: 0px; bottom: 0px" v-if="x.statusID == 3">

    </div>

    <div class="h-3"></div>

    <div class="px-5">

        <div class="font-bold text-20">@{{ x.name }}</div>
        <div class="h-3"></div>

        <div class="flex items-center mb-3">
            <img src="/img/icon-103.png" class="mr-2 h-5">
            <div class="">時間：@{{ x.timestampFrom | datetimeWeek }}</div>
        </div>
        <div class="flex items-center mb-3">
            <img src="/img/icon-84.png" class="mr-2 h-5">
            <!-- <div class="">地點：@{{ x.location }}</div> -->
            <!-- <div class="">地點：@{{ typeText('community', x.communityID) }} @{{ x.location }}</div> -->
            <div class="">地點：@{{ x.locationText }}</div>
        </div>
        <div class="flex items-center mb-3">
            <img src="/img/icon-86.png" class="mr-2 h-5">
            <div class="">人數：@{{ x.countAvailable }}</div>
            <div class="ml-2 border py-0 px-1 border-red text-red">剩餘名額：@{{ x.countRemain }}</div>
        </div>

        <div class="h-5"></div>
        <div v-if="x.teacher">
            <div class="flex items-center">

                <div class="w-20 mr-3">
                    <div class="rounded-full bg-f3f4f6 w-20" style="padding-bottom: 100%; background-size: cover; background-position: center;"
                        :style="{ backgroundImage: 'url(/storage/photo/' + x.teacher.photo + ')'}"></div>
                </div>
                <div class="text-16 mr-2 font-bold">@{{ x.teacher.name }}</div>
                <div class="text-14 text-666">@{{ x.teacher.title }}</div>

            </div>
        </div>

    </div>

    <div class="mt-6">

        <template v-if="x.isActive != 1">
            <a class="block bg-F0F0F0 py-4 text-center text-red" v-if="x.statusID == 2">課程已取消</a>
        </template>

        <template v-else>
            <template v-if="x.isSignup">
                <a class="block bg-582602 py-4 text-center text-white" v-if="x.statusID == 2" :href="'courseItem?id=' + x.id">已成功報名</a>
            </template>
            <template v-else>
                <a class=" block bg-red py-4 text-center text-white" v-if="x.statusID == 1" :href="'courseItem?id=' + x.id">詳細資訊</a>
                <a class="block bg-F0F0F0 py-4 text-center text-red" v-if="x.statusID == 2" :href="'courseItem?id=' + x.id">報名已截止</a>
            </template>
        </template>
    </div>

</div>
