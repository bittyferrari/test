@extends('__layout/main')

<!-- -------------------- -->

@section('head')
<!-- -------------------- -->

<style>
    #footer-spacing {
        height: 88px !important;

    }
</style>
@stop

<!-- -------------------- -->
@section('content')

<div id="vue" v-cloak>
    <div class="max-w-md mx-auto bg-white min-h-screen pb-5">

        <div class="h-3"></div>

        <div class="px-5">

            <div class="h-6"></div>
            <input v-model="condition.name" class="form-control search" placeholder="搜尋課程" @keyup.enter="getListing()">
            <div class="h-6"></div>

            <div class="text-16 font-bold">課程分類</div>
            <div class="h-4"></div>
            <div class="flex  text-center">

                <div class="w-1/4">
                    <div class="px-3 inline-block pointer" @click="condition.typeID = 1; getListing()">

                        <img src="/img/icon-96.png" class="w-full" v-if="condition.typeID == 1">
                        <img src="/img/icon-100.png" class="w-full" v-else>
                    </div>
                    <div>
                        全部
                    </div>
                </div>


                <div class="w-1/4">
                    <div class="px-3 inline-block pointer" @click="condition.typeID = 2; getListing()">

                        <img src="/img/icon-95.png" class="w-full" v-if="condition.typeID == 2">
                        <img src="/img/icon-99.png" class="w-full" v-else>
                    </div>
                    <div>
                        尚未開課
                    </div>
                </div>



                <div class="w-1/4">
                    <div class="px-3 inline-block pointer" @click="condition.typeID = 3; getListing()">

                        <div class="  mx-auto">
                            <img src="/img/icon-97.png" class="w-full" v-if="condition.typeID == 3">
                            <img src="/img/icon-101.png" class="w-full" v-else>
                        </div>
                    </div>
                    <div>
                        開課中
                    </div>

                </div>

                <div class="w-1/4">
                    <div class="px-3 inline-block pointer" @click="condition.typeID = 4; getListing()">
                        <img src="/img/icon-98.png" class="w-full" v-if="condition.typeID == 4">
                        <img src="/img/icon-102.png" class="w-full" v-else>

                    </div>
                    <div>
                        已結束課程
                    </div>

                </div>

            </div>

            <div class="h-10"></div>

            <div class="text-16 font-bold">全部課程</div>
            <div class="h-2"></div>


            <div v-if="items.length <= 0">
                無
            </div>
            @include('/userSchool/_include/courseItem')

        </div>

        <div class="h-5"></div>

        <!-- page -->
        <ul class="flex justify-center items-center" style="justify-content: start;">

            <img class="w-4 pointer mr-2" src="/img/icon-107.png" @click="prevPage()" style="transform: rotate(180deg);"
                :class="{'opacity-50': condition._page == 1 }">

            <li v-for="x of pages" @click="changePage(x)" class="mx-2 text-18 font-bold pointer px-2"
                :class="{ 'border border-red rounded-full text-red': (x == condition._page)}">
                @{{ x }}
            </li>
            <img class="w-4 pointer ml-2" src="/img/icon-107.png" @click="nextPage()" :class="{'opacity-50': condition._page == pageTotal }">

        </ul>

        <div class="h-5"></div>

    </div>
</div>
@stop
<!-- -------------------- -->

@section('js')

<script>

    vueListing.data = Object.assign(vueListing.data, {
        getListingUrl: 'getListingCourse',
    });

    vueListing.data.condition = Object.assign(vueListing.data.condition, {
        typeID: 1,
        typeID2: null,
        name: '',
        communityID: null,
        // _pageSize: 1,
        _pageSize: 5,
    });


    vueListing.methods = Object.assign(vueListing.methods, {
        likeDo(q) {
            const data = {
                courseID: q.id
            };
            this.$http.post('/_helper/courseLikeDo', data).then(function (r) {
                q.isLike = r.body.data.isLike;
            });

        }
    });


    var vue = new Vue(vueListing);
</script>
<!-- -------------------- -->

@stop
