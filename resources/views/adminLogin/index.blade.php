<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Admin Login</title>
    <link href="/css/styles.css" rel="stylesheet" />

    <!-- CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <style>
        *[v-cloak] {
            display: none;
        }

        body {
            background-color: #444;
        }
    </style>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script> -->
</head>

<body class=" ">
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <main id="vue" v-cloak>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-5">
                            <div class="card shadow-lg border-0 rounded-lg mt-5">
                                <div class="card-header bg-primary">
                                    <h3 class="text-center font-weight-light my-4 text-white">Login</h3>
                                </div>
                                <div class="card-body">
                                    <!-- <form> -->
                                    <div class="form-group">
                                        <label class="small mb-1" for="inputEmailAddress">Email</label><input class="form-control py-4" id="inputEmailAddress"
                                            type="email" placeholder="Enter email address" v-model="email" @keyup.enter="submitDo()"
                                            :disabled="isResetPassword" />
                                    </div>
                                    <div class="form-group">
                                        <label class="small mb-1" for="inputPassword">Password</label><input class="form-control py-4" type="password"
                                            placeholder="Enter password" v-model="password" @keyup.enter="submitDo()" :disabled="isResetPassword" />
                                    </div>
                                    <div class="form-group" v-if="isResetPassword">
                                        <label class="small mb-1" for="inputPassword">New password</label><input class="form-control py-4" type="password"
                                            placeholder="Enter new password" v-model="passwordNew" />
                                    </div>

                                    <div class="form-group d-flex align-items-center justify-content-center mt-4 mb-0" v-if="!isResetPassword">
                                        <button class="btn btn-primary" @click="submitDo()">Login</button>
                                    </div>

                                    <div class="form-group d-flex align-items-center justify-content-center mt-4 mb-0" v-if="isResetPassword">
                                        <button class="btn btn-primary" @click="resetPasswordDo()">Reset password</button>
                                    </div>

                                    <div v-if="isResetPassword">
                                        <hr />
                                        New password at least 12 characters and contain characters from at least two of the following four categories.

                                        <div>
                                            <ul class="pl-4 mt-2">
                                                <li>Upper case (A through Z);</li>
                                                <li>Lower case (a through z);</li>
                                                <li>Digits (0-9);</li>
                                                <li>Special Characters (!, $, #, %, etc.).</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <!-- </form> -->
                                </div>
                                <!-- <div class="card-footer text-center">
										<div class="small"><a href="register.html">Need an account? Sign up!</a></div>
                                    </div>
                                     -->
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <!-- <div id="layoutAuthentication_footer">
				<footer class="py-4 bg-light mt-auto">
					<div class="container-fluid">
						<div class="d-flex align-items-center justify-content-between small">
							<div class="text-muted">Copyright &copy; Your Website 2019</div>
							<div>
								<a href="#">Privacy Policy</a>
								&middot;
								<a href="#">Terms &amp; Conditions</a>
							</div>
						</div>
					</div>
				</footer>
            </div>
             -->
    </div>
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script> -->
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> -->
    <!-- <script src="/js/scripts.js"></script> -->

    <script src="{{ mix('/app/app.js') }}"></script>

    <!-- ------------ -->
    {{ @printJson('vueData', $vueData)}}
    <!-- ------------ -->

    <script>
        var vue = new Vue({
            el: '#vue',
            data: {
                isResetPassword: false,
                // isResetPassword: true,
                email: '',
                password: '',
                passwordNew: '',
                env: vueData.env,
            },
            mounted() {
                if (this.env == 'local') {
                    // this.indexStep = 0;
                }
            },
            methods: {
                resetPasswordDo() {
                    // check new password

                    const passwordNew = this.passwordNew;

                    if (passwordNew.length < 12) {
                        alert('at least 12 characters');
                    } else {
                        let countMatch = 0;
                        if (passwordNew.search(/[a-z]/) != -1) {
                            countMatch++;
                            console.log('match a-z');
                        }
                        if (passwordNew.search(/[A-Z]/) != -1) {
                            countMatch++;
                            console.log('match A-Z');
                        }

                        if (passwordNew.search(/[0-9]/) != -1) {
                            countMatch++;
                            console.log('match 0-9');
                        }

                        if (passwordNew.search(/[!@#\$%\^\&+-]/) != -1) {
                            countMatch++;
                            console.log('match !@#$%^&*+=-0-9');
                        }

                        console.log(countMatch);

                        if (countMatch >= 2) {
                            const url = 'resetPasswordDo';
                            this.$http
                                .post(url, this._data)
                                .then(function (r) {
                                    const body = r.body;
                                    const statusID = body.statusID;

                                    switch (statusID) {
                                        // case 1:
                                        // 	alert('Login failed, email or password wrong.');
                                        // 	break;
                                        // case 2:
                                        // 	alert('reset password to continue.');
                                        // 	this.isResetPassword = true;
                                        // 	break;

                                        case 0:
                                            this.alert('Password reseted, use new password to login');
                                            location.reload();
                                            break;

                                        default:
                                            this.alert('Reset failed, please trry again');
                                            break;
                                    }
                                })
                                .catch(function (e) {
                                    this.alert('Reset failed, please trry again');
                                    location.reload();
                                });
                        } else {
                            this.alert('At least two of the following four categories.');
                        }
                    }
                },
                submitDo() {
                    const url = 'loginDo';
                    this.$http
                        .post(url, this._data)
                        .then(function (r) {
                            const body = r.body;
                            const statusID = body.statusID;

                            switch (statusID) {
                                case 1:
                                    this.alert('Login failed, email or password wrong.');
                                    break;
                                case 2:
                                    this.alert('reset password to continue.');
                                    this.isResetPassword = true;
                                    break;

                                default:
                                    switch (document.location.pathname) {
                                        case '/adminLogin/index':
                                        case '/adminLogin':
                                            document.location = '/_admin/dashboard/index';
                                            break;
                                        default:
                                            location.reload();
                                            break;
                                    }
                                    break;
                            }
                        })
                        .catch(function (e) {
                            this.alert('Login failed, please try again.');
                        });
                },
            },
        });
    </script>
</body>

</html>
