// require('./config');

import config from './config';

var vueItemData = {
	el: '#vue',
	data: {
		updateUrl: 'updateDo',
		item: {},
		uploadUrl: config.uploadUrl,
		notValidMessage: '',
		notValidMessages: [],

		text: {
			save: 'Save',
			saving: 'Saving..',
			back: 'Back to Listing',
		},
		isProcessing: false,
		responseSource: {},
		submitMessage: {
			0: '儲存成功',
		},
	},
	mounted() {},

	computed: {},

	methods: {
		onBeforeSave() {},
		onAfterSave() {},
		onSaveError() {},
		isPermission(q, moduleName = null) {
			// return true;
			if (this.admin.id == 1) {
				return true;
			} else {
				if (moduleName == null) {
					moduleName = this.moduleName;
				}
				const permissionID = moduleName + '-' + q;
				const temp = this.admin.permissions.find((z) => z == permissionID);
				return temp != null;
			}
		},

		getPhoto(x) {
			return this.uploadUrl + x;
		},

		checkCarLicense(x, key) {
			try {
				x[key] = x[key].replace(/[^a-z0-9]/gi, '');
				x[key] = x[key].toUpperCase();
			} catch (e) {
				console.log(e);
				// yada
			}
		},

		removeItem(x, index) {
			x.splice(index, 1);
		},

		setMainPhoto(index) {
			this.item.photo = this.item.photoJson[index].photo;
		},

		randomMd5() {
			return Math.random().toString(36).substr(2);
		},

		changePhoto(variable, key, accept = 'image/png, image/jpeg', maxSize = 1) {
			let self = this;
			let fileInput = this.$el.querySelector('.file');
			fileInput = document.createElement('input');
			fileInput.setAttribute('type', 'file');
			fileInput.setAttribute('accept', accept);
			// fileInput.setAttribute('accept', 'image/png, image/gif, image/jpeg, image/bmp, image/x-icon, image/svg+xml');
			fileInput.classList.add('d-none');
			fileInput.addEventListener('change', () => {
				if (fileInput.files != null && fileInput.files[0] != null) {
					// check file size first
					console.log(fileInput.files[0]);

					var fileSize = fileInput.files[0].size;
					// if (fileSize / 1024 > maxSize * 1024) {
					//     self.alert("File size can't above " + maxSize + "MB");
					//     return;
					// }

					const file = fileInput.files[0];

					if (accept == 'image/svg+xml') {
						// ignore width height check
						self.uploadFile(variable, key, file);
					} else {
						const _URL = window.URL || window.webkitURL;
						const img = new Image();
						img.onload = function () {
							if (this.width == this.height || true) {
								// _URL.revokeObjectURL(objectUrl);
								self.uploadFile(variable, key, file);
								console.log(this);
							} else {
							}
						};

						img.src = _URL.createObjectURL(file);
					}
				} else {
				}
			});
			this.$el.appendChild(fileInput);
			fileInput.click();
		},

		uploadFile(variable, key, file) {
			const self = this;
			//do upload file
			let formData = new FormData();
			console.log(file.name);
			formData.append('file', file, file.name);

			console.log('zzzzzzz');
			let xhr = new XMLHttpRequest();
			const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
			xhr.open('POST', '/_admin/_helper/uploadFile', true);
			xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);
			xhr.onload = function () {
				if (xhr.status === 200) {
					let json = JSON.parse(xhr.response);
					variable[key] = json.fileName;
					self.alert('Upload complete.');
				} else {
					self.alert('Upload failed.');
				}
			};
			xhr.onerror = function () {
				self.alert('Upload failed.');
			};
			xhr.send(formData);
		},


		uploadFileCallback(callback = null) {
			let self = this;
			let fileInput = this.$el.querySelector('.file');
			fileInput = document.createElement('input');
			fileInput.setAttribute('type', 'file');
			//fileInput.setAttribute('accept', accept);
			// fileInput.setAttribute('accept', 'image/png, image/gif, image/jpeg, image/bmp, image/x-icon, image/svg+xml');
			fileInput.classList.add('hidden');

			fileInput.addEventListener('change', () => {
				const returnData = {
					file: '',
					name: '',
					isSuccess: false,
				};
				if (fileInput.files != null && fileInput.files[0] != null) {
					const file = fileInput.files[0];

					console.log(file);
					returnData.name = file.name;

					let formData = new FormData();
					// formData.append('files', file, file.name);
					formData.append('file', file, file.name);
					let xhr = new XMLHttpRequest();
					const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
					xhr.open('POST', '/_admin/_helper/uploadFile', true);
					xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);

					xhr.onload = function () {
						if (xhr.status === 200) {
							returnData.isSuccess = true;
							let json = JSON.parse(xhr.response);
							//variable[key] = json.fileName;
							//variable[key] = json[0];
							//self.alert('Upload complete.');
							// returnData.file = json[0];

							if (callback != null) {
								// callback(returnData);
								callback(json);
							}
						} else {
							self.alert('Upload failed.');
						}
					};
					xhr.onerror = function () {
						self.alert('Upload failed.');
					};
					xhr.send(formData);
				} else {
				}
			});
			this.$el.appendChild(fileInput);
			fileInput.click();
		},



		getOptionName(x, id, defaultText) {
			if (!defaultText) {
				defaultText = '--';
			}
			var temp = x.find((z) => z.id == id);
			if (temp) {
				defaultText = temp.name;
			} else {
			}
			return defaultText;
		},

		typeText: function (key, id, defaultText = '--', fieldKey = 'name') {
			const temp = this.option[key].find((z) => z.id == id);
			if (temp) {
				defaultText = temp[fieldKey];
			}

			return defaultText;
		},

		getType(key, index) {
			let defaultObject = {};

			const temp = this.option[key].find((z) => z.id == index);
			if (temp) {
				defaultObject = temp;
			}
			return defaultObject;
		},

		/*
		uploadFile(event, key) {
			var self = this;
			var files = event.target.files;

			//single file veriosn
			if (files.length > 0) {
				var file = files[0];
				var reader = new FileReader();
				reader.onload = function() {
					var data = new FormData();
					// data.append('file', event.target.files);
					data.append('file', files[0]);
					self.$http.post('/_admin/_helper/uploadFile', data).then(function(r) {
						self.item[key] = r.body.fileName;
					});
				};
				reader.onerror = function() {};
				reader.readAsDataURL(file);
			}
		},
*/

		arrayMoveUp(x, i) {
			var b = x[i];
			x[i] = x[i - 1];
			x[i - 1] = b;
			x.push({});
			x.pop();
		},

		arrayMoveDown(x, i) {
			var b = x[i];
			x[i] = x[i + 1];
			x[i + 1] = b;
			x.push({});
			x.pop();
		},

		isEmpty(x) {
			return x === null || x === '' || typeof x == undefined;
		},

		uploadFiles(event, key) {
			// var self = this;
			var files = event.target.files;
			if (files.length > 0) {
				var data = new FormData();
				for (var i = 0; i < files.length; i++) {
					data.append('files[]', files[i]);
				}
				this.$http.post('/_admin/_helper/uploadFiles', data).then(function (r) {
					// console.log(r);
					for (var i in r.body) {
						var x = r.body[i];
						if (x.fileName) {
							var a = {
								photo: x.fileName,
							};
							this.item[key].push(a);
						}
					}

					if (this.item.photoJson.length <= 1) {
						this.item.photo = this.item.photoJson[0].photo;
					}
				});
			}
		},

		isValid() {
			let result = true;

			for (const i in this.fieldRequired) {
				const x = this.fieldRequired[i];
				if (this.isEmpty(this.item[x.field])) {
					result = false;
					this.notValidMessages.push(x.message);
				}
			}

			return result;
		},

		alert(message, callback = null) {
			let options = {
				okText: 'OK',
				backdropClose: true,
			};

			this.$dialog
				.alert(message, options)
				.then(function (dialog) {
					if (callback != null) {
						callback();
					}
				})
				.catch(function () {
					if (callback != null) {
						callback();
					}
				});
		},

		isInclude(q, qq) {
			let result = false;
			try {
				result = q.includes(qq);
			} catch (e) {
				// yada
			}
			return result;
		},

		submitDo() {
			this.notValidMessage = '';
			this.notValidMessages = [];
			if (this.isValid()) {
				this.isProcessing = true;
				this.$http
					.post(this.updateUrl, this._data)
					.then(function (res) {
						this.isProcessing = false;
						this.responseSource = res;

						if (res.body.statusID != 0) {
							this.onSaveError();
						} else {
							if (res.body.data) {
								var data = res.body.data;
								this.item.id = data.item.id;
								if (data.item.createdAt) {
									this.item.createdAt = data.item.createdAt;
								}
								this.alert('Save successed');
								this.onAfterSave();
							}
						}
					})
					.catch(function () {
						this.alert('Save failed');
						this.isProcessing = false;
						this.onSaveError();
					});
			} else {
				this.notValidMessage = this.notValidMessages.join('\n');
				window.alert(this.notValidMessage);
				// this.alert('Please fill in all required field.');
			}
		},
	},
};

// module.exports = vueItemData;
export default vueItemData;
