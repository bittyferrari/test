var vueHelper = {
	el: '#vue',
	data: {
		// main -----------------------------------------------------------
		uploadUrl: '/upload/',
		option: [],
		isProcessing: false,
		responseSource: {},
		isListing: true,

		// listing -----------------------------------------------------------
		items: [],
		optionPerPage: [2, 10, 30, 50, 100, 200],
		condition: {
			_orderField: 'id',
			_orderDirection: 'desc',
			_page: 1,
			_pageSize: 10,
			id: null,
		},
		countTotal: 0,
		pageTotal: 0,
		itemInterval: 0,
		getListingUrl: 'getListing',
		isGetListingFirstTime: true,
		isSearching: false,
		isFirstTime: true,
		isChangeCondition: false,
		isChanged: false,
		pages: [],
		showingFrom: 0,
		showingTo: 0,
		pageFrom: 0,
		pageTo: 0,
		listingFields: [],
		isShowDelete: false,
		isShowAlertAfterSubmit: true,

		// item -----------------------------------------------------------
		updateUrl: 'updateDo',
		item: {},
		notValidMessages: [],
		fieldRequired: [],

		isReloadAfterSubmit: false,
	},

	mounted() {
		this.initConditionFromUrl();
		this.onMounted();
		if (this.isListing && this.isGetListingFirstTime) {
			this.getListing();
		} else {
			//if (this.item.id == 0) {
			//    //this.item.id = null;
			//    this.item.guid = null;
			//    this.item.createdAt = null;
			//}
		}
	},
	filters: {
		hourMinute(value) {
			try {
				if (value) {
					value = value.replace('T', ' ');
					value = value.substr(11, 5);
				}
			} catch (e) {}
			return value;
		},
		time(value) {
			try {
				if (value) {
					value = value.replace('T', ' ');
					value = value.substr(0, 19);
				}
			} catch (e) {}
			return value;
		},

		number(value) {
			try {
				return new Intl.NumberFormat('zh-Hant-TW').format(value);
			} catch (e) {
				return value;
			}
		},

		timeToPercent(q) {
			var offset = 60 * 60 * 7 * 1000;
			var temp = Date.parse(q) + 60 * 60 * 8 * 1000;
			temp = temp % (60 * 60 * 24 * 1000);
			temp -= offset;
			temp = temp / (60 * 60 * 24 * 1000 - offset);
			temp = temp * 100;

			if (temp > 100) {
				temp = 100;
			}
			if (temp < 0) {
				temp = 0;
			}
			return temp;
		},

		dateTime(value) {
			if (value) {
				value = value.replace('T', ' ');
			}
			return value;
		},

		date(q) {
			try {
				q = q.substring(0, 10);
			} catch (e) {}
			return q;
		},

		dateTimeNoMillisecond(value) {
			if (value) {
				value = value.replace('T', ' ').substring(0, 19);
			}
			return value;
		},

		dateTimeNoSeconds(value) {
			if (value) {
				value = value.replace('T', ' ').substring(0, 16);
			}
			return value;
		},

		formatDate(value) {
			if (value) {
				value = value.replace('T', ' ').substring(0, 10);
			}
			return value;
		},

		formatTime(value) {
			if (value) {
				value = value.replace('T', ' ').substring(11, 16);
			}
			return value;
		},
	},
	methods: {
		onBeforeSubmit() {},

		onMounted() {},

		onAfterGetListing() {},

		getDate(date = new Date()) {
			var seperator1 = '-';
			var year = date.getFullYear();
			var month = date.getMonth() + 1;
			var strDate = date.getDate();

			if (month >= 1 && month <= 9) {
				month = '0' + month;
			}
			if (strDate >= 0 && strDate <= 9) {
				strDate = '0' + strDate;
			}
			var currentdate = year + '-' + month + '-' + strDate;
			return currentdate;
		},

		vueFilterDate(value) {
			if (value) {
				value = value.replace('T', ' ').substring(0, 10);
			}
			return value;
		},

		vueFilterDateTime(value) {
			if (value) {
				value = value.replace('T', ' ');
			}
			return value;
		},

		getAge(b) {
			const today = new Date();
			const birthDate = new Date(b);
			let age = today.getFullYear() - birthDate.getFullYear();
			let m = today.getMonth() - birthDate.getMonth();
			if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
				age--;
			}
			return age;
		},

		removeItem(x, index) {
			x.splice(index, 1);
		},

		randomMd5() {
			return Math.random().toString(36).substr(2);
		},

		isPermission(q, module = null) {
			if (this.self.roleID == 1) {
				return true;
			} else {
				if (module == null) {
					module = this.controllerName;
				}
				const permissionID = module + '-' + q;
				console.log(permissionID);
				const temp = this.self.permissions.find((z) => z == permissionID);
				return temp != null;
			}
		},

		changePageSize() {
			this.condition._page = 1;
			this.getListing();
		},

		initConditionFromUrl() {
			var keyPairs = [];
			var params = window.location.search.substring(1).split('&');
			for (var i = params.length - 1; i >= 0; i--) {
				var temp = params[i].split('=');
				this.condition[temp[0]] = temp[1];
			}
		},

		getListing() {
			var url = this.getListingUrl;

			if (this.isChangeCondition) {
				this.condition._page = 1;
			}
			this.isChangeCondition = false;

			// const requestData = {
			// 	condition: this.condition,
			// 	pageSize: this.condition._pageSize,
			// 	page: this.condition._page,
			// 	orderField: this.condition._orderField,
			// 	orderDirection: this.condition._orderDirection,
			// 	extend: {},
			// };

			const requestData = this.condition;

			this.isSearching = true;
			// this.$http.post(url, this._data.condition).then(function (r) {
			this.$http.post(url, requestData).then(function (r) {
				this.responseSource = r.body;
				var data = r.body;

				this.pages = [];

				this.isFirstTime = false;
				this.isSearching = false;

				this.items = r.body.data.items;
				this.countTotal = r.body.data.countTotal;

				this.pageTotal = Math.ceil(this.countTotal / this.condition._pageSize);
				// console.log(this.pageTotal);
				var showingFrom = 0;
				var showingTo = 0;

				/*
                showingFrom = (this.condition._page - 1) * this.condition._pageSize + 1;
                showingTo = (this.condition._page - 0) * this.condition._pageSize;
                */

				let pageFrom = this.condition._page - 4;
				let pageTo = this.condition._page + 4;

				showingFrom = (this.condition._page - 1) * this.condition._pageSize + 1;
				showingTo = this.condition._page * this.condition._pageSize;

				if (showingTo > this.countTotal) {
					showingTo = this.countTotal;
				}

				if (pageFrom <= 1) {
					pageFrom = 1;
				}
				if (pageTo > this.pageTotal) {
					pageTo = this.pageTotal;
				}

				// this.pageTotal = data.pageTotal;

				this.showingFrom = showingFrom;
				this.showingTo = showingTo;
				this.pageFrom = pageFrom;
				this.pageTo = pageTo;

				// set page
				// for (var i = 1; i <= this.pageTotal; i++) {
				//     this.pages.push(i);
				// }
				for (var i = pageFrom; i <= pageTo; i++) {
					this.pages.push(i);
				}

				this.onAfterGetListing();
			});
		},

		isDesc(x) {
			if (this.condition._orderField == x.fieldName && this.condition._orderDirection == 'desc') {
				return true;
			} else {
				return false;
			}
		},

		isAsc(x) {
			if (this.condition._orderField == x.fieldName && this.condition._orderDirection == 'asc') {
				return true;
			} else {
				return false;
			}
		},

		typeText(key, id, defaultText = '--', fieldKey = 'name') {
			const temp = this.option[key].find((z) => z.id == id);
			if (temp) {
				defaultText = temp[fieldKey];
			}

			return defaultText;
		},

		getType(key, index) {
			let defaultObject = {};

			const temp = this.option[key].find((z) => z.id == index);
			if (temp) {
				defaultObject = temp;
			}
			return defaultObject;
		},

		changeOrder(x) {
			if (this.condition._orderField != x.fieldName) {
				this.condition._orderDirection = 'asc';
			} else {
				if (this.condition._orderDirection == 'asc') {
					this.condition._orderDirection = 'desc';
				} else {
					this.condition._orderDirection = 'asc';
				}
			}
			this.condition._orderField = x.fieldName;
			this.getListing();
		},

		changePage(page) {
			this.condition._page = page;
			this.getListing();
		},

		nextPage() {
			if (this.condition._page >= this.pageTotal) {
			} else {
				this.condition._page++;
				this.getListing();
			}
		},

		previousPage() {
			if (this.condition._page <= 1) {
			} else {
				this.condition._page--;
				this.getListing();
			}
		},

		firstPage() {
			if (this.condition._page == 1) {
			} else {
				this.condition._page = 1;
				this.getListing();
			}
		},

		lastPage() {
			if (this.condition._page == this.pageTotal) {
			} else {
				this.condition._page = this.pageTotal;
				this.getListing();
			}
		},

		inputSearch() {
			this.condition._page = 1;
			this.getListing();
		},

		deleteDo(q) {
			if (confirm('Confirm delete?')) {
				var id = q.id;
				var self = this;
				var url = 'deleteDo';
				this.$http.post(url, { id: id }).then(function (r) {
					if (r.body.statusID == 0) {
						this.getListing();
					} else {
						alert('Delete failed: ' + r.body.message);
					}
				});
			}
		},

		// item ------------------------------------------------------------------------------------------------------
		// item ------------------------------------------------------------------------------------------------------
		// item ------------------------------------------------------------------------------------------------------
		// item ------------------------------------------------------------------------------------------------------
		// item ------------------------------------------------------------------------------------------------------

		onBeforeSubmit() {},

		onAfterSubmit() {},

		onSubmitError() {},

		removeItem(x, index) {
			x.splice(index, 1);
		},

		changePhoto(variable, key, accept = 'image/png, image/jpeg', maxSize = 10) {
			let self = this;
			let fileInput = this.$el.querySelector('.file');
			fileInput = document.createElement('input');
			fileInput.setAttribute('type', 'file');
			fileInput.setAttribute('accept', accept);
			// fileInput.setAttribute('accept', 'image/png, image/gif, image/jpeg, image/bmp, image/x-icon, image/svg+xml');
			fileInput.classList.add('hidden');
			fileInput.addEventListener('change', () => {
				if (fileInput.files != null && fileInput.files[0] != null) {
					// check file size first
					var fileSize = fileInput.files[0].size;
					if (fileSize / 1024 > maxSize * 1024) {
						self.alert("File size can't above " + maxSize + 'MB');
						return;
					}

					const file = fileInput.files[0];

					self.uploadFileDo(variable, key, file);
					/*
					if (accept == 'image/svg+xml') {
						// ignore width height check
						self.uploadFileDo(variable, key, file);
					} else {
						const _URL = window.URL || window.webkitURL;
						const img = new Image();
						img.onload = function () {
							// self.uploadFile(variable, key, file);
							self.uploadFile(variable, key, file);
						};

						img.src = _URL.createObjectURL(file);
					}
                    */
				} else {
				}
			});
			this.$el.appendChild(fileInput);
			fileInput.click();
		},

		uploadFile(callback = null) {
			let self = this;
			let fileInput = this.$el.querySelector('.file');
			fileInput = document.createElement('input');
			fileInput.setAttribute('type', 'file');
			//fileInput.setAttribute('accept', accept);
			// fileInput.setAttribute('accept', 'image/png, image/gif, image/jpeg, image/bmp, image/x-icon, image/svg+xml');
			fileInput.classList.add('hidden');

			fileInput.addEventListener('change', () => {
				const returnData = {
					file: '',
					name: '',
					isSuccess: false,
				};
				if (fileInput.files != null && fileInput.files[0] != null) {
					const file = fileInput.files[0];

					console.log(file);
					returnData.name = file.name;

					let formData = new FormData();
					formData.append('files', file, file.name);
					let xhr = new XMLHttpRequest();
					xhr.open('POST', '/helper/uploadFile', true);
					xhr.onload = function () {
						if (xhr.status === 200) {
							returnData.isSuccess = true;
							let json = JSON.parse(xhr.response);
							//variable[key] = json.fileName;
							//variable[key] = json[0];
							//self.alert('Upload complete.');
							returnData.file = json[0];

							if (callback != null) {
								callback(returnData);
							}
						} else {
							self.alert('Upload failed.');
						}
					};
					xhr.onerror = function () {
						self.alert('Upload failed.');
					};
					xhr.send(formData);
				} else {
				}
			});
			this.$el.appendChild(fileInput);
			fileInput.click();
		},

		uploadFileDo(variable, key, file) {
			const self = this;
			//do upload file
			let formData = new FormData();
			// formData.append('files', file, file.name);
			formData.append('file', file, file.name);
			let xhr = new XMLHttpRequest();
			const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
			// xhr.open('POST', '/helper/uploadFile', true);
			// xhr.open('POST', '/_admin/_helper/uploadFileDo', true);
			// xhr.open('POST', '/_admin/_helper/uploadFilesDo', true);
			xhr.open('POST', '/_admin/_helper/uploadFileDo', true);
			xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);

			xhr.onload = function () {
				if (xhr.status === 200) {
					let json = JSON.parse(xhr.response);
					//variable[key] = json.fileName;
					// variable[key] = json[0];
					variable[key] = json.fileName;
					self.alert('Upload complete.');
				} else {
					self.alert('Upload failed.');
				}
			};
			xhr.onerror = function () {
				self.alert('Upload failed.');
			};
			xhr.send(formData);
		},

		arrayMoveUp(x, i) {
			var b = x[i];
			x[i] = x[i - 1];
			x[i - 1] = b;
			x.push({});
			x.pop();
		},

		arrayMoveDown(x, i) {
			var b = x[i];
			x[i] = x[i + 1];
			x[i + 1] = b;
			x.push({});
			x.pop();
		},

		isEmpty(x) {
			return x === null || x === '' || typeof x == undefined;
		},

		post(url, json, callback) {
			//var headers = {};
			//if (localStorage.getItem('token')) {
			//    headers.Authorization =
			//        'Bearer ' + localStorage.getItem('token');
			//}
			//var self = this;
			this.isProcessing = true;
			try {
				this.$http.post(url, json).then(function (r) {
					this.isProcessing = false;
					callback(r);
				});
			} catch (e) {}
		},

		uploadFiles(event, key) {
			// var self = this;
			var files = event.target.files;
			if (files.length > 0) {
				var data = new FormData();
				for (var i = 0; i < files.length; i++) {
					data.append('files[]', files[i]);
				}
				// window.alert(key);
				this.$http.post('/_admin/_helper/uploadFilesDo', data).then(function (r) {
					// console.log(r);
					for (var i in r.body) {
						var x = r.body[i];
						if (x.fileName) {
							var a = {
								photo: x.fileName,
							};
							this.item[key].push(a);
						}
					}

					if (this.item.photoJson.length <= 1) {
						this.item.photo = this.item.photoJson[0].photo;
					}
				});
			}
		},

		alert(message) {
			let options = {
				okText: 'OK',
				backdropClose: true,
			};
			this.$dialog.alert(message, options).then(function (dialog) {
				// console.log('Closed');
				console.log(dialog);
			});
		},

		isInclude(q, qq) {
			let result = false;
			try {
				result = q.includes(qq);
			} catch (e) {
				// yada
			}
			return result;
		},

		getSubmitData() {
			return {
				item: this.item,
				extend: {},
			};
		},

		isValid() {
			let result = true;
			for (const i in this.fieldRequired) {
				const x = this.fieldRequired[i];
				if (this.isEmpty(this.item[x])) {
					result = false;
					//this.notValidMessages.push(x.message);
				}
			}
			return result;
		},

		copyDo() {
			this.item.id = 0;
			this.item.guid = '00000000-0000-0000-0000-000000000000';
			this.submitDo();
		},

		submitDo() {
			this.onBeforeSubmit();
			this.notValidMessage = '';
			this.notValidMessages = [];
			if (this.isValid()) {
				this.isProcessing = true;
				this.$http
					.post(this.updateUrl, this.getSubmitData())
					.then(function (res) {
						this.isProcessing = false;
						// this.isChanged = false;
						this.responseSource = res;
						const body = res.body;
						if (res.body.statusID != 0) {
							this.onSubmitError();
						} else {
							this.item.id = body.data.item.id;
							// this.item.guid = body.item.guid;
							this.item.createdAt = body.data.item.createdAt;
							//if (res.body.data) {
							//var data = res.body.data;
							//this.item.id = data.item.id;
							//if (data.item.createdAt) {
							//    this.item.createdAt = data.item.createdAt;
							//}

							if (this.isShowAlertAfterSubmit) {
								window.alert('save success');
							}

							this.onAfterSubmit();

							if (this.isReloadAfterSubmit) {
								location.reload();
							}
							//}
						}
					})
					.catch(function () {
						window.alert('save failed');

						this.isProcessing = false;
						this.onSubmitError();
					});
			} else {
				if (this.notValidMessages.length <= 0) {
					this.notValidMessages.push('Please fill in all required field.');
				}

				var message = this.notValidMessages.join('\n');
				// if (this.isCompleted == false) {
				window.alert(message);
				// }

				//window.alert('Please fill in all required field.');
			}
		},
	},

	created() {},
};

export default vueHelper;
