import $ from 'jquery';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vueItem from './vueItem';
import vueListing from './vueListing';

import vueMultiselect from 'vue-multiselect';

import VuejsDialog from 'vuejs-dialog';
import 'vuejs-dialog/dist/vuejs-dialog.min.css';

// import { QrcodeStream, QrcodeDropZone, QrcodeCapture } from 'vue-qrcode-reader';
import VueQrcodeReader from 'vue-qrcode-reader';
import VueQrcode from '@chenfengyuan/vue-qrcode';

import VueSplide from '@splidejs/vue-splide';


// filter
import filterCurrency from './filter/currency';
import filterNumber from './filter/number';
import filterInt from './filter/int';
import filterPercent from './filter/percent';
import filterTime from './filter/time';
import filterDate from './filter/date';
import filterAsset from './filter/asset';
import filterPad from './filter/pad';
import filterNumberPositive from './filter/numberPositive';
import filterDatetimeWeek from './filter/datetimeWeek';
import filterTimeNoSecond from './filter/timeNoSecond';

// import 'viewerjs/dist/viewer.css';
// import Viewer from 'v-viewer';

var VueTouch = require('vue-touch');

// require('./bootstrap');

// register globally
Vue.component('multiselect', vueMultiselect);

//components
Vue.component('mmm', require('./components/multiselect.vue').default);
Vue.component('summernote', require('./components/summernote.vue').default);
Vue.component('chart', require('./components/chart.vue').default);
Vue.component('barchart', require('./components/barchart.vue').default);
Vue.component('datepicker', require('./components/datepicker.vue').default);
Vue.component('timepicker', require('./components/timepicker.vue').default);
Vue.component(VueQrcode.name, VueQrcode);
// Vue.component('QrcodeStream', QrcodeStream);
// Vue.component('QrcodeDropZone', QrcodeDropZone);
// Vue.component('QrcodeCapture', QrcodeCapture);

Vue.use(VueQrcodeReader);
Vue.use(VueResource);
Vue.use(VuejsDialog);
// Vue.use(Viewer);
Vue.use(VueTouch, { name: 'v-touch' });
Vue.use(VueSplide);


// filter
Vue.filter('currency', filterCurrency);
Vue.filter('number', filterNumber);
Vue.filter('percent', filterPercent);
Vue.filter('int', filterInt);
Vue.filter('time', filterTime);
Vue.filter('timeNoSecond', filterTimeNoSecond);
Vue.filter('date', filterDate);
Vue.filter('asset', filterAsset);
Vue.filter('pad', filterPad);
Vue.filter('numberPositive', filterNumberPositive);
Vue.filter('datetimeWeek', filterDatetimeWeek);

// vue resource csrf
Vue.http.interceptors.push(function (request, next) {
	const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
	request.headers.set('X-CSRF-TOKEN', csrfToken);
	next(function (response) {
		if (response.status === 401) {
			window.location.href = response.body.redirect;
		}
	});
});

window.Vue = Vue;
window.$ = $;
window.vueListing = vueListing;
window.vueItem = vueItem;
