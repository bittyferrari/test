export default value => {
    if (value == null || value == '') {
        return '--';
    } else {
        try {
            return value.substring(0, 10);
        } catch (e) {
            return '--';
        }
    }
};
