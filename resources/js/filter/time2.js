export default value => {
    if (value == null || value == '') {
        return '--';
    } else {
        try {
            // return new Date(value + '+00:00').toLocaleString();
            // return new Date(value).toLocaleString('en-GB');

            var timestamp = new Date(value).valueOf();
            
            timestamp += 60 * 8 * 1000;

            var time = new Date(timestamp);

            var iso = time.toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);

			// return iso[1] + ' ' + iso[2] + timeZone;
			return iso[1] + ' ' + iso[2];


            // return new Date(value).toLocaleString('en-GB');
        } catch (e) {
            return '--';
        }
    }
};
