export default (value) => {
	try {
		return Math.abs(value);
	} catch (e) {
		return value;
	}
};
