export default value => {
	if (value == null || value == '') {
		return '--';
	} else {
		try {
			// return new Date(value + '+00:00').toLocaleString();
			// return new Date(value).toLocaleString('en-GB');

			// 04/02/2020, 13:30:00
			var temp = new Date(value).toLocaleString('en-GB');

			var year = temp.substr(6, 4);
			var month = temp.substr(3, 2);
			var day = temp.substr(0, 2);
			var time = temp.substr(12, 5);

			return year + '-' + month + '-' + day + ' ' + time;
		} catch (e) {
			return '--';
		}
	}
};
