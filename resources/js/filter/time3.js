export default value => {
    if (value == null || value == '') {
        return '--';
    } else {
        try {
            // return new Date(value + '+00:00').toLocaleString();
            return new Date(value).toLocaleString('en-GB');
        } catch (e) {
            return '--';
        }
    }
};
