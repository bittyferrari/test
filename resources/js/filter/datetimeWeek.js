export default (value) => {
	if (value == null || value == '') {
		return '--';
	} else {
		try {
			var temp = new Date(value);

			var year = temp.getFullYear();
			var month = temp.getMonth() + 1;
			var day = temp.getDate();
			var hour = temp.getHours();
			var minute = temp.getMinutes();

			month = ('' + month).padStart(2, '0');
			day = ('' + day).padStart(2, '0');
			hour = ('' + hour).padStart(2, '0');
			minute = ('' + minute).padStart(2, '0');

			var arrayOfWeekdays = ['日', '一', '二', '三', '四', '五', '六'];
			var weekDay = temp.getDay();
			var weekdayName = arrayOfWeekdays[weekDay];

			return year + '-' + month + '-' + day + '(' + weekdayName + ')' + hour + ':' + minute;
		} catch (e) {
			return '--';
		}
	}
};
