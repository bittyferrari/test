export default (value, width = 2, fill = '0') => {
	value = value + '';
	return value.length >= width ? value : new Array(width - value.length + 1).join(fill) + value;
};
