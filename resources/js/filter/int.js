export default value => {
    if (Number.isNaN(value)) {
        return '--';
    } else {
        return parseInt(value);
    }
};
