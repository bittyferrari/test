export default value => {
    // return Math.round(value * 100) / 100 + '%';
    if (Number.isNaN(value)) {
        return '--';
    } else {
        return Math.round(value * 100 * 100) / 100 + '%';
    }
};
