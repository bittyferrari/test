const formatter = new Intl.NumberFormat('zh-Hant-TW', {
    style: 'currency',
    currency: 'TWD',
    minimumFractionDigits: 0,
});

export default (value) => {
    return formatter.format(Math.round(value));
};
