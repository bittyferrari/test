const uploadUrl = '/storage/photo/';
const vueConfig = {
    uploadUrl: uploadUrl,
    pageSizeDefault: 10,
};

module.exports = vueConfig;
