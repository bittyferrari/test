import config from './config';

var vueListData = {
	el: '#vue',
	data: {
		items: [],
		uploadUrl: config.uploadUrl,
		condition: {
			_orderField: 'id',
			_orderType: 'desc',
			_page: 1,
			_pageSize: config.pageSizeDefault,
		},
		totalItem: 0,
		pageTotal: 0,
		getListingUrl: 'getListing',
		isGetListingFirstTime: true,
		isSearching: false,
		isFirstTime: true,
		isChangeCondition: false,
		option: [],
		text: {
			notFound: '找不到項目',
			search: '搜尋',
			update: '檢視',
			delete: '刪除',
			create: '建立',

			// notFound: 'Not Found',
			// search: 'Search',
			// update: 'Edit',
			// delete: 'Delete',
			// create: 'Create',
		},
		dataResponse: {},
		pages: [],
		showingFrom: 0,
		showingTo: 0,
	},
	computed: {},

	mounted: function () {
		this.initConditionFromUrl();
		this.onMounted();
		if (this.isGetListingFirstTime) {
			this.getListing();
		}
	},
	methods: {
		onMounted() {},

		removeItem(x, index) {
			x.splice(index, 1);
		},

		randomMd5() {
			return Math.random().toString(36).substr(2);
		},

		checkCarLicense(x, key) {
			try {
				x[key] = x[key].replace(/[^a-z0-9]/gi, '');
				x[key] = x[key].toUpperCase();
			} catch (e) {
				console.log(e);
				// yada
			}
		},

		isPermission(q, moduleName = null) {
			// return true;
			if (this.admin.id == 1) {
				return true;
			} else {
				if (moduleName == null) {
					moduleName = this.moduleName;
				}
				const permissionID = moduleName + '-' + q;
				const temp = this.admin.permissions.find((z) => z == permissionID);
				return temp != null;
			}
		},

		changePageSize() {
			this.condition._page = 1;
			this.getListing();
		},

		isInclude(q, qq) {
			let result = false;
			try {
				result = q.includes(qq);
			} catch (e) {
				// yada
			}
			return result;
		},


		initConditionFromUrl: function () {
			var keyPairs = [];
			var params = window.location.search.substring(1).split('&');
			for (var i = params.length - 1; i >= 0; i--) {
				var temp = params[i].split('=');
				this.condition[temp[0]] = temp[1];
			}
		},

		isEmpty(x) {
			return x === null || x === '' || typeof x == undefined;
		},

		afterGetListing() {},

		getListing() {
			var url = this.getListingUrl;

			if (this.isChangeCondition) {
				this.condition._page = 1;
			}
			this.isChangeCondition = false;

			this.isSearching = true;
			this.$http.post(url, this._data.condition).then(function (r) {
				this.dataResponse = r.body.data;
				var data = r.body.data;

				this.pages = [];

				this.isFirstTime = false;
				this.isSearching = false;

				this.items = r.body.data.items;
				this.pageTotal = r.body.data.pageTotal;
				this.totalItem = r.body.data.totalItem;

				var showingFrom = 0;
				var showingTo = 0;
				/*
                showingFrom = (this.condition._page - 1) * this.condition._pageSize + 1;
                showingTo = (this.condition._page - 0) * this.condition._pageSize;
                */

				showingFrom = this.condition._page - 4;
				showingTo = this.condition._page + 4;

				if (showingFrom <= 1) {
					showingFrom = 1;
				}
				if (showingTo > this.pageTotal) {
					showingTo = this.pageTotal;
				}

				this.pageTotal = data.pageTotal;
				this.showingFrom = showingFrom;
				this.showingTo = showingTo;

				//set page
				// for (var i = 1; i <= this.pageTotal; i++) {
				//     this.pages.push(i);
				// }
				for (var i = showingFrom; i <= showingTo; i++) {
					this.pages.push(i);
				}

				this.afterGetListing();
			});
		},

		isDesc: function (x) {
			if (this.condition._orderField == x && this.condition._orderType == 'desc') {
				return true;
			} else {
				return false;
			}
		},

		isAsc: function (x) {
			if (this.condition._orderField == x && this.condition._orderType == 'asc') {
				return true;
			} else {
				return false;
			}
		},

		typeText: function (key, id, defaultText = '--', fieldKey = 'name') {
			const temp = this.option[key].find((z) => z.id == id);
			if (temp) {
				defaultText = temp[fieldKey];
			}

			return defaultText;
		},

		getType(key, index) {
			let defaultObject = {};

			const temp = this.option[key].find((z) => z.id == index);
			if (temp) {
				defaultObject = temp;
			}
			return defaultObject;
		},

		changeOrder: function (x) {
			if (this.condition._orderField != x) {
				this.condition._orderType = 'asc';
			} else {
				if (this.condition._orderType == 'asc') {
					this.condition._orderType = 'desc';
				} else {
					this.condition._orderType = 'asc';
				}
			}
			this.condition._orderField = x;
			this.getListing();
		},

		changePage: function (page) {
			this.condition._page = page;
			this.getListing();
		},

		nextPage: function () {
			if (this.condition._page >= this.pageTotal) {
			} else {
				this.condition._page++;
				this.getListing();
			}
		},

		prevPage: function () {
			if (this.condition._page <= 1) {
			} else {
				this.condition._page--;
				this.getListing();
			}
		},

		inputSearch: function () {
			this.condition._page = 1;
			this.getListing();
		},

		deleteItem: function (index) {
			if (confirm('Confirm to delete?')) {
				var id = this.items[index].id;
				var self = this;
				var url = 'deleteDo';
				this.$http.post(url, { id: id }).then(function (r) {
					// if (r.body.statusID == 0) {
					// console.log(r);
					if (r.body.statusID == 0) {
						// this.items.splice(index, 1);
						this.getListing();
					} else {
						this.alert('Delete failed');
					}
				});
			}
		},

		alert(message, callback = null) {
			let options = {
				okText: 'OK',
				backdropClose: true,
				// backdropClose: false,
			};

			this.$dialog
				.alert(message, options)
				.then(function (dialog) {
					// console.log('Closed');
					// console.log(dialog);
					if (callback != null) {
						callback();
					}
				})
				.catch(function () {
					if (callback != null) {
						callback();
					}
					// console.log('Clicked on cancel');
				});
		},

		implodeTypes: function (x, key) {
			var text = '';
			try {
				var types = this.option[key];
				var x = x.split(',');
				for (var i in x) {
					var xx = x[i];
					if (types[xx]) {
						text += types[xx] + ',';
					}
				}
				if (text == '') {
					text = '--';
				} else {
					text = text.substr(0, text.length - 1);
				}
			} catch (e) {
				text = '--';
			}
			return text;
		},
	},
	created: function () {},
};

// module.exports = vueListData;
//
export default vueListData;
