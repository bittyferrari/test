<?php

namespace App\Helper;

class SMSHttp
{
    public $smsHost;
    public $sendSMSUrl;
    public $getCreditUrl;
    public $batchID;
    public $credit;
    public $processMsg;

    public function SMSHttp()
    {
        $this->smsHost = "api.every8d.com";
        $this->sendSMSUrl = "http://" . $this->smsHost . "/API21/HTTP/sendSMS.ashx";
        $this->getCreditUrl = "http://" . $this->smsHost . "/API21/HTTP/getCredit.ashx";
        $this->batchID = "";
        $this->credit = 0.0;
        $this->processMsg = "";
    }

    /// <summary>
    /// 取得帳號餘額
    /// </summary>
    /// <param name="userID">帳號</param>
    /// <param name="password">密碼</param>
    /// <returns>true:取得成功；false:取得失敗</returns>
    public function getCredit($userID, $password)
    {
        $success = false;
        $postDataString = "UID=" . $userID . "&PWD=" . $password;
        $resultString = $this->httpPost($this->getCreditUrl, $postDataString);
        if (substr($resultString, 0, 1) == "-") {
            $this->processMsg = $resultString;
        } else {
            $success = true;
            $this->credit = $resultString;
        }
        return $success;
    }

    /// <summary>
    /// 傳送簡訊
    /// </summary>
    /// <param name="userID">帳號</param>
    /// <param name="password">密碼</param>
    /// <param name="subject">簡訊主旨，主旨不會隨著簡訊內容發送出去。用以註記本次發送之用途。可傳入空字串。</param>
    /// <param name="content">簡訊發送內容</param>
    /// <param name="mobile">接收人之手機號碼。格式為: +886912345678或09123456789。多筆接收人時，請以半形逗點隔開( , )，如0912345678,0922333444。</param>
    /// <param name="sendTime">簡訊預定發送時間。-立即發送：請傳入空字串。-預約發送：請傳入預計發送時間，若傳送時間小於系統接單時間，將不予傳送。格式為YYYYMMDDhhmnss；例如:預約2009/01/31 15:30:00發送，則傳入20090131153000。若傳遞時間已逾現在之時間，將立即發送。</param>
    /// <returns>true:傳送成功；false:傳送失敗</returns>
    public function sendSMS($userID, $password, $subject, $content, $mobile, $sendTime)
    {
        $success = false;
        // $postDataString = "UID=" . $userID;
        // $postDataString .= "&PWD=" . $password;
        // $postDataString .= "&SB=" . $subject;
        // $postDataString .= "&MSG=" . $content;
        // $postDataString .= "&DEST=" . $mobile;
        // $postDataString .= "&ST=" . $sendTime;

        $postData = [
            'UID' => $userID,
            'PWD' => $password,
            'SB' => $subject,
            'MSG' => $content,
            'DEST' => $mobile,
            'ST' => '',
        ];
        // $resultString = $this->httpPost($this->sendSMSUrl, $postDataString);
        // print $this->sendSMSUrl . 'kkkkkkkkk';
        $resultString = $this->httpPost('http://api.every8d.com/API21/HTTP/sendSMS.ashx', $postData);
        // print 'aaaaaaaaaaa' . $resultString . 'zzzzzzzzzzzzzzz';
        if (substr($resultString, 0, 1) == "-") {
            $this->processMsg = $resultString;
        } else {
            $success = true;
            $strArray = explode(",", $resultString);
            $this->credit = $strArray[0];
            // $this->batchID = $strArray[4];
            // $this->batchID = $strArray[4];
        }
        return $success;
    }

    public function httpPost($url, $postData)
    {
        $res = '';

        // print $url;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'User-Agent: Super Agent/0.0.1',
        ));
        $httpStatus = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // print $httpStatus . 'hhhhhhhhhhh';

        $html = curl_exec($curl);
        // print $html;

        // print 'qqqqqq';
        // print curl_error($curl);
        // print 'yyyyyyyyy';


        return;

    }
}

class SmsHelper
{

    static $file = null;
    static $toEmail;
    static $title = '總太';
    static $data = null;

    // public static function sendDo($email, $data, $typeID, $file = null)
    public static function sendDo($phone, $subject, $content)
    {

        // if (env('APP_ENV') != 'production') {
        //     $email = 'bittyferrari@gmail.com';
        // }

        $smsHttp = new SMSHttp();

        // $userID = env('SMS_USERID');
        // $password = env('SMS_PASSWORD');

        $userID = env('EVERY8D_USERNAME');
        $password = env('EVERY8D_PASSWORD');

        // $subject = 'test';
        // $content = 'content 1234 test';
        // $mobile = '0970325555';

        $mobile = $phone;
        $sendTime = '';
        $smsHttp->sendSMS($userID, $password, $subject, $content, $mobile, $sendTime);

    }

}
