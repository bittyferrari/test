<?php

use App\Model\Admin;
use App\Model\CourseUser;
use App\Model\Setting;
use App\Model\Store;
use App\Model\Transaction;
use App\Model\User;
use App\Model\UserCoupon;
use Illuminate\Support\Facades\Storage;

function isCourseSignable($q)
{

    $course = $q;

    $isSignable = false;

    if ($course['isCancel']) {

    } else {

        // check time
        $now = time() * 1000;

        if ($course['signupTimestampTo'] >= $now && $course['signupTimestampFrom'] <= $now) {

            $count = CourseUser::where('courseID', '=', $course['id'])->count();
            // $sequence = $count + 1;
            $isAlternate = $count >= $course['countAvailable'];

            if ($isAlternate) {
                if ($course['isAlternatable']) {
                    $isSignable = true;
                }
            } else {
                $isSignable = true;
            }

        }

    }

    return $isSignable;

}
function getCourseStatus($q)
{
    $now = time();

    $statusID = 1;
    if ($q['isActive'] != 1) {
        $statusID = 3;
    } else {

        // 詳細資訊
        if ($q['timestampTo'] > $now) {
            $statusID = 1;
        }

        // 報名已截止
        if ($q['signupTimestampTo'] < $now) {
            $statusID = 2;
        }
    }

    return $statusID;

}
function recalculateUserCoupon($id, $isCallApi = false)
{

    // $now = date('Y-m-d H:i:s');
    $now = date('Y-m-d');
    $items = UserCoupon::where('userID', '=', $id)->whereDate('timeFrom', '<=', $now)->whereDate('timeTo', '>=', $now)->get();

    $sumPrice = 0;
    $sumPricePoint = 0;

    foreach ($items as $x) {
        $price = $x['price'];
        $pricePoint = $x['pricePoint'];

        $priceUsed = $x['priceUsed'];
        $pricePointUsed = $x['pricePointUsed'];

        $sumPrice += ($price - $priceUsed);
        $sumPricePoint += ($pricePoint - $pricePointUsed);
    }

    $user = User::find($id);
    if ($user) {
        if ($isCallApi || $user['priceCoupon'] != $sumPrice || $user['pricePointCoupon'] != $sumPricePoint) {
            $user['priceCoupon'] = $sumPrice;
            $user['pricePointCoupon'] = $sumPricePoint;
            $user->save();
            updateUserPoint($user);
        }

    }

    return $user;
}

function updateUserPoint($q)
{

    if (!empty($q['nationID'])) {

        $apiKey = env('ZONGTAI_API_KEY');

        $curl = curl_init();

        $postData = null;
        $postData['id_number'] = $q['nationID'];
        $postData['zongtai_points'] = $q['price'];
        $postData['activity_points'] = $q['pricePointCoupon'];

        curl_setopt_array($curl, array(
            // CURLOPT_URL => 'http://localhost:8001/api/members/L220969315',
            CURLOPT_URL => 'http://zongtai-hub.weya.tw/api/members/' . $q['nationID'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            // CURLOPT_POSTFIELDS => 'active=1&id_number=L220969315&gui_number=&email=test%40test.io&password=test12ab&name=%E5%8A%89%E4%BD%B3%E7%9A%93&mobile=&phone=&sex=male&birthday=&address=&facebook=&line=&wechat=&zongtai_points=2&activity_points=10',
            CURLOPT_POSTFIELDS => http_build_query($postData),
            CURLOPT_HTTPHEADER => ['Authorization: Bearer ' . $apiKey],
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        // echo '========' . $response;
    }

}

function devPrint($q, $separator = '<br />')
{
    $isDev = false;
    if (isset($GLOBALS['isDev'])) {
        $isDev = $GLOBALS['isDev'];
    }
    if ($isDev) {
        if (is_array($q)) {
            print '<pre>';
            print_r($q);
            print '</pre>';
        } else {
            print $q;
        }
        print $separator . "\n";
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function filterHtml($v)
{
    return trim(strip_tags($v));
}

function setLoginedSession($user)
{
    setSession('adminID', $user['id']);
    // setSession('userID', $user['id']);
    // setSession('user', $user);
    setSession('admin', $user);
    setSession('isLogin', true);
    setSession('isBackend', true);
    setSession('userRoleID', $user['roleID']);
    setSession('userName', $user['name']);
}

function stripTags($x, $length = 200)
{
    $x = strip_tags($x);
    if (mb_strlen($x, 'utf-8') > $length) {
        $x = mb_substr($x, 0, $length, 'utf-8') . '...';
    } else {
        $x = mb_substr($x, 0, $length, 'utf-8');
    }
    return $x;
}

function isEmail($v)
{
    if (!filter_var($v, FILTER_VALIDATE_EMAIL)) {
        return false;
    } else {
        return true;
    }
}

function exexplode($x)
{
    $temp = explode(',', $x);
    $temp = array_filter($temp);
    $zzz = [];
    foreach ($temp as $xx) {
        if (!empty($xx)) {
            $zzz[] = $xx;
        }
    }
    return $zzz;
}

function imimplode($x)
{
    $v = null;
    if ($x) {
        $v = implode(',', $x);
        if ($v) {
            $v = ',' . $v . ',';
        }
    }
    return $v;
}

function _md5($x)
{
    return md5('xxxx' . $x . 'zzzz');
}

function randomColor()
{
    return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
}

function getRatio()
{
    $ratio = Setting::where('key', '=', 'ratio')->first()['value'];
    // return 1.2;
    return $ratio;
}

function randomMd5($xxx = '')
{
    return md5($xxx . 'aaa' . uniqid() . 'bbb' . time() . rand(0, 99999));
}

function randomString($length = 20, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890')
{
    mt_srand((double) microtime() * 1000000 * getmypid());
    $password = '';
    while (strlen($password) < $length) {
        $password .= substr($chars, (mt_rand() % strlen($chars)), 1);
    }
    return $password;
}
function randomNumber($length = 20)
{
    $chars = '1234567890';
    mt_srand((double) microtime() * 1000000 * getmypid());
    $password = '';
    while (strlen($password) < $length) {
        $password .= substr($chars, (mt_rand() % strlen($chars)), 1);
    }
    return $password;
}

function isDev()
{
    return env('APP_ENV') != 'production';
}

function getOption()
{
    $a = null;

    $a = config('_constant');
    return $a;
}

function type($key)
{
    $a = null;

    switch ($key) {
        case 'city':
            $a = [];
            break;
        default;
            $a = config('_constant')[$key];
            break;
    }
    return $a;
}

function getWebService($url)
{
    libxml_disable_entity_loader(false);
    $opts = [
        'ssl' => [
            'verify_peer' => false,
        ],
        'https' => [
            'curl_verify_ssl_peer' => false,
            'curl_verify_ssl_host' => false,
        ],
    ];
    $streamContext = stream_context_create($opts);
    $client = new SoapClient($url, [
        'stream_context' => $streamContext,
    ]);
    return $client;
}

function setSession($key, $value)
{
    session()->put($key, $value);
    session()->save();
}

function saveFile($file, $fileName, $path = null)
{
    Storage::disk('public')->put('/photo/' . $fileName, file_get_contents($file));
}

function getSession($key, $defaultValue = null)
{
    return session()->get($key, $defaultValue);
}

function jsonEncode($x)
{
    return json_encode($x, JSON_UNESCAPED_UNICODE);
}

function jsonDecode($x)
{
    return json_decode($x, true);
}

function setListSearch($value)
{
    $GLOBALS['listSearch'] = $value;
}

function printJson($variableName, $variable, $isReturn = false)
{
    if ($isReturn) {
        return '<script>var ' . $variableName . ' = ' . json_encode($variable) . ';</script>';
    } else {
        print '<script>var ' . $variableName . ' = ' . json_encode($variable) . ';</script>';
    }
}

function returnJson($x)
{
    header('Content-Type: application/javascript');
    print json_encode($x);
    exit;
}

function addWhere(&$result, $field, $type = '=', $dbField = null, $whereType = null)
{
    $search = $GLOBALS['listSearch'];

    if (empty($dbField)) {
        $dbField = $field;
    }

    if (isset($search[$field])) {
        $value = $search[$field];
        if (!empty($value)) {
            switch ($type) {
                case 'like':
                    $result = $result->where($dbField, 'like', '%' . $value . '%');
                    break;
                default:
                    if ($whereType == 'date') {
                        $result = $result->whereDate($dbField, $type, $value);
                    } else {
                        $result = $result->where($dbField, $type, $value);

                    }
                    break;
            }
        }
    }
}

function saveSession()
{
    session()->save();
}

function forgetSession($key)
{
    session()->forget($key);
}

function getStoreID()
{
    return getSession('storeID');
}

function getStore($storeID = null)
{
    if ($storeID) {
        return Store::find($storeID);
    } else {
        return Store::find(getStoreID());
    }
}

function getUserID()
{
    return getSession('userID');
}

function getStoreTotal($id)
{
    $storeTotal = 0;
    $storeTotal = Transaction::where('storeID', '=', $id)->where('isSuccess', '=', 1)->where('typeID', '!=', 1)->sum('price');
    $storeTotal *= -1;

    $priceStoreSettlement = Transaction::where('storeID', '=', $id)->where('isSuccess', '=', 1)->where('priceStoreSettlement', '>', 0)->sum('priceStoreSettlement');

    $storeTotal -= $priceStoreSettlement;
    return $storeTotal;
}

function getUser($userID = null)
{
    if ($userID) {
        return User::find($userID);
    } else {
        return User::find(getUserID());
    }
}

function getIdByName($q, $qq)
{
    $id = null;
    foreach ($q as $x) {
        if ($x['name'] == $qq) {
            $id = $x['id'];
        }
    }
    return $id;
}

function getAdminID()
{
    return getSession('adminID');
}

function getAdmin($id = null)
{
    if ($id) {
        return Admin::find($id);
    } else {
        return Admin::find(getAdminID());
    }
}

function arrayText($array, $v, $defaultText = '--')
{
    $text = '';
    if (isset($array[$v])) {
        $text = $array[$v];
    } else {
        $text = '--';
    }
    return $text;

}

function typeText($typeKey, $v, $defaultText = '--')
{
    $text = '';
    $type = type($typeKey);
    foreach ($type as $x) {
        if ($x['id'] == $v) {
            $defaultText = $x['name'];
        }
    }
    return $defaultText;
}

function v($z, $action = null, $controller = null)
{

    $data = $z->viewData;
    $z->viewData['option'] = array_merge(getOption(), $z->vueOption);
    $z->viewData['env'] = env('APP_ENV');

    $data['vueData'] = $z->viewData;
    if (empty($controller)) {
        $controller = $GLOBALS['controller'];
    }
    if (empty($action)) {
        $action = $GLOBALS['action'];
    }
    $data['meta'] = $z->meta;
    return view($controller . '/' . $action, $data);
}

function getIP()
{
    $ip = null;
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        if (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = '127.0.0.1';
        }
    }
    return $ip;
}

function getControllerName()
{
    if (isset($GLOBALS['controllerName'])) {
        return $GLOBALS['controllerName'];
    } else {
        return null;
    }
}

function getActionName()
{
    if (isset($GLOBALS['actionName'])) {
        return $GLOBALS['actionName'];
    } else {
        return null;
    }
}

// model ----------------------------------------------------------------------------------
function setModelData(&$item, &$data, $ignoreKeys = [])
{
    $ignoreKeys[] = 'id';
    $ignoreKeys[] = 'created_at';
    $ignoreKeys[] = 'updated_at';
    $ignoreKeys[] = 'deleted_at';

    foreach ($data as $key => $v) {
        if (!in_array($key, $ignoreKeys)) {
            $item[$key] = $v;

            if (!is_array($v)) {
            } else {
                // $item[$key] = json_encode($v, JSON_UNESCAPED_UNICODE);
            }
        } else {
            if ($key != 'id') {
                unset($item[$key]);
            }
        }
    }
}

function curlGet($url, $isJson = true)
{
    //get html
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $result = curl_exec($curl);

    if ($isJson) {
        $result = jsonDecode($result);
    }

    return $result;
}

function getApiKey()
{
    return env('API_KEY');
}

function curlPost($url, $data, $isJson = true)
{
    //get html
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $postJson = json_encode($data);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postJson);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

    $result = curl_exec($curl);
    if ($isJson) {
        $result = jsonDecode($result);
    }

    return $result;
}

function getWhereCondition($conditions, $search = null)
{
    if ($search == null) {
        $search = request();
    }
    $where = [];
    foreach ($conditions as $condition) {
        $field = $condition[0];
        switch ($field) {
            case '_page':
            case '_pageSize':
            case '_orderField':
            case '_orderType':
                continue 2;
                break;
        }
        $valueKey = $condition[0];
        if (isset($condition[2])) {
            $valueKey = $condition[2];
        } else {
            if (strpos($valueKey, '.') !== false) {
                $valueKey = explode('.', $valueKey)[1];
            }
        }
        if (isset($search[$valueKey]) && $search[$valueKey] !== '' && $search[$valueKey] !== null) {
            $operator = '=';
            if (isset($condition[1])) {
                $operator = $condition[1];
            }
            $value = $search[$valueKey];
            if ($operator == 'like') {
                $value = '%' . $value . '%';
            }
            $where[] = [$field, $operator, $value];
        }
    }
    return $where;
}

function getListCondition()
{
    $data = [];

    $request = request();

    $page = $request['_page'];
    $pageSize = $request['_pageSize'];
    $orderField = $request['_orderField'];
    $orderType = $request['_orderType'];

    if (!$pageSize) {
        $pageSize = 10;
    } else {
        $pageSize = intval($pageSize);
    }

    if ($pageSize > 500) {
        $pageSize = 500;
    }

    $page = intval($page);
    $skip = ($page - 1) * $pageSize;

    if (empty($orderField)) {
        $orderField = 'id';
    }
    if (empty($orderType)) {
        $orderType = 'ASC';
    }

    $data['orderField'] = $orderField;
    $data['orderType'] = $orderType;
    $data['page'] = $page;
    $data['pageSize'] = $pageSize;
    $data['skip'] = $skip;

    return $data;
}

function getRandomString($length = 10, $characters = null)
{
    if (empty($characters)) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getListData($result, $listData, $select = ['*'])
{
    $json = [];

    $itemCount = $result->count();

    $data = $result->select($select)->skip($listData['skip'])->take($listData['pageSize'])->get();

    $json['items'] = $data;
    $json['totalItem'] = intval($itemCount);
    $json['pageTotal'] = ceil($itemCount / $listData['pageSize']);

    return $json;
}

function getDatetime($time = null)
{
    if ($time == null) {
        $time = time();
    }
    return date('Y-m-d H:i:s', $time);
}
