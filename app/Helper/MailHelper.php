<?php

namespace App\Helper;

use App\Model\MailQueue;
use App\Model\Setting;
use Mail;

class MailHelper
{

    static $file = null;
    static $toEmail = null;
    static $title = 'test';
    static $data = null;

    public static function sendDo($email, $data, $typeID, $file = null)
    {

        if (env('APP_ENV') != 'production') {
            $email = 'bittyferrari@gmail.com';
            return;
        }

        self::$toEmail = $email;
        self::$data = $data;

        switch ($typeID) {
            case 'template1':
                self::$title = 'test' . $data['createdUser']['name'] . 'xxx';
                self::sendMailDo($email, $data, $typeID);
                break;
            case 'testing':
                self::$title = '測試信';
                self::sendMailDo($email, $data, $typeID);
                break;
        }
    }

    public static function addMailQueue($email, $data, $templateID)
    {
        $mailQueue = new MailQueue;
        $mailQueue['email'] = $email;
        $mailQueue['json'] = jsonEncode($data);
        $mailQueue['typeID'] = $templateID;
        $mailQueue->save();
    }

    public static function sendMailDo($email, $data, $templateID)
    {
        if ($data == null) {
            $data = [];
        }

        // use setting
        $items = Setting::get();
        foreach ($items as $x) {

            switch ($x['key']) {
                case 'MAIL_HOST':
                case 'MAIL_PORT':
                case 'MAIL_USERNAME':
                case 'MAIL_PASSWORD':
                case 'MAIL_ENCRYPTION':
                case 'MAIL_FROM_NAME':
                    $mailSetting[$x['key']] = $x['value'];
                    break;
                default:
                    break;
            }
        }

        $transport = (new \Swift_SmtpTransport($mailSetting['MAIL_HOST'], $mailSetting['MAIL_PORT']))
            ->setUsername($mailSetting['MAIL_USERNAME'])
            ->setPassword($mailSetting['MAIL_PASSWORD'])
            ->setEncryption($mailSetting['MAIL_ENCRYPTION']);

        \Mail::setSwiftMailer(new \Swift_Mailer($transport));

        try {
            Mail::send('_mail/' . $templateID, $data, function ($message) use ($email) {
                // $bccEmail = 'sc.lin@spatial-topology.com';
                // $bccEmail = 'tchparking001@gmail.com';

                if (self::$file) {
                    // $message->to($email, 'test')->subject(self::$title)->attach(self::$file, array('as' => 'xxxx' . self::$data['code'] . '.pdf', 'mime' => 'application/pdf'));
                } else {
                    // $message->bcc($bccEmail, 'sc.lin');
                    $message->bcc($bccEmail, 'test-backup');
                    $message->to($email, 'test')->subject(self::$title);

                }
            });
        } catch (\Exception $e) {
        }
    }

}
