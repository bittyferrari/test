<?php

namespace App\Macros;

use Illuminate\Routing\Router as DefaultRouter;
use Illuminate\Support\Facades\Route;

class Router
{
    public static function registerMacros()
    {
        if (!DefaultRouter::hasMacro('crud')) {
            // DefaultRouter::macro('crud', function ($prefix, $controller, $get = [], $post = [], $namingPrefix = 'admin') {
            DefaultRouter::macro('crud', function ($prefix, $controller, $get = [], $post = []) {
                $get[] = 'listing';
                $get[] = 'item';
                $post[] = 'getListing';
                $post[] = 'updateDo';
                $post[] = 'deleteDo';
                Route::prefix($prefix)->group(function () use ($prefix, $controller, $get, $post) {
                    foreach ($get as $x) {
                        // $name = "$namingPrefix.$prefix.$x";
                        Route::get('/' . $x, $controller . '@' . $x);
                    }
                    foreach ($post as $x) {
                        // $name = "$namingPrefix.$prefix.$x";
                        Route::post('/' . $x, $controller . '@' . $x);
                    }
                });
            });
        }
    }
}
