<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class PermissionMiddleware
{
    // protected $auth;

    public function __construct(Guard $auth)
    {
        // $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        // return $next($request);
        // $user = $this->auth->user();
        // $user = getUser();
        $user = getAdmin();

        // $name = $user['name'];
        $isPermission = false;

        $action = $request->route()->getAction();
        $controller = class_basename($action['controller']);
        list($controller, $action) = explode('@', $controller);
        $controller = lcfirst($controller);
        $controller = str_replace('Controller', '', $controller);

        if ($user) {
            // is super admin
            // if ($user['id'] == 1 || $user['roleID'] == 1) {
            if ($user['id'] == 1) {
                $isPermission = true;
            } else {

                // exception
                switch ($controller) {
                    case 'self':
                        $isPermission = true;
                        break;
                }

                // // merger all role permissions
                // $permissions = [];
                // $roles = Role::whereIn('id', $user['role_ids'])->select(['permissions'])->get();
                // foreach ($roles as $x) {
                //     foreach ($x['permissions'] as $xx) {
                //         $permissions[] = $xx;
                //     }
                // }

                $permissions = $user['permissions'];

                if (!$isPermission) {

                    // check permission
                    $request = request();

                    $permissionType = '';

                    switch ($controller) {
                        // always allow controller
                        case '_helper':
                        case 'dashboard':
                            $isPermission = true;
                            break;
                        default:
                            switch ($action) {
                                case 'index':
                                case 'listing':
                                case 'item':
                                case 'getListing':
                                case 'map':
                                case 'qrcode':
                                case 'exportDo':
                                case 'getData':
                                    $permissionType = 'read';
                                    break;
                                case 'updateDo':
                                case 'batchCreate':
                                case 'batchCreateDo':
                                    $permissionType = 'save';
                                    break;
                                case 'deleteDo':
                                    $permissionType = 'delete';
                                    break;
                            }
                            break;
                    }

                    $permissionType = $controller . '-' . $permissionType;

                    foreach ($permissions as $x) {
                        if ($permissionType == $x) {
                            $isPermission = true;
                            break;
                        }
                    }
                }

            }
        }

        if ($isPermission) {
            return $next($request);
        } else {
            abort(403);
        }
    }
}
