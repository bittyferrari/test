<?php

namespace App\Http\Controllers;

use App\Model\User;
use App\Model\_LineLog;

class LiffController extends _Controller
{

    public function index()
    {
        $v = &$this->vueData;

        $v['liffID'] = env('LINE_LIFF_ID');
        $v['typeID'] = request('typeID');

        if ($v['typeID'] == 'register') {
            setSession('registerRecommendCode', request('recommendCode'));
        }

        $self = getUser();

        $v['self'] = $self;

        if ($self) {

            return $this->view();

        } else {

            setSession('liffTypeID', $v['typeID']);
            return $this->view();
        }

    }

    public function setUserDo()
    {
        $lineLog = new _LineLog;

        $accessToken = request('accessToken');
        $url = 'https://api.line.me/oauth2/v2.1/verify';

        $curl = curl_init();
        $postData = null;
        $postData['id_token'] = $accessToken;
        $postData['client_id'] = env('LINE_LOGIN_CHANNEL_ID');

        $lineLog['requestJson'] = jsonEncode($postData);

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        /*
        "{"iss":"https://access.line.me","sub":"U16532a41e8ae864124460bd001d531c4","aud":"1654358492","exp":1593355191,"iat":1593351591,
        "name":"Bitty","picture":"https://profile.line-scdn.net/0m0206aca572515724a62789999e16e30dc9e89b926f67"}"
         */

        /*
        {"iss":"https://access.line.me","sub":"U2363548b8a020320464218f840beb7dd","aud":"1654358492",
        "exp":1594722524,"iat":1594718924,"name":"陳建仲",
        "picture":"https://profile.line-scdn.net/0hTbZ-2FZwC01nVCEeDGB0GlsRBSAQeg0FH2dMexEHXC1ON0weUjpNKkZRAHxCM0tPW2dDIkoHBXwZ"}
         */

        $curlResult = curl_exec($curl);

        $lineLog['response'] = $curlResult;
        $lineLog->save();

        $json = jsonDecode($curlResult);

        $isSuccess = false;

        if (isset($json['sub'])) {
            $isSuccess = true;

            $lineUserID = $json['sub'];

            // $photoLine = null;
            // $nameLine = null;

            // create of update user
            $item = User::where('lineUserID', '=', $json['sub'])->first();

            if (!$item) {
                $item = new User;
                $item['lineUserID'] = $lineUserID;
                // $item['photo'] = $linePhoto;
                // $item['name'] = $nameLine;
            }

            if (isset($json['name'])) {
                $nameLine = $json['name'];
                $item['nameLine'] = $nameLine;
                $item['name'] = $nameLine;
            }
            if (isset($json['picture'])) {
                $photoLine = $json['picture'];
                $item['photoLine'] = $photoLine;
            }

            $item->save();

            setSession('userID', $item['id']);
        }

        $data = [];
        $data['isSuccess'] = $isSuccess;
        $this->setData($data);

        return $this->returnJson();

    }

}
