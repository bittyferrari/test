<?php

namespace App\Http\Controllers;

use App\Model\Course;
use App\Model\CourseLike;
use Illuminate\Http\Request;
use Storage;

class _HelperController extends _Controller
{

    public function refereshCapchaDo()
    {
        $urlCaptcha = captcha_src();
        $this->setData($urlCaptcha);
        return $this->returnJson();
    }

    public function uploadFile(Request $request)
    {

        $fileOriginName = $request->file('file')->getClientOriginalName();
        $ext = pathinfo($fileOriginName, PATHINFO_EXTENSION);
        $fileName = md5(uniqid(rand(), true)) . '.' . $ext;
        $file = $request->file('file');

        Storage::disk('public')->put('/photo/' . $fileName, file_get_contents($file));

        $data = ['fileName' => $fileName];
        return response()->json($data);

    }

    public function uploadFiles(Request $request)
    {

        $data = array();

        $files = $request->file('files');

        foreach ($files as $file) {

            $fileOriginName = $file->getClientOriginalName();
            $ext = pathinfo($fileOriginName, PATHINFO_EXTENSION);

            $fileName = md5(uniqid(rand(), true)) . '.' . $ext;

            saveFile($file->getRealPath(), $fileName);

            $data[] = array('fileName' => $fileName);

        }

        returnJson($data);
    }

    public function sendSmsDo()
    {
        die();

        $self = getUser();
        if (!$self) {
            die();
        }

        $v = &$this->vueData;

        $phone = request('phone');

        $verifyCode = randomNumber(6);

        $subject = '驗證碼';
        $smsBody = '您的驗證碼:' . $verifyCode;

        SmsHelper::sendDo($phone, $subject, $smsBody);

        $self['verifyCode'] = $verifyCode;
        $self->save();

        // check is get prize
        $data = [];
        $this->setData($data);
        $this->setStatusID($statusID);
        return $this->returnJson();
    }

    public function courseLikeDo()
    {

        $self = getUser();
        if (!$self) {
            die();
        }

        $v = &$this->vueData;

        $request = request();

        $courseID = $request['courseID'];

        $course = Course::find($courseID);

        $isLike = false;
        if ($course) {

            $item = CourseLike::where('userID', '=', $self['id'])->where('courseID', '=', $courseID)->first();

            if ($item) {
                $item->delete();
            } else {
                $item = new CourseLike;

                $isLike = true;
                $item['userID'] = $self['id'];
                $item['courseID'] = $course['id'];
                $item->save();
            }
            // return redirect('/userSchool/main');
        }

        // check is get prize
        $data = [];
        $data['isLike'] = $isLike;
        $this->setData($data);
        // $this->setStatusID($statusID);
        return $this->returnJson();
    }

}
