<?php

namespace App\Http\Controllers;

use App\Model\Admin;
use App\Model\Store;
// use App\Model\TransactionReset;
use App\Model\Transaction;
use Hash;

class ManagerController extends _Controller
{

    public function getListing()
    {
        $listData = getListCondition();
        $result = Store::orderby($listData['orderField'], $listData['orderType']);

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['typeID'];
        $conditions[] = ['name', 'like'];

        $where = getWhereCondition($conditions);
        $result = $result->where($where);

        $data = getListData($result, $listData);
        $this->setData($data);
        return $this->returnJson();
    }

    public function login()
    {
        $v = &$this->vueData;
        return $this->view();
    }

    public function loginDo()
    {
        $this->setStatusID(1);

        $v = &$this->vueData;

        $username = request('username');
        $password = request('password');

        $admin = Admin::where('username', '=', $username)->first();

        if ($admin) {

            if (Hash::check($password, $admin['password'])) {
                setSession('adminID', $admin['id']);
                $this->setStatusID(0);
            }

        }

        return $this->returnJson();
    }

    public function password()
    {
        $v = &$this->vueData;

        $admin = getAdmin();
        if ($admin == null) {
            return redirect('/manager/login');
        }

        $v['item'] = $admin;

        return $this->view();
    }

    public function updatePasswordDo()
    {
        $v = &$this->vueData;

        $admin = getAdmin();
        if ($admin == null) {
            return redirect('/manager/login');
        }

        $password = request('password');

        $admin['password'] = bcrypt($password);

        $admin->save();

        return $this->returnJson();
    }

    public function main()
    {
        $admin = getAdmin();
        if (!$admin) {
            redirect('/manager/login');
        }
        $v = &$this->vueData;

        $v['admin'] = $admin;
        return $this->view();
    }

    public function index()
    {
        $admin = getAdmin();
        if (!$admin) {
            redirect('/manager/login');
        }
        $v = &$this->vueData;

        $v['admin'] = $admin;
        return $this->view();
    }

    public function profile()
    {
        $admin = getAdmin();
        if (!$admin) {
            redirect('/manager/login');
        }
        $v = &$this->vueData;

        $v['admin'] = $admin;
        return $this->view();
    }

    public function setting()
    {
        $admin = getAdmin();
        if (!$admin) {
            redirect('/manager/login');
        }
        $v = &$this->vueData;

        $v['admin'] = $admin;
        return $this->view();
    }

    public function scan2()
    {
        $v = &$this->vueData;
        $admin = getAdmin();

        if ($admin == null) {
            return redirect('/manager/login');
        }
        return $this->view();
    }

    public function scan()
    {
        $v = &$this->vueData;

        $admin = getAdmin();

        if ($admin == null) {
            return redirect('/manager/login');
        }

        $v['testingMd5'] = Transaction::orderby('id', 'desc')->first()['md5'];

        return $this->view();
    }

    public function reset()
    {
        $v = &$this->vueData;
        return $this->view();
    }

    public function transaction()
    {
        $v = &$this->vueData;

        $admin = getAdmin();
        if ($admin == null) {
            return redirect('/store/login');
        }

        $typeID = request('typeID');
        $year = request('year');
        $month = request('month');

        if (empty($year) || empty($month)) {
            $year = date('Y');
            $month = date('m');
        }

        $year = intval($year);
        $month = intval($month);

        if (empty($typeID)) {
            $typeID = 'exchange';
        }

        $items = [];
        if ($typeID == 'exchange') {
            $items = Transaction::where('adminID', '=', $admin['id'])->where('typeID', '=', 11)
                ->whereYear('createdAt', $year)->whereMonth('createdAt', $month)->where('isSuccess', '=', 1)->orderby('id', 'desc')->get();
        } else {
            // $items = Transaction::where('adminID', '=', $admin['id'])->where('typeID', '=', 11)
            //     ->whereYear('createdAt', $year)->whereMonth('createdAt', $month)->where('isSuccess', '=', 1)->orderby('id', 'desc')->get();
        }

        $items = Transaction::where('adminID', '=', $admin['id'])->whereIn('typeID', [10, 11])
            ->whereYear('createdAt', $year)->whereMonth('createdAt', $month)->where('isSuccess', '=', 1)->orderby('id', 'desc')->get();

        $stores = Store::select(['id', 'name'])->whereIn('id', $items->pluck('storeID'))->get();

        $this->vueOption['store'] = $stores;
        $v['items'] = $items;
        $v['typeID'] = $typeID;
        $v['year'] = $year;
        $v['month'] = $month;

        return $this->view();
    }

    public function updateDo()
    {
        $store = getStore();
        if ($store == null) {
            return redirect('/store/login');
        }

        $v = &$this->vueData;
        return $this->view();
    }

    public function logoutDo()
    {

        forgetSession('adminID');
        forgetSession('admin');

        return redirect('/manager/login');
    }

}
