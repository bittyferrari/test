<?php

namespace App\Http\Controllers;

use App\Model\Store;
use App\Model\Transaction;
use App\Model\User;
use App\Model\UserCoupon;
use App\Model\_RequestLog;
use DateTime;

class TransactionController extends _Controller
{

    /*
    .##.....##..######..########.########.
    .##.....##.##....##.##.......##.....##
    .##.....##.##.......##.......##.....##
    .##.....##..######..######...########.
    .##.....##.......##.##.......##...##..
    .##.....##.##....##.##.......##....##.
    ..#######...######..########.##.....##
     */
    public function userCancelDo()
    {
        $requestLog = new _RequestLog;
        $requestLog->save();

        $self = getUser();

        $id = request('id');
        $item = Transaction::find($id);

        if ($item && $item['userID'] == $self['id']) {
            $item['isCancel'] = 1;
            $item->save();
        }

        $data = [];
        $data['item'] = $item;

        $this->setData($data);
        return $this->returnJson();
    }

    public function userConfirmDo()
    {

        $requestLog = new _RequestLog;
        $requestLog->save();

        $self = getUser();

        // $id = request('id');
        $id = request('id');

        $statusID = -1;
        $item = Transaction::find($id);

        if ($item) {

            // if ($item['userID'] == $self['id'] && $item['storeID'] != null && $item['isStoreScan'] == 1 && $item['price'] != 0) {
            if ($item['userID'] == $self['id'] && $item['storeID'] != null && $item['isStoreScan'] == 1) {

                if ($item['isSuccess'] == 0 && $item['isCancel'] == 0 && $item['isUserConfirm'] == 0) {

                    $store = getStore($item['storeID']);
                    $user = getUser($item['userID']);

                    $item['isEnd'] = 1;
                    $item['confirmedAt'] = date('Y-m-d H:i:s');
                    $statusID = 0;

                    switch ($item['typeID']) {

                        case 1: // save
                            if ($item['isStoreEnough'] == 1) {
                                $item['isUserConfirm'] = 1;
                                $item['isSuccess'] = 1;
                                $user['price'] += $item['price'];
                                $user->save();
                                $store['priceStore'] += $item['priceOrigin'];
                                $store->save();
                            }
                            break;
                        case 2: // exchange
                            $user = recalculateUserCoupon($item['userID']);

                            if ($user['price'] + $user['priceCoupon'] + $user['pricePointCoupon'] + $item['price'] >= 0) {

                                // use coupon first
                                $now = date('Y-m-d');
                                $userCoupons = UserCoupon::orderby('timeTo', 'asc')
                                    ->where('userID', '=', $user['id'])->whereDate('timeFrom', '<=', $now)->whereDate('timeTo', '>=', $now)->get();

                                $priceTemp = $item['price'] * -1;

                                foreach ($userCoupons as $x) {

                                    $price = $x['price'];
                                    $pricePoint = $x['pricePoint'];
                                    $priceUsed = $x['priceUsed'];
                                    $pricePointUsed = $x['pricePointUsed'];

                                    $priceAvailable = $price - $priceUsed;
                                    $pricePointAvailable = $pricePoint - $pricePointUsed;

                                    if ($pricePointAvailable > 0) {
                                        $differ = 0;
                                        if ($priceTemp > $pricePointAvailable) {
                                            $differ = $pricePointAvailable;
                                        } else {
                                            $differ = $priceTemp;
                                        }

                                        $priceTemp -= $differ;
                                        $x['pricePointUsed'] += $differ;

                                        $item['usedCouponPricePoint'] += $differ;
                                    }

                                    // print 'priceTEmp=' . $priceTemp;
                                    if ($priceTemp > 0) {

                                        if ($priceAvailable > 0) {
                                            $differ = 0;

                                            if ($priceTemp > $priceAvailable) {
                                                $differ = $priceAvailable;
                                            } else {
                                                $differ = $priceTemp;
                                            }

                                            $priceTemp -= $differ;
                                            $x['priceUsed'] += $differ;

                                            $item['usedCouponPrice'] += $differ;

                                        }

                                    }

                                    $x->save();

                                    if ($priceTemp <= 0) {
                                        break;
                                    }

                                }

                                if ($priceTemp > 0) {
                                    if ($user['price'] >= $priceTemp) {
                                        $user['price'] -= $priceTemp;
                                        $user->save();
                                        $item['usedPrice'] += $priceTemp;
                                        $priceTemp = 0;
                                    }
                                }

                                if ($priceTemp <= 0) {
                                    $item['isUserConfirm'] = 1;
                                    $item['isSuccess'] = 1;
                                }

                            } else {
                                // not enough failed
                                // $item['isSuccess'] = 0;
                                // $item['isEnd'] = 1;
                            }

                            break;
                        case 3: // refund

                            // update refunded transcation
                            $refundTransaction = Transaction::find($item['refundTransactionID']);
                            if ($refundTransaction) {

                                if ($refundTransaction['usedPrice'] > 0) {

                                    $user['price'] += $refundTransaction['usedPrice'];
                                    $item['isUserConfirm'] = 1;
                                    $item['isSuccess'] = 1;
                                    $user->save();

                                    $refundTransaction['timeRefunded'] = new DateTime();
                                    $refundTransaction['isRefunded'] = 1;
                                    $refundTransaction->save();

                                }

                            }

                            break;
                    }
                    $item->save();

                    if ($item['isSuccess'] == 1) {
                        // print 'zzzzzzzzzzzzz';
                        recalculateUserCoupon($item['userID'], true);
                        // refresh user price pricePointCoupon
                        // updateUserPoint($user);
                    }

                } else {
                    // transcation confirmed already
                    $statusID = 3;
                }

            } else {

                // not own by user
                $statusID = 2;
            }

        } else {
            // not found
            $statusID = 1;
        }

        $data = [];

        $this->setData($data);

        return $this->returnJson();

    }

    /*
    .##.....##....###....##....##....###.....######...########.########.
    .###...###...##.##...###...##...##.##...##....##..##.......##.....##
    .####.####..##...##..####..##..##...##..##........##.......##.....##
    .##.###.##.##.....##.##.##.##.##.....##.##...####.######...########.
    .##.....##.#########.##..####.#########.##....##..##.......##...##..
    .##.....##.##.....##.##...###.##.....##.##....##..##.......##....##.
    .##.....##.##.....##.##....##.##.....##..######...########.##.....##
     */

    public function managerCancelDo()
    {
        $requestLog = new _RequestLog;
        $requestLog->save();

        $admin = getAdmin();
        if (!$admin) {
            die();
        }

        $v = &$this->vueData;

        $md5 = request('md5');
        // $typeID = request('typeID');

        $item = null;
        $user = null;

        $store = [];

        $statusID = -1;
        $isSuccess = false;

        $item = Transaction::where('md5', '=', $md5)->orderby('id', 'desc')->first();

        if ($item && ($item['typeID'] == 10 || $item['typeID'] == 11)) {

            $now = time();

            $isSuccess = true;

            $item['isCancel'] = 1;
            $item->save();

        } else {
            $statusID = 2;
        }

        $data = [];
        $data['item'] = $item;
        $data['store'] = $store;
        $data['isSuccess'] = $isSuccess;

        $this->setData($data);

        $this->setStatusID($statusID);
        return $this->returnJson();
    }

    public function managerConfirmDo()
    {
        $requestLog = new _RequestLog;
        $requestLog->save();

        $admin = getAdmin();
        if (!$admin) {
            die();
        }

        $v = &$this->vueData;

        $md5 = request('md5');
        // $typeID = request('typeID');

        $item = null;
        $user = null;

        $store = [];

        $statusID = -1;
        $isSuccess = false;

        $item = Transaction::where('md5', '=', $md5)->orderby('id', 'desc')->first();

        if ($item && ($item['typeID'] == 10 || $item['typeID'] == 11)) {

            $store = getStore($item['storeID']);
            if ($store) {

                $now = time();

                $item['isSuccess'] = 1;
                $item['isAdminConfirm'] = 1;
                $item['isEnd'] = 1;

                $isSuccess = true;

                switch ($item['typeID']) {
                    case 10:

                        $storeTotal = 0;
                        $storeTotal = Transaction::where('storeID', '=', $item['storeID'])->where('isSuccess', '=', 1)->where('typeID', '!=', 1)->sum('price');
                        $storeTotal *= -1;
                        $priceStoreSettlement = Transaction::where('storeID', '=', $item['storeID'])->where('isSuccess', '=', 1)->where('priceStoreSettlement', '>', 0)->sum('priceStoreSettlement');
                        $storeTotal -= $priceStoreSettlement;
                        $item['priceBeforeSettlement'] = $storeTotal;

                        break;
                    case 11:

                        $item['priceBeforeReset'] = $store['priceStoreMax'] - $store['priceStore'];
                        $store['priceStore'] = $store['priceStore'] - $item['priceReset'];

                        break;
                }

                $store->save();
                $item->save();

            }
        } else {
            $statusID = 2;
        }

        $data = [];
        $data['item'] = $item;
        $data['store'] = $store;
        $data['isSuccess'] = $isSuccess;

        $this->setData($data);

        $this->setStatusID($statusID);
        return $this->returnJson();
    }

    public function managerScanDo()
    {
        $requestLog = new _RequestLog;
        $requestLog->save();

        $admin = getAdmin();
        if (!$admin) {
            die();
        }

        $v = &$this->vueData;

        $md5 = request('md5');
        // $typeID = request('typeID');

        $item = null;
        $user = null;

        $store = [];

        $statusID = -1;
        $isSuccess = false;

        $item = Transaction::where('md5', '=', $md5)->orderby('id', 'desc')->first();

        if ($item && ($item['typeID'] == 10 || $item['typeID'] == 11)) {

            $now = time();
            if (strtotime($item['expiredAt']) > $now) {

                $store = Store::find($item['storeID']);

                $user = User::find($item['userID']);
                $isSuccess = true;

                $item['adminID'] = $admin['id'];
                $item['isAdminScan'] = 1;

                switch ($item['typeID']) {
                    case 10:
                        // $item['isSuccess'] = 1;
                        // $item['isEnd'] = 1;
                        break;
                    case 11:
                        // $item['isSuccess'] = 1;
                        // $item['isEnd'] = 1;
                        break;

                }

                if ($isSuccess) {
                    // $item['storeID'] = $store['id'];
                    // $item['isStoreScan'] = 1;
                    $item['scanedAt'] = date('Y-m-d H:i:s');
                    $item->save();
                    $statusID = 0;
                }

            } else {
                $statusID = 1;
            }

        } else {
            $statusID = 2;
        }

        $data = [];
        $data['item'] = $item;
        $data['store'] = $store;
        $data['isSuccess'] = $isSuccess;

        $this->setData($data);

        $this->setStatusID($statusID);
        return $this->returnJson();
    }

    /*
    ..######..########..#######..########..########
    .##....##....##....##.....##.##.....##.##......
    .##..........##....##.....##.##.....##.##......
    ..######.....##....##.....##.########..######..
    .......##....##....##.....##.##...##...##......
    .##....##....##....##.....##.##....##..##......
    ..######.....##.....#######..##.....##.########
     */

    public function storeCancelDo()
    {
        $requestLog = new _RequestLog;
        $requestLog->save();

        // $self = getUser();
        $store = getStore();

        $id = request('id');
        $item = Transaction::find($id);

        if ($item && $item['storeID'] == $store['id']) {

            $item['isStoreCancel'] = 1;
            $item['isEnd'] = 1;
            $item->save();
        }

        $data = [];
        $data['item'] = $item;

        $this->setData($data);
        return $this->returnJson();
    }

    public function storeCreateDo()
    {
        $store = getStore();
        if (!$store) {
            die();
        }

        $request = request();

        $typeID = $request['typeID'];

        $data = [];
        $isSuccess = true;

        $price = 0;

        $item = new Transaction;

        switch ($typeID) {

            case 10:
                $priceStoreSettlement = $request['priceStoreSettlement'];
                $priceStoreSettlement = intval($priceStoreSettlement);

                $storeTotal = 0;

                $storeTotal = Transaction::where('storeID', '=', $store['id'])->where('isSuccess', '=', 1)->where('typeID', '!=', 1)->sum('price');
                $storeTotal *= -1;

                $temp = Transaction::where('storeID', '=', $store['id'])->where('isSuccess', '=', 1)->where('priceStoreSettlement', '>', 0)->sum('priceStoreSettlement');

                $storeTotal -= $temp;

                if ($priceStoreSettlement > $storeTotal) {
                    $isSuccess = false;
                    $item['isSuccess'] = 0;
                    $item['isEnd'] = 1;
                }

                $item['priceStoreSettlement'] = $priceStoreSettlement;

                break;
            case 11:
                $priceReset = $request['priceReset'];
                $priceReset = intval($priceReset);

                // no need check
                /*
                if ($priceReset > ($store['priceStoreMax'] - $store['priceStore'])) {
                $isSuccess = false;
                $item['isSuccess'] = 0;
                $item['isEnd'] = 1;
                }
                 */

                $item['priceReset'] = $priceReset;
                break;

            default:
                $isSuccess = false;
                break;

        }

        if ($isSuccess) {
            $item['storeID'] = $store['id'];
            // $item['price'] = $price;
            $item['typeID'] = $typeID;
            $item['expiredAt'] = date('Y-m-d H:i:s', time() + 60);
            $item->save();
        }

        $data['item'] = $item;
        $data['isSuccess'] = $isSuccess;
        $this->setData($data);
        return $this->returnJson();

    }

    public function storeScanDo()
    {
        $requestLog = new _RequestLog;
        $requestLog->save();

        $store = getStore();
        if (!$store) {
            die();
        }

        $v = &$this->vueData;

        $md5 = request('md5');
        // $typeID = request('typeID');

        $item = null;
        $user = null;

        $statusID = -1;

        $isSuccess = false;

        $item = Transaction::where('md5', '=', $md5)->orderby('id', 'desc')->first();

        if ($item) {

            $now = time();
            if (strtotime($item['expiredAt']) > $now) {

                $user = User::find($item['userID']);

                // check type
                /*
                ['id' => 1, 'name' => '儲值'],
                ['id' => 2, 'name' => '消費'],
                ['id' => 3, 'name' => '退點'],
                ['id' => 4, 'name' => '獎品'],
                ['id' => 5, 'name' => '餐點'],
                ['id' => 6, 'name' => '湯品'],
                ['id' => 7, 'name' => '門票'],
                ['id' => 8, 'name' => '轉移'],
                 */

                $isSuccess = true;

                switch ($item['typeID']) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        // no
                        $isSuccess = false;
                        break;
                    case 5:
                        if ($user['isExchangeMeal'] != 1) {
                            $user['isExchangeMeal'] = 1;
                            $user->save();

                            $item['isSuccess'] = 1;
                            $item['confirmedAt'] = date('Y-m-d H:i:s');
                            $item['isEnd'] = 1;
                        } else {
                            $isSuccess = false;
                        }
                        break;

                    case 6:
                        if ($user['isExchangeSoup'] != 1) {
                            $user['isExchangeSoup'] = 1;
                            $user->save();

                            $item['isSuccess'] = 1;
                            $item['confirmedAt'] = date('Y-m-d H:i:s');
                            $item['isEnd'] = 1;
                        } else {
                            $isSuccess = false;
                        }

                        break;

                    case 7:

                        if ($user['price'] >= $item['price']) {
                            $user['price'] -= $item['price'];
                            $user->save();
                            $item['isSuccess'] = 1;
                            $item['confirmedAt'] = date('Y-m-d H:i:s');
                            $item['isEnd'] = 1;
                        } else {
                            $isSuccess = false;
                        }

                        break;
                    case 8:
                        // no
                        $isSuccess = false;
                        break;
                }

                if ($isSuccess) {
                    $item['storeID'] = $store['id'];
                    $item['isStoreScan'] = 1;
                    $item['scanedAt'] = date('Y-m-d H:i:s');
                    $item->save();
                    $statusID = 0;
                }

            } else {
                $statusID = 1;
            }

        } else {
            $statusID = 2;
        }

        $data = [];
        $data['item'] = $item;
        $data['user'] = $user;
        $data['isSuccess'] = $isSuccess;

        $this->setData($data);

        $this->setStatusID($statusID);
        return $this->returnJson();
    }

    public function storeUpdateDo()
    {
        $requestLog = new _RequestLog;
        $requestLog->save();

        $store = getStore();

        $item = null;

        $ratio = getRatio();

        if ($store) {

            $price = request('price');
            $md5 = request('md5');
            $typeID = request('typeID');

            $price = intval($price);

            if ($price > 0 || $typeID == 'refund') {

                $item = Transaction::where('md5', '=', $md5)->first();

                // if ($item && $item['isCancel'] != 1 && $item['storeID'] == null) {

                $now = time();
                if ($item && $item['isCancel'] != 1 && strtotime($item['expiredAt']) > $now && $item['typeID'] == null) {

                    switch ($typeID) {
                        case 'save': // save
                            $typeID = 1;

                            if ($store['isStoreValue'] != 1) {
                                $price = 0;
                                $item['isStoreEnough'] = 0;
                                $item['isEnd'] = 1;
                            } else {

                                $item['priceOrigin'] = $price;
                                $item['priceStore'] = $price;

                                $price *= $ratio;
                                $price = intval($price);

                                // check store priceStore
                                if (intval($store['priceStore'] + $item['priceOrigin']) <= intval($store['priceStoreMax'])) {
                                    // not enough
                                    // $item['isSuccess'] = true;
                                    $item['isStoreEnough'] = 1;
                                    // $item['isSuccess'] = true;
                                } else {
                                    // $item['isSuccess'] = false;
                                    $price = 0;
                                    $item['isStoreEnough'] = 0;
                                    $item['isEnd'] = 1;
                                }

                            }

                            break;
                        case 'exchange': // exchange
                            $typeID = 2;

                            if ($store['isExchangable'] != 1) {
                                $price = 0;
                                $item['isStoreEnough'] = 0;
                                $item['isEnd'] = 1;
                            } else {

                                $item['priceOrigin'] = $price * -1;

                                $user = recalculateUserCoupon($item['userID']);

                                $item['isUserPriceEnough'] = 1;

                                $price = $price * -1;
                                if ($user['priceCoupon'] + $user['pricePointCoupon'] + $user['price'] >= abs($price)) {
                                    // ok
                                } else {
                                    // failed, cant use cash
                                    $item['isEnd'] = 1;
                                    $item['isUserPriceEnough'] = 0;
                                }

                            }

                            break;

                        case 'refund': // refund
                            if ($store['isRefundable'] != 1) {
                                $price = 0;
                                $item['isStoreEnough'] = 0;
                                $item['isEnd'] = 1;
                            } else {

                                $refundTransactionID = request('refundTransactionID');
                                $transactionTemp = Transaction::find($refundTransactionID);

                                if ($transactionTemp
                                    && $transactionTemp['usedPrice'] > 0
                                    && $transactionTemp['isSuccess'] == 1
                                    && $transactionTemp['isRefunded'] == 0
                                    && $transactionTemp['userID'] == $item['userID']) {

                                    // check store
                                    $storeTotal = getStoreTotal($store['id']);

                                    // $price = $transactionTemp['price'] * -1;
                                    $price = $transactionTemp['usedPrice'];

                                    if ($storeTotal > $price) {
                                        $item['refundTransactionID'] = $transactionTemp['id'];
                                    } else {

                                        // failed
                                        $item['isSuccess'] = 0;
                                        $item['isEnd'] = 1;
                                        $price = 0;
                                    }

                                } else {
                                    $price = 0;
                                    $item['isEnd'] = 1;
                                }

                                $item['priceOrigin'] = $price;

                                $typeID = 3;
                            }
                            break;
                    }

                    $item['ratio'] = $ratio;
                    $item['typeID'] = $typeID;
                    $item['price'] = $price;
                    $item['isStoreScan'] = 1;
                    $item['isStoreSet'] = 1;
                    $item['storeID'] = $store['id'];
                    $item->save();
                }

            }

        }

        $data = [];
        $data['item'] = $item;

        $this->setData($data);
        return $this->returnJson();

    }

    /*
    ..######...#######..##.....##.##.....##..#######..##....##
    .##....##.##.....##.###...###.###...###.##.....##.###...##
    .##.......##.....##.####.####.####.####.##.....##.####..##
    .##.......##.....##.##.###.##.##.###.##.##.....##.##.##.##
    .##.......##.....##.##.....##.##.....##.##.....##.##..####
    .##....##.##.....##.##.....##.##.....##.##.....##.##...###
    ..######...#######..##.....##.##.....##..#######..##....##
     */
    public function createDo()
    {
        $self = getUser();
        if (!$self) {
            die();
        }

        $request = request();

        $typeID = $request['typeID'];

        $data = [];
        $isSuccess = true;

        $price = 0;

        $item = new Transaction;

        switch ($typeID) {
            case 1:
            case 2:
            case 3:
                break;
            case 4:
                // no
                $isSuccess = false;
                break;
            case 5:
                // check is used
                $data['isUsed'] = false;
                if ($self['isExchangeMeal'] == 1) {
                    $isSuccess = false;
                    $data['isUsed'] = true;
                }
                break;
            case 6:
                // check is used
                $data['isUsed'] = false;
                if ($self['isExchangeSoup'] == 1) {
                    $isSuccess = false;
                    $data['isUsed'] = true;
                }
                break;
            case 7:
                $countTicket = $request['countTicket'];
                if ($countTicket <= 0) {
                    die();
                }
                $price = $countTicket * 100;
                if ($price > $self['price']) {
                    $isSuccess = false;
                    // $price *= -1;
                    $data['isPriceEnough'] = false;
                }
                $price *= -1;
                break;
            case 8:
                // no
                $isSuccess = false;
                break;
            case 10:

                $priceStoreSettlement = $request['priceStoreSettlement'];

                $priceStoreSettlement = intval($priceStoreSettlement);

                // $price = $countTicket * 100;
                // if ($price > $self['price']) {
                $item['priceStoreSettlement'] = $priceStoreSettlement;

                // }
                break;
            default:
                $isSuccess = false;
                break;
        }

        if ($isSuccess) {
            $item['userID'] = $self['id'];
            $item['price'] = $price;
            $item['typeID'] = $typeID;
            $item['expiredAt'] = date('Y-m-d H:i:s', time() + 60);

            $item->save();
        } else {
            $item = null;
        }

        $data['item'] = $item;
        $data['isSuccess'] = $isSuccess;
        $this->setData($data);
        return $this->returnJson();

    }

    public function getItem()
    {

        $self = getUser();
        $store = getStore();

        $isStore = false;
        $isUser = false;

        $user = null;
        if ($store != null) {
            $isStore = true;
        }

        $id = request('id');
        $md5 = request('md5');

        $item = null;
        if (empty($md5)) {
            $item = Transaction::where('userID', '=', $self['id'])->find($id);
        } else {
            $item = Transaction::where('md5', '=', $md5)->first();
        }

        $isExpired = true;

        $isFound = $item != null;

        if ($item) {

            if (!empty($item['storeID'])) {
                $store = Store::find($item['storeID']);
            }
            $now = time();
            if (strtotime($item['expiredAt']) > $now) {
                $isExpired = false;
                if ($store != null) {
                    $user = User::find($item['userID']);
                }
                $isUser = $self['id'] == $item['userID'];
            }
        }

        $data = [];

        $data['item'] = $item;
        $data['store'] = $store;
        $data['user'] = $user;
        $data['isStore'] = $isStore;
        $data['isUser'] = $isUser;
        $data['isFound'] = $isFound;
        $data['isExpired'] = $isExpired;

        $this->setData($data);
        return $this->returnJson();
    }

}
