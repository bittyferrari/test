<?php

namespace App\Http\Controllers;

use App\Model\Admin;
use App\Model\UserPasswordLog;
use DateTime;
use Hash;

class AdminLoginController extends _Controller
{

    public function logoutDo()
    {

        $userID = getUserID();

        // add log
        // $userLogin = new UserLogin;
        // $userLogin['userID'] = $userID;
        // $userLogin['typeID'] = 2;
        // $userLogin->save();

        forgetSession('userID');
        forgetSession('user');
        forgetSession('isLogin');
        forgetSession('isBackend');
        forgetSession('userRoleID');
        forgetSession('userName');

        return redirect('/adminLogin/index');
    }

    public function index()
    {

        $v = &$this->vueData;

        return $this->view();
    }

    public function resetPasswordDo()
    {

        $this->setStatusID(1);
        $v = &$this->vueData;

        $request = request();

        $email = $request['email'];
        $password = $request['password'];
        $passwordNew = $request['passwordNew'];

        $where = null;
        $where[] = ['email', '=', $email];
        $item = Admin::where($where)->first();

        $userLoginTypeID = 3;
        $userLoginUserID = null;

        if ($item) {

            $userLoginUserID = $item['id'];

            if (!empty($item['password']) && Hash::check($password, $item['password'])) {

                $item['password'] = bcrypt($passwordNew);
                $item['timePasswordChanged'] = new DateTime();
                $item->save();

                $this->setStatusID(0);

                // add password log
                $userPasswordLog = new UserPasswordLog;
                $userPasswordLog['userID'] = $item['id'];
                $userPasswordLog['byUserID'] = $item['id'];
                $userPasswordLog['password'] = $item['password'];
                $userPasswordLog->save();

            }
        }

        return $this->returnJson();

    }

    public function loginDo()
    {

        $v = &$this->vueData;

        $request = request();

        $email = $request['email'];
        $password = $request['password'];

        $where = null;
        $where[] = ['email', '=', $email];
        $item = Admin::where($where)->first();

        $userLoginTypeID = 3;
        $userLoginUserID = null;

        if ($item) {

            $userLoginUserID = $item['id'];

            if ($item['isBackend'] == 1 && !empty($item['password']) && Hash::check($password, $item['password'])) {

                $isNeedChangePassword = true;

                // check is password need reset
                $timePasswordChanged = $item['timePasswordChanged'];

                if ($timePasswordChanged != null) {

                    $time = strtotime($item['timePasswordChanged']);
                    $now = time();
                    if ($now - 31536000 < $time) {
                        $isNeedChangePassword = false;
                    }

                }

                $isNeedChangePassword = false;

                if ($isNeedChangePassword == false) {

                    setLoginedSession($item);
                    $userLoginTypeID = 1;

                } else {
                    $this->setStatusID(2);

                }

            } else {
                $userLoginTypeID = 3;
                forgetSession('userID');
                forgetSession('user');
                forgetSession('isLogin');
                forgetSession('isBackend');
                forgetSession('userRoleID');
                forgetSession('userName');
                $this->setStatusID(1);

            }

        } else {
            forgetSession('userID');
            forgetSession('user');
            forgetSession('isLogin');
            forgetSession('isBackend');
            forgetSession('userRoleID');
            forgetSession('userName');
            $this->setStatusID(1);

        }

        return $this->returnJson();

    }

}
