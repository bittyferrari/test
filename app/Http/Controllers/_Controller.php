<?php

namespace App\Http\Controllers;

use App\Model\Community;
use App\Model\Teacher;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Route;
use JWTAuth;

class _Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $viewPrefix = '/';
    public $vueData = null;
    public $controller;
    public $action;
    public $title = '';
    public $isLogin = false;
    public $user = null;
    public $userID = null;
    public $vueOption = [];

    public $meta = [
        'title' => 'TITLE',
        'description' => 'DESCRIPTION',
        'image' => '',
        'url' => '',
    ];

    public $jsonHelper = [
        'data' => [],
        'message' => '',
        'statusID' => 0,
    ];

    public function getJwtUserID()
    {
        $userID = null;
        try {
            $token = JWTAuth::parseToken();
            $payload = JWTAuth::parseToken()->getPayload();
            $userID = $payload['userID'];
        } catch (Exception $e) {
            // return  response()->json(['error' => $e->getMessage()]);
        }
        return $userID;
    }

    public function addOption($q)
    {
        switch ($q) {
            case 'community':
                $this->vueOption[$q] = Community::select(['id', 'name'])->get();
                break;
            case 'teacher':
                $this->vueOption[$q] = Teacher::select(['id', 'name'])->get();
                break;

        }
    }

    protected function view($action = null, $controller = null)
    {
        $data = $this->vueData;
        $this->vueData['env'] = config('app.env');
        $this->vueData['isDev'] = config('app.env') == 'local';

        // get controller, action name
        $request = request();
        $action = $request->route()->getAction();
        $controller = class_basename($action['controller']);
        list($controller, $action) = explode('@', $controller);
        $controller = lcfirst($controller);
        $controller = str_replace('Controller', '', $controller);

        $this->vueData['option'] = array_merge(config('_constant'), $this->vueOption);
        $this->vueData['controllerName'] = $controller;
        $this->vueData['actionName'] = $action;
        $this->vueData['moduleName'] = $controller;

        // $this->vueData['user'] = getUser();
        // $this->vueData['self'] = getUser();
        // $this->vueData['self'] = getUser();
        $this->vueData['admin'] = getAdmin();
        $this->vueData['APP_URL'] = config('_constant')['APP_URL'];

        // $self = Auth::user();
        // $this->vueData['self'] = $self;

        // use multi roles permissions
        // $permissions = [];
        // $roles = Role::whereIn('id', $self['role_ids'])->select(['permissions'])->get();
        // foreach ($roles as $x) {
        //     foreach ($x['permissions'] as $xx) {
        //         $permissions[] = $xx;
        //     }
        // }
        // $this->vueData['self']['permissions'] = $permissions;
        // $this->vueData['locale'] = session()->get('locale');

        $data['vueData'] = $this->vueData;
        // $data['locale'] = $this->vueData['locale'];

        // return view('admin.' . $controller . '.' . $action, $data);
        return view($this->viewPrefix . $controller . '.' . $action, $data);
    }

    public function returnStatus($x)
    {
        return Response::make('', $x);
    }

    public function setData($x)
    {
        $this->jsonHelper['data'] = $x;
    }

    public function setMessage($x)
    {
        $this->jsonHelper['message'] = $x;

    }

    public function setStatusID($x)
    {
        $this->jsonHelper['statusID'] = $x;
    }

    public function returnJson()
    {
        $header = [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'utf-8',
        ];
        return response()->json($this->jsonHelper, 200, $header, JSON_UNESCAPED_UNICODE);
    }

    public function getControllerAction()
    {
        $request = request();
        $action = $request->route()->getAction();
        $controller = class_basename($action['controller']);
        list($controller, $action) = explode('@', $controller);
        return array('action' => $action, 'controller' => $controller);
    }

    public function __construct()
    {

        //----------------------------------------
        $request = request();
        $action = $request->route()->getAction();
        $controller = class_basename($action['controller']);
        list($controller, $action) = explode('@', $controller);
        $controller = lcfirst($controller);
        $controller = str_replace('Controller', '', $controller);

        $this->controller = $controller;
        $this->action = $action;
        $GLOBALS['controller'] = $controller;
        $GLOBALS['action'] = $action;

        //----------------------------------------

        //get controllerPath;
        $uri = Route::getFacadeRoot()->current()->uri();
        $xx = explode('/', $uri);

        unset($xx[count($xx) - 1]);

    }

}
