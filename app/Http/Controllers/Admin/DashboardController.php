<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\DirectionLog;
use App\Model\Poi;
use App\Model\PoiLog;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends _ControllerAdmin
{

    public function index()
    {

        $v = &$this->vueData;

        // $timestamp = time();
        // $timestamp -= 86400 * 30;
        // $time = date('Y-m-d H:i:s', $timestamp);

        // $items = Poi::select(['id', 'name'])->where('isRoutable', '=', 1)->get();

        // $chartData = [];
        // $chartLabels = [];
        // $chartDataQrcode = [];
        // $chartDataDestination = [];

        // // calculate
        // foreach ($items as $x) {
        //     $countQrcode = PoiLog::where('createdAt', '>=', $time)->where('poiID', '=', $x['id'])->count();
        //     $countDestination = DirectionLog::where('createdAt', '>=', $time)->where('toPoiID', '=', $x['id'])->count();

        //     $x['countQrcode'] = $countQrcode;
        //     $x['countDestination'] = $countDestination;
        //     $x->save();

        //     $chartLabels[] = $x['name'];
        // }

        // $items = PoiLog::get();

        // $poiTop10 = DirectionLog::get();

        // $poiQrcodeTop10 = Poi::select(['id', 'name', 'countQrcode'])->where('isRoutable', '=', 1)->orderBy('countQrcode', 'desc')->take(10)->get();
        // $poiDestincationTop10 = Poi::select(['id', 'name', 'countDestination'])->where('isRoutable', '=', 1)->orderBy('countDestination', 'desc')->take(10)->get();

        // $v['poiQrcodeTop10'] = $poiQrcodeTop10;
        // $v['poiDestincationTop10'] = $poiDestincationTop10;

        // $chartLabelsQrcode = [];
        // $chartLabelsDestination = [];

        // foreach ($poiQrcodeTop10 as $x) {
        //     $chartDataQrcode[] = $x['countQrcode'];
        //     $chartLabelsQrcode[] = $x['name'];
        // }

        // foreach ($poiDestincationTop10 as $x) {
        //     $chartDataDestination[] = $x['countDestination'];
        //     $chartLabelsDestination[] = $x['name'];
        // }

        // $v['chartDataQrcode'] = $chartDataQrcode;
        // $v['chartDataDestination'] = $chartDataDestination;
        // $v['chartLabelsQrcode'] = $chartLabelsQrcode;
        // $v['chartLabelsDestination'] = $chartLabelsDestination;

        return $this->view();
    }

    public function exportDo()
    {

        $v = &$this->vueData;

        // $timestamp = time();
        // $timestamp -= 86400 * 365;
        // $time = date('Y-m-d H:i:s', $timestamp);

        $dateFrom = request('dateFrom');
        $dateTo = request('dateTo');

        $timeFrom = $dateFrom . ' 00:00:00';
        $timeTo = $dateTo . ' 23:59:59';

        $timestampFrom = strtotime($timeFrom);

        if (time() - 31536000 > $timestampFrom) {
            die('can\'t export past 1 year.');
        }

        $items = Poi::select(['id', 'name'])->where('isRoutable', '=', 1)->get();

        $chartData = [];
        $chartLabels = [];
        $chartDataQrcode = [];
        $chartDataDestination = [];

        // calculate
        foreach ($items as $x) {
            $countQrcode = PoiLog::where('createdAt', '>=', $timeFrom)->where('createdAt', '<=', $timeTo)->where('poiID', '=', $x['id'])->count();
            $countDestination = DirectionLog::where('createdAt', '>=', $timeFrom)->where('createdAt', '<=', $timeTo)->where('toPoiID', '=', $x['id'])->count();

            $x['countQrcode'] = $countQrcode;
            $x['countDestination'] = $countDestination;
            $x->save();

            $chartLabels[] = $x['name'];
        }

        return Excel::download(new ExcelExport, 'export.xlsx');

    }

}

class ExcelExport implements FromArray, ShouldAutoSize
{
    function array(): array
    {

        $items = Poi::orderby('countQrcode', 'desc')->where('isRoutable', '=', 1)->get();

        $data = [];
        $row = [];
        $row[0] = 'poi';
        $row[1] = 'qrcode times';
        $row[2] = 'destination times';
        $data[] = $row;

        foreach ($items as $x) {

            $row = [];

            if (empty($x['countQrcode'])) {
                $x['countQrcode'] = 0;
            }
            if (empty($x['countDestination'])) {
                $x['countDestination'] = 0;
            }

            $row[0] = '' . $x['name'];
            $row[1] = '' . $x['countQrcode'];
            $row[2] = '' . $x['countDestination'];

            $data[] = $row;
        }

        return $data;

    }
}
