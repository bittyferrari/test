<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\Community;
use App\Model\User as Model;
use App\Model\UserPasswordLog;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Facades\Excel;

class ExcelExport implements FromArray, ShouldAutoSize
{
    function array(): array
    {

        $request = request();

        $listData = getListCondition();
        $result = Model::orderby($listData['orderField'], $listData['orderType']);

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['roleID'];
        $conditions[] = ['isCompleteRegister'];
        $conditions[] = ['name', 'like'];
        $conditions[] = ['birthday', 'like'];
        $conditions[] = ['nameLine', 'like'];
        $conditions[] = ['phone', 'like'];
        $conditions[] = ['nationID', 'like'];
        $conditions[] = ['email', 'like'];
        $conditions[] = ['username', 'like'];
        $conditions[] = ['recommendCode', 'like'];

        $where = getWhereCondition($conditions);

        $result = $result->where($where);

        $items = $result->get();

        $data = [];
        $row = [];
        $row[] = 'ID';
        $row[] = 'Line名稱';
        $row[] = '生日';
        $row[] = '手機';
        $row[] = '總太點數';
        $row[] = '活動點數';
        $row[] = '推薦碼';
        $row[] = '是否已註冊';
        $row[] = '推薦人';
        $row[] = '源推薦人';
        $data[] = $row;

        $userIDs = [];
        foreach ($items as $x) {
            $userIDs[] = $x['registerRecommendUserID'];
            $userIDs[] = $x['registerParentRecommendUserID'];
        }

        $users = Model::select(['id', 'name'])->whereIn('id', $userIDs)->get()->pluck('name', 'id');

        foreach ($items as $x) {

            $row = [];

            $row[] = $x['id'];
            $row[] = $x['nameLine'];
            $row[] = $x['birthday'];
            $row[] = $x['phone'];
            $row[] = $x['price'];
            $row[] = $x['pricePointCoupon'];
            $row[] = $x['recommendCode'];
            $row[] = typeText('is', $x['isCompleteRegister']);

            if (isset($users[$x['registerRecommendUserID']])) {
                $row[] = $users[$x['registerRecommendUserID']];
            } else {
                $row[] = '--';
            }
            if (isset($users[$x['registerParentRecommendUserID']])) {
                $row[] = $users[$x['registerParentRecommendUserID']];
            } else {
                $row[] = '--';
            }

            $data[] = $row;
        }

        return $data;
    }
}

class UserController extends _ControllerAdmin
{

    public function listing()
    {
        $v = &$this->vueData;
        return $this->view();
    }

    public function getListing()
    {
        $listData = getListCondition();
        $result = Model::orderby($listData['orderField'], $listData['orderType']);

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['roleID'];
        $conditions[] = ['isCompleteRegister'];
        $conditions[] = ['name', 'like'];
        $conditions[] = ['birthday', 'like'];
        $conditions[] = ['nameLine', 'like'];
        $conditions[] = ['phone', 'like'];
        $conditions[] = ['nationID', 'like'];
        $conditions[] = ['email', 'like'];
        $conditions[] = ['username', 'like'];
        $conditions[] = ['recommendCode', 'like'];

        $where = getWhereCondition($conditions);

        $result = $result->where($where);

        $request = request();

        if (!empty($request['recommendUserName'])) {
            $userIDs = Model::where('nameLine', 'like', '%' . $request['recommendUserName'] . '%')->select(['id'])->get();
            $result = $result->whereIn('registerRecommendUserID', $userIDs);
        }

        if (!empty($request['parentRecommendUserName'])) {
            $userIDs = Model::where('nameLine', 'like', '%' . $request['parentRecommendUserName'] . '%')->select(['id'])->get();
            $result = $result->whereIn('registerParentRecommendUserID', $userIDs);
        }

        if (!empty($request['recommendUserRecommendCode'])) {
            $userIDs = Model::where('recommendCode', 'like', '%' . $request['recommendUserRecommendCode'] . '%')->select(['id'])->get();
            $result = $result->whereIn('registerRecommendUserID', $userIDs);
        }

        if (!empty($request['parentRecommendUserRecommendCode'])) {
            $userIDs = Model::where('recommendCode', 'like', '%' . $request['parentRecommendUserRecommendCode'] . '%')->select(['id'])->get();
            $result = $result->whereIn('registerParentRecommendUserID', $userIDs);
        }

        $data = getListData($result, $listData);

        $userIDs = [];
        foreach ($data['items'] as $x) {
            $userIDs[] = $x['registerRecommendUserID'];
            $userIDs[] = $x['registerParentRecommendUserID'];
        }

        $data['user'] = Model::select(['id', 'name', 'recommendCode'])->whereIn('id', $userIDs)->get();

        $this->setData($data);
        return $this->returnJson();
    }

    public function exportDo()
    {
        return Excel::download(new ExcelExport, 'export.xlsx');
    }

    public function updateDo()
    {
        $this->setStatusID(1);

        $request = request('item');
        $id = $request['id'];

        $item = Model::find($id);
        if (!$item) {
            $item = new Model;
        }

        $isChangePassword = false;
        $ignoreField = ['price', 'pricePointCoupon'];
        if (empty($request['password'])) {
            $ignoreField = ['password', 'timePasswordChanged'];
        } else {
            $request['password'] = bcrypt($request['password']);
            $request['timePasswordChanged'] = null;
            $isChangePassword = true;
        }

        setModelData($item, $request, $ignoreField);

        $isSuccess = $item->save();
        if ($isSuccess) {
            $this->setStatusID(0);
            if ($isChangePassword) {
                $self = getUser();
                // add password log
                $userPasswordLog = new UserPasswordLog;
                $userPasswordLog['userID'] = $item['id'];
                $userPasswordLog['byUserID'] = $self['id'];
                $userPasswordLog['password'] = $item['password'];
                $userPasswordLog->save();
            }
        }

        $data = [];
        $data['item'] = $item;
        $this->setData($data);
        return $this->returnJson();
    }

    public function deleteDo()
    {
        $this->setStatusID(1);
        $id = request('id');
        $item = Model::find($id);
        if ($item) {
            $result = $item->delete();
            if ($result) {
                $this->setStatusID(0);
            }
        }
        return $this->returnJson();
    }

    public function item()
    {
        $v = &$this->vueData;

        $id = request('id');
        $item = Model::find($id);
        if ($item) {
        } else {
            $item = new Model;
        }

        $v['item'] = $item;

        $v['fieldRequired'] = Model::$fieldRequired;
        $v['recommendUser'] = Model::where('id', '=', $item['registerRecommendUserID'])->first();
        $v['parentRecommendUser'] = Model::where('id', '=', $item['registerParentRecommendUserID'])->first();

        $this->vueOption['community'] = Community::select(['id', 'name'])->get();

        return $this->view();
    }

}
