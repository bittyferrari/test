<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\Vehicle;
use Illuminate\Http\Request;
use Storage;

class _HelperController extends _ControllerAdmin
{

    public function checkBlacklist()
    {
        $isBlacklist = false;

        $carLicense = request('carLicense');
        $vehicle = Vehicle::where('carLicense', '=', $carLicense)->first();
        if ($vehicle) {
            $isBlacklist = $vehicle['isBlacklist'] == 1;
            if ($vehicle['isUnlock'] == 1) {
                $isBlacklist = false;
            }
        }
        return response()->json($isBlacklist);
    }

    public function uploadFile(Request $request)
    {

        $fileOriginName = $request->file('file')->getClientOriginalName();
        $ext = pathinfo($fileOriginName, PATHINFO_EXTENSION);
        $fileName = md5(uniqid(rand(), true)) . '.' . $ext;

        $file = $request->file('file');

        Storage::disk('public')->put('/photo/' . $fileName, file_get_contents($file));

        $data = ['fileName' => $fileName];
        return response()->json($data);

    }

    public function uploadFiles(Request $request)
    {

        $data = array();

        $files = $request->file('files');

        foreach ($files as $file) {

            $fileOriginName = $file->getClientOriginalName();
            $ext = pathinfo($fileOriginName, PATHINFO_EXTENSION);

            $fileName = md5(uniqid(rand(), true)) . '.' . $ext;

            saveFile($file->getRealPath(), $fileName);

            $data[] = array('fileName' => $fileName);

        }

        returnJson($data);
    }
}
