<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\Admin;
use App\Model\Store;
use App\Model\Transaction;
use App\Model\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Facades\Excel;

class ExcelExport implements FromArray, ShouldAutoSize
{
    function array(): array
    {

        $request = request();

        $storeID = request('storeID');
        $dateFrom = request('dateFrom');
        $dateTo = request('dateTo');

        $items = Transaction::whereDate('createdAt', '>=', $dateFrom)->whereDate('createdAt', '<=', $dateTo)->where('isSuccess', '=', 1);

        if (!empty($storeID) && $storeID != 'null') {
            $items = $items->where('storeID', '=', $storeID);
        }

        $items = $items->get();

        $userIDs = $items->pluck('userID');
        $users = User::select(['id', 'name'])->whereIn('id', $userIDs)->get()->pluck('name', 'id');
        $storeIDs = $items->pluck('storeID');
        $stores = Store::select(['id', 'name'])->whereIn('id', $storeIDs)->get()->pluck('name', 'id');

        $adminIDs = $items->pluck('adminID');
        $admins = Admin::select(['id', 'name'])->whereIn('id', $adminIDs)->get()->pluck('name', 'id');

        $data = [];
        $row = [];
        $row[] = 'ID';
        $row[] = '時間';
        $row[] = '交易類別';
        $row[] = '商家';
        $row[] = '交易對象';
        $row[] = '儲值面額';
        $row[] = '點數';
        $row[] = '補現金';
        $row[] = '成功';
        $data[] = $row;

        foreach ($items as $x) {

            $row = [];

            $row[] = $x['id'];
            $row[] = $x['createdAt'];
            $row[] = typeText('transactionTypeChinese', $x['typeID']);
            $row[] = arrayText($stores, $x['storeID']);

            switch ($x['typeID']) {
                case 10:
                case 11:
                    $row[] = arrayText($admins, $x['adminID']);
                    break;
                default:
                    $row[] = arrayText($users, $x['userID']);
                    break;
            }


            switch ($x['typeID']) {
                case 1:
                    $row[] = $x['priceOrigin'] * -1;
                    break;
                case 11:
                    $row[] = $x['priceReset'];
                    break;
                default:
                    $row[] = '';
                    break;
            }

            $price = '';
            switch ($x['typeID']) {
                case 1:
                    break;
                case 10:
                    $price = $x['priceStoreSettlement'] * -1;
                    break;
                default:
                    $price = $x['price'] * -1;
                    break;
            }

            $row[] = $price;
            $row[] = $x['priceCash'];
            $row[] = typeText('is', $x['isSuccess']);

            $data[] = $row;
        }

        return $data;
    }
}

class AccountingController extends _ControllerAdmin
{
    public $moduleName = '對帳';

    public function index()
    {
        $v = &$this->vueData;

        $this->vueOption['store'] = Store::select(['id', 'name'])->get();
        $this->vueOption['user'] = User::select(['id', 'name'])->get();
        $this->vueOption['admin'] = Admin::select(['id', 'name'])->get();

        return $this->view();
    }

    public function exportDo()
    {
        return Excel::download(new ExcelExport, 'export.xlsx');
    }

    public function getData()
    {

        $storeID = request('storeID');
        $dateFrom = request('dateFrom');
        $dateTo = request('dateTo');

        $items = Transaction::whereDate('createdAt', '>=', $dateFrom)->whereDate('createdAt', '<=', $dateTo)->where('isSuccess', '=', 1);

        if (!empty($storeID)) {
            $items = $items->where('storeID', '=', $storeID);
        }

        $items = $items->get();

        $data = [];
        $data['items'] = $items;

        $this->setData($data);
        return $this->returnJson();

    }

}
