<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\Community;
use App\Model\CourseUser as Model;
use App\Model\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Facades\Excel;

class CouponUserImport implements ToModel
{
    public function model(array $row)
    {
        return new Model([
            'phone' => $row[0],
            'nationID' => $row[1],
        ]);
    }
}

class CourseUserController extends _ControllerAdmin
{

    public function importDo()
    {
        $request = request();

        $items = Excel::toArray(new CouponUserImport, request()->file('file'));

        $couponID = $request['couponID'];
        foreach ($items[0] as $k => $x) {
            if ($k == 0) {
                continue;
            }

            $item = Model::where('couponID', '=', $couponID)->where('phone', '=', $x[0])->where('nationID', '=', $x[1])->first();
            if (!$item) {

                $item = new Model;
                $item['couponID'] = $couponID;

                $phone = $x[0];

                $phone = str_replace('-', '', $phone);
                $phone = str_replace('－', '', $phone);
                $phone = str_pad($phone, 10, '0', STR_PAD_LEFT);

                $item['phone'] = $phone;
                $item['nationID'] = $x[1];
                $item->save();
            }

        }
    }

    public function listing()
    {

        $v = &$this->vueData;
        $this->vueOption['user'] = User::select(['id', 'name'])->get();
        return $this->view();
    }

    public function item()
    {
        $v = &$this->vueData;

        $id = request('id');
        $item = Model::find($id);
        if ($item) {
        } else {
            $item = new Model;
            $item['couponID'] = request('couponID');
        }

        $v['item'] = $item;

        $v['fieldRequired'] = Model::$fieldRequired;

        $this->vueOption['user'] = User::select(['id', 'name'])->get();
        $this->vueOption['community'] = Community::select(['id', 'name'])->get();

        return $this->view();
    }

    public function getListing()
    {
        $listData = getListCondition();
        $result = Model::orderby($listData['orderField'], $listData['orderType']);

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['courseID'];
        $conditions[] = ['userID'];
        $conditions[] = ['isAlternate'];

        $conditions[] = ['phone', 'like'];
        $conditions[] = ['name', 'like'];

        $where = getWhereCondition($conditions);
        $result = $result->where($where);

        $data = getListData($result, $listData);
        $this->setData($data);
        return $this->returnJson();
    }

    public function updateDo()
    {
        $this->setStatusID(1);

        $request = request('item');
        $id = $request['id'];

        // check username used
        $item = Model::find($id);
        if (!$item) {
            $item = new Model;
        }

        setModelData($item, $request, ['price', 'isAlternate']);

        $isSuccess = $item->save();
        if ($isSuccess) {
            $this->setStatusID(0);
        }

        $data = [];
        $data['item'] = $item;
        $this->setData($data);

        return $this->returnJson();
    }

    public function deleteDo()
    {
        $this->setStatusID(1);
        $id = request('id');
        $item = Model::find($id);
        if ($item) {
            $result = $item->delete();
            if ($result) {
                $this->setStatusID(0);
            }
        }
        return $this->returnJson();
    }

}
