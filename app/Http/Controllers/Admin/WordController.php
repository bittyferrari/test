<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\Advertisement;
use App\Model\Word as Model;

class WordController extends _ControllerAdmin
{

    public function listing()
    {
        $v = &$this->vueData;
        return $this->view();
    }

    public function getListing()
    {
        $listData = getListCondition();
        $result = Model::orderby($listData['orderField'], $listData['orderType']);

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['name', 'like'];
        $conditions[] = ['md5', 'like'];
        $conditions[] = ['isActive'];

        $where = getWhereCondition($conditions);
        $result = $result->where($where);

        $data = getListData($result, $listData);
        $this->setData($data);
        return $this->returnJson();
    }

    public function updateDo()
    {
        $this->setStatusID(1);

        $request = request('item');
        $id = $request['id'];

        $item = Model::find($id);
        if (!$item) {
            $item = new Model;
        }

        setModelData($item, $request);

        $isSuccess = $item->save();
        if ($isSuccess) {
            $this->setStatusID(0);
        }

        $data = [];
        $data['item'] = $item;
        $this->setData($data);

        return $this->returnJson();
    }

    public function deleteDo()
    {
        $this->setStatusID(1);
        $id = request('id');
        $item = Model::find($id);
        if ($item) {
            $result = $item->delete();
            if ($result) {
                $this->setStatusID(0);
            }
        }
        return $this->returnJson();
    }

    public function item()
    {
        $v = &$this->vueData;

        $id = request('id');
        $item = Model::find($id);
        if ($item) {
        } else {
            $item = new Model;
        }

        $v['item'] = $item;

        $v['fieldRequired'] = Model::$fieldRequired;

        $this->vueOption['advertisement'] = Advertisement::select(['id', 'name'])->get();

        return $this->view();
    }

}
