<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\Event as Model;
use App\Model\UserPasswordLog;

class EventController extends _ControllerAdmin
{

    public function listing()
    {
        $v = &$this->vueData;
        return $this->view();
    }

    public function getListing()
    {
        $listData = getListCondition();
        $result = Model::orderby($listData['orderField'], $listData['orderType']);

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['name', 'like'];

        $where = getWhereCondition($conditions);

        $result = $result->where($where);

        $data = getListData($result, $listData);
        $this->setData($data);
        return $this->returnJson();
    }

    public function updateDo()
    {
        $this->setStatusID(1);

        $request = request('item');
        $id = $request['id'];

        $item = Model::find($id);
        if (!$item) {
            $item = new Model;
        }

        $isChangePassword = false;
        $ignoreField = [];
        if (empty($request['password'])) {
            $ignoreField = ['password', 'timePasswordChanged'];
        } else {
            $request['password'] = bcrypt($request['password']);
            $request['timePasswordChanged'] = null;
            $isChangePassword = true;
        }

        setModelData($item, $request, $ignoreField);

        $isSuccess = $item->save();
        if ($isSuccess) {
            $this->setStatusID(0);
            if ($isChangePassword) {
                $self = getUser();
                // add password log
                $userPasswordLog = new UserPasswordLog;
                $userPasswordLog['userID'] = $item['id'];
                $userPasswordLog['byUserID'] = $self['id'];
                $userPasswordLog['password'] = $item['password'];
                $userPasswordLog->save();
            }
        }

        $data = [];
        $data['item'] = $item;
        $this->setData($data);
        return $this->returnJson();
    }

    public function deleteDo()
    {
        $this->setStatusID(1);
        $id = request('id');
        $item = Model::find($id);
        if ($item) {
            $result = $item->delete();
            if ($result) {
                $this->setStatusID(0);
            }
        }
        return $this->returnJson();
    }

    public function item()
    {
        $v = &$this->vueData;

        $id = request('id');
        $item = Model::find($id);
        if ($item) {
        } else {
            $item = new Model;
        }

        $v['item'] = $item;
        $v['fieldRequired'] = Model::$fieldRequired;

        return $this->view();
    }

}
