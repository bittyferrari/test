<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\User as Model;
use DateTime;

class SelfController extends _ControllerAdmin
{

    public function updateDo()
    {
        $this->setStatusID(1);

        $request = request('item');
        $id = $request['id'];

        $item = Model::find($id);
        if (!$item) {
            $item = new Model;
        }

        if (empty($request['password'])) {
        } else {
            $item['password'] = bcrypt($request['password']);
            $item['timePasswordChanged'] = new DateTime();
        }

        $item['photo'] = $request['photo'];
        $item['name'] = $request['name'];

        $isSuccess = $item->save();
        if ($isSuccess) {
            $this->setStatusID(0);
        }

        $data = [];
        $data['item'] = $item;
        $this->setData($data);
        return $this->returnJson();
    }

    public function deleteDo()
    {
        $this->setStatusID(1);
        $id = request('id');
        $item = Model::find($id);
        if ($item) {
            $result = $item->delete();
            if ($result) {
                $this->setStatusID(0);
            }
        }
        return $this->returnJson();
    }

    public function item()
    {
        $v = &$this->vueData;

        $id = getUserID();
        $item = Model::find($id);
        if ($item) {
        } else {
            $item = new Model;
        }

        $v['item'] = $item;

        return $this->view();
    }

}
