<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\Store as Model;
use App\Model\Transaction;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Facades\Excel;

class RecordImport implements ToModel
{
    public function model(array $row)
    {
        return new Model([
            'phone' => $row[0],
            'nationID' => $row[1],
        ]);
    }
}

class ExcelExport implements FromArray, ShouldAutoSize
{
    function array(): array
    {

        $request = request();

        $listData = getListCondition();
        $result = Model::orderby($listData['orderField'], $listData['orderType']);

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['name', 'like'];
        $conditions[] = ['username', 'like'];

        $where = getWhereCondition($conditions);

        $result = $result->where($where);

        $items = $result->get();

        $data = [];
        $row = [];
        $row[] = 'ID';
        $row[] = '名稱';
        $row[] = '帳號';
        $row[] = '目前累積點數';
        $row[] = '目前可儲值餘額';
        $row[] = '可否儲值';
        $row[] = '啟用';
        $row[] = '順序';
        $data[] = $row;

        // $users = Model::select(['id', 'name'])->whereIn('id', $userIDs)->get()->pluck('name', 'id');

        foreach ($items as $x) {

            $storeTotal = 0;

            $storeTotal = Transaction::where('storeID', '=', $x['id'])->where('isSuccess', '=', 1)->where('typeID', '!=', 1)->sum('price');
            $storeTotal *= -1;

            $priceStoreSettlement = Transaction::where('storeID', '=', $x['id'])
                ->where('isSuccess', '=', 1)->where('priceStoreSettlement', '>', 0)->sum('priceStoreSettlement');

            $storeTotal -= $priceStoreSettlement;

            $x['storeTotal'] = $storeTotal;

            $row = [];

            $row[] = $x['id'];
            $row[] = $x['name'];
            $row[] = $x['username'];
            $row[] = $x['storeTotal'];
            $row[] = $x['priceStoreMax'] - $x['priceStore'];
            $row[] = typeText('is', $x['isStoreValue']);
            $row[] = typeText('is', $x['isActive']);
            $row[] = $x['isActsequenceive'];

            $data[] = $row;
        }

        return $data;
    }
}

class StoreController extends _ControllerAdmin
{

    public function exportDo()
    {
        return Excel::download(new ExcelExport, 'export.xlsx');
    }

    public function uploadExcelDo()
    {

        $request = request();
        $items = Excel::toArray(new RecordImport, $request->file('file'));

        // $id = $_GET['id'];
        // $equipment = Model::find($id);

        foreach ($items[0] as $i => $x) {

            if ($i < 1) {
                continue;
            }
            if (empty($x[0])) {
                continue;
            }

            // insert
            $item = new Model;
            $item['name'] = $x[0];
            $item['personInCharge'] = $x[1];
            $item['phone'] = $x[2];
            $item['username'] = $x[3];
            // $item['password'] = $x[4];
            $item['password'] = bcrypt($x[4]);
            $item['isActive'] = $x[5] == 'yes' ? 1 : 0;
            $item['isStoreValue'] = $x[6] == 'yes' ? 1 : 0;
            $item['isExchangable'] = $x[7] == 'yes' ? 1 : 0;
            $item['isRefundable'] = $x[8] == 'yes' ? 1 : 0;

            $item['addressText'] = $x[9];
            $item['nameUrl'] = $x[10];
            $item['url'] = $x[11];

            $typeID = getIdByName(config('_constant')['storeType'], $x[12]);
            $item['typeID'] = $typeID;

            // try {
            //     $qq = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($item['exe_time']);
            //     $item['time'] = $qq->format('Y-m-d H:i:s');
            // } catch (\Exception $e) {
            // }

            $item->save();
        }

        $data = [];
        $data['isSuccess'] = true;
        $this->setData($data);

        return $this->returnJson();
    }

    public function listing()
    {
        $v = &$this->vueData;

        return $this->view();
    }

    public function getListing()
    {
        $listData = getListCondition();
        $result = Model::orderby($listData['orderField'], $listData['orderType']);

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['name', 'like'];
        $conditions[] = ['username', 'like'];

        $where = getWhereCondition($conditions);
        $result = $result->where($where);

        $data = getListData($result, $listData);

        $data['items'] = $data['items']->map(function ($z) {

            $storeTotal = 0;

            $storeTotal = Transaction::where('storeID', '=', $z['id'])->where('isSuccess', '=', 1)->where('typeID', '!=', 1)->sum('price');
            $storeTotal *= -1;

            $priceStoreSettlement = Transaction::where('storeID', '=', $z['id'])
                ->where('isSuccess', '=', 1)->where('priceStoreSettlement', '>', 0)->sum('priceStoreSettlement');

            $storeTotal -= $priceStoreSettlement;

            $z['storeTotal'] = $storeTotal;

            return $z;

        });

        $this->setData($data);
        return $this->returnJson();
    }

    public function updateDo()
    {
        $this->setStatusID(1);

        $request = request('item');
        $id = $request['id'];
        $username = $request['username'];

        // check username used
        $store = Model::where('username', '=', $username)->where('id', '!=', $id)->first();
        if ($store) {
            $this->setStatusID(1);
        } else {

            $item = Model::find($id);
            if (!$item) {
                $item = new Model;
            }

            $ignoreField = ['priceStore'];
            if (empty($request['password'])) {
                $ignoreField = ['password'];
            } else {
                $request['password'] = bcrypt($request['password']);
                // $request['timePasswordChanged'] = null;
                $isChangePassword = true;
            }

            // setModelData($item, $request);
            setModelData($item, $request, $ignoreField);

            $isSuccess = $item->save();
            if ($isSuccess) {
                $this->setStatusID(0);
            }

            $data = [];
            $data['item'] = $item;
            $this->setData($data);

        }
        return $this->returnJson();
    }

    public function deleteDo()
    {
        $this->setStatusID(1);
        $id = request('id');
        $item = Model::find($id);
        if ($item) {
            $result = $item->delete();
            if ($result) {
                $this->setStatusID(0);
            }
        }
        return $this->returnJson();
    }

    public function item()
    {
        $v = &$this->vueData;

        $id = request('id');
        $item = Model::find($id);
        if ($item) {
        } else {
            $item = new Model;
        }

        $v['item'] = $item;

        $v['fieldRequired'] = Model::$fieldRequired;

        return $this->view();
    }

}
