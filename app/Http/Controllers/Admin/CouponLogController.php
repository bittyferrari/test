<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\CouponLog as Model;
use App\Model\User;

class CouponLogController extends _ControllerAdmin
{

    public function listing()
    {
        $v = &$this->vueData;

        $this->vueOption['user'] = User::select(['id', 'name'])->get();

        return $this->view();
    }

    public function item()
    {
        $v = &$this->vueData;

        $id = request('id');
        $item = Model::find($id);
        if ($item) {
        } else {
            $item = new Model;
            $item['couponID'] = request('couponID');
        }

        $v['item'] = $item;

        $v['fieldRequired'] = Model::$fieldRequired;

        $this->vueOption['user'] = User::select(['id', 'name'])->get();

        return $this->view();
    }

    public function getListing()
    {
        $listData = getListCondition();
        $result = Model::orderby($listData['orderField'], $listData['orderType']);

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['couponID'];
        $conditions[] = ['price'];
        $conditions[] = ['isSuccess'];
        $conditions[] = ['nationID', 'like'];
        $conditions[] = ['phone', 'like'];

        $where = getWhereCondition($conditions);
        $result = $result->where($where);

        $data = getListData($result, $listData);

        $data['items'] = $data['items']->map(function ($z) {

            $z['userPhotoLine'] = null;
            $z['userName'] = null;

            $user = User::select(['name', 'photoLine'])->find($z['userID']);
            if ($user) {
                $z['userPhotoLine'] = $user['photoLine'];
                $z['userName'] = $user['name'];

            }

            return $z;
        });

        $this->setData($data);

        return $this->returnJson();
    }

    public function updateDo()
    {
        $this->setStatusID(1);

        $request = request('item');
        $id = $request['id'];

        $item = Model::find($id);
        if (!$item) {
            $item = new Model;
        }

        setModelData($item, $request);

        $isSuccess = $item->save();
        if ($isSuccess) {
            $this->setStatusID(0);
        }

        $data = [];
        $data['item'] = $item;
        $this->setData($data);

        return $this->returnJson();
    }

    public function deleteDo()
    {
        $this->setStatusID(1);
        $id = request('id');
        $item = Model::find($id);
        if ($item) {
            $result = $item->delete();
            if ($result) {
                $this->setStatusID(0);
            }
        }
        return $this->returnJson();
    }

}
