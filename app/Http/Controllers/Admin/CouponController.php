<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\Coupon as Model;
use App\Model\Event;
use App\Model\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Facades\Excel;

class CouponExcelExport implements FromArray, ShouldAutoSize
{
    function array(): array
    {

        $request = request();
        $items = Model::get();

        $data = [];
        $row = [];
        $row[] = 'ID';
        $row[] = '名稱';
        $row[] = '代碼';
        $row[] = '金額';
        $row[] = '起';
        $row[] = '訖';
        $row[] = '已兌換次數';
        $row[] = '限量';
        $data[] = $row;

        foreach ($items as $x) {

            $row = [];

            $row[] = $x['id'];
            $row[] = $x['name'];
            $row[] = $x['code'];
            $row[] = $x['price'];
            $row[] = $x['timeFrom'];
            $row[] = $x['timeTo'];
            $row[] = $x['countUsed'];
            $row[] = $x['countLimit'];

            $data[] = $row;
        }

        return $data;
    }
}
class CouponController extends _ControllerAdmin
{

    public function exportDo()
    {
        return Excel::download(new CouponExcelExport, 'export.xlsx');
    }

    public function listing()
    {
        $v = &$this->vueData;

        $this->vueOption['user'] = User::select(['id', 'name'])->get();

        return $this->view();
    }

    public function batchCreate()
    {
        $v = &$this->vueData;

        $id = request('id');

        $item = [];
        $item['timeFrom'] = date('Y-m-d');
        $item['timeTo'] = date('Y-m-d');

        $v['item'] = $item;

        $v['fieldRequired'] = Model::$fieldRequired;

        $this->vueOption['user'] = User::select(['id', 'name'])->get();
        $this->vueOption['event'] = Event::select(['id', 'name'])->get();

        return $this->view();
    }

    public function batchCreateDo()
    {

        $result = [];

        $requestItem = request();
        $count = $requestItem['count'];
        $countSuccess = 0;

        $timesRetry = 0;
        for ($i = 1; $i <= $count; $i++) {

            // $nnn = sprintf('%0' . strlen($count) . 'd', $i);
            // $nnn = sprintf('%03d', $i);
            $nnn = '';
            $number = $requestItem['prefix'] . $nnn . getRandomString($requestItem['randomTextLength'], $requestItem['randomTexts']);

            $item = Model::where('code', '=', $number)->first();

            if (!$item) {

                $item = new Model;
                $item['name'] = $requestItem['name'];
                $item['code'] = $number;
                $item['isActive'] = $requestItem['isActive'];
                $item['timeFrom'] = $requestItem['timeFrom'];
                $item['timeTo'] = $requestItem['timeTo'];
                $item['priceTypeID'] = $requestItem['priceTypeID'];
                $item['createdTypeID'] = 2;
                $item['eventID'] = $requestItem['eventID'];
                $item['countLimit'] = $requestItem['countLimit'];
                $item['price'] = $requestItem['price'];
                $item['pricePoint'] = $requestItem['pricePoint'];

                $r = $item->save();

                if ($r) {
                    $countSuccess++;
                }
            } else {
                $i -= 1;
                $timesRetry++;

                if ($timesRetry > 100) {
                    break;

                }
            }

        }

        $result['countSuccess'] = $countSuccess;

        return returnJson($result);

    }

    public function item()
    {


        $v = &$this->vueData;

        $id = request('id');
        $item = Model::find($id);
        if ($item) {
        } else {
            $item = new Model;
            $item['timeFrom'] = date('Y-m-d');
            $item['timeTo'] = date('Y-m-d');
        }

        $v['item'] = $item;

        $v['fieldRequired'] = Model::$fieldRequired;

        $this->vueOption['user'] = User::select(['id', 'name'])->get();
        $this->vueOption['event'] = Event::select(['id', 'name'])->get();

        return $this->view();
    }

    public function getListing()
    {
        $listData = getListCondition();
        $result = Model::orderby($listData['orderField'], $listData['orderType']);

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['name', 'like'];
        $conditions[] = ['code', 'like'];
        $conditions[] = ['isActive'];
        $conditions[] = ['countLimit'];
        $conditions[] = ['price'];
        $conditions[] = ['pricePoint'];
        $conditions[] = ['priceTypeID'];

        $where = getWhereCondition($conditions);
        $result = $result->where($where);

        $data = getListData($result, $listData);
        $this->setData($data);
        return $this->returnJson();
    }

    public function updateDo()
    {


        $this->setStatusID(1);

        $request = request('item');
        $id = $request['id'];

        $item = Model::find($id);
        if (!$item) {
            $item = new Model;
        }

        setModelData($item, $request, ['countUsed']);

        $isSuccess = $item->save();
        if ($isSuccess) {
            $this->setStatusID(0);
        }

        $data = [];
        $data['item'] = $item;
        $this->setData($data);

        return $this->returnJson();
    }

    public function deleteDo()
    {
        $this->setStatusID(1);
        $id = request('id');
        $item = Model::find($id);
        if ($item) {
            $result = $item->delete();
            if ($result) {
                $this->setStatusID(0);
            }
        }
        return $this->returnJson();
    }

}
