<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\Setting;

class SettingController extends _ControllerAdmin
{
    public $moduleName = '系統設定';

    public function index()
    {
        $v = &$this->vueData;

        $mail = [];
        $jobs = [];
        $countAppointmentRemain = '';

        $items = Setting::get();

        $v['items'] = $items;

        return $this->view();

    }

    public function updateDo()
    {

        $requestItems = request('items');

        foreach ($requestItems as $x) {

            switch ($x['key']) {
                default:
                    $item = Setting::where('key', '=', $x['key'])->first();
                    $item['value'] = $x['value'];
                    $item->save();
                    break;
            }
        }

        return $this->returnJson();

    }

}
