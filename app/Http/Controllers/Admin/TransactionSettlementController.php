<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\_ControllerAdmin;
use App\Model\Admin;
use App\Model\Store;
use App\Model\Transaction as Model;

class TransactionSettlementController extends _ControllerAdmin
{

    public function listing()
    {
        $v = &$this->vueData;
        $this->vueOption['store'] = Store::select(['id', 'name'])->get();
        $this->vueOption['admin'] = Admin::select(['id', 'name'])->get();

        return $this->view();
    }

    public function getListing()
    {
        $listData = getListCondition();
        $result = Model::orderby($listData['orderField'], $listData['orderType']);

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['adminID'];
        $conditions[] = ['storeID'];
        $conditions[] = ['isSuccess'];
        $conditions[] = ['name', 'like'];

        $where = getWhereCondition($conditions);
        $result = $result->where($where);
        $result = $result->where('typeID', '=', 10);

        $data = getListData($result, $listData);
        $this->setData($data);
        return $this->returnJson();
    }

    public function updateDo()
    {
        $this->setStatusID(1);

        $request = request('item');
        $id = $request['id'];

        $item = Model::find($id);
        if (!$item) {
            $item = new Model;
        }

        setModelData($item, $request);

        $isSuccess = $item->save();
        if ($isSuccess) {
            $this->setStatusID(0);
        }

        $data = [];
        $data['item'] = $item;
        $this->setData($data);
        return $this->returnJson();
    }

    public function deleteDo()
    {
        $this->setStatusID(1);
        $id = request('id');
        $item = Model::find($id);
        if ($item) {
            $result = $item->delete();
            if ($result) {
                $this->setStatusID(0);
            }
        }
        return $this->returnJson();
    }

    public function item()
    {
        $v = &$this->vueData;

        $id = request('id');
        $item = Model::find($id);
        if ($item) {
        } else {
            $item = new Model;
        }

        $v['item'] = $item;
        $this->vueOption['store'] = Store::select(['id', 'name'])->get();
        $this->vueOption['user'] = User::select(['id', 'name', 'lineUserID'])->get();

        $v['fieldRequired'] = Model::$fieldRequired;

        return $this->view();
    }

}
