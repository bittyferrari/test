<?php

namespace App\Http\Controllers;

use App\Model\Community;
use App\Model\Course;
use App\Model\CourseLike;
use App\Model\CourseUser;
use App\Model\Teacher;
use App\Model\Transaction;

class UserSchoolController extends _Controller
{

    public function updateApplicationDo()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;

        if (!$self) {
            die();
        }

        $request = request();
        $statusID = 0;

        if (isDev()) {
            // $verifyCode = '12345';
        }

        $courseID = $request['courseID'];

        $course = Course::find($courseID);

        $isAlternate = false;

        if ($course && $course['isActive']) {

            $isProcess = true;
            // check price
            if ($course['price'] > 0 && $self['price'] < $course['price']) {
                $isProcess = false;
                $statusID = 2;
            }

            if ($isProcess) {

                $isCreate = false;

                $item = CourseUser::where('userID', '=', $self['id'])->where('courseID', '=', $courseID)->first();
                if (!$item) {
                    $isCreate = true;
                    $item = new CourseUser;
                } else {
                    $statusID = 4;
                }

                $count = CourseUser::where('courseID', '=', $courseID)->count();
                $sequence = $count + 1;

                $isAlternate = $count >= $course['countAvailable'];

                if ($isAlternate == false || ($isAlternate && $course['isAlternatable'] == 1)) {

                    $item['userID'] = $self['id'];
                    $item['courseID'] = $course['id'];

                    $item['communityID'] = $request['communityID'];

                    $item['timeFrom'] = $course['timeFrom'];
                    $item['timeTo'] = $course['timeTo'];
                    $item['timestampFrom'] = $course['timestampFrom'];
                    $item['timestampTo'] = $course['timestampTo'];
                    $item['courseName'] = $course['name'];
                    $item['courseContent'] = $course['content'];
                    $item['coursePhoto'] = $course['photo'];
                    $item['isAlternate'] = $isAlternate;
                    $item['name'] = $request['name'];
                    $item['phone'] = $request['phone'];
                    $item['price'] = $course['price'];
                    $item['photo'] = null;
                    $item['sequence'] = $sequence;

                    $item->save();

                    if ($isCreate && $isAlternate == false) {
                        // create Transaction
                        $transaction = new Transaction;
                        $transaction['userID'] = $self['id'];
                        $transaction['price'] = $course['price'] * -1;
                        $transaction['typeID'] = 12;
                        $transaction['isEnd'] = 1;
                        $transaction['isSuccess'] = 1;
                        $transaction['isActive'] = 1;
                        $transaction['isUserConfirm'] = 1;
                        $transaction['courseID'] = $course['id'];
                        $transaction['courseName'] = $course['name'];
                        $transaction->save();

                        $self['price'] -= $course['price'];
                        $self->save();
                    }
                } else {
                    $statusID = 3;
                }

            }

        } else {
            $statusID = 1;
        }

        $data = [];
        $data['isAlternate'] = $isAlternate;

        $this->setData($data);

        $this->setStatusID($statusID);
        return $this->returnJson();
    }

    public function main()
    {
        $v = &$this->vueData;

        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function my()
    {
        $v = &$this->vueData;

        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function teacherItem()
    {

        $now = time() * 1000;

        $v = &$this->vueData;

        $self = getUser();
        $v['self'] = $self;

        $id = request('id');

        $item = Teacher::find($id);

        $v['item'] = $item;

        // $courses = Course::where('isActive', '=', 1)->where('teacherID', '=', $item['id'])->where('timestampFrom', '<=', $now * 1000)->get();
        $items = Course::whereNotNull('signupTimestampFrom')->whereNotNull('signupTimestampTo')
            ->where('signupTimestampFrom', '<=', $now)->where('signupTimestampTo', '>=', $now)
            ->where('isvisible', '=', 1)->where('teacherID', '=', $item['id'])->get();

        // $items->map(function ($z) use ($self) {

        //     $teacher = Teacher::find($z['teacherID']);
        //     $community = Community::find($z['communityID']);

        //     if (!$teacher) {
        //         $teacher = new Teacher;
        //         $teacher['name'] = '--';
        //     }
        //     if (!$community) {
        //         $community = new Community;
        //         $community['name'] = '--';
        //     }

        //     $z['teacher'] = $teacher;
        //     $z['community'] = $community;

        //     $z['isLike'] = CourseLike::where('userID', '=', $self['id'])->where('courseID', '=', $z['id'])->count() > 0;

        //     return $z;
        // });
        $items->map(function ($z) use ($self, $now) {

            $teacher = Teacher::select(['id', 'name', 'photo', 'title'])->find($z['teacherID']);
            $community = Community::find($z['communityID']);

            if (!$teacher) {
                $teacher = new Teacher;
                $teacher['name'] = '--';
            }
            if (!$community) {
                $community = new Community;
                $community['name'] = '--';
            }

            $z['teacher'] = $teacher;
            $z['community'] = $community;

            $z['isLike'] = CourseLike::where('userID', '=', $self['id'])->where('courseID', '=', $z['id'])->count() > 0;

            // 詳細資訊
            $statusID = getCourseStatus($z);

            // $statusID = 1;
            // if ($z['isActive'] != 1) {
            //     $statusID = 3;
            // } else {

            //     // 詳細資訊
            //     if ($z['timestampTo'] > $now) {
            //         $statusID = 1;
            //     }

            //     // 報名已截止
            //     if ($z['signupTimestampTo'] < $now) {
            //         $statusID = 2;
            //     }

            // }

            $z['statusID'] = $statusID;

            $countSignup = CourseUser::where('courseID', '=', $z['id'])->count();
            $countRemain = $z['countAvailable'] - $countSignup;
            if ($countRemain <= 0) {
                $countRemain = 0;
            }

            $z['countRemain'] = $countRemain;

            $isSignup = CourseUser::where('courseID', '=', $z['id'])->where('userID', '=', $self['id'])->count() > 0;
            $z['isSignup'] = $isSignup;

            return $z;
        });

        $v['items'] = $items;

        $this->addOption('community');

        return $this->view();
    }

    public function getListingCourse()
    {
        $listData = getListCondition();
        $result = Course::orderby($listData['orderField'], $listData['orderType']);

        $request = request();

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['name', 'like'];
        $conditions[] = ['communityID'];

        $where = getWhereCondition($conditions);
        // $result = $result->where($where)->where('isVisible', '=', 1);
        $result = $result->where($where)->where('isVisible', '=', 1);

        $typeID = $request['typeID'];
        $typeID2 = $request['typeID2'];

        $self = getUser();

        $now = time() * 1000;
        switch ($typeID) {
            // all
            case 1:
                break;
            // 尚未開課
            case 1:
                $result = $result->where('signupTimestampFrom', '>', $now)->where('timestampFrom', '>', $now);
                break;
            // 開課中
            case 2:
                $result = $result->where('signupTimestampFrom', '<=', $now)->where('signupTimestampTo', '>=', $now);
                break;
            // 已結束課程
            case 3:
                $result = $result->where('timestampTo', '<=', $now);
                break;
        }

        //////////////////////////
        switch ($typeID2) {
            //    已收藏
            case 1:
                $courseIDs = CourseLike::select(['courseID'])->where('userID', '=', $self['id'])->get()->pluck('courseID');
                $result = $result->whereIn('id', $courseIDs);
                break;
            case 2:
                // 已預約
                $courseIDs = CourseUser::select(['courseID'])->where('userID', '=', $self['id'])->get()->pluck('courseID');
                $result = $result->whereIn('id', $courseIDs);
                // $result = $result->where('signupTimestampFrom', '<=', $now)->where('signupTimestampTo', '>=', $now);
                break;
        }

        $data = getListData($result, $listData);

        $self = getUser();

        $data['items']->map(function ($z) use ($self, $now) {

            $teacher = Teacher::select(['id', 'name', 'photo', 'title'])->find($z['teacherID']);
            $community = Community::find($z['communityID']);

            if (!$teacher) {
                $teacher = new Teacher;
                $teacher['name'] = '--';
            }
            if (!$community) {
                $community = new Community;
                $community['name'] = '--';
            }

            $z['teacher'] = $teacher;
            $z['community'] = $community;

            $z['isLike'] = CourseLike::where('userID', '=', $self['id'])->where('courseID', '=', $z['id'])->count() > 0;

            // 詳細資訊
            $statusID = 1;

            if ($z['isActive'] != 1) {
                $statusID = 3;
            } else {

                // 詳細資訊
                if ($z['timestampTo'] > $now) {
                    $statusID = 1;
                }

                // 報名已截止
                if ($z['signupTimestampTo'] < $now) {
                    $statusID = 2;
                }

            }

            $z['statusID'] = $statusID;

            $countSignup = CourseUser::where('courseID', '=', $z['id'])->count();
            $countRemain = $z['countAvailable'] - $countSignup;
            if ($countRemain <= 0) {
                $countRemain = 0;
            }

            $z['countRemain'] = $countRemain;

            $isSignup = CourseUser::where('courseID', '=', $z['id'])->where('userID', '=', $self['id'])->count() > 0;
            $z['isSignup'] = $isSignup;

            return $z;
        });

        $this->setData($data);
        return $this->returnJson();
    }

    public function getListingTeacher()
    {
        $listData = getListCondition();
        $result = Teacher::orderby($listData['orderField'], $listData['orderType']);

        $conditions = [];
        // $conditions[] = ['id'];
        $conditions[] = ['name', 'like'];
        // $conditions[] = ['communityID'];

        $where = getWhereCondition($conditions);
        $result = $result->where($where)->where('isActive', '=', 1);

        $data = getListData($result, $listData);
        $this->setData($data);
        return $this->returnJson();
    }

    public function community()
    {
        $v = &$this->vueData;

        $self = getUser();
        $v['self'] = $self;

        $v['items'] = Community::get();
        return $this->view();
    }

    public function teacher()
    {
        $v = &$this->vueData;
        // $v['items'] = Teacher::where('isActive', '=', 1)->get();
        return $this->view();
    }

    public function course()
    {
        $v = &$this->vueData;
        $this->addOption('community');

        return $this->view();
    }

    public function courseItem()
    {
        $v = &$this->vueData;

        $id = request('id');

        $item = Course::find($id);

        if ($item['isVisible']) {

            $self = getUser();

            $v['item'] = $item;

            $teacher = Teacher::find($item['teacherID']);

            $v['teacher'] = $teacher;

            $this->vueOption['community'] = Community::select(['id', 'name'])->get();
            $this->vueOption['teacher'] = Teacher::select(['id', 'name'])->get();

            // $isLike = false;
            $isLike = CourseLike::where('userID', '=', $self['id'])->where('courseID', '=', $item['id'])->count() > 0;
            $v['isLike'] = $isLike;

            $statusID = getCourseStatus($item);
            $v['statusID'] = $statusID;

            $countSignup = CourseUser::where('courseID', '=', $item['id'])->count();
            $countRemain = $item['countAvailable'] - $countSignup;
            if ($countRemain <= 0) {
                $countRemain = 0;
            }
            $v['countRemain'] = $countRemain;

            $v['teachers'] = TEacher::whereIn('id', $item['teacherIDs'])->get();

            $v['teacher'] = Teacher::find($item['teacherID']);

            $this->vueOption['teacher'] = Teacher::select(['id', 'name'])->get();

            return $this->view();

        } else {
            return redirect('/userSchool/main');
        }
    }

    public function application()
    {
        $v = &$this->vueData;

        $request = request();

        $courseID = $request['courseID'];
        $course = Course::find($courseID);

        if (!$course) {
            return redirect('/userSchool/main');
        }

        if (!$course['isVisible']) {
            return redirect('/userSchool/main');
        }

        $count = CourseUser::where('courseID', '=', $courseID)->count();
        $isAlternate = $count >= $course['countAvailable'];

        $v['course'] = $course;
        $v['isAlternate'] = $isAlternate;
        $v['self'] = getUser();

        $this->vueOption['community'] = Community::select(['id', 'name'])->get();
        return $this->view();
    }

    public function applicationAlternate()
    {
        $v = &$this->vueData;
        return $this->view();
    }

}
