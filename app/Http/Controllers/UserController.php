<?php

namespace App\Http\Controllers;

use App\Helper\SmsHelper;
use App\Model\Community;
use App\Model\Coupon;
use App\Model\CouponLog;
use App\Model\CouponUser;
use App\Model\Prize;
use App\Model\Store;
use App\Model\Transaction;
use App\Model\User;
use App\Model\UserCoupon;
use App\Model\Word;

class UserController extends _Controller
{

    public function updateProfileDo()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;

        if (!$self) {
            die();
        }

        $request = request();
        $statusID = 0;

        // $verifyCode = randomNumber(5);
        if (isDev()) {
            // $verifyCode = '12345';
        }

        // $phone = $request['phone'];

        // $subject = '驗證碼';
        // $smsBody = '您的驗證碼:' . $verifyCode;
        // SmsHelper::sendDo($phone, $subject, $smsBody);

        $self['name'] = $request['name'];
        $self['phone'] = $request['phone'];
        $self['memo'] = $request['memo'];
        $self['household'] = $request['household'];
        $self['nationID'] = $request['nationID'];
        $self['communityID'] = $request['communityID'];
        // $self['recommendCode'] = $request['recommendCode'];
        $self['addressText'] = $request['addressText'];
        $self['birthday'] = $request['birthday'];

        // $recommendCode = $request['recommendCode'];

        // $self['verifyCode'] = $verifyCode;
        $self->save();

        $data = [];

        $this->setData($data);

        $this->setStatusID($statusID);
        return $this->returnJson();
    }

    public function test()
    {
        $v = &$this->vueData;
        return $this->view();
    }

    public function index()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function information()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function main()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function news()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function school()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function shop()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function schedule()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function recommend()
    {
        $v = &$this->vueData;
        $self = getUser();

        $v['liffID'] = env('LINE_LIFF_ID');

        if ($self['isCompleteRegister'] != 1) {
            die();
        }

        // $v['url'] = env('LINE_LIFF_URL') . '?typeID=register&recommendCode=999';
        $v['url'] = env('APP_URL') . '/recommend/' . $self['recommendCode'];

        $v['self'] = $self;
        return $this->view();
    }

    public function match()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function registerDo()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;

        if (!$self) {
            die();
        }

        $request = request();
        $statusID = 0;

        $verifyCode = randomNumber(5);
        if (isDev()) {
            // $verifyCode = '12345';
        }

        $phone = $request['phone'];

        $subject = '驗證碼';
        $smsBody = '您的驗證碼:' . $verifyCode;
        SmsHelper::sendDo($phone, $subject, $smsBody);

        $self['name'] = $request['name'];
        $self['phone'] = $request['phone'];
        // $self['nationID'] = $request['nationID'];
        // $self['email'] = $request['email'];
        $self['memo'] = $request['memo'];
        $self['household'] = $request['household'];
        $self['nationID'] = $request['nationID'];

        $self['communityID'] = $request['communityID'];
        // $self['recommendCode'] = $request['recommendCode'];
        $self['addressText'] = $request['addressText'];
        $self['birthday'] = $request['birthday'];

        $registerRecommendCode = $request['registerRecommendCode'];

        // find user by recommendCode
        if (!empty($registerRecommendCode)) {
            // if ($self['isCompleteRegister'] == 0) {
            if (empty($self['registerRecommendUserID'])) {
                $self['registerRecommendCode'] = $registerRecommendCode;
                $recommendUser = User::where('recommendCode', '=', $registerRecommendCode)->first();
                if ($recommendUser) {
                    $self['registerRecommendUserID'] = $recommendUser['id'];

                    // find parent recommend
                    $parentRecommendUser = User::find($recommendUser['registerRecommendUserID']);
                    if ($parentRecommendUser) {

                        $self['registerParentRecommendUserID'] = $parentRecommendUser['id'];

                    }
                    // $self['registerRecommendCode'] = $registerRecommendCode;
                }

            }
            // }
        }

        $self['verifyCode'] = $verifyCode;

        $self->save();

        $data = [];

        $this->setData($data);

        $this->setStatusID($statusID);
        return $this->returnJson();

    }

    public function registerVerifyDo()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;

        if (!$self) {
            die();
        }
        $statusID = 1;

        $request = request();

        $verifyCode = $request['verifyCode'];

        if ($self['verifyCode'] == $verifyCode) {
            $statusID = 0;

            $self['verifyCode'] = randomMd5();
            $self['isCompleteRegister'] = 1;
            $self['timeCompleteRegister'] = date('Y-m-d H:i:s');
            $self->save();
        }

        $data = [];

        $this->setData($data);

        $this->setStatusID($statusID);
        return $this->returnJson();
    }

    public function meal()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function ticket()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function coupon()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function couponDo()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;

        if (!$self) {
            die();
        }

        $statusID = 1;
        $request = request();

        $rules = ['captcha' => 'required|captcha'];
        $validator = validator()->make(request()->all(), $rules);

        $coupon = null;

        // if (!$validator->fails()) {
        $code = $request['code'];

        $statusID = 0;

        $dateNow = date('Y-m-d');
        $now = time();
        $coupon = Coupon::where('isActive', '=', 1)->where('code', '=', $code)->first();
        // $coupon = Coupon::where('isActive', '=', 1)->where('code', '=', $code)->where('timeFrom', '<=', $dateNow)->where('timeTo', '>=', $dateNow)->first();

        if ($coupon) {

            $tiemFrom = strtotime($coupon['timeFrom'] . ' 00:00:00');
            $timeTo = strtotime($coupon['timeTo'] . ' 23:59:59');

            if ($tiemFrom <= $now && $timeTo >= $now) {

                // check is limit
                if ($coupon['countLimit'] == 0 || ($coupon['countLimit'] != 0 && $coupon['countUsed'] < $coupon['countLimit'])) {

                    // check is used
                    // $couponLog = CouponLog::where('couponID', '=', $coupon['id'])->where('userID', '=', $self['id'])->first();
                    // $couponLog = CouponLog::where('couponID', '=', $coupon['id'])->where('userID', '=', $self['id'])->where('isSuccess', '=', 1)->first();
                    $couponLog = CouponLog::where(function ($q) use ($coupon) {
                        if ($coupon['eventID'] != null) {
                            $q->where('couponID', '=', $coupon['id'])->orWhere('eventID', '=', $coupon['eventID']);
                        } else {
                            $q->where('couponID', '=', $coupon['id']);
                        }
                    })
                        ->where('userID', '=', $self['id'])->where('isSuccess', '=', 1)->first();

                    if ($couponLog) {
                        $statusID = 3;
                    } else {

                        $couponLog = new CouponLog;
                        $couponLog['couponID'] = $coupon['id'];
                        $couponLog['userID'] = $self['id'];
                        $couponLog['price'] = $coupon['price'];
                        $couponLog['pricePoint'] = $coupon['pricePoint'];
                        $couponLog['code'] = $coupon['code'];
                        $couponLog['phone'] = $self['phone'];
                        $couponLog['nationID'] = $self['nationID'];
                        $couponLog['eventID'] = $coupon['eventID'];
                        $couponLog->save();

                        // check coupon user

                        // is use listing
                        $couponUser = true;
                        if ($coupon['isUseListing'] == 1) {
                            $couponUser = CouponUser::where('couponID', '=', $coupon['id'])->where(function ($query) use ($self) {
                                $query->where('phone', '=', $self['phone'])->orWhere('nationID', '=', $self['nationID']);
                            })->first();
                        }

                        if ($couponUser) {
                            $coupon['countUsed'] += 1;
                            $coupon->save();

                            $couponLog['isSuccess'] = 1;
                            $couponLog->save();

                            $statusID = 0;

                            $price = 0;
                            $pricePoint = 0;
                            // add price to user
                            switch ($coupon['priceTypeID']) {
                                case 1:
                                    $self['price'] += $coupon['price'];
                                    $self->save();
                                    $price = $coupon['price'];
                                    break;
                                case 2:
                                    $self['pricePoint'] += $coupon['pricePoint'];
                                    $self->save();
                                    $pricePoint = $coupon['pricePoint'];
                                    break;
                            }

                            // add transaction
                            $item = new Transaction;
                            $item['userID'] = $self['id'];
                            $item['price'] = $coupon['price'];
                            $item['pricePoint'] = $coupon['pricePoint'];
                            $item['typeID'] = 9;
                            $item['expiredAt'] = date('Y-m-d H:i:s');
                            $item['isSuccess'] = 1;
                            $item['isEnd'] = 1;
                            $item['couponTypeID'] = $coupon['typeID'];
                            $item['priceTypeID'] = $coupon['priceTypeID'];
                            $item['couponTimeFrom'] = $coupon['timeFrom'];
                            $item['couponTimeTo'] = $coupon['timeTo'];
                            $item->save();

                            // add user coupon
                            $userCoupon = new UserCoupon;
                            $userCoupon['couponID'] = $coupon['id'];
                            $userCoupon['userID'] = $self['id'];
                            $userCoupon['timeFrom'] = $coupon['timeFrom'];
                            $userCoupon['timeTo'] = $coupon['timeTo'];
                            $userCoupon['price'] = $price;
                            $userCoupon['pricePoint'] = $pricePoint;
                            $userCoupon['priceTypeID'] = $coupon['priceTypeID'];
                            $userCoupon->save();

                            // update user dateCouponNearest
                            $userCoupon = UserCoupon::orderby('timeTo', 'asc')->where('userID', '=', $self['id'])->first();
                            if ($userCoupon) {
                                $self['dateCouponNearest'] = $userCoupon['timeTo'];
                                $self->save();
                            }

                            // refresh user price pricePointCoupon
                            recalculateUserCoupon($self['id'], true);

                        } else {
                            $statusID = 4;
                        }
                    }

                } else {
                    $statusID = 5;
                }
            } else {
                $statusID = 6;
            }

        } else {
            $statusID = 2;
        }

        $data = [];
        $data['item'] = $coupon;

        $this->setData($data);
        $this->setStatusID($statusID);
        return $this->returnJson();
    }

    public function deal()
    {

        // $qq = recalculateUserCoupon(2);
        // print_r($qq);
        // die();

        $v = &$this->vueData;
        $self = getUser();

        $self = recalculateUserCoupon($self['id']);

        $v['self'] = $self;

        // nearest coupno
        $couponNearest = UserCoupon::where('userID', '=', $self['id'])->orderby('timeTo', 'asc')->first();
        $v['couponNearest'] = $couponNearest;

        return $this->view();
    }

    public function manual()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function transfer()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        return $this->view();
    }

    public function register()
    {
        $v = &$this->vueData;
        $self = getUser();

        $isRecommendCodeReadonly = false;
        if ($self['isCompleteRegister'] == 0) {
            $self['registerRecommendCode'] = getSession('registerRecommendCode');
            if (!empty($self['registerRecommendCode'])) {
                $isRecommendCodeReadonly = true;
                // die('asd');
            }
        }

        $v['self'] = $self;
        $v['item'] = $self;

        $v['isLockRecommendCode'] = $self['registerRecommendUserID'] != null;
        $v['isRecommendCodeReadonly'] = $isRecommendCodeReadonly;
        $this->vueOption['community'] = Community::select(['id', 'name'])->get();

        return $this->view();
    }

    public function profile()
    {
        $v = &$this->vueData;
        $self = getUser();
        $v['self'] = $self;
        $v['item'] = $self;

        $this->vueOption['community'] = Community::select(['id', 'name'])->get();

        $recommendUser = User::find($self['registerRecommendUserID']);
        $v['recommendUser'] = $recommendUser;

        return $this->view();
    }

    public function transferDo()
    {
        $self = getUser();

        if (!$self) {
            die();
        }

        $statusID = 1;

        $request = request();

        $price = $request['price'];
        $phone = $request['phone'];
        $verifyCode = $request['verifyCode'];

        $price = intval($price);
        // $xxxx = '12345';

        $rules = ['captcha' => 'required|captcha'];
        $validator = validator()->make(request()->all(), $rules);

        if (!$validator->fails()) {

            // if ($verifyCode == $xxxx) {

            // find phone
            $user = User::where('phone', '=', $phone)->first();
            if ($user) {

                if ($self['phone'] != $user['phone']) {

                    if ($self['price'] >= $price) {

                        $statusID = 0;

                        $user['price'] += $price;
                        $user->save();

                        $self['price'] -= $price;
                        $self->save();

                        // create transcation
                        $item = new Transaction;
                        $item['typeID'] = 8;
                        $item['price'] = $price;
                        $item['userID'] = $user['id'];
                        $item['phoneTransferFrom'] = $self['phone'];
                        $item['phoneTransferTo'] = $user['phone'];
                        $item['isSuccess'] = 1;
                        $item['isEnd'] = 1;
                        $item['transferFromUserID'] = $self['id'];
                        $item['transferToUserID'] = $user['id'];
                        $item->save();

                        $item = new Transaction;
                        $item['typeID'] = 8;
                        $item['price'] = $price * -1;
                        $item['userID'] = $self['id'];
                        $item['phoneTransferFrom'] = $self['phone'];
                        $item['phoneTransferTo'] = $user['phone'];

                        $item['transferFromUserID'] = $self['id'];
                        $item['transferToUserID'] = $user['id'];

                        $item['isSuccess'] = 1;
                        $item['isEnd'] = 1;
                        $item->save();

                    } else {
                        $statusID = 4;

                    }

                } else {
                    $statusID = 3;

                }

            } else {
                $statusID = 2;

            }

        } else {
            $statusID = 1;

        }

        $data = [];

        $this->setData($data);

        $this->setStatusID($statusID);
        return $this->returnJson();

    }

    public function event()
    {
        $v = &$this->vueData;

        $self = getUser();
        $v['self'] = $self;

        $countAvailable = Prize::where('isTaken', '=', 0)->count();
        $v['hasPrize'] = $countAvailable > 0;

        return $this->view();
    }

    public function map()
    {
        $v = &$this->vueData;
        $self = getUser();

        $v['self'] = $self;

        return $this->view();
    }

    public function scanDo()
    {

        $self = getUser();
        if (!$self) {
            die();
        }

        $isWord = false;

        $v = &$this->vueData;

        $md5 = request('md5');

        $wordID = null;

        $word = Word::where('md5', '=', $md5)->where('isActive', '=', 1)->first();

        $isWord = $word != null;

        $statusID = 0;

        if ($word['typeID'] !== null) {
            $self['isWord' . $word['typeID']] = 1;
            $self->save();
        }

        // check is get prize
        $data = [];
        $data['isWord'] = $isWord;
        $data['item'] = $word;

        $this->setData($data);

        $this->setStatusID($statusID);
        return $this->returnJson();
    }

    public function getPrizeDo()
    {
        $self = getUser();
        if (!$self) {
            die();
        }

        $statusID = 1;

        $isGetPrize = false;

        if ($self['isWord1'] == 1 && $self['isWord2'] == 1 && $self['isWord3'] == 1 && $self['isWord4'] == 1 && $self['isWord5'] == 1) {

            // $prize = Prize::where('isTaken', '=', 0)->where('userID', '!=', $self['id'])->first();

            // check user has taken already
            $prize = Prize::where('userID', '=', $self['id'])->first();

            if (!$prize) {

                $prize = Prize::where('isTaken', '=', 0)->first();
                if ($prize) {

                    // add point
                    $prize['timeTaken'] = date('Y-m-d H:i:s');
                    $prize['isTaken'] = 1;
                    // $prize['userID'] = 10;
                    $prize['userID'] = $self['id'];
                    $prize->save();

                    $self['price'] += 120;
                    $self['isGetPrize'] = 1;
                    $self->save();

                    $isGetPrize = true;
                    $statusID = 0;

                    // add transaction log
                    $item = new Transaction;
                    $item['userID'] = $self['id'];
                    $item['expiredAt'] = date('Y-m-d H:i:s');
                    $item['typeID'] = 4;
                    $item['isActive'] = 1;
                    $item['storeID'] = null;
                    $item['price'] = 120;
                    $item['priceOrigin'] = 0;
                    $item['priceCash'] = 0;
                    $item['isSuccess'] = 1;
                    $item['ratio'] = 1;
                    $item['isEnd'] = 1;
                    $item->save();

                } else {
                    $statusID = 3;

                }

            } else {
                $statusID = 2;
            }

        }

        $data = [];
        $data['isGetPrize'] = $isGetPrize;

        $this->setData($data);

        $this->setStatusID($statusID);
        return $this->returnJson();

    }

    public function scan()
    {
        $v = &$this->vueData;
        $self = getUser();

        $v['self'] = $self;

        $v['testingMd5'] = '44af8ea1732fd2e0f8799ab6262cbbb5';

        $v['liffID'] = env('LINE_LIFF_ID');

        return $this->view();
    }

    public function treasure()
    {
        $v = &$this->vueData;
        $self = getUser();

        $v['self'] = $self;

        $countAvailable = Prize::where('isTaken', '=', 0)->count();

        $v['hasPrize'] = $countAvailable > 0;

        return $this->view();
    }

    public function eventExplanation()
    {
        $v = &$this->vueData;
        $self = getUser();

        $v['self'] = $self;

        return $this->view();
    }

    public function store()
    {
        $v = &$this->vueData;

        $items = Store::select(['id', 'photo', 'name', 'phone', 'addressText', 'url', 'nameUrl'])->where('isActive', '=', 1)->orderby('sequence', 'asc')->get();

        $v['items'] = $items;

        return $this->view();
    }

    public function setUserID()
    {
        setSession('userID', 2);
        die();
    }
    public function setUserID2()
    {
        setSession('userID', 3);
        die();
    }


    public function exchange()
    {
        return redirect('/user/transaction');
    }

    public function log()
    {
        $v = &$this->vueData;

        $typeID = request('typeID');
        $year = request('year');
        $month = request('month');

        if (empty($year) || empty($month)) {
            $year = date('Y');
            $month = date('m');
        }

        if (empty($typeID)) {
            $typeID = 'exchange';
        }
        $year = intval($year);
        $month = intval($month);

        $self = getUser();

        $v['self'] = $self;

        recalculateUserCoupon($self['id']);

        // $items = Transaction::where('userID', '=', $self['id'])->where('isSuccess', '=', 1)->get();
        // $items = Transaction::where('userID', '=', $self['id'])->where('isStoreScan', '=', 1)->get();
        // $items = Transaction::where('userID', '=', $self['id'])->get();
        $items = [];

        // ['id' => 1, 'name' => '儲值'],
        // ['id' => 2, 'name' => '消費'],
        // ['id' => 3, 'name' => '退點'],
        // ['id' => 4, 'name' => '獎品'],
        // ['id' => 5, 'name' => '餐點'],
        // ['id' => 6, 'name' => '湯品'],
        // ['id' => 7, 'name' => '門票'],
        // ['id' => 8, 'name' => '轉移'],
        // ['id' => 9, 'name' => '序號']

        if ($typeID == 'log') {
            // [消費 退點 餐點 湯品 門票]
            // $items = Transaction::where('userID', '=', $self['id'])->whereIn('typeID', [2, 3, 5, 6, 7])
            $items = Transaction::where('userID', '=', $self['id'])->whereIn('typeID', [2, 3, 5, 6, 7, 8, 12])
                ->whereYear('createdAt', $year)->whereMonth('createdAt', $month)->where('isEnd', '=', 1)->orderby('id', 'desc')->get();
        } else {
            // exchange
            // [獎品 轉移 序號 儲值]
            // $items = Transaction::where('userID', '=', $self['id'])->whereIn('typeID', [4, 8, 9, 1])
            $items = Transaction::where('userID', '=', $self['id'])->whereIn('typeID', [4, 9, 1])
                ->whereYear('createdAt', $year)->whereMonth('createdAt', $month)->where('isEnd', '=', 1)->orderby('id', 'desc')->get();
        }

        // $items = Transaction::where('userID', '=', $self['id'])->whereYear('createdAt', '=', $year)->whereMonth('createdAt', '=' , $month)->get();

        $v['items'] = $items;

        $stores = Store::select(['id', 'name'])->whereIn('id', $items->pluck('storeID'))->get();
        $this->vueOption['store'] = $stores;

        // $v['credit'] = Transaction::where('userID', '=', $self['id'])->where('isSuccess', '=', 1)->sum('price');

        $v['typeID'] = $typeID;
        $v['year'] = $year;
        $v['month'] = $month;

        // nearest coupno
        $couponNearest = UserCoupon::where('userID', '=', $self['id'])->orderby('timeTo', 'asc')->first();
        $v['couponNearest'] = $couponNearest;

        return $this->view();
    }

    public function transaction()
    {
        $v = &$this->vueData;

        // $userID = request('userID');
        $userID = getUserID();
        $user = User::find($userID);

        if (!$user) {
            die();
        }

        if(!$user['isCompleteRegister']){
            die();
        }

        $item = new Transaction;

        if ($user) {

            $item['userID'] = $user['id'];
            // $item['md5'] = randomMd5($user['id'] . $user['lineUserID']);
            // $item['md5'] = 'ea044688370eeac2bd6faf2930b62212';
            $item['expiredAt'] = date('Y-m-d H:i:s', time() + 60);
            $item->save();
        }
        // $item = Transaction::find(167);

        $v['item'] = $item;

        $v['liffID'] = env('LINE_LIFF_ID');

        return $this->view();
    }

    public function error()
    {
        $v = &$this->vueData;
        return $this->view();
    }

}
