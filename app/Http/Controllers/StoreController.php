<?php

namespace App\Http\Controllers;

use App\Model\Store;
use App\Model\Transaction;
use App\Model\User;
use Hash;

class StoreController extends _Controller
{

    public function getListing()
    {
        $listData = getListCondition();
        $result = Store::orderby('sequence', 'asc');

        $conditions = [];
        $conditions[] = ['id'];
        $conditions[] = ['typeID'];
        $conditions[] = ['name', 'like'];

        $where = getWhereCondition($conditions);
        $result = $result->where($where)->where('isActive', '=', 1);

        $listData['pageSize'] = 1000;

        $data = getListData($result, $listData);
        $this->setData($data);
        return $this->returnJson();
    }

    public function getListingTransaction()
    {
        $store = getStore();
        if (!$store) {
            die();
        }

        // ['id' => 1, 'name' => 'save'],
        // ['id' => 2, 'name' => 'exchange'],
        // ['id' => 3, 'name' => 'refund'],
        // ['id' => 4, 'name' => 'prize'],
        // ['id' => 5, 'name' => 'meal'],
        // ['id' => 6, 'name' => 'soup'],
        // ['id' => 7, 'name' => 'ticket'],
        // ['id' => 8, 'name' => 'transfer'],
        // ['id' => 9, 'name' => 'coupon'],
        // ['id' => 10, 'name' => 'storeSettlement'],

        $items = [];
        $users = [];

        $md5 = request('md5');

        $item = Transaction::where('md5', '=', $md5)->first();

        if ($item) {

            $items = Transaction::orderby('id', 'desc')->where('storeID', '=', $store['id'])->where('userID', '=', $item['userID'])
                ->where('isSuccess', '=', 1)->where('isRefunded', '!=', 1)->whereIn('typeID', [2, 7])->get();

            $users = User::select(['id', 'name'])->whereIn('id', $items->pluck('userID'))->get();
        }

        $data['items'] = $items;
        $data['users'] = $users;

        $this->setData($data);
        return $this->returnJson();
    }

    public function login()
    {
        $v = &$this->vueData;
        return $this->view();
    }

    public function loginDo()
    {
        $this->setStatusID(1);

        $v = &$this->vueData;

        $username = request('username');
        $password = request('password');

        $store = Store::where('username', '=', $username)->first();

        if ($store) {
            if (Hash::check($password, $store['password'])) {
                setSession('storeID', $store['id']);
                $this->setStatusID(0);
            }
        }

        return $this->returnJson();
    }

    public function index()
    {
        $store = getStore();
        if (!$store) {
            redirect('/store/login');
        }

        $v = &$this->vueData;
        return $this->view();
    }

    public function profile()
    {
        $store = getStore();
        if (!$store) {
            return redirect('/store/login');
        }
        $v = &$this->vueData;

        $storeTotal = 0;

        $storeTotal = Transaction::where('storeID', '=', $store['id'])->where('isSuccess', '=', 1)->where('typeID', '!=', 1)->sum('price');
        $storeTotal *= -1;

        $priceStoreSettlement = Transaction::where('storeID', '=', $store['id'])->where('isSuccess', '=', 1)->where('priceStoreSettlement', '>', 0)->sum('priceStoreSettlement');

        $storeTotal -= $priceStoreSettlement;

        $v['storeTotal'] = $storeTotal;
        $v['store'] = $store;

        return $this->view();
    }

    public function settlement()
    {
        $v = &$this->vueData;

        $store = getStore();
        if ($store == null) {
            return redirect('/store/login');
        }

        $v['self'] = $store;

        $storeTotal = 0;
        $storeTotal = Transaction::where('storeID', '=', $store['id'])->where('isSuccess', '=', 1)->where('typeID', '!=', 1)->sum('price');
        $storeTotal *= -1;

        $priceStoreSettlement = Transaction::where('storeID', '=', $store['id'])->where('isSuccess', '=', 1)->where('priceStoreSettlement', '>', 0)->sum('priceStoreSettlement');

        $storeTotal -= $priceStoreSettlement;

        $v['storeTotal'] = $storeTotal;

        return $this->view();
    }

    public function scan()
    {
        $v = &$this->vueData;

        $store = getStore();

        if ($store == null) {
            return redirect('/store/login');
        }

        $priceSaveRemain = $store['priceStoreMax'] - $store['priceStore'];

        if ($priceSaveRemain < 0) {
            $priceSaveRemain = 0;
        }
        $v['priceSaveRemain'] = $priceSaveRemain;

        $v['ratio'] = getRatio();
        $v['self'] = $store;

        $v['testingMd5'] = '';
        // if (isDev()) {
        $v['testingMd5'] = Transaction::orderby('id', 'desc')->first()['md5'];
        // }

        return $this->view();
    }

    public function setting()
    {
        $v = &$this->vueData;

        $store = getStore();
        if ($store == null) {
            return redirect('/store/login');
        }

        $v['item'] = $store;

        return $this->view();
    }

    public function password()
    {
        $v = &$this->vueData;

        $store = getStore();
        if ($store == null) {
            return redirect('/store/login');
        }

        $v['item'] = $store;

        return $this->view();
    }

    public function reset()
    {
        $v = &$this->vueData;

        $store = getStore();
        if ($store == null) {
            return redirect('/store/login');
        }

        // $item = new TransactionReset;
        $item = new Transaction;

        if ($store) {
            $item['isAdminConfirm'] = 0;
            $item['typeID'] = 11;
            $item['storeID'] = $store['id'];
            $item['expiredAt'] = date('Y-m-d H:i:s', time() + 60);
            $item->save();
        }

        $v['item'] = $item;
        $v['md5'] = $item['md5'];
        $v['store'] = $store;

        return $this->view();
    }

    public function updatePasswordDo()
    {
        $v = &$this->vueData;

        $store = getStore();
        if ($store == null) {
            return redirect('/store/login');
        }

        $password = request('password');

        $store['password'] = bcrypt($password);
        $store->save();

        return $this->returnJson();
    }

    public function transaction()
    {
        $v = &$this->vueData;

        $store = getStore();
        if ($store == null) {
            return redirect('/store/login');
        }

        $typeID = request('typeID');
        $year = request('year');
        $month = request('month');

        if (empty($year) || empty($month)) {
            $year = date('Y');
            $month = date('m');
        }

        $year = intval($year);
        $month = intval($month);

        if (empty($typeID)) {
            $typeID = 'exchange';
        }

        $items = [];
        if ($typeID == 'exchange') {
            $items = Transaction::where('storeID', '=', $store['id'])->where('typeID', '!=', 1)
                ->whereYear('createdAt', $year)->whereMonth('createdAt', $month)->where('isEnd', '=', 1)->orderby('id', 'desc')->get();
        } else {
            $items = Transaction::where('storeID', '=', $store['id'])->where('typeID', '=', 1)
                ->whereYear('createdAt', $year)->whereMonth('createdAt', $month)->where('isEnd', '=', 1)->orderby('id', 'desc')->get();
        }

        $users = User::select(['id', 'name'])->whereIn('id', $items->pluck('userID'))->get();

        $this->vueOption['user'] = $users;
        $v['items'] = $items;
        $v['typeID'] = $typeID;
        $v['year'] = $year;
        $v['month'] = $month;

        return $this->view();
    }

    public function transaction2()
    {
        $v = &$this->vueData;

        $store = getStore();
        if ($store == null) {
            return redirect('/store/login');
        }

        $items = Transaction::get();

        $v['items'] = $items;
        return $this->view();
    }

    public function updateDo()
    {
        $store = getStore();
        if ($store == null) {
            return redirect('/store/login');
        }

        $v = &$this->vueData;
        return $this->view();
    }

    public function logoutDo()
    {

        forgetSession('storeID');
        forgetSession('store');

        return redirect('/store/login');
    }

}
