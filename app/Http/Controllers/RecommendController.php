<?php

namespace App\Http\Controllers;

class RecommendController extends _Controller
{

    public function item()
    {

        $recommendCode = request('recommendCode');

        $url = env('LINE_LIFF_URL') . '?typeID=register&recommendCode=' . $recommendCode;


        setSession('registerRecommendCode', $recommendCode);


        // die($recommendCode);
        return redirect($url);

        // $v = &$this->vueData;

        // $recommendCode = request('recommendCode');

        // $item = User::where('recommendCode', '=', $recommendCode)->first();

        // if ($item) {
        //     setSession('recommendCode', $recommendCode);
        // }

        // $v['recommendCode'] = $recommendCode;

        // $v['liffID'] = env('LINE_LIFF_ID');

        // return $this->view();

    }

}
