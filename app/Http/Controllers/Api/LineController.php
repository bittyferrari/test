<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\_Controller;
use App\Model\Advertisement;
use App\Model\MessageLog;
use App\Model\Store;
use App\Model\User;
use App\Model\Word;

class LineController extends _Controller
{

    public function login()
    {

        $LINE_MESSAGE_ACCESS_TOKEN = env('LINE_MESSAGE_ACCESS_TOKEN');
        $LINE_MESSAGE_ID = env('LINE_MESSAGE_ID');
        $LINE_MESSAGE_SECRET = env('LINE_MESSAGE_SECRET');

        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($LINE_MESSAGE_ACCESS_TOKEN);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $LINE_MESSAGE_SECRET]);

        $body = file_get_contents('php://input');

        $messageLog = new MessageLog;
        $messageLog['body'] = $body;

        $messageLog->save();

    }

    public function setUserRichmenu()
    {
        /*
    curl -v -X POST https://api.line.me/v2/bot/user/{userId}/richmenu/{richMenuId} \
    -H "Authorization: Bearer {channel access token}"
     */
    }

    public function webhook()
    {
        // die('ok');

        $LINE_MESSAGE_ACCESS_TOKEN = env('LINE_MESSAGE_ACCESS_TOKEN');
        $LINE_MESSAGE_ID = env('LINE_MESSAGE_ID');
        $LINE_MESSAGE_SECRET = env('LINE_MESSAGE_SECRET');

        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($LINE_MESSAGE_ACCESS_TOKEN);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $LINE_MESSAGE_SECRET]);

        $body = file_get_contents('php://input');

        $messageLog = new MessageLog;
        $messageLog['body'] = $body;

        $messageLog->save();

        $json = jsonDecode($body);

        $events = $json['events'];

        $messageLog['replyJson'] = 'aaaaaaa';
        $messageLog->save();

        foreach ($events as $x) {

            $replyToken = $x['replyToken'];

            // check user
            $lineUserID = $x['source']['userId'];

            $messageLog['replyJson'] = '22222' . $lineUserID . jsonEncode($x);
            $messageLog->save();

            $user = User::where('lineUserID', '=', $lineUserID)->first();
            if (!$user) {
                $user = new User;
                $user['lineUserID'] = $lineUserID;
                $messageLog['replyJson'] = '777777' . $lineUserID . jsonEncode($x);
                $messageLog->save();

                $user->save();
                $messageLog['replyJson'] = '8888' . $lineUserID . jsonEncode($x);
                $messageLog->save();

            }

            $message = $x['message'];
            $userText = $message['text'];

            $messageLog['userID'] = $user['id'];
            $messageLog['lineUserID'] = $lineUserID;
            $messageLog['userMessage'] = $userText;

            switch ($userText) {

                default:

                    $isMatchCase = false;
                    $isReplyText = true;

                    $replyMessage = '歡迎光臨';

                    if (mb_substr($userText, 0, 8, 'utf-8') == '#我集到字了: ') {

                        $isMatchCase = true;
                        $temp = explode(': ', $userText);

                        if (isset($temp[1])) {

                            $text = $temp[1];

                            $wordTypeID = null;

                            $wordType = config('_constant')['wordType'];
                            foreach ($wordType as $x) {
                                if ($x['name'] == $text) {
                                    $wordTypeID = $x['id'];
                                }
                            }

                            // $bot->replyText($replyToken, '1232131-'.$temp[1].'asdasd'.$wordTypeID);

                            $advertisement = null;
                            if ($wordTypeID != null) {
                                $word = Word::where('typeID', '=', $wordTypeID)->first();
                                if ($word) {
                                    $advertisement = Advertisement::find($word['advertisementID']);
                                }

                            } else {
                                $word = Word::where('name', '=', $text)->first();
                                if ($word) {
                                    $advertisement = Advertisement::find($word['advertisementID']);
                                }
                            }

                            // $bot->replyText($replyToken, '1232131');

                            // if ($advertisement && $advertisement['isActive'] == 1) {
                            if ($advertisement) {

                                // $bot->replyText($replyToken, '12321zzzzz31'.$advertisement['name']);

                                $isMatchCase = true;
                                $isReplyText = false;

                                /*
                                .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
                                ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
                                ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
                                ....###.......###.......###.......###.......###.......###.......###.......###.......###...
                                ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
                                ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
                                .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
                                 */

                                $items = Advertisement::whereIn('id', [$advertisement['id']])->get();

                                $replyJson = [
                                    'type' => 'template',
                                    'altText' => '我是推薦',
                                    'template' => [
                                        'type' => 'carousel',
                                        'actions' => [],
                                        'columns' => [],
                                    ],
                                ];

                                $messageTemplateColumns = [];

                                $storeIDs = [];
                                $advertisementIDs = [];
                                $now = time();

                                foreach ($items as $x) {

                                    $timestampFrom = 976623132;
                                    $timestampTo = 253400616732;

                                    if ($x['timeFrom'] != null) {
                                        $timestampFrom = strtotime($x['timeFrom']);
                                    }
                                    if ($x['timeTo'] != null) {
                                        $timestampTo = strtotime($x['timeTo']);
                                    }

                                    if (true || $timestampFrom <= $now && $timestampTo >= $now) {

                                        if ($x['countSend'] < $x['countSendLimit']) {

                                            $store = Store::select(['id', 'name', 'phone', 'photo', 'md5'])->find($x['storeID']);
                                            if ($store) {

                                                $storeIDs[] = $store['id'];
                                                $advertisementIDs[] = $x['id'];

                                                $x['countSend'] += 1;
                                                $x->save();

                                                $actions = [];

                                                if (!empty($x['url'])) {
                                                    // $actions[] = [
                                                    //     'type' => 'message',
                                                    //     'label' => '連結',
                                                    //     'text' => '#連結: ' . $x['md5'],
                                                    // ];
                                                    $actions[] = [
                                                        'type' => 'uri',
                                                        'label' => '連結',
                                                        'uri' => $x['url'],
                                                    ];
                                                }
                                                if (!empty($store['addressText'])) {
                                                    $actions[] = [
                                                        'type' => 'message',
                                                        'label' => '地圖',
                                                        'text' => '#地圖: ' . $store['md5'],
                                                    ];
                                                }

                                                $messageTemplateColumns[] = [
                                                    'thumbnailImageUrl' => env('APP_URL') . '/storage/photo/' . $x['photo'],
                                                    'title' => $store['name'],
                                                    'text' => $x['name'],
                                                    'actions' => $actions,
                                                ];

                                            }
                                        }

                                    }

                                }

                                // $bot->replyText($replyToken, '12321zzzzz31'.$advertisement['name'].jsonEncode($messageTemplateColumns));

                                $replyJson['template']['columns'] = $messageTemplateColumns;

                                $replyMessage = new \LINE\LINEBot\MessageBuilder\RawMessageBuilder($replyJson);
                                $bot->replyMessage($replyToken, $replyMessage);
                                $messageLog['replyJson'] = jsonEncode($replyJson);
                                $messageLog['storeIDs'] = imimplode($storeIDs);
                                $messageLog['advertisementIDs'] = imimplode($advertisementIDs);

                                /*
                            .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
                            ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
                            ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
                            ....###.......###.......###.......###.......###.......###.......###.......###.......###...
                            ...##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##..
                            ..##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##.
                            .##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##.##.....##
                             */

                            }

                        }
                    }

                    if ($isReplyText) {
                        $bot->replyText($replyToken, $replyMessage);
                    } else {

                        if (!$isMatchCase) {
                            $replyMessage = '歡迎光臨';
                        }

                    }

                    break;

                case '我完成兌換了':
                case '我完成儲值了':
                    // default:

                    $items = Advertisement::where('isActive', '=', 1)->orderby('sequence', 'asc')->get();

                    $replyJson = [
                        'type' => 'template',
                        'altText' => '我是推薦',
                        'template' => [
                            'type' => 'carousel',
                            'actions' => [],
                            'columns' => [],
                        ],
                    ];

                    $messageTemplateColumns = [];

                    $storeIDs = [];
                    $advertisementIDs = [];
                    $now = time();
                    foreach ($items as $x) {

                        $timestampFrom = 976623132;
                        $timestampTo = 253400616732;

                        if ($x['timeFrom'] != null) {
                            $timestampFrom = strtotime($x['timeFrom']);
                        }
                        if ($x['timeTo'] != null) {
                            $timestampTo = strtotime($x['timeTo']);
                        }

                        if ($timestampFrom <= $now && $timestampTo >= $now) {

                            if ($x['countSend'] < $x['countSendLimit']) {

                                $store = Store::select(['id', 'name', 'phone', 'photo', 'md5'])->find($x['storeID']);
                                if ($store) {

                                    $storeIDs[] = $store['id'];
                                    $advertisementIDs[] = $x['id'];

                                    $x['countSend'] += 1;
                                    $x->save();

                                    $actions = [];

                                    if (!empty($x['url'])) {
                                        // $actions[] = [
                                        //     'type' => 'message',
                                        //     'label' => '連結',
                                        //     'text' => '#連結: ' . $x['md5'],
                                        // ];
                                        $actions[] = [
                                            'type' => 'uri',
                                            'label' => '連結',
                                            'uri' => $x['url'],
                                        ];
                                    }
                                    if (!empty($store['addressText'])) {
                                        $actions[] = [
                                            'type' => 'message',
                                            'label' => '地圖',
                                            'text' => '#地圖: ' . $store['md5'],
                                        ];
                                    }

                                    $messageTemplateColumns[] = [
                                        'thumbnailImageUrl' => env('APP_URL') . '/storage/photo/' . $x['photo'],
                                        'title' => $store['name'],
                                        'text' => $x['name'],
                                        'actions' => $actions,
                                    ];

                                }
                            }

                        }

                    }

                    $replyJson['template']['columns'] = $messageTemplateColumns;

                    $replyMessage = new \LINE\LINEBot\MessageBuilder\RawMessageBuilder($replyJson);
                    $bot->replyMessage($replyToken, $replyMessage);
                    $messageLog['replyJson'] = jsonEncode($replyJson);
                    $messageLog['storeIDs'] = imimplode($storeIDs);
                    $messageLog['advertisementIDs'] = imimplode($advertisementIDs);

                    break;

            }

        }

        $messageLog->save();

    }

}
