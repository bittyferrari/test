<?php

namespace App\Http\Controllers;

class _ControllerAdmin extends _Controller
{

    public $title = 'Admin';
    public $vueOption = [];

    public $viewPrefix = '/_admin/';

}
