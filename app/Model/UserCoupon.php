<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCoupon extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'user_coupon';
    public static $snakeAttributes = false;

    public static function boot()
    {
        parent::boot();
    }

    protected $attributes = [
        'id' => null,
        'couponID' => null,
        'userID' => null,
        // 'phone' => null,
        // 'name' => null,
        // 'photo' => '_default.jpg',
        // 'photoMap' => null,
    ];

}
