<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class _Log extends Model
{

    protected $connection = 'mysqlLog';

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    public static $snakeAttributes = false;

    protected $table = '_log';

    protected $attributes = array(
        'id' => null,
        // 'typeID' => 1,
        'tableName' => null,
        'ip' => null,
        'jsonData' => null,
        'userID' => null,
        // 'createTime' => null,
        'tableName' => null,
        'dataID' => null,
        'controllerName' => null,
        'actionName' => null,
    );

}
