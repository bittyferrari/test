<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'admin';

    public static $fieldRequired = [
        [
            'field' => 'roleID',
            'message' => '',
        ],
        [
            'field' => 'name',
            'message' => '',
        ],
        [
            'field' => 'isBackend',
            'message' => '',
        ],

    ];

    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {

            $a = [];
            switch ($model['roleID']) {
            }

        });
    }

    protected $hidden = [
        'password',
    ];

    protected $attributes = [
        'id' => null,
        'name' => null,
        'email' => null,
        // 'username' => null,
        'roleID' => 2,
        'photo' => '_default.jpg',
        'photoJson' => '[]',
        'permissions' => '[]',
        'isBackend' => 0,
    ];

    protected $casts = [
        'photoJson' => 'array',
        'permissions' => 'array',
    ];

}
