<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'teacher';
    public static $snakeAttributes = false;

    public static function boot()
    {
        parent::boot();
    }

    protected $attributes = [
        'id' => null,
        // 'phone' => null,
        // 'name' => null,
        // 'photo' => '_default.jpg',
        'photo' => '_default.jpg',
        'photos' => '[]',
        'isInCourse' => 1,
    ];

    protected $casts = [
        'photos' => 'array',
    ];

}
