<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseUser extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'course_user';
    public static $snakeAttributes = false;

    public static function boot()
    {
        parent::boot();
    }

    protected $attributes = [
        'id' => null,
        'courseID' => null,
        'userID' => null,
        'phone' => null,
        'photo' => '_default.jpg',
        'photos' => '[]',
        'isAlternate' => false,
        // 'tel' => null,
        'phone' => null,
        'price' => 0,
        'name' => null,
        'sequence' => 0,

        // 'name' => null,
        // 'photoMap' => null,
    ];

    protected $casts = [
        'photos' => 'array',
    ];

}
