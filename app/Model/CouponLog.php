<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponLog extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'coupon_log';
    public static $snakeAttributes = false;

    public static function boot()
    {
        parent::boot();
    }

    protected $attributes = [
        'id' => null,
        'isSuccess' => 0,
        // 'name' => null,
        // 'photo' => '_default.jpg',
        // 'photoMap' => null,
    ];

}
