<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class MailQueue extends _BaseModel
{
    use SoftDeletes;


    protected $table = 'mail_queue';

    protected $attributes = [
        'id' => null,
    ];

}
