<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'transaction';

    public static $fieldRequired = [
        [
            'field' => 'name',
            'message' => '',
        ],
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model['md5'] = date('Y-m-d-H-i-s') . '-' . randomMd5();
        });

    }

    protected $attributes = [
        'id' => null,
        'usedCouponPrice' => 0,
        'usedCouponPricePoint' => 0,
        'usedPrice' => 0,
        'courseID' => null,


        // 'name' => null,
        // 'photo' => '_default.jpg',
    ];

}
