<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'log';
    public static $snakeAttributes = false;

    public static function boot()
    {
        parent::boot();
    }

    protected $attributes = [
        'id' => null,
        'name' => null,
        // 'photo' => '_default.jpg',
        // 'photoMap' => null,
    ];


}
