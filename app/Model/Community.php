<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Community extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'community';
    public static $snakeAttributes = false;

    public static function boot()
    {
        parent::boot();
    }

    protected $attributes = [
        'id' => null,
        // 'couponID' => null,
        // 'nationID' => null,
        'name' => null,
        // 'name' => null,
        // 'photo' => '_default.jpg',
        // 'photoMap' => null,
    ];

}
