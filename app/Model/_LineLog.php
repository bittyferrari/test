<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class _LineLog extends Model
{

    protected $connection = 'mysqlLog';

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    public static $snakeAttributes = false;

    protected $table = '_line_log';

    protected $attributes = array(
        'id' => null,
        'requestJson' => null,
        'response' => null,
    );

}
