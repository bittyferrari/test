<?php
namespace App\Model;

use App\Model\_Log;
use Illuminate\Database\Eloquent\Model;

class _BaseModel extends Model
{

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT = 'deletedAt';

    // public static $isSaveLog = false;
    public static $isSaveLog = true;

    public static $snakeAttributes = false;

    public static $fieldRequired = [
        // [
        //     'field' => 'name',
        //     'message' => '',
        // ],
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        // self::$isSaveLog = !isDev();
        // self::$isSaveLog = true;
        // self::$isSaveLog = false;
    }

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    public static function saveLog($model)
    {
        if (self::$isSaveLog) {
            $log = new _Log;
            // $log['typeID'] = 1;
            $log['tableName'] = $model->getTable();
            $log['dataID'] = $model['id'];
            $log['userID'] = getUserID();
            // $log['adminID'] = getAdminID();
            $log['ip'] = getIP();
            $jsonData = $model->toJson(JSON_UNESCAPED_UNICODE);
            $log['jsonData'] = $jsonData;

            if (isset($GLOBALS['controller'])) {
                $log['controllerName'] = $GLOBALS['controller'];
            }
            if (isset($GLOBALS['action'])) {
                $log['actionName'] = $GLOBALS['action'];
            }
            $log->save();
        }
    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function ($model) {
            $log = new _Log;
            // $log['typeID'] = 1;
            $log['tableName'] = $model->getTable();
            $log['dataID'] = $model['id'];
            $log['userID'] = getUserID();
            // $log['adminID'] = getAdminID();
            $log['ip'] = getIP();
            // $jsonData = $model->toJson(JSON_UNESCAPED_UNICODE);
            // $log['jsonData'] = $jsonData;
            $log['controllerName'] = $GLOBALS['controller'];
            $log['actionName'] = $GLOBALS['action'];
            $log['isDelete'] = true;
            // $log->save();
        });

        static::saved(function ($model) {
            self::saveLog($model);
        });

    }

}
