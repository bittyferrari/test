<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advertisement extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'advertisement';

    public static $fieldRequired = [
        [
            'field' => 'isStoreValue',
            'message' => '',
        ],
    ];

    protected $hidden = [
        // 'password',
    ];

    protected $casts = [
        'photos' => 'array',
    ];

    protected $attributes = [
        'id' => null,
        // 'name' => null,
        'photo' => '_default.jpg',

        'storeID' => null,
        'isActive' => 0,

        'timeFrom' => null,
        'timeTo' => null,
        'photos' => '[]',

        'url' => '',
        'countSend' => 0,
        'countSendLimit' => 0,
        'messageTypeID' => 1,
        'messageContent' => '',
        'sequence' => 9999,

        // 'mapWidth' => 1000,
        // 'mapHeight' => 1000,
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model['md5'])) {
                $model['md5'] = randomMd5();
            }
        });
    }

}
