<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'coupon';
    public static $snakeAttributes = false;

    public static function boot()
    {
        parent::boot();
    }

    protected $attributes = [
        'id' => null,
        'name' => '我是序號名稱',
        'code' => '',
        'price' => 0,
        'pricePoint' => 0,
        'isActive' => 1,
        'timeFrom' => '2000-01-01',
        'timeTo' => '9999-09-09',
        'countLimit' => 0,
        'isUseListing' => 0,
        'createdTypeID' => 1,
        'typeID' => 1,
        'priceTypeID' => 1,
        'eventID' => null,

        // 'photo' => '_default.jpg',
        // 'photoMap' => null,
    ];

}
