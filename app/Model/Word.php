<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Word extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'word';

    public static $fieldRequired = [
        [
            'field' => 'isStoreValue',
            'message' => '',
        ],
    ];

    protected $casts = [
        // 'eventIDs' => 'array',
    ];

    protected $attributes = [
        'id' => null,
        'name' => '',
        'photo' => '_default.jpg',
        'isActive' => 1,
        'content' => '',

        'md5' => null,

        // 'eventIDs' => '[]',
        'sequence' => 9999,

        'typeID' => null,

        // 'mapWidth' => 1000,
        // 'mapHeight' => 1000,
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model['md5'])) {
                $model['md5'] = randomMd5();
            }
        });
    }

}
