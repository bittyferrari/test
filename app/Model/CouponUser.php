<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponUser extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'coupon_user';
    public static $snakeAttributes = false;

    public static function boot()
    {
        parent::boot();
    }

    protected $attributes = [
        'id' => null,
        'couponID' => null,
        'nationID' => null,
        'phone' => null,
        // 'name' => null,
        // 'photo' => '_default.jpg',
        // 'photoMap' => null,
    ];

}
