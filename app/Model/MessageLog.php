<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageLog extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'message_log';

    protected $attributes = [
        'id' => null,
    ];

    // protected $casts = [
    //     'photos' => 'array',
    // ];

    public static function boot()
    {
        parent::boot();
        // static::saving(function ($model) {
        //     if (empty($model['md5'])) {
        //         $model['md5'] = randomMd5();
        //     }
        // });

    }

}
