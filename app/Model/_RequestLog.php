<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class _RequestLog extends Model
{
    protected $connection = 'mysqlLog';

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = '_request_log';
    public static $snakeAttributes = false;

    protected $attributes = [
        'id' => null,
        'requestJson' => null,
        'ip' => null,
        'controllerName' => null,
        'actionName' => null,
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model['ip'] = getIP();
            $model['requestJson'] = jsonEncode(request()->all());
            if (isset($GLOBALS['controller'])) {
                $model['controllerName'] = $GLOBALS['controller'];
            }
            if (isset($GLOBALS['action'])) {
                $model['actionName'] = $GLOBALS['action'];
            }
        });

    }

}
