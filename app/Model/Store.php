<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'store';

    public static $fieldRequired = [
        [
            'field' => 'isStoreValue',
            'message' => '',
        ],
    ];

    protected $hidden = [
        'password',
    ];

    protected $casts = [
        'eventIDs' => 'array',
    ];

    protected $attributes = [
        'id' => null,
        'name' => null,
        'photo' => '_default.jpg',
        'isActive' => 0,
        'isStoreValue' => 0,
        'isRefundable' => 0,
        'isExchangable' => 0,
        'username'=>'',
        'eventIDs' => '[]',
        'sequence' => 9999,

        // 'mapWidth' => 1000,
        // 'mapHeight' => 1000,
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model['md5'])) {
                $model['md5'] = randomMd5();
            }
        });
    }

}
