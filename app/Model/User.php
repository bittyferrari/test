<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'user';

    public static $fieldRequired = [
        [
            'field' => 'roleID',
            'message' => '',
        ],
        [
            'field' => 'name',
            'message' => '',
        ],
        [
            'field' => 'isBackend',
            'message' => '',
        ],

    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {

            $recommendCode = '';
            $isFound = true;
            $i = 0;
            while ($isFound) {
                $i++;
                if ($i > 10) {
                    break;
                }

                // generate recommend code
                $recommendCode = randomString(6, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
                $count = User::withTrashed()->where('recommendCode', '=', $recommendCode)->count();
                if ($count <= 0) {
                    $isFound = false;
                }
            }
            $model['recommendCode'] = $recommendCode;

        });
    }

    protected $hidden = [
        'password',
    ];

    protected $attributes = [
        'id' => null,
        'name' => null,
        'email' => null,
        'roleID' => 1,
        'photo' => '_default.jpg',
        'photoJson' => '[]',
        'permissions' => '[]',
        'price' => 0,
        'communityID' => null,
        'addressText' => null,
        'household' => null,
        'recommendCode' => null,
        'nationID' => null,
    ];

    protected $casts = [
        'photoJson' => 'array',
        'permissions' => 'array',
    ];

}
