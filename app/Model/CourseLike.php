<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseLike extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'course_like';
    public static $snakeAttributes = false;

    public static function boot()
    {
        parent::boot();
    }

    protected $attributes = [
        'id' => null,
        'courseID' => null,
        'userID' => null,
    ];

    protected $casts = [
        // 'photos' => 'array',
    ];

}
