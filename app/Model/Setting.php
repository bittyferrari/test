<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Setting extends _BaseModel
{

    protected $table = 'setting';

    public static function boot()
    {
        parent::boot();
    }

    protected $attributes = [
        'id' => null,
    ];

}
