<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'course';
    public static $snakeAttributes = false;

    protected $attributes = [
        'id' => null,
        'teacherID' => null,
        'communityID' => null,
        'name' => null,
        'content' => null,
        'date' => null,
        'isActive' => 0,
        'price' => 0,
        'countAvailable' => 1,
        // 'phone' => null,
        // 'name' => null,
        'photo' => '_default.jpg',
        // 'photoMap' => null,
        // 'photo' => null,
        'photos' => '[]',
        'teacherIDs' => '[]',
        'communityIDs' => '[]',

        'hasOnline' => 0,
        'isRestrictSameCommunity' => 0,

        'isVisible' => 1,
        'isAlternatable' => 0,
        'countAvailable' => 9999,
        'signupTimestampFrom' => null,
        'signupTimestampTo' => null,
    ];

    protected $casts = [
        'photos' => 'array',
        'teacherIDs' => 'array',
        'communityIDs' => 'array',

    ];

    public static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            if (!empty($model['timestampFrom'])) {
                $model['timeFrom'] = date('Y-m-d H:i:s', $model['timestampFrom'] / 1000);
            }
            if (!empty($model['timestampTo'])) {
                $model['timeTo'] = date('Y-m-d H:i:s', $model['timestampTo'] / 1000);
                // $model['timestampTo'] = strtotime($model['timeTo']);
                // $model['timeTo'] = date('Y-m-d H:i:s', $model['timestampTo']);
            }

            if (!empty($model['signupTimestampFrom'])) {
                $model['signupFrom'] = date('Y-m-d H:i:s', $model['signupTimestampFrom'] / 1000);
            }
            if (!empty($model['signupTimestampTo'])) {
                $model['signupTo'] = date('Y-m-d H:i:s', $model['signupTimestampTo'] / 1000);
            }

        });

    }

}
