<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCouponLog extends _BaseModel
{
    use SoftDeletes;

    protected $table = 'user_coupon_log';
    public static $snakeAttributes = false;

    public static function boot()
    {
        parent::boot();
    }

    protected $attributes = [
        'id' => null,
        'userCouponID' => null,
        'couponID' => null,
        'userID' => null,
        'pricetUsed' => 0,
        'pricePointUsed' => 0,
    ];

}
