const mix = require('laravel-mix');
let tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');

mix.version();

// custom styles
mix.sass('resources/sass/admin.scss', 'public/app');
// mix.sass('resources/sass/app.scss', 'public/app');
mix.sass('resources/sass/app.scss', 'public/app').options({
	processCssUrls: false,
	// postCss: [tailwindcss('./path/to/your/tailwind.config.js')],
	postCss: [tailwindcss('tailwind.config.js')],
});

// custom js
mix.js('resources/js/app.js', 'public/app');

mix.browserSync({
	ui: false,
	ghostMode: false,
	notify: false,
	port: '3333',
	proxy: 'localhost:8048',
	files: ['./app/**/*.php', './resources/views/**/*.php', './public/scripts/**/*.js', './public/styles/**/*.css'],
});
