<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'HomeController@index');

/*
.########.########...#######..##....##.########.########.##....##.########.
.##.......##.....##.##.....##.###...##....##....##.......###...##.##.....##
.##.......##.....##.##.....##.####..##....##....##.......####..##.##.....##
.######...########..##.....##.##.##.##....##....######...##.##.##.##.....##
.##.......##...##...##.....##.##..####....##....##.......##..####.##.....##
.##.......##....##..##.....##.##...###....##....##.......##...###.##.....##
.##.......##.....##..#######..##....##....##....########.##....##.########.
 */

Route::get('/liff/index', 'LiffController@index');
Route::post('/liff/setUserDo', 'LiffController@setUserDo');

Route::prefix('/_helper')->group(function () {

    Route::post('/sendSmsDo', '_HelperController@sendSmsDo');
    Route::post('/refereshCapchaDo', '_HelperController@refereshCapchaDo');
    Route::post('/courseLikeDo', '_HelperController@courseLikeDo');

});

Route::get('/test/test', 'TestController@test');

// Route::get('/recommend/item', 'RecommendController@item');
Route::get('/recommend/{recommendCode}', 'RecommendController@item');

Route::prefix('/transaction')->group(function () {

    Route::post('/createDo', 'TransactionController@createDo');
    Route::post('/getItem', 'TransactionController@getItem');
    Route::post('/userCancelDo', 'TransactionController@userCancelDo');
    Route::post('/userConfirmDo', 'TransactionController@userConfirmDo');
    Route::post('/storeUpdateDo', 'TransactionController@storeUpdateDo');
    Route::post('/storeScanDo', 'TransactionController@storeScanDo');
    Route::post('/managerScanDo', 'TransactionController@managerScanDo');
    Route::post('/storeCancelDo', 'TransactionController@storeCancelDo');
    Route::post('/storeCreateDo', 'TransactionController@storeCreateDo');
    Route::post('/managerConfirmDo', 'TransactionController@managerConfirmDo');
    Route::post('/managerCancelDo', 'TransactionController@managerCancelDo');

});

Route::prefix('/user')->group(function () {
    // Route::get('/event', 'UserController@event');
    Route::get('/map', 'UserController@map');
    Route::get('/scan', 'UserController@scan');
    // Route::get('/treasure', 'UserController@treasure');
    Route::get('/index', 'UserController@index');
    Route::get('/coupon', 'UserController@coupon');
    Route::get('/profile', 'UserController@profile');
    Route::get('/qrcode', 'UserController@qrcode');
    Route::get('/exchange', 'UserController@exchange');
    Route::get('/transaction', 'UserController@transaction');
    Route::get('/error', 'UserController@error');
    Route::get('/store', 'UserController@store');
    Route::get('/log', 'UserController@log');
    // Route::get('/eventExplanation', 'UserController@eventExplanation');
    Route::get('/information', 'UserController@information');
    Route::get('/schedule', 'UserController@schedule');
    Route::get('/match', 'UserController@match');
    Route::get('/manual', 'UserController@manual');
    Route::get('/register', 'UserController@register');
    Route::get('/transfer', 'UserController@transfer');
    Route::get('/main', 'UserController@main');
    Route::get('/deal', 'UserController@deal');
    Route::get('/meal', 'UserController@meal');
    Route::get('/ticket', 'UserController@ticket');
    Route::get('/recommend', 'UserController@recommend');
    Route::get('/shop', 'UserController@shop');
    Route::get('/school', 'UserController@school');
    Route::get('/news', 'UserController@news');

    // @TODO remove
    Route::get('/setUserID', 'UserController@setUserID');
    Route::get('/setUserID2', 'UserController@setUserID2');
    Route::get('/test', 'UserController@test');

    Route::post('/updateDo', 'UserController@updateDo');
    Route::post('/checkTransaction', 'UserController@checkTransaction');
    Route::post('/checkTransactionDo', 'UserController@checkTransactionDo');
    Route::post('/confirmTransactionDo', 'UserController@confirmTransactionDo');
    Route::post('/getPrizeDo', 'UserController@getPrizeDo');
    Route::post('/scanDo', 'UserController@scanDo');
    Route::post('/registerDo', 'UserController@registerDo');
    Route::post('/registerVerifyDo', 'UserController@registerVerifyDo');
    Route::post('/transferDo', 'UserController@transferDo');
    Route::post('/couponDo', 'UserController@couponDo');
    Route::post('/updateProfileDo', 'UserController@updateProfileDo');

});

// user school
Route::prefix('/userSchool')->group(function () {
    // Route::get('/event', 'UserController@event');
    Route::get('/teacher', 'UserSchoolController@teacher');
    Route::get('/course', 'UserSchoolController@course');
    Route::get('/courseItem', 'UserSchoolController@courseItem');
    Route::get('/application', 'UserSchoolController@application');
    Route::get('/applicationAlternate', 'UserSchoolController@applicationAlternate');
    Route::get('/my', 'UserSchoolController@my');
    Route::get('/community', 'UserSchoolController@community');
    Route::get('/main', 'UserSchoolController@main');
    Route::get('/teacherItem', 'UserSchoolController@teacherItem');
    Route::post('/getListingCourse', 'UserSchoolController@getListingCourse');
    Route::post('/getListingTeacher', 'UserSchoolController@getListingTeacher');

    Route::post('/updateApplicationDo', 'UserSchoolController@updateApplicationDo');

});

Route::prefix('/store')->group(function () {
    Route::get('/index', 'StoreController@index');
    Route::get('/coupon', 'StoreController@coupon');
    Route::get('/scan', 'StoreController@scan');
    Route::get('/profile', 'StoreController@profile');
    Route::get('/qrcode', 'StoreController@qrcode');
    Route::get('/transaction', 'StoreController@transaction');
    Route::get('/transaction2', 'StoreController@transaction2');
    Route::get('/login', 'StoreController@login');
    Route::get('/reset', 'StoreController@reset');
    Route::get('/password', 'StoreController@password');
    Route::get('/setting', 'StoreController@setting');
    Route::get('/logoutDo', 'StoreController@logoutDo');
    Route::get('/settlement', 'StoreController@settlement');

    Route::post('/getListing', 'StoreController@getListing');
    Route::post('/getListingTransaction', 'StoreController@getListingTransaction');

    Route::post('/loginDo', 'StoreController@loginDo');
    Route::post('/updateDo', 'StoreController@updateDo');
    Route::post('/updatePasswordDo', 'StoreController@updatePasswordDo');
    Route::post('/checkTransactionDo', 'StoreController@checkTransactionDo');
});

Route::prefix('/manager')->group(function () {
    Route::get('/login', 'ManagerController@login');
    Route::get('/index', 'ManagerController@index');
    Route::get('/scan', 'ManagerController@scan');
    Route::get('/scan2', 'ManagerController@scan2');
    Route::get('/transaction', 'ManagerController@transaction');
    Route::get('/logoutDo', 'ManagerController@logoutDo');
    Route::get('/main', 'ManagerController@main');
    Route::get('/profile', 'ManagerController@profile');
    Route::get('/setting', 'ManagerController@setting');
    Route::get('/password', 'ManagerController@password');

    Route::get('/logoutDo', 'ManagerController@logoutDo');

    Route::post('/updatePasswordDo', 'ManagerController@updatePasswordDo');
    Route::post('/loginDo', 'ManagerController@loginDo');
    Route::post('/scanDo', 'ManagerController@scanDo');
    Route::post('/scan2Do', 'ManagerController@scan2Do');
});

/*
.########.....###.....######..##....##.########.##....##.########.
.##.....##...##.##...##....##.##...##..##.......###...##.##.....##
.##.....##..##...##..##.......##..##...##.......####..##.##.....##
.########..##.....##.##.......#####....######...##.##.##.##.....##
.##.....##.#########.##.......##..##...##.......##..####.##.....##
.##.....##.##.....##.##....##.##...##..##.......##...###.##.....##
.########..##.....##..######..##....##.########.##....##.########.
 */

Route::get('/adminLogin', function () {return redirect('adminLogin/index');});
Route::get('/adminLogin/index', 'AdminLoginController@index');
Route::get('/adminLogin/logoutDo', 'AdminLoginController@logoutDo');
Route::post('/adminLogin/loginDo', 'AdminLoginController@loginDo');
Route::post('/adminLogin/resetPasswordDo', 'AdminLoginController@resetPasswordDo');

Route::prefix('/_admin')->namespace('Admin')->middleware(['permission'])->group(function () {

    //helper
    Route::post('/_helper/uploadFile', '_HelperController@uploadFile');
    Route::post('/_helper/uploadFiles', '_HelperController@uploadFiles');
    // Route::post('/_helper/checkBlacklist', '_HelperController@checkBlacklist');

    // Route::crud('userLogin', 'UserLoginController');
    Route::crud('user', 'UserController', [], ['exportDo']);
    Route::crud('transaction', 'TransactionController');
    Route::crud('transactionReset', 'TransactionResetController');
    Route::crud('transactionSettlement', 'TransactionSettlementController');
    Route::crud('store', 'StoreController', ['scan'], ['scanDo', 'uploadExcelDo', 'exportDo']);
    Route::crud('admin', 'AdminController');
    Route::crud('event', 'AdvertisementController');
    Route::crud('word', 'WordController');
    Route::crud('prize', 'PrizeController');
    Route::crud('community', 'CommunityController');
    Route::crud('teacher', 'TeacherController');
    Route::crud('course', 'CourseController');
    Route::crud('courseUser', 'CourseUserController');
    Route::crud('event', 'EventController');
    Route::crud('coupon', 'CouponController', ['batchCreate', 'exportDo'], ['batchCreateDo']);
    Route::crud('couponLog', 'CouponLogController');
    Route::crud('couponUser', 'CouponUserController', [], ['importDo']);

    Route::get('/dashboard/index', 'DashboardController@index');
    Route::get('/dashboard/exportDo', 'DashboardController@exportDo');

    Route::get('/userLogin/listing', 'UserLoginController@listing');
    Route::get('/userLogin/exportDo', 'UserLoginController@exportDo');
    Route::post('/userLogin/getListing', 'UserLoginController@getListing');

    Route::get('/map/item', 'MapController@item');

    Route::get('/setting/index', 'SettingController@index');
    Route::post('/setting/updateDo', 'SettingController@updateDo');

    Route::get('/self/item', 'SelfController@item');
    Route::post('/self/updateDo', 'SelfController@updateDo');

    Route::get('/accounting/index', 'AccountingController@index');
    Route::get('/accounting/exportDo', 'AccountingController@exportDo');
    Route::post('/accounting/getData', 'AccountingController@getData');

    Route::get('/test/test', 'TestController@test');

});
