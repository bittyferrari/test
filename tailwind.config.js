module.exports = {
	important: true,

	theme: {
		fontFamily: {
			display: ['ggggg', 'sans-serif'],
			body: ['Graphik', 'sans-serif'],
		},

		// borderColor: (theme) => ({
		// 	'333': '#333333',
		// }),

		colors: {
			aaa: '#aaa',
			333: '#333333',
			666: '#666666',
			f3f4f6: '#f3f4f6',
			F0F0F0: '#F0F0F0',
			'2F80ED': '#2F80ED',
			FFFDF0: '#FFFDF0',
			F5ECED: '#F5ECED',
			F8F8F8: '#F8F8F8',
			// blue: '#6AA4D9',
			normal: '#232120',
			828282: '#828282',
			'4F4F4F': '#4F4F4F',
			f3f3f3: '#F3F3F3',
			blue: '#152244',
			blue2: '#6D80B0',
			c4c4c4: '#C4C4C4',
			yellow: '#FF8709',
			yellow2: '#FF9900',
			yellow3: '#FFECC7',
			'light-yellow': '#FFF8BF',
			'light-yellow2': '#FEF1DA',
			gray: '#F2F2F2',
			gray2: '#BDBDBD',
			E7E7E7: '#E7E7E7',
            808080:'#808080',

            FBEBEF:'#FBEBEF',
			f7f7f7: '#f7f7f7',

			eee: '#eee',
			ccc: '#ccc',
			green: '#1AB33D',
			'19b23d': '#19b23d',
			purple: '#c280ff',
			red2: '#FB4A59',
			pink: '#FFF0F0',
			white: '#FFF',
			black: '#232120',
			fef9f9: '#fef9f9',
			red: '#D4261F',
			'light-blue': '#D3F5FC',
			C7B489: '#C7B489',
			C2762C: '#C2762C',
			A38442: '#A38442',
			582602: '#582602',

			fffafa: '#fffafa',
			FEECDC: '#FEECDC',

			orange: '#CF330E',
			999: '#999',
			000: '#000',

			AD9152: '#AD9152',

			////////////////////////////////////////////////////////
			'on-red2': '#FB4A59',
			'on-white': '#FFF',
		},

		borderWidth: {
			default: '1px',
			0: '0',
			2: '2px',
			3: '3px',
			4: '4px',
			6: '6px',
			8: '8px',
			99: '99px',
		},

		extend: {
			borderRadius: {
				3: '3px',
				5: '5px',
				6: '6px',
				10: '10px',
				14: '14px',
				20: '20px',
			},

			// borderWidth: {
			// 	'1': '1px',
			// },

			/*

			colors: {
				'333': '#333333',
				FFFDF0: '#FFFDF0',
				normal: '#232120',
				'828282': '#828282',
				f3f3f3: '#F3F3F3',
				blue: '#0974A2',
				c4c4c4: '#C4C4C4',
				'light-blue': '#D3F5FC',
				yellow: '#FF8709',
				'light-yellow': '#FFF8BF',
				'light-yellow2': '#FEF1DA',
				gray: '#F2F2F2',
				gray2: '#BDBDBD',

				eee: '#eee',
				ccc: '#ccc',
				green: '#1AB33D',
				purple: '#c280ff',
				red: '#FC2B3A',
				red2: '#FB4A59',
				pink: '#FD748C',
			},
*/
			opacity: {
				80: '.8',
			},

			fontSize: {
				10: '10px',
				12: '12px',
				14: '14px',
				15: '15px',
				16: '16px',
				17: '17px',
				18: '18px',
				19: '19px',
				20: '20px',
				22: '22px',
				24: '24px',
				26: '26px',
				28: '28px',
				30: '30px',
				32: '32px',
				34: '34px',
				36: '36px',
				38: '38px',
				40: '40px',
				50: '50px',
				60: '60px',
			},

			spacing: {
				30: '7rem',
				// '2': '12px',
				// '3': '16px',
				// '4': '24px',
				// '5': '32px',
				// '6': '48px',
			},
		},
	},
	variants: {
		// borderColor: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
	},
	plugins: [],
};
