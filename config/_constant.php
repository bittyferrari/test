<?php

return [
    'city' => [],

    'gender' => [
        ['id' => 1, 'name' => 'Mr.'],
        ['id' => 2, 'name' => 'Mrs.'],
    ],

    'is' => [
        ['id' => 0, 'name' => 'No'],
        ['id' => 1, 'name' => 'Yes'],
    ],

    // 'community' => [
    //     ['id' => 1, 'name' => '社區1'],
    //     ['id' => 2, 'name' => '社區2'],
    //     ['id' => 3, 'name' => '社區3'],
    //     ['id' => 4, 'name' => '社區4'],
    //     ['id' => 5, 'name' => '社區5'],
    //     ['id' => 6, 'name' => '社區6'],
    //     ['id' => 7, 'name' => '社區7'],
    //     ['id' => 8, 'name' => '社區8'],
    //     ['id' => 9, 'name' => '社區9'],
    //     ['id' => 10, 'name' => '社區10'],
    //     ['id' => 11, 'name' => '社區11'],
    // ],

    'transactionType' => [
        ['id' => 1, 'name' => 'save'],
        ['id' => 2, 'name' => 'exchange'],
        ['id' => 3, 'name' => 'refund'],
        ['id' => 4, 'name' => 'prize'],
        ['id' => 5, 'name' => 'meal'],
        ['id' => 6, 'name' => 'soup'],
        ['id' => 7, 'name' => 'ticket'],
        ['id' => 8, 'name' => 'transfer'],
        ['id' => 9, 'name' => 'coupon'],
        ['id' => 10, 'name' => 'storeSettlement'],
        ['id' => 11, 'name' => 'storeReset'],
        ['id' => 12, 'name' => 'course'],
    ],

    'courseStatus' => [
        ['id' => 1, 'name' => '尚未開課'],
        ['id' => 2, 'name' => '開課中'],
        ['id' => 3, 'name' => '已結束'],
    ],

    'courseStatus2' => [
        ['id' => 1, 'name' => '未額滿可報名'],
        ['id' => 1, 'name' => '已額滿可候補'],
        ['id' => 1, 'name' => '已額滿不可候補'],
        ['id' => 1, 'name' => '報名時間已截止'],
        ['id' => 1, 'name' => '使用者已經完成報名'],
        ['id' => 1, 'name' => '使用者等待候補中'],
        ['id' => 1, 'name' => '課程結束了'],
        ['id' => 1, 'name' => '課程取消了'],
        ['id' => 1, 'name' => '候補成功'],
    ],

    'transactionTypeChinese' => [
        ['id' => 1, 'name' => '儲值'],
        ['id' => 2, 'name' => '消費'],
        ['id' => 3, 'name' => '退點'],
        ['id' => 4, 'name' => '獎品'],
        ['id' => 5, 'name' => '餐點'],
        ['id' => 6, 'name' => '湯品'],
        ['id' => 7, 'name' => '門票'],
        ['id' => 8, 'name' => '點數轉移'],
        ['id' => 9, 'name' => '序號'],
        ['id' => 10, 'name' => '商家結算'],
        ['id' => 11, 'name' => '補充儲值額度'],
        ['id' => 12, 'name' => '課程費用'],
    ],

    'userPermission' => [
        [
            'name' => '商家',
            'module' => 'store',
            'action' => ['read', 'save', 'delete'],
        ],
        [
            'name' => '紀錄',
            'module' => 'transaction',
            'action' => ['read', 'save', 'delete'],
        ],
        [
            'name' => '廣告',
            'module' => 'advertisement',
            'action' => ['read', 'save', 'delete'],
        ],

        [
            'name' => '會員',
            'module' => 'user',
            'action' => ['read', 'save', 'delete'],
        ],
        // [
        //     'name' => 'Product',
        //     'module' => 'product',
        //     'action' => ['read', 'save', 'delete'],
        // ],
        [
            'name' => '管理員',
            'module' => 'admin',
            'action' => ['read', 'save', 'delete'],
        ],

        [
            'name' => '設定',
            'module' => 'setting',
            'action' => ['read', 'save', 'delete'],
        ],
        [
            'name' => '對帳',
            'module' => 'accounting',
            'action' => ['read', 'save', 'delete'],
        ],
        [
            'name' => '補充紀錄',
            'module' => 'transactionReset',
            'action' => ['read', 'save', 'delete'],
        ],

        [
            'name' => '集字',
            'module' => 'word',
            'action' => ['read', 'save', 'delete'],
        ],

        [
            'name' => '獎品',
            'module' => 'prize',
            'action' => ['read', 'save', 'delete'],
        ],

    ],

    'messageType' => [
        ['id' => 1, 'name' => '對話框列表'],
        ['id' => 2, 'name' => '對話框大圖'],
    ],

    'userAdminRole' => [
        ['id' => 1, 'name' => '系統帳號'],
        ['id' => 2, 'name' => '管理主帳號'],
    ],

    'userRole' => [
        ['id' => 1, 'name' => '使用者'],
    ],

    'storeType' => [
        ['id' => 1, 'name' => '美食'],
        ['id' => 2, 'name' => '美妝'],
        ['id' => 3, 'name' => '服飾'],
        ['id' => 4, 'name' => '配件'],
        ['id' => 5, 'name' => '飲料'],
        ['id' => 6, 'name' => '鞋'],
        ['id' => 7, 'name' => '娛樂'],
        ['id' => 8, 'name' => '其他'],
    ],

    'adminRole' => [
        ['id' => 1, 'name' => '系統帳號'],
        ['id' => 2, 'name' => '管理主帳號'],
        // ['id' => 3, 'name' => '使用者'],
    ],

    // 'couponType' => [
    //     ['id' => 1, 'name' => '200', 'price' => 200],
    //     ['id' => 2, 'name' => '500', 'price' => 500],
    // ],

    /*
    用現金儲值 +序號 是總太點數
    活動點數是 像優惠卷的概念
    舊的是總太點數
     */
    'couponType' => [
        ['id' => 1, 'name' => '總太點數'],
        ['id' => 2, 'name' => '活動點數'],
    ],

    'priceType' => [
        ['id' => 1, 'name' => '總太點數'],
        ['id' => 2, 'name' => '活動點數'],
    ],

    // 'event' => [
    //     ['id' => 1, 'name' => '振興卷'],
    //     ['id' => 2, 'name' => '逢甲折扣卷'],
    //     ['id' => 3, 'name' => '逢甲年貨大街卷'],
    // ],

    'wordType' => [
        ['id' => 1, 'name' => '全'],
        ['id' => 2, 'name' => '國'],
        ['id' => 3, 'name' => '反'],
        ['id' => 4, 'name' => '毒'],
        ['id' => 5, 'name' => '愛在台中'],
    ],

    // 'DIRECTION_API_KEY' => env('DIRECTION_API_KEY'),
    // 'DIRECTION_API_BASE_URL' => env('DIRECTION_API_BASE_URL'),

    'APP_URL' => env('APP_URL'),

];
